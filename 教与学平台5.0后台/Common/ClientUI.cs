﻿/* ==============================================================================
  * 功能描述：ClientUI  
  * 创 建 者：Fubz
  * 创建日期：2013/6/21 14:13:29
  * ==============================================================================
 */
using System.Text;


namespace Common
{
    /// <summary>
    /// 客户端页面一下简单js
    /// </summary>
    public class ClientUI
    {
        /// <summary>
        /// 简单弹出对话框功能
        /// </summary>
        /// <param name="pageCurrent">
        /// 当前的页面(this)
        /// </param>
        /// <param name="strMsg">
        /// 弹出信息的内容
        /// </param>
        public static void Alert(System.Web.UI.Page pageCurrent, string strMsg)
        {
            //Replace \n
            strMsg = strMsg.Replace("\n", "file://n/");
            //Replace \r
            strMsg = strMsg.Replace("\r", "file://r/");
            //Replace "
            strMsg = strMsg.Replace("\"", "\\\"");
            //Replace '
            strMsg = strMsg.Replace("\'", "\\\'");

            pageCurrent.ClientScript.RegisterStartupScript(pageCurrent.GetType(),
                System.Guid.NewGuid().ToString(),
                "<script>window.alert('" + strMsg + "')</script>"
                );
        }
        /// <summary>
        /// 简单弹出对话框并跳转
        /// </summary>
        /// <param name="pageCurrent">当前页面(this)</param>
        /// <param name="strMsg">信息内容</param>
        /// <param name="GoBackUrl">跳转URL</param>
        public static void Alert(System.Web.UI.Page pageCurrent, string strMsg, string GoBackUrl)
        {
            //Replace \n
            strMsg = strMsg.Replace("\n", "file://n/");
            //Replace \r
            strMsg = strMsg.Replace("\r", "file://r/");
            //Replace "
            strMsg = strMsg.Replace("\"", "\\\"");
            //Replace '
            strMsg = strMsg.Replace("\'", "\\\'");

            pageCurrent.ClientScript.RegisterStartupScript(pageCurrent.GetType(),
                System.Guid.NewGuid().ToString(),
                "<script>window.alert('" + strMsg + "');window.location.href='" + GoBackUrl + "'</script>"
                );
        }

        /// <summary>
        /// 简单弹出对话框功能
        /// </summary>
        /// <param name="pageCurrent">
        /// 当前的页面(this)
        /// </param>
        /// <param name="strMsg">
        /// 弹出信息的内容
        /// </param>
        public static void AlertRefresh(System.Web.UI.Page pageCurrent, string strMsg)
        {
            //Replace \n
            strMsg = strMsg.Replace("\n", "file://n/");
            //Replace \r
            strMsg = strMsg.Replace("\r", "file://r/");
            //Replace "
            strMsg = strMsg.Replace("\"", "\\\"");
            //Replace '
            strMsg = strMsg.Replace("\'", "\\\'");

            pageCurrent.ClientScript.RegisterStartupScript(pageCurrent.GetType(),
                System.Guid.NewGuid().ToString(),
                "<script>window.alert('" + strMsg + "');window.location.href=window.location.href;;</script>"
                );
        }
        /// <summary>
        /// 简单弹出对话框并跳到父页(主要用于框架页)
        /// </summary>
        /// <param name="pageCurrent">当前页面(this)</param>
        /// <param name="strMsg">信息内容</param>
        /// <param name="GoBackUrl">跳转URL</param>
        public static void AlertFrame(System.Web.UI.Page pageCurrent, string strMsg, string GoBackUrl)
        {
            //Replace \n
            strMsg = strMsg.Replace("\n", "file://n/");
            //Replace \r
            strMsg = strMsg.Replace("\r", "file://r/");
            //Replace "
            strMsg = strMsg.Replace("\"", "\\\"");
            //Replace '
            strMsg = strMsg.Replace("\'", "\\\'");

            pageCurrent.ClientScript.RegisterStartupScript(pageCurrent.GetType(),
                System.Guid.NewGuid().ToString(),
                "<script>window.alert('" + strMsg + "');window.top.location.href='" + GoBackUrl + "'</script>"
                );
        }
        /// <summary>
        /// 弹出简单对话框并关闭页面
        /// </summary>
        /// <param name="pageCurrent">当前页面(this)</param>
        /// <param name="message">信息</param>
        public static void AlertClose(System.Web.UI.Page pageCurrent, string message)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script language=\"javascript\">\n");
            sb.Append("alert(\"" + message.Trim() + "\"); \n");
            sb.Append(" window.open('','_parent','');   window.close();\n");
            sb.Append("</script>\n");
            pageCurrent.ClientScript.RegisterClientScriptBlock(pageCurrent.GetType(),
                System.Guid.NewGuid().ToString(), sb.ToString());
        }
        /// <summary>
        /// 显示一个弹出窗口,并转向上一页
        /// </summary>
        /// <param name="message"></param>
        public static void AlertBack(System.Web.UI.Page pageCurrent, string message)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<script language=\"javascript\"> \n");
            sb.Append("alert(\"" + message.Trim() + "\"); \n");
            sb.Append("var p=document.referrer; \n");
            sb.Append("window.location.href=p;\n");
            sb.Append("</script>");
            pageCurrent.ClientScript.RegisterClientScriptBlock(pageCurrent.GetType(),
                System.Guid.NewGuid().ToString(), sb.ToString());
        }
    }
}