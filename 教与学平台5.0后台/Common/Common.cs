﻿/* ==============================================================================
  * 功能描述：Common  
  * 创 建 者：Fubz
  * 创建日期：2013/6/6 15:10:32
  * ==============================================================================
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Security.Cryptography;

namespace Common
{
    /// <summary>
    /// 公共类
    /// </summary>
    public class Common
    {

        /// <summary>
        /// 任何一个正整数都可以拆分成2的幂形式,二进制,
        /// </summary>
        /// <param name="data"></param>
        /// <returns>返回字符串拼接，一逗号分隔，</returns>
        public static string getMySplitData(int data)
        {

            string result = string.Empty;
            int k = 0;
            data = 7 - data;    //只用于此项目
            while (data > 0)
            {
                if ((data & 1) == 1)    //(data & 1) == 1  获取的是整数与运算
                {
                    result += k + ",";
                }
                k++;
                data >>= 1;
            }
            if (result.Length > 0)
                result = result.Substring(0, result.Length - 1);

            return result;

        }
        /// <summary>
        /// 任何一个正整数都可以拆分成2的幂形式,二进制,
        /// </summary>
        /// <param name="data"></param>
        /// <returns>返回字符串拼接，一逗号分隔，</returns>
        public static string getMySchoolSplitData(int data)
        {
            string result = string.Empty;
            int k = 0;
            while (data > 0)
            {
                if ((data & 1) == 1)    //(data & 1) == 1  获取的是整数与运算
                {
                    result += k + ","; //  result += System.Math.Pow(2, k) + ",";
                }
                k++;
                data >>= 1;
            }
            if (result.Length > 0)
                result = result.Substring(0, result.Length - 1);

            return result;

        }

        /// <summary>
        /// 获取单位的
        /// </summary>
        /// <param name="data"></param>
        /// <returns>返回字符串拼接，一逗号分隔，</returns>
        public static string getMyUnitGradeType(int data)
        {
            string result = string.Empty;
            int k = 0;
            while (data > 0)
            {
                if ((data & 1) == 1)    //(data & 1) == 1  获取的是整数与运算
                {
                    //result += k + ","; //  
                    result += getMyGradeNameByValue(int.Parse(System.Math.Pow(2, k).ToString())) + ",";
                }
                k++;
                data >>= 1;
            }
            if (result.Length > 0)
                result = result.Substring(0, result.Length - 1);

            return result;

        }


        /// <summary>
        /// 获取单位的
        /// </summary>
        /// <param name="data"></param>
        /// <returns>返回字符串拼接，一逗号分隔，</returns>
        public static string getUnitGradeString(int data)
        {
            string result = string.Empty;
            int k = 0;
            while (data > 0)
            {
                if ((data & 1) == 1)    //(data & 1) == 1  获取的是整数与运算
                {
                    //result += k + ","; //  
                    result += int.Parse(System.Math.Pow(2, k).ToString()) + ",";
                }
                k++;
                data >>= 1;
            }
            if (result.Length > 0)
                result = result.Substring(0, result.Length - 1);

            return result;

        }


        /// <summary>
        /// 获取学段对应的数据值
        /// </summary>
        /// <param name="grade"></param>
        /// <returns></returns>
        public static int getGradeValue(string grade)
        {
            int result = 1;

            switch (grade)
            {
                case "高中":
                    result = 4;
                    break;
                case "初中":
                    result = 2;
                    break;
                case "初中高中":
                case "高中初中":
                    result = 6;
                    break;
                case "小学":
                    result = 1;
                    break;
                case "小学高中":
                case "高中小学":
                    result = 5;
                    break;
                case "小学初中":
                case "初中小学":
                    result = 3;
                    break;
                case "小学初中高中":
                case "小学高中初中":
                case "初中小学高中":
                case "初中高中小学":
                case "高中小学初中":
                case "高中初中小学":
                    result = 7;
                    break;
            }
            return result;
        }

        /// <summary>
        /// 根据值获取学段的名称
        /// </summary>
        /// <param name="grade"></param>
        /// <returns></returns>
        private static string getMyGradeNameByValue(int grade)
        {
            string result = string.Empty;
            switch (grade)
            {
                case 1:
                    result = "小学";
                    break;
                case 2:
                    result = "初中";
                    break;
                case 4:
                    result = "高中";
                    break;
            }

            return result;
        }

        /// <summary>
        /// 判断是否是混合制学校,count>1表示是混合制学校
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool checkGradeNum(int data)
        {
            bool flag = true;
            int count = 0;
            while (data > 0)
            {
                data &= (data - 1);
                count++;
            }
            flag = count > 1 ? true : false;

            return flag;
        }

        /// <summary>
        /// 截取中英文混合字符串
        /// </summary>
        /// <param name="s">字符串</param>
        /// <param name="l">长度</param>
        /// <param name="endStr">指当字符串"string s"超过指定长度"int l"时，对结尾的处理，例如加..</param>
        /// <returns></returns>
        public static string getStr(string s, int l, string endStr)
        {
            string temp = s.Substring(0, (s.Length < l) ? s.Length : l);

            if (Regex.Replace(temp, "[\u4e00-\u9fa5]", "zz", RegexOptions.IgnoreCase).Length <= l)
            {
                return temp;
            }
            for (int i = temp.Length; i >= 0; i--)
            {
                temp = temp.Substring(0, i);
                if (Regex.Replace(temp, "[\u4e00-\u9fa5]", "zz", RegexOptions.IgnoreCase).Length <= l - endStr.Length)
                {
                    return temp + endStr;
                }
            }
            return endStr;
        }

        /// <summary>
        /// datatable to arrilist
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static ArrayList DataTable2ArrayList(DataTable data)
        {
            ArrayList array = new ArrayList();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                DataRow row = data.Rows[i];

                Hashtable record = new Hashtable();
                for (int j = 0; j < data.Columns.Count; j++)
                {
                    object cellValue = row[j];
                    if (cellValue.GetType() == typeof(DBNull))
                    {
                        cellValue = null;
                    }
                    record[data.Columns[j].ColumnName] = cellValue;
                }
                array.Add(record);
            }
            return array;
        }

        /// <summary>
        /// 处理字符串，前台页面字符串长度问题
        /// </summary>
        /// <param name="strString"></param>
        /// <returns></returns>
        public static string getResultOfString(string strString)
        {
            string result = string.Empty;
            if (strString.StartsWith("必修") || strString.StartsWith("选修"))
            {
                result = getStr(strString, 6, "");
                if (result.EndsWith("-") || result.EndsWith("("))
                {
                    result = result.Substring(0, result.Length - 1);
                }
            }
            else
                result = strString;

            return result;
        }

        /// <summary>
        /// 获取文件大小的转换
        /// </summary>
        /// <param name="paht">文件路径</param>
        /// <returns></returns>
        public static String FormatFileSize(string path)
        {
            long fileSize = 0;

            System.IO.FileInfo f = new System.IO.FileInfo(path);
            if (!f.Exists)
                return "0 KB(没有找到文件)";
            fileSize = f.Length;
            if (fileSize < 0)
            {
                throw new ArgumentOutOfRangeException("fileSize");
            }
            else if (fileSize >= 1024 * 1024 * 1024)
            {
                return string.Format("{0:########0.00} GB", ((Double)fileSize) / (1024 * 1024 * 1024));
            }
            else if (fileSize >= 1024 * 1024)
            {
                return string.Format("{0:####0.00} MB", ((Double)fileSize) / (1024 * 1024));
            }
            else if (fileSize >= 1024)
            {
                return string.Format("{0:####0.00} KB", ((Double)fileSize) / 1024);
            }
            else
            {
                return string.Format("{0} bytes", fileSize);
            }
        }
        /// <summary>
        /// MD5加密
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string MD5(String str)
        {
            str = "啊哈%6。去死12" + str + "l吧loce(*^__^*) 嘻嘻……";
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] data = System.Text.Encoding.Default.GetBytes(str);
            byte[] result = md5.ComputeHash(data);
            String ret = "";
            for (int i = 0; i < result.Length; i++)
                ret += result[i].ToString("x").PadLeft(2, '0');
            return ret;
        }

        /// <summary>
        /// 判断表
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static bool checkTable(DataTable dt)
        {
            if (dt == null || dt.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

}