﻿/* ==============================================================================
  * 功能描述：Json  
  * 创 建 者：Fubz
  * 创建日期：2013/6/6 10:27:01
  * ==============================================================================
 */
using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using System.Data;
using System.Text;

namespace Common
{
    public class JSON
    {
        public static string DateTimeFormat = "yyyy'-'MM'-'dd  HH':'mm':'ss";
        public static string Encode(object o)
        {
            if (o == null || o.ToString() == "null") return null;

            if (o != null && (o.GetType() == typeof(String) || o.GetType() == typeof(string)))
            {
                return o.ToString();
            }
            IsoDateTimeConverter dt = new IsoDateTimeConverter();
            dt.DateTimeFormat = DateTimeFormat;
            return JsonConvert.SerializeObject(o, dt);
        }

        public static object Decode(string json)
        {
            if (String.IsNullOrEmpty(json)) return "";
            object o = JsonConvert.DeserializeObject(json);
            if (o.GetType() == typeof(String) || o.GetType() == typeof(string))
            {
                o = JsonConvert.DeserializeObject(o.ToString());
            }
            object v = toObject(o);
            return v;
        }
        public static object Decode(string json, Type type)
        {
            return JsonConvert.DeserializeObject(json, type);
        }

        public static T Decode<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        private static object toObject(object o)
        {
            if (o == null) return null;

            if (o.GetType() == typeof(string))
            {
                //判断是否符合2013-06-08T10:00:00的格式
                string s = o.ToString();
                if (s.Length == 19 && s[10] == 'T' && s[4] == '-' && s[13] == ':')
                {
                    o = System.Convert.ToDateTime(o);
                }
            }
            else if (o is JObject)
            {
                JObject jo = o as JObject;

                Hashtable h = new Hashtable();

                foreach (KeyValuePair<string, JToken> entry in jo)
                {
                    h[entry.Key] = toObject(entry.Value);
                }

                o = h;
            }
            else if (o is IList)
            {

                ArrayList list = new ArrayList();
                list.AddRange((o as IList));
                int i = 0, l = list.Count;
                for (; i < l; i++)
                {
                    list[i] = toObject(list[i]);
                }
                o = list;

            }
            else if (typeof(JValue) == o.GetType())
            {
                JValue v = (JValue)o;
                o = toObject(v.Value);
            }
            else
            {
            }
            return o;
        }

        public static string TableToJson(DataTable dt)
        {
            return TableToJson(dt, "data");
        }

        /// <summary>
        /// datatable 转换成json
        /// </summary>
        /// <param name="dt">table</param>
        /// <param name="objName">数据集合名称</param>
        /// <param name="flag">是否获取当前日期</param>
        /// <returns></returns>
        public static string TableToJson(DataTable dt, string objName)
        {
            StringBuilder jsonBuilder = new StringBuilder();
            if (dt.Rows.Count > 0)
            {
                jsonBuilder.Append("{\"" + objName);   //{}大括号可以自己加
                jsonBuilder.Append("\":[");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    jsonBuilder.Append("{");
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        jsonBuilder.Append("\"");
                        jsonBuilder.Append(dt.Columns[j].ColumnName);
                        jsonBuilder.Append("\":\"");
                        jsonBuilder.Append(dt.Rows[i][j].ToString());
                        jsonBuilder.Append("\",");
                    }
                    jsonBuilder.Remove(jsonBuilder.Length - 1, 1);
                    jsonBuilder.Append("},");
                }
                jsonBuilder.Remove(jsonBuilder.Length - 1, 1);
                jsonBuilder.Append("]}");

            }
            else
                jsonBuilder.Append("{\"" + objName + "\":\"\"}");   //设置为空

            return jsonBuilder.ToString();
        }


        /// <summary>
        /// datatable 转换成json
        /// </summary>
        /// <param name="dt">table</param>
        /// <param name="objName">数据集合名称</param>
        /// <param name="flag">是否获取当前日期</param>
        /// <returns></returns>
        public static string TableToJson(DataTable dt, string objName, bool flag)
        {
            StringBuilder jsonBuilder = new StringBuilder();
            if (dt.Rows.Count > 0)
            {
                jsonBuilder.Append("{\"" + objName);   //{}大括号可以自己加
                jsonBuilder.Append("\":[");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    jsonBuilder.Append("{");
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        jsonBuilder.Append("\"");
                        jsonBuilder.Append(dt.Columns[j].ColumnName);
                        jsonBuilder.Append("\":\"");
                        jsonBuilder.Append(dt.Rows[i][j].ToString());
                        jsonBuilder.Append("\",");
                    }
                    jsonBuilder.Remove(jsonBuilder.Length - 1, 1);
                    jsonBuilder.Append("},");
                }
                jsonBuilder.Remove(jsonBuilder.Length - 1, 1);
                jsonBuilder.Append("]");
                jsonBuilder.Append(",\"time\":\"" + DateTime.Now.ToLocalTime() + "\"}");
            }
            else
                jsonBuilder.Append("{\"" + objName + "\":\"\",\"time\":\"" + DateTime.Now.ToShortDateString() + "\"}");   //设置为空

            return jsonBuilder.ToString();
        }

        /// <summary>
        /// datarow[] 转换成json
        /// </summary>
        /// <param name="drs">datarow数组</param>
        /// <param name="objName">数据集合名称</param>
        /// <param name="flag">是否有大括号</param>
        /// <returns></returns>
        public static string TableToJson(DataRow[] drs)
        {
            StringBuilder jsonBuilder = new StringBuilder();
            DataTable dt = drs[0].Table;
            if (drs.Length > 0)
            {
                jsonBuilder.Append("[");
                for (int i = 0; i < drs.Length; i++)
                {
                    jsonBuilder.Append("{");
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        jsonBuilder.Append("\"");
                        jsonBuilder.Append(dt.Columns[j].ColumnName);
                        jsonBuilder.Append("\":\"");
                        jsonBuilder.Append(drs[i][j].ToString());
                        jsonBuilder.Append("\",");
                    }
                    jsonBuilder.Remove(jsonBuilder.Length - 1, 1);
                    jsonBuilder.Append("},");
                }
                jsonBuilder.Remove(jsonBuilder.Length - 1, 1);
                jsonBuilder.Append("]");
            }
            return jsonBuilder.ToString();
        }

    }
}