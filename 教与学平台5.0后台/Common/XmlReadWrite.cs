﻿/* ==============================================================================
  * 功能描述：XmlReadWrite  
  * 创 建 者：Fubz
  * 创建日期：2014/4/10 9:05:54
  * ==============================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace Common
{
    public class XmlDAL<T> where T : new()
    {
        Encoding code = Encoding.GetEncoding("gb2312");  //编码类型  

        public void WriteXml(string path, T obj)//path为包含文件名的XML文件完整路径，obj为数据类型    
        {
            XmlSerializer mySerializer = new XmlSerializer(typeof(T));
            StreamWriter myWriter = new StreamWriter(path, false, code);
            mySerializer.Serialize(myWriter, obj);
            myWriter.Close();
        }

        public T ReadXml(string path)
        {
            T ob;
            XmlSerializer mySerializer = new XmlSerializer(typeof(T));
            TextReader myReader = new StreamReader(path, code);
            ob = (T)mySerializer.Deserialize(myReader);
            myReader.Close();
            return ob;
        }
    }
}