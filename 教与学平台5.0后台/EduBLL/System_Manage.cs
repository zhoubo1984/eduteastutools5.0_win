﻿using System;
using System.Data;
using System.Collections.Generic;
using Common;
using EduModel;
namespace EduBLL
{
	/// <summary>
	/// 1
	/// </summary>
	public partial class System_Manage
	{
		private readonly EduDAL.System_Manage dal=new EduDAL.System_Manage();
		public System_Manage()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int SM_SN)
		{
			return dal.Exists(SM_SN);
		}

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string ubID)
        {
            return dal.Exists(ubID);
        }

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(EduModel.System_Manage model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.System_Manage model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SM_SN)
		{
			
			return dal.Delete(SM_SN);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string SM_SNlist )
		{
			return dal.DeleteList(SM_SNlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.System_Manage GetModel(int SM_SN)
		{
			
			return dal.GetModel(SM_SN);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public EduModel.System_Manage GetModelByCache(int SM_SN)
		{
			
			string CacheKey = "System_ManageModel-" + SM_SN;
			object objModel = Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(SM_SN);
					if (objModel != null)
					{
						int ModelCache = Common.ConfigHelper.GetConfigInt("ModelCache");
						Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (EduModel.System_Manage)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EduModel.System_Manage> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EduModel.System_Manage> DataTableToList(DataTable dt)
		{
			List<EduModel.System_Manage> modelList = new List<EduModel.System_Manage>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				EduModel.System_Manage model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

