﻿using System;
using System.Data;
using System.Collections.Generic;
using Common;
using EduModel;
namespace EduBLL
{
	/// <summary>
	/// 教师学科教材信息
	/// </summary>
	public partial class Teacher_Subject_Book_Class
	{
		private readonly EduDAL.Teacher_Subject_Book_Class dal=new EduDAL.Teacher_Subject_Book_Class();
		public Teacher_Subject_Book_Class()
		{}
		#region  BasicMethod
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(Guid TSBC_GUID)
		{
			return dal.Exists(TSBC_GUID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(EduModel.Teacher_Subject_Book_Class model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.Teacher_Subject_Book_Class model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int TSBC_SN)
		{
			
			return dal.Delete(TSBC_SN);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(Guid TSBC_GUID)
		{
			
			return dal.Delete(TSBC_GUID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string TSBC_SNlist )
		{
			return dal.DeleteList(TSBC_SNlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Teacher_Subject_Book_Class GetModel(int TSBC_SN)
		{
			
			return dal.GetModel(TSBC_SN);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public EduModel.Teacher_Subject_Book_Class GetModelByCache(int TSBC_SN)
		{
			
			string CacheKey = "Teacher_Subject_Book_ClassModel-" + TSBC_SN;
			object objModel = Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(TSBC_SN);
					if (objModel != null)
					{
						int ModelCache = Common.ConfigHelper.GetConfigInt("ModelCache");
						Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (EduModel.Teacher_Subject_Book_Class)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EduModel.Teacher_Subject_Book_Class> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EduModel.Teacher_Subject_Book_Class> DataTableToList(DataTable dt)
		{
			List<EduModel.Teacher_Subject_Book_Class> modelList = new List<EduModel.Teacher_Subject_Book_Class>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				EduModel.Teacher_Subject_Book_Class model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

