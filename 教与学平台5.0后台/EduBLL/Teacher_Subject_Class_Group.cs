﻿using System;
using System.Data;
using System.Collections.Generic;
using Common;
using EduModel;
namespace EduBLL
{
	/// <summary>
	/// 教师学科教材信息
	/// </summary>
	public partial class Teacher_Subject_Class_Group
	{
		private readonly EduDAL.Teacher_Subject_Class_Group dal=new EduDAL.Teacher_Subject_Class_Group();
		public Teacher_Subject_Class_Group()
		{}
		#region  BasicMethod
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(Guid TSCG_Guid)
		{
			return dal.Exists(TSCG_Guid);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(EduModel.Teacher_Subject_Class_Group model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.Teacher_Subject_Class_Group model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int TSCG_SN)
		{
			
			return dal.Delete(TSCG_SN);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(Guid TSCG_Guid)
		{
			
			return dal.Delete(TSCG_Guid);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string TSCG_SNlist )
		{
			return dal.DeleteList(TSCG_SNlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Teacher_Subject_Class_Group GetModel(int TSCG_SN)
		{
			
			return dal.GetModel(TSCG_SN);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public EduModel.Teacher_Subject_Class_Group GetModelByCache(int TSCG_SN)
		{
			
			string CacheKey = "Teacher_Subject_Class_GroupModel-" + TSCG_SN;
			object objModel = Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(TSCG_SN);
					if (objModel != null)
					{
						int ModelCache = Common.ConfigHelper.GetConfigInt("ModelCache");
						Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (EduModel.Teacher_Subject_Class_Group)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EduModel.Teacher_Subject_Class_Group> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EduModel.Teacher_Subject_Class_Group> DataTableToList(DataTable dt)
		{
			List<EduModel.Teacher_Subject_Class_Group> modelList = new List<EduModel.Teacher_Subject_Class_Group>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				EduModel.Teacher_Subject_Class_Group model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

