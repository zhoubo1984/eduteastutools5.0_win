﻿using System;
using System.Data;
using System.Collections.Generic;
using Common;
using EduModel;
namespace EduBLL
{
    /// <summary>
    /// 教师扩展信息
    /// </summary>
    public partial class U_TeacherInfo
    {
        private readonly EduDAL.U_TeacherInfo dal = new EduDAL.U_TeacherInfo();
        public U_TeacherInfo()
        { }
        #region  BasicMethod
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(Guid TI_GUID)
        {
            return dal.Exists(TI_GUID);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EduModel.U_TeacherInfo model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EduModel.U_TeacherInfo model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int TI_SN)
        {

            return dal.Delete(TI_SN);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(Guid TI_GUID)
        {

            return dal.Delete(TI_GUID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string TI_SNlist)
        {
            return dal.DeleteList(TI_SNlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.U_TeacherInfo GetModel(int TI_SN)
        {

            return dal.GetModel(TI_SN);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.U_TeacherInfo GetModel(Guid guid)
        {

            return dal.GetModel(guid);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public EduModel.U_TeacherInfo GetModelByCache(int TI_SN)
        {

            string CacheKey = "U_TeacherInfoModel-" + TI_SN;
            object objModel = Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(TI_SN);
                    if (objModel != null)
                    {
                        int ModelCache = Common.ConfigHelper.GetConfigInt("ModelCache");
                        Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (EduModel.U_TeacherInfo)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<EduModel.U_TeacherInfo> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<EduModel.U_TeacherInfo> DataTableToList(DataTable dt)
        {
            List<EduModel.U_TeacherInfo> modelList = new List<EduModel.U_TeacherInfo>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                EduModel.U_TeacherInfo model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = dal.DataRowToModel(dt.Rows[n]);
                    if (model != null)
                    {
                        modelList.Add(model);
                    }
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

