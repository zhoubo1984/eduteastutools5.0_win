﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
    /// <summary>
    /// 数据访问类:Class_Base
    /// </summary>
    public partial class Class_Base
    {
        public Class_Base()
        { }
        #region  BasicMethod

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(Guid CB_GUID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from Class_Base");
            strSql.Append(" where CB_GUID=@CB_GUID ");
            SqlParameter[] parameters = {
					new SqlParameter("@CB_GUID", SqlDbType.UniqueIdentifier,16)			};
            parameters[0].Value = CB_GUID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Exists(EduModel.Class_Base model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" select count(1) from Class_Base where ");
            strSql.Append(" CB_UnitGUID=@CB_UnitGUID and  ");
            strSql.Append(" CB_StartYear=@CB_StartYear and  ");
            strSql.Append(" CB_Name=@CB_Name and  ");
            strSql.Append(" GradeState=@GradeState   ");
            SqlParameter[] parameters = {
					new SqlParameter("@CB_UnitGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@CB_StartYear", SqlDbType.NVarChar,50),
					new SqlParameter("@CB_Name", SqlDbType.NVarChar,50),
					new SqlParameter("@GradeState", SqlDbType.Int,4)};
            parameters[0].Value = model.CB_UnitGUID;
            parameters[1].Value = model.CB_StartYear;
            parameters[2].Value = model.CB_Name;
            parameters[3].Value = model.GradeState;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EduModel.Class_Base model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Class_Base(");
            strSql.Append("CB_GUID,CB_UnitGUID,CB_StartYear,CB_Name,GradeState,State,CB_Index)");
            strSql.Append(" values (");
            strSql.Append("@CB_GUID,@CB_UnitGUID,@CB_StartYear,@CB_Name,@GradeState,@State,@CB_Index)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@CB_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@CB_UnitGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@CB_StartYear", SqlDbType.NVarChar,50),
					new SqlParameter("@CB_Name", SqlDbType.NVarChar,50),
					new SqlParameter("@GradeState", SqlDbType.Int,4),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@CB_Index", SqlDbType.Int,4)};
            parameters[0].Value =model.CB_GUID;
            parameters[1].Value = model.CB_UnitGUID;
            parameters[2].Value = model.CB_StartYear;
            parameters[3].Value = model.CB_Name;
            parameters[4].Value = model.GradeState;
            parameters[5].Value = model.State;
            parameters[6].Value = model.CB_Index;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EduModel.Class_Base model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Class_Base set ");
            strSql.Append("CB_UnitGUID=@CB_UnitGUID,");
            strSql.Append("CB_StartYear=@CB_StartYear,");
            strSql.Append("CB_Name=@CB_Name,");
            strSql.Append("GradeState=@GradeState,");
            strSql.Append("State=@State,");
            strSql.Append("CB_Index=@CB_Index");
            strSql.Append(" where CB_SN=@CB_SN");
            SqlParameter[] parameters = {
					new SqlParameter("@CB_UnitGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@CB_StartYear", SqlDbType.NVarChar,50),
					new SqlParameter("@CB_Name", SqlDbType.NVarChar,50),
					new SqlParameter("@GradeState", SqlDbType.Int,4),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@CB_Index", SqlDbType.Int,4),
					new SqlParameter("@CB_SN", SqlDbType.Int,4),
					new SqlParameter("@CB_GUID", SqlDbType.UniqueIdentifier,16)};
            parameters[0].Value = model.CB_UnitGUID;
            parameters[1].Value = model.CB_StartYear;
            parameters[2].Value = model.CB_Name;
            parameters[3].Value = model.GradeState;
            parameters[4].Value = model.State;
            parameters[5].Value = model.CB_Index;
            parameters[6].Value = model.CB_SN;
            parameters[7].Value = model.CB_GUID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int CB_SN)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Class_Base ");
            strSql.Append(" where CB_SN=@CB_SN");
            SqlParameter[] parameters = {
					new SqlParameter("@CB_SN", SqlDbType.Int,4)
			};
            parameters[0].Value = CB_SN;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(Guid CB_GUID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Class_Base ");
            strSql.Append(" where CB_GUID=@CB_GUID ");
            SqlParameter[] parameters = {
					new SqlParameter("@CB_GUID", SqlDbType.UniqueIdentifier,16)			};
            parameters[0].Value = CB_GUID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string CB_SNlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Class_Base ");
            strSql.Append(" where CB_SN in (" + CB_SNlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.Class_Base GetModel(int CB_SN)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 CB_SN,CB_GUID,CB_UnitGUID,CB_StartYear,CB_Name,GradeState,State,CB_Index from Class_Base ");
            strSql.Append(" where CB_SN=@CB_SN");
            SqlParameter[] parameters = {
					new SqlParameter("@CB_SN", SqlDbType.Int,4)
			};
            parameters[0].Value = CB_SN;

            EduModel.Class_Base model = new EduModel.Class_Base();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.Class_Base GetModel(EduModel.Class_Base model)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 CB_SN,CB_GUID,CB_UnitGUID,CB_StartYear,CB_Name,GradeState,State,CB_Index from Class_Base ");
            strSql.Append(" where ");
            strSql.Append("CB_UnitGUID=@CB_UnitGUID and ");
            strSql.Append(" CB_StartYear=@CB_StartYear and ");
            strSql.Append(" CB_Name=@CB_Name  and ");
            strSql.Append(" GradeState=@GradeState ");
            SqlParameter[] parameters = {
					new SqlParameter("@CB_UnitGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@CB_StartYear", SqlDbType.NVarChar,50),
					new SqlParameter("@CB_Name", SqlDbType.NVarChar,50),
					new SqlParameter("@GradeState", SqlDbType.Int,4)};
            parameters[0].Value = model.CB_UnitGUID;
            parameters[1].Value = model.CB_StartYear;
            parameters[2].Value = model.CB_Name;
            parameters[3].Value = model.GradeState;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.Class_Base GetModel(Guid guid)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 CB_SN,CB_GUID,CB_UnitGUID,CB_StartYear,CB_Name,GradeState,State,CB_Index from Class_Base ");
            strSql.Append(" where CB_GUID=@CB_GUID");
            SqlParameter[] parameters = {
					new SqlParameter("@CB_GUID", SqlDbType.UniqueIdentifier,16)
			};
            parameters[0].Value = guid;

            EduModel.Class_Base model = new EduModel.Class_Base();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.Class_Base DataRowToModel(DataRow row)
        {
            EduModel.Class_Base model = new EduModel.Class_Base();
            if (row != null)
            {
                if (row["CB_SN"] != null && row["CB_SN"].ToString() != "")
                {
                    model.CB_SN = int.Parse(row["CB_SN"].ToString());
                }
                if (row["CB_GUID"] != null && row["CB_GUID"].ToString() != "")
                {
                    model.CB_GUID = new Guid(row["CB_GUID"].ToString());
                }
                if (row["CB_UnitGUID"] != null && row["CB_UnitGUID"].ToString() != "")
                {
                    model.CB_UnitGUID = new Guid(row["CB_UnitGUID"].ToString());
                }
                if (row["CB_StartYear"] != null)
                {
                    model.CB_StartYear = row["CB_StartYear"].ToString();
                }
                if (row["CB_Name"] != null)
                {
                    model.CB_Name = row["CB_Name"].ToString();
                }
                if (row["GradeState"] != null && row["GradeState"].ToString() != "")
                {
                    model.GradeState = int.Parse(row["GradeState"].ToString());
                }
                if (row["State"] != null && row["State"].ToString() != "")
                {
                    model.State = int.Parse(row["State"].ToString());
                }
                if (row["CB_Index"] != null && row["CB_Index"].ToString() != "")
                {
                    model.CB_Index = int.Parse(row["CB_Index"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select CB_SN,CB_GUID,CB_UnitGUID,CB_StartYear,CB_Name,GradeState,State,CB_Index ");
            strSql.Append(" FROM Class_Base ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" CB_SN,CB_GUID,CB_UnitGUID,CB_StartYear,CB_Name,GradeState,State,CB_Index ");
            strSql.Append(" FROM Class_Base ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Class_Base ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.CB_SN desc");
            }
            strSql.Append(")AS Row, T.*  from Class_Base T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Class_Base";
            parameters[1].Value = "CB_SN";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

