﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
	/// <summary>
	/// 数据访问类:EbookUser
	/// </summary>
	public partial class EbookUser
	{
		public EbookUser()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("SN", "EbookUser"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int SN)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from EbookUser");
			strSql.Append(" where SN=@SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
			parameters[0].Value = SN;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EduModel.EbookUser model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into EbookUser(");
			strSql.Append("EB_ID,EB_Name,IsUse,EB_Psw,EB_AccountID)");
			strSql.Append(" values (");
			strSql.Append("@EB_ID,@EB_Name,@IsUse,@EB_Psw,@EB_AccountID)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@EB_ID", SqlDbType.NVarChar,-1),
					new SqlParameter("@EB_Name", SqlDbType.NVarChar,-1),
					new SqlParameter("@IsUse", SqlDbType.Bit,1),
					new SqlParameter("@EB_Psw", SqlDbType.NVarChar,-1),
					new SqlParameter("@EB_AccountID", SqlDbType.NVarChar,-1)};
			parameters[0].Value = model.EB_ID;
			parameters[1].Value = model.EB_Name;
			parameters[2].Value = model.IsUse;
			parameters[3].Value = model.EB_Psw;
			parameters[4].Value = model.EB_AccountID;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.EbookUser model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update EbookUser set ");
			strSql.Append("EB_ID=@EB_ID,");
			strSql.Append("EB_Name=@EB_Name,");
			strSql.Append("IsUse=@IsUse,");
			strSql.Append("EB_Psw=@EB_Psw,");
			strSql.Append("EB_AccountID=@EB_AccountID");
			strSql.Append(" where SN=@SN");
			SqlParameter[] parameters = {
					new SqlParameter("@EB_ID", SqlDbType.NVarChar,-1),
					new SqlParameter("@EB_Name", SqlDbType.NVarChar,-1),
					new SqlParameter("@IsUse", SqlDbType.Bit,1),
					new SqlParameter("@EB_Psw", SqlDbType.NVarChar,-1),
					new SqlParameter("@EB_AccountID", SqlDbType.NVarChar,-1),
					new SqlParameter("@SN", SqlDbType.Int,4)};
			parameters[0].Value = model.EB_ID;
			parameters[1].Value = model.EB_Name;
			parameters[2].Value = model.IsUse;
			parameters[3].Value = model.EB_Psw;
			parameters[4].Value = model.EB_AccountID;
			parameters[5].Value = model.SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from EbookUser ");
			strSql.Append(" where SN=@SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
			parameters[0].Value = SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string SNlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from EbookUser ");
			strSql.Append(" where SN in ("+SNlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.EbookUser GetModel(int SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 SN,EB_ID,EB_Name,IsUse,EB_Psw,EB_AccountID from EbookUser ");
			strSql.Append(" where SN=@SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
			parameters[0].Value = SN;

			EduModel.EbookUser model=new EduModel.EbookUser();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.EbookUser DataRowToModel(DataRow row)
		{
			EduModel.EbookUser model=new EduModel.EbookUser();
			if (row != null)
			{
				if(row["SN"]!=null && row["SN"].ToString()!="")
				{
					model.SN=int.Parse(row["SN"].ToString());
				}
				if(row["EB_ID"]!=null)
				{
					model.EB_ID=row["EB_ID"].ToString();
				}
				if(row["EB_Name"]!=null)
				{
					model.EB_Name=row["EB_Name"].ToString();
				}
				if(row["IsUse"]!=null && row["IsUse"].ToString()!="")
				{
					if((row["IsUse"].ToString()=="1")||(row["IsUse"].ToString().ToLower()=="true"))
					{
						model.IsUse=true;
					}
					else
					{
						model.IsUse=false;
					}
				}
				if(row["EB_Psw"]!=null)
				{
					model.EB_Psw=row["EB_Psw"].ToString();
				}
				if(row["EB_AccountID"]!=null)
				{
					model.EB_AccountID=row["EB_AccountID"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select SN,EB_ID,EB_Name,IsUse,EB_Psw,EB_AccountID ");
			strSql.Append(" FROM EbookUser ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" SN,EB_ID,EB_Name,IsUse,EB_Psw,EB_AccountID ");
			strSql.Append(" FROM EbookUser ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM EbookUser ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.SN desc");
			}
			strSql.Append(")AS Row, T.*  from EbookUser T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "EbookUser";
			parameters[1].Value = "SN";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

