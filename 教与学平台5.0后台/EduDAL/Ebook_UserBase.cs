﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
    /// <summary>
    /// 数据访问类:Ebook_UserBase
    /// </summary>
    public partial class Ebook_UserBase
    {
        public Ebook_UserBase()
        { }
        #region  BasicMethod

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("SN", "Ebook_UserBase");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int SN)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from Ebook_UserBase");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SN;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EduModel.Ebook_UserBase model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Ebook_UserBase(");
            strSql.Append("User_Guid,Eb_UserID)");
            strSql.Append(" values (");
            strSql.Append("@User_Guid,@Eb_UserID)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@User_Guid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@Eb_UserID", SqlDbType.NVarChar,-1)};
            parameters[0].Value = model.User_Guid;
            parameters[1].Value = model.Eb_UserID;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EduModel.Ebook_UserBase model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Ebook_UserBase set ");
            strSql.Append("User_Guid=@User_Guid,");
            strSql.Append("Eb_UserID=@Eb_UserID");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@User_Guid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@Eb_UserID", SqlDbType.NVarChar,-1),
					new SqlParameter("@SN", SqlDbType.Int,4)};
            parameters[0].Value = model.User_Guid;
            parameters[1].Value = model.Eb_UserID;
            parameters[2].Value = model.SN;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SN)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Ebook_UserBase ");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SN;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string SNlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Ebook_UserBase ");
            strSql.Append(" where SN in (" + SNlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.Ebook_UserBase GetModel(int SN)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 SN,User_Guid,Eb_UserID from Ebook_UserBase ");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SN;

            EduModel.Ebook_UserBase model = new EduModel.Ebook_UserBase();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.Ebook_UserBase DataRowToModel(DataRow row)
        {
            EduModel.Ebook_UserBase model = new EduModel.Ebook_UserBase();
            if (row != null)
            {
                if (row["SN"] != null && row["SN"].ToString() != "")
                {
                    model.SN = int.Parse(row["SN"].ToString());
                }
                if (row["User_Guid"] != null && row["User_Guid"].ToString() != "")
                {
                    model.User_Guid = new Guid(row["User_Guid"].ToString());
                }
                if (row["Eb_UserID"] != null)
                {
                    model.Eb_UserID = row["Eb_UserID"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select SN,User_Guid,Eb_UserID ");
            strSql.Append(" FROM Ebook_UserBase ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" SN,User_Guid,Eb_UserID ");
            strSql.Append(" FROM Ebook_UserBase ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Ebook_UserBase ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.SN desc");
            }
            strSql.Append(")AS Row, T.*  from Ebook_UserBase T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Ebook_UserBase";
            parameters[1].Value = "SN";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

