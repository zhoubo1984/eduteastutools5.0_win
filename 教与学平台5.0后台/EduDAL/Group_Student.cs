﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
	/// <summary>
	/// 数据访问类:Group_Student
	/// </summary>
	public partial class Group_Student
	{
		public Group_Student()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(Guid GS_Guid)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Group_Student");
			strSql.Append(" where GS_Guid=@GS_Guid ");
			SqlParameter[] parameters = {
					new SqlParameter("@GS_Guid", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = GS_Guid;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EduModel.Group_Student model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Group_Student(");
			strSql.Append("GS_Guid,GS_TSCGGuid,GS_StudentGuid,GS_IsLeader)");
			strSql.Append(" values (");
			strSql.Append("@GS_Guid,@GS_TSCGGuid,@GS_StudentGuid,@GS_IsLeader)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@GS_Guid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@GS_TSCGGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@GS_StudentGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@GS_IsLeader", SqlDbType.Bit,1)};
			parameters[0].Value = model.GS_Guid;
			parameters[1].Value = model.GS_TSCGGuid;
			parameters[2].Value = model.GS_StudentGuid;
			parameters[3].Value = model.GS_IsLeader;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.Group_Student model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Group_Student set ");
			strSql.Append("GS_TSCGGuid=@GS_TSCGGuid,");
			strSql.Append("GS_StudentGuid=@GS_StudentGuid,");
			strSql.Append("GS_IsLeader=@GS_IsLeader");
			strSql.Append(" where GS_SN=@GS_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@GS_TSCGGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@GS_StudentGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@GS_IsLeader", SqlDbType.Bit,1),
					new SqlParameter("@GS_SN", SqlDbType.Int,4),
					new SqlParameter("@GS_Guid", SqlDbType.UniqueIdentifier,16)};
			parameters[0].Value = model.GS_TSCGGuid;
			parameters[1].Value = model.GS_StudentGuid;
			parameters[2].Value = model.GS_IsLeader;
			parameters[3].Value = model.GS_SN;
			parameters[4].Value = model.GS_Guid;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int GS_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Group_Student ");
			strSql.Append(" where GS_SN=@GS_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@GS_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = GS_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(Guid GS_Guid)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Group_Student ");
			strSql.Append(" where GS_Guid=@GS_Guid ");
			SqlParameter[] parameters = {
					new SqlParameter("@GS_Guid", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = GS_Guid;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string GS_SNlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Group_Student ");
			strSql.Append(" where GS_SN in ("+GS_SNlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Group_Student GetModel(int GS_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 GS_SN,GS_Guid,GS_TSCGGuid,GS_StudentGuid,GS_IsLeader from Group_Student ");
			strSql.Append(" where GS_SN=@GS_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@GS_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = GS_SN;

			EduModel.Group_Student model=new EduModel.Group_Student();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Group_Student DataRowToModel(DataRow row)
		{
			EduModel.Group_Student model=new EduModel.Group_Student();
			if (row != null)
			{
				if(row["GS_SN"]!=null && row["GS_SN"].ToString()!="")
				{
					model.GS_SN=int.Parse(row["GS_SN"].ToString());
				}
				if(row["GS_Guid"]!=null && row["GS_Guid"].ToString()!="")
				{
					model.GS_Guid= new Guid(row["GS_Guid"].ToString());
				}
				if(row["GS_TSCGGuid"]!=null && row["GS_TSCGGuid"].ToString()!="")
				{
					model.GS_TSCGGuid= new Guid(row["GS_TSCGGuid"].ToString());
				}
				if(row["GS_StudentGuid"]!=null && row["GS_StudentGuid"].ToString()!="")
				{
					model.GS_StudentGuid= new Guid(row["GS_StudentGuid"].ToString());
				}
				if(row["GS_IsLeader"]!=null && row["GS_IsLeader"].ToString()!="")
				{
					if((row["GS_IsLeader"].ToString()=="1")||(row["GS_IsLeader"].ToString().ToLower()=="true"))
					{
						model.GS_IsLeader=true;
					}
					else
					{
						model.GS_IsLeader=false;
					}
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select GS_SN,GS_Guid,GS_TSCGGuid,GS_StudentGuid,GS_IsLeader ");
			strSql.Append(" FROM Group_Student ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" GS_SN,GS_Guid,GS_TSCGGuid,GS_StudentGuid,GS_IsLeader ");
			strSql.Append(" FROM Group_Student ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Group_Student ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.GS_SN desc");
			}
			strSql.Append(")AS Row, T.*  from Group_Student T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Group_Student";
			parameters[1].Value = "GS_SN";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

