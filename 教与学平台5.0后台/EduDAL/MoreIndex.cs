﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
    /// <summary>
    /// 数据访问类:MoreIndex
    /// </summary>
    public partial class MoreIndex
    {
        public MoreIndex()
        { }
        #region  BasicMethod

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("SN", "MoreIndex");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int SN)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from MoreIndex");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SN;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EduModel.MoreIndex model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into MoreIndex(");
            strSql.Append("IndexGuid,UserGuid,UnitGuid,IndexTypeName,IndexName,IndexHref,State,IndexImage)");
            strSql.Append(" values (");
            strSql.Append("@IndexGuid,@UserGuid,@UnitGuid,@IndexTypeName,@IndexName,@IndexHref,@State,@IndexImage)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@IndexGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@UnitGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@IndexTypeName", SqlDbType.NVarChar,-1),
					new SqlParameter("@IndexName", SqlDbType.NVarChar,-1),
					new SqlParameter("@IndexHref", SqlDbType.NVarChar,-1),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@IndexImage", SqlDbType.Image)};
            parameters[0].Value = model.IndexGuid;
            parameters[1].Value = model.UserGuid;
            parameters[2].Value = model.UnitGuid;
            parameters[3].Value = model.IndexTypeName;
            parameters[4].Value = model.IndexName;
            parameters[5].Value = model.IndexHref;
            parameters[6].Value = model.State;
            parameters[7].Value = model.IndexImage;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EduModel.MoreIndex model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update MoreIndex set ");
            strSql.Append("IndexGuid=@IndexGuid,");
            strSql.Append("UserGuid=@UserGuid,");
            strSql.Append("UnitGuid=@UnitGuid,");
            strSql.Append("IndexTypeName=@IndexTypeName,");
            strSql.Append("IndexName=@IndexName,");
            strSql.Append("IndexHref=@IndexHref,");
            strSql.Append("State=@State,");
            strSql.Append("IndexImage=@IndexImage");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@IndexGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@UnitGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@IndexTypeName", SqlDbType.NVarChar,-1),
					new SqlParameter("@IndexName", SqlDbType.NVarChar,-1),
					new SqlParameter("@IndexHref", SqlDbType.NVarChar,-1),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@IndexImage", SqlDbType.Image),
					new SqlParameter("@SN", SqlDbType.Int,4)};
            parameters[0].Value = model.IndexGuid;
            parameters[1].Value = model.UserGuid;
            parameters[2].Value = model.UnitGuid;
            parameters[3].Value = model.IndexTypeName;
            parameters[4].Value = model.IndexName;
            parameters[5].Value = model.IndexHref;
            parameters[6].Value = model.State;
            parameters[7].Value = model.IndexImage;
            parameters[8].Value = model.SN;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SN)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from MoreIndex ");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SN;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string SNlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from MoreIndex ");
            strSql.Append(" where SN in (" + SNlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.MoreIndex GetModel(int SN)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 SN,IndexGuid,UserGuid,UnitGuid,IndexTypeName,IndexName,IndexHref,State,IndexImage from MoreIndex ");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SN;

            EduModel.MoreIndex model = new EduModel.MoreIndex();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.MoreIndex DataRowToModel(DataRow row)
        {
            EduModel.MoreIndex model = new EduModel.MoreIndex();
            if (row != null)
            {
                if (row["SN"] != null && row["SN"].ToString() != "")
                {
                    model.SN = int.Parse(row["SN"].ToString());
                }
                if (row["IndexGuid"] != null && row["IndexGuid"].ToString() != "")
                {
                    model.IndexGuid = new Guid(row["IndexGuid"].ToString());
                }
                if (row["UserGuid"] != null && row["UserGuid"].ToString() != "")
                {
                    model.UserGuid = new Guid(row["UserGuid"].ToString());
                }
                if (row["UnitGuid"] != null && row["UnitGuid"].ToString() != "")
                {
                    model.UnitGuid = new Guid(row["UnitGuid"].ToString());
                }
                if (row["IndexTypeName"] != null)
                {
                    model.IndexTypeName = row["IndexTypeName"].ToString();
                }
                if (row["IndexName"] != null)
                {
                    model.IndexName = row["IndexName"].ToString();
                }
                if (row["IndexHref"] != null)
                {
                    model.IndexHref = row["IndexHref"].ToString();
                }
                if (row["State"] != null && row["State"].ToString() != "")
                {
                    model.State = int.Parse(row["State"].ToString());
                }
                if (row["IndexImage"] != null && row["IndexImage"].ToString() != "")
                {
                    model.IndexImage = (byte[])row["IndexImage"];
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select SN,IndexGuid,UserGuid,UnitGuid,IndexTypeName,IndexName,IndexHref,State,IndexImage ");
            strSql.Append(" FROM MoreIndex ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" SN,IndexGuid,UserGuid,UnitGuid,IndexTypeName,IndexName,IndexHref,State,IndexImage ");
            strSql.Append(" FROM MoreIndex ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM MoreIndex ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.SN desc");
            }
            strSql.Append(")AS Row, T.*  from MoreIndex T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "MoreIndex";
            parameters[1].Value = "SN";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

