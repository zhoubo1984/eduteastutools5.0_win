﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
	/// <summary>
	/// 数据访问类:NetworkType
	/// </summary>
	public partial class NetworkType
	{
		public NetworkType()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("NT_ID", "NetworkType"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int NT_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from NetworkType");
			strSql.Append(" where NT_ID=@NT_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@NT_ID", SqlDbType.Int,4)			};
			parameters[0].Value = NT_ID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(EduModel.NetworkType model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into NetworkType(");
			strSql.Append("NT_ID,NT_Name)");
			strSql.Append(" values (");
			strSql.Append("@NT_ID,@NT_Name)");
			SqlParameter[] parameters = {
					new SqlParameter("@NT_ID", SqlDbType.Int,4),
					new SqlParameter("@NT_Name", SqlDbType.NVarChar,-1)};
			parameters[0].Value = model.NT_ID;
			parameters[1].Value = model.NT_Name;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.NetworkType model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update NetworkType set ");
			strSql.Append("NT_Name=@NT_Name");
			strSql.Append(" where NT_ID=@NT_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@NT_Name", SqlDbType.NVarChar,-1),
					new SqlParameter("@NT_ID", SqlDbType.Int,4)};
			parameters[0].Value = model.NT_Name;
			parameters[1].Value = model.NT_ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int NT_ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from NetworkType ");
			strSql.Append(" where NT_ID=@NT_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@NT_ID", SqlDbType.Int,4)			};
			parameters[0].Value = NT_ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string NT_IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from NetworkType ");
			strSql.Append(" where NT_ID in ("+NT_IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.NetworkType GetModel(int NT_ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 NT_ID,NT_Name from NetworkType ");
			strSql.Append(" where NT_ID=@NT_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@NT_ID", SqlDbType.Int,4)			};
			parameters[0].Value = NT_ID;

			EduModel.NetworkType model=new EduModel.NetworkType();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.NetworkType DataRowToModel(DataRow row)
		{
			EduModel.NetworkType model=new EduModel.NetworkType();
			if (row != null)
			{
				if(row["NT_ID"]!=null && row["NT_ID"].ToString()!="")
				{
					model.NT_ID=int.Parse(row["NT_ID"].ToString());
				}
				if(row["NT_Name"]!=null)
				{
					model.NT_Name=row["NT_Name"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select NT_ID,NT_Name ");
			strSql.Append(" FROM NetworkType ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" NT_ID,NT_Name ");
			strSql.Append(" FROM NetworkType ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM NetworkType ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.NT_ID desc");
			}
			strSql.Append(")AS Row, T.*  from NetworkType T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "NetworkType";
			parameters[1].Value = "NT_ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

