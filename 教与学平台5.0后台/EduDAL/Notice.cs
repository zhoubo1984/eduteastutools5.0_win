﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
using System.Configuration;
namespace EduDAL
{
    /// <summary>
    /// 数据访问类:Notice
    /// </summary>
    public partial class Notice
    {
        public Notice()
        { }
        #region  BasicMethod

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId(string cn)
        {
            return DbHelperSQL.GetMaxID("SN", "Notice", cn);
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int SN, string cn)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from Notice");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SN;

            return DbHelperSQL.Exists(strSql.ToString(), cn, parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EduModel.Notice model, string cn)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Notice(");
            strSql.Append("N_GUID,N_Title,N_Content,N_CreateDate,N_UserGUID,N_UnitGUID,N_PublishDate,N_IsPublish)");
            strSql.Append(" values (");
            strSql.Append("@N_GUID,@N_Title,@N_Content,@N_CreateDate,@N_UserGUID,@N_UnitGUID,@N_PublishDate,@N_IsPublish)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@N_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@N_Title", SqlDbType.NVarChar,-1),
					new SqlParameter("@N_Content", SqlDbType.NVarChar,-1),
					new SqlParameter("@N_CreateDate", SqlDbType.DateTime),
					new SqlParameter("@N_UserGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@N_UnitGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@N_PublishDate", SqlDbType.DateTime),
					new SqlParameter("@N_IsPublish", SqlDbType.Bit,1)};
            parameters[0].Value = model.N_GUID;
            parameters[1].Value = model.N_Title;
            parameters[2].Value = model.N_Content;
            parameters[3].Value = model.N_CreateDate;
            parameters[4].Value = model.N_UserGUID;
            parameters[5].Value = model.N_UnitGUID;
            parameters[6].Value = model.N_PublishDate;
            parameters[7].Value = model.N_IsPublish;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), cn, parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EduModel.Notice model, string cn)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Notice set ");
            strSql.Append("N_GUID=@N_GUID,");
            strSql.Append("N_Title=@N_Title,");
            strSql.Append("N_Content=@N_Content,");
            strSql.Append("N_CreateDate=@N_CreateDate,");
            strSql.Append("N_UserGUID=@N_UserGUID,");
            strSql.Append("N_UnitGUID=@N_UnitGUID,");
            strSql.Append("N_PublishDate=@N_PublishDate,");
            strSql.Append("N_IsPublish=@N_IsPublish");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@N_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@N_Title", SqlDbType.NVarChar,-1),
					new SqlParameter("@N_Content", SqlDbType.NVarChar,-1),
					new SqlParameter("@N_CreateDate", SqlDbType.DateTime),
					new SqlParameter("@N_UserGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@N_UnitGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@N_PublishDate", SqlDbType.DateTime),
					new SqlParameter("@N_IsPublish", SqlDbType.Bit,1),
					new SqlParameter("@SN", SqlDbType.Int,4)};
            parameters[0].Value = model.N_GUID;
            parameters[1].Value = model.N_Title;
            parameters[2].Value = model.N_Content;
            parameters[3].Value = model.N_CreateDate;
            parameters[4].Value = model.N_UserGUID;
            parameters[5].Value = model.N_UnitGUID;
            parameters[6].Value = model.N_PublishDate;
            parameters[7].Value = model.N_IsPublish;
            parameters[8].Value = model.SN;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), cn, parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SN, string cn)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Notice ");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SN;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), cn, parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string SNlist, string cn)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Notice ");
            strSql.Append(" where SN in (" + SNlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), cn);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.Notice GetModel(int SN, string cn)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 SN,N_GUID,N_Title,N_Content,N_CreateDate,N_UserGUID,N_UnitGUID,N_PublishDate,N_IsPublish from Notice ");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SN;

            EduModel.Notice model = new EduModel.Notice();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), cn, parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.Notice DataRowToModel(DataRow row)
        {
            EduModel.Notice model = new EduModel.Notice();
            if (row != null)
            {
                if (row["SN"] != null && row["SN"].ToString() != "")
                {
                    model.SN = int.Parse(row["SN"].ToString());
                }
                if (row["N_GUID"] != null && row["N_GUID"].ToString() != "")
                {
                    model.N_GUID = new Guid(row["N_GUID"].ToString());
                }
                if (row["N_Title"] != null)
                {
                    model.N_Title = row["N_Title"].ToString();
                }
                if (row["N_Content"] != null)
                {
                    model.N_Content = row["N_Content"].ToString();
                }
                if (row["N_CreateDate"] != null && row["N_CreateDate"].ToString() != "")
                {
                    model.N_CreateDate = DateTime.Parse(row["N_CreateDate"].ToString());
                }
                if (row["N_UserGUID"] != null && row["N_UserGUID"].ToString() != "")
                {
                    model.N_UserGUID = new Guid(row["N_UserGUID"].ToString());
                }
                if (row["N_UnitGUID"] != null && row["N_UnitGUID"].ToString() != "")
                {
                    model.N_UnitGUID = new Guid(row["N_UnitGUID"].ToString());
                }
                if (row["N_PublishDate"] != null && row["N_PublishDate"].ToString() != "")
                {
                    model.N_PublishDate = DateTime.Parse(row["N_PublishDate"].ToString());
                }
                if (row["N_IsPublish"] != null && row["N_IsPublish"].ToString() != "")
                {
                    if ((row["N_IsPublish"].ToString() == "1") || (row["N_IsPublish"].ToString().ToLower() == "true"))
                    {
                        model.N_IsPublish = true;
                    }
                    else
                    {
                        model.N_IsPublish = false;
                    }
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere, string cn)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select SN,N_GUID,N_Title,N_Content,N_CreateDate,N_UserGUID,N_UnitGUID,N_PublishDate,N_IsPublish ");
            strSql.Append(" FROM Notice ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString(), cn);
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder, string cn)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" SN,N_GUID,N_Title,N_Content,N_CreateDate,N_UserGUID,N_UnitGUID,N_PublishDate,N_IsPublish ");
            strSql.Append(" FROM Notice ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString(), cn);
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere, string cn)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Notice ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString(), cn);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex, string cn)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.SN desc");
            }
            strSql.Append(")AS Row, T.*  from Notice T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString(), cn);
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Notice";
            parameters[1].Value = "SN";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

