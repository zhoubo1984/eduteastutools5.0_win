﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
using System.Configuration;
namespace EduDAL
{
    /// <summary>
    /// 数据访问类:Picture
    /// </summary>
    public partial class Picture
    {
        public Picture()
        { }
        #region  BasicMethod

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("SN", "Picture", ConfigurationSettings.AppSettings["NoticeDb"]);
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int SN)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from Picture");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SN;

            return DbHelperSQL.Exists(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"], parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EduModel.Picture model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Picture(");
            strSql.Append("P_Title,P_Path,P_Upload_Date,P_Upload_UserGuid,P_UnitGuid,P_IsShow,P_Href)");
            strSql.Append(" values (");
            strSql.Append("@P_Title,@P_Path,@P_Upload_Date,@P_Upload_UserGuid,@P_UnitGuid,@P_IsShow,@P_Href)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@P_Title", SqlDbType.NVarChar,-1),
					new SqlParameter("@P_Path", SqlDbType.NVarChar,-1),
					new SqlParameter("@P_Upload_Date", SqlDbType.DateTime),
					new SqlParameter("@P_Upload_UserGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@P_UnitGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@P_IsShow", SqlDbType.Bit,1),
                    new SqlParameter("@P_Href", SqlDbType.NVarChar,-1)};
            parameters[0].Value = model.P_Title;
            parameters[1].Value = model.P_Path;
            parameters[2].Value = model.P_Upload_Date;
            parameters[3].Value = model.P_Upload_UserGuid;
            parameters[4].Value = model.P_UnitGuid;
            parameters[5].Value = model.P_IsShow;
            parameters[6].Value = model.P_Href;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"], parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EduModel.Picture model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Picture set ");
            strSql.Append("P_Title=@P_Title,");
            strSql.Append("P_Path=@P_Path,");
            strSql.Append("P_Upload_Date=@P_Upload_Date,");
            strSql.Append("P_Upload_UserGuid=@P_Upload_UserGuid,");
            strSql.Append("P_UnitGuid=@P_UnitGuid,");
            strSql.Append("P_IsShow=@P_IsShow,");
            strSql.Append("P_Href=@P_Href");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@P_Title", SqlDbType.NVarChar,-1),
					new SqlParameter("@P_Path", SqlDbType.NVarChar,-1),
					new SqlParameter("@P_Upload_Date", SqlDbType.DateTime),
					new SqlParameter("@P_Upload_UserGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@P_UnitGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@P_IsShow", SqlDbType.Bit,1),
					new SqlParameter("@SN", SqlDbType.Int,4),
                    new SqlParameter("@P_Href", SqlDbType.NVarChar,-1),};
            parameters[0].Value = model.P_Title;
            parameters[1].Value = model.P_Path;
            parameters[2].Value = model.P_Upload_Date;
            parameters[3].Value = model.P_Upload_UserGuid;
            parameters[4].Value = model.P_UnitGuid;
            parameters[5].Value = model.P_IsShow;
            parameters[6].Value = model.SN;
            parameters[7].Value = model.P_Href;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"], parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SN)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Picture ");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SN;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"], parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string SNlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Picture ");
            strSql.Append(" where SN in (" + SNlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"]);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.Picture GetModel(int SN)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 SN,P_Title,P_Path,P_Upload_Date,P_Upload_UserGuid,P_UnitGuid,P_IsShow,P_Href from Picture ");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SN;

            EduModel.Picture model = new EduModel.Picture();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"], parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.Picture DataRowToModel(DataRow row)
        {
            EduModel.Picture model = new EduModel.Picture();
            if (row != null)
            {
                if (row["SN"] != null && row["SN"].ToString() != "")
                {
                    model.SN = int.Parse(row["SN"].ToString());
                }
                if (row["P_Title"] != null)
                {
                    model.P_Title = row["P_Title"].ToString();
                }
                if (row["P_Path"] != null)
                {
                    model.P_Path = row["P_Path"].ToString();
                }
                if (row["P_Href"] != null)
                {
                    model.P_Href = row["P_Href"].ToString();
                }
                if (row["P_Upload_Date"] != null && row["P_Upload_Date"].ToString() != "")
                {
                    model.P_Upload_Date = DateTime.Parse(row["P_Upload_Date"].ToString());
                }
                if (row["P_Upload_UserGuid"] != null && row["P_Upload_UserGuid"].ToString() != "")
                {
                    model.P_Upload_UserGuid = new Guid(row["P_Upload_UserGuid"].ToString());
                }
                if (row["P_UnitGuid"] != null && row["P_UnitGuid"].ToString() != "")
                {
                    model.P_UnitGuid = new Guid(row["P_UnitGuid"].ToString());
                }
                if (row["P_IsShow"] != null && row["P_IsShow"].ToString() != "")
                {
                    if ((row["P_IsShow"].ToString() == "1") || (row["P_IsShow"].ToString().ToLower() == "true"))
                    {
                        model.P_IsShow = true;
                    }
                    else
                    {
                        model.P_IsShow = false;
                    }
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select SN,P_Title,P_Path,P_Upload_Date,P_Upload_UserGuid,P_UnitGuid,P_IsShow,P_Href ");
            strSql.Append(" FROM Picture ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"]);
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" SN,P_Title,P_Path,P_Upload_Date,P_Upload_UserGuid,P_UnitGuid,P_IsShow,P_Href ");
            strSql.Append(" FROM Picture ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"]);
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Picture ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"]);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.SN desc");
            }
            strSql.Append(")AS Row, T.*  from Picture T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"]);
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Picture";
            parameters[1].Value = "SN";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

