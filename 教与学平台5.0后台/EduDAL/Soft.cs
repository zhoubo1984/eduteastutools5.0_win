﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
using System.Configuration;

namespace EduDAL
{
    /// <summary>
    /// 数据访问类:Soft
    /// </summary>
    public partial class Soft
    {
        public Soft()
        { }
        #region  BasicMethod

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("SN", "Soft", ConfigurationSettings.AppSettings["NoticeDb"]);
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int SN)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from Soft");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SN;

            return DbHelperSQL.Exists(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"], parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EduModel.Soft model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Soft(");
            strSql.Append("S_GUID,S_Name,S_Content,S_Index,S_Date,S_UserGUID,S_UnitGUID)");
            strSql.Append(" values (");
            strSql.Append("@S_GUID,@S_Name,@S_Content,@S_Index,@S_Date,@S_UserGUID,@S_UnitGUID)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@S_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@S_Name", SqlDbType.NVarChar,-1),
					new SqlParameter("@S_Content", SqlDbType.NVarChar,-1),
					new SqlParameter("@S_Index", SqlDbType.NVarChar,-1),
					new SqlParameter("@S_Date", SqlDbType.DateTime),
					new SqlParameter("@S_UserGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@S_UnitGUID", SqlDbType.UniqueIdentifier,16)};
            parameters[0].Value = model.S_GUID;
            parameters[1].Value = model.S_Name;
            parameters[2].Value = model.S_Content;
            parameters[3].Value = model.S_Index;
            parameters[4].Value = model.S_Date;
            parameters[5].Value = model.S_UserGUID;
            parameters[6].Value = model.S_UnitGUID;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"], parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EduModel.Soft model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Soft set ");
            strSql.Append("S_GUID=@S_GUID,");
            strSql.Append("S_Name=@S_Name,");
            strSql.Append("S_Content=@S_Content,");
            strSql.Append("S_Index=@S_Index,");
            strSql.Append("S_Date=@S_Date,");
            strSql.Append("S_UserGUID=@S_UserGUID,");
            strSql.Append("S_UnitGUID=@S_UnitGUID");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@S_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@S_Name", SqlDbType.NVarChar,-1),
					new SqlParameter("@S_Content", SqlDbType.NVarChar,-1),
					new SqlParameter("@S_Index", SqlDbType.NVarChar,-1),
					new SqlParameter("@S_Date", SqlDbType.DateTime),
					new SqlParameter("@S_UserGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@S_UnitGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@SN", SqlDbType.Int,4)};
            parameters[0].Value = model.S_GUID;
            parameters[1].Value = model.S_Name;
            parameters[2].Value = model.S_Content;
            parameters[3].Value = model.S_Index;
            parameters[4].Value = model.S_Date;
            parameters[5].Value = model.S_UserGUID;
            parameters[6].Value = model.S_UnitGUID;
            parameters[7].Value = model.SN;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"], parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SN)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Soft ");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SN;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"], parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string SNlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Soft ");
            strSql.Append(" where SN in (" + SNlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"]);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.Soft GetModel(int SN)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 SN,S_GUID,S_Name,S_Content,S_Index,S_Date,S_UserGUID,S_UnitGUID from Soft ");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SN;

            EduModel.Soft model = new EduModel.Soft();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"], parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.Soft DataRowToModel(DataRow row)
        {
            EduModel.Soft model = new EduModel.Soft();
            if (row != null)
            {
                if (row["SN"] != null && row["SN"].ToString() != "")
                {
                    model.SN = int.Parse(row["SN"].ToString());
                }
                if (row["S_GUID"] != null && row["S_GUID"].ToString() != "")
                {
                    model.S_GUID = new Guid(row["S_GUID"].ToString());
                }
                if (row["S_Name"] != null)
                {
                    model.S_Name = row["S_Name"].ToString();
                }
                if (row["S_Content"] != null)
                {
                    model.S_Content = row["S_Content"].ToString();
                }
                if (row["S_Index"] != null)
                {
                    model.S_Index = row["S_Index"].ToString();
                }
                if (row["S_Date"] != null && row["S_Date"].ToString() != "")
                {
                    model.S_Date = DateTime.Parse(row["S_Date"].ToString());
                }
                if (row["S_UserGUID"] != null && row["S_UserGUID"].ToString() != "")
                {
                    model.S_UserGUID = new Guid(row["S_UserGUID"].ToString());
                }
                if (row["S_UnitGUID"] != null && row["S_UnitGUID"].ToString() != "")
                {
                    model.S_UnitGUID = new Guid(row["S_UnitGUID"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select SN,S_GUID,S_Name,S_Content,S_Index,S_Date,S_UserGUID,S_UnitGUID ");
            strSql.Append(" FROM Soft ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"]);
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" SN,S_GUID,S_Name,S_Content,S_Index,S_Date,S_UserGUID,S_UnitGUID ");
            strSql.Append(" FROM Soft ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"]);
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Soft ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"]);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.SN desc");
            }
            strSql.Append(")AS Row, T.*  from Soft T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString(), ConfigurationSettings.AppSettings["NoticeDb"]);
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Soft";
            parameters[1].Value = "SN";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

