﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
	/// <summary>
	/// 数据访问类:Student_Class
	/// </summary>
	public partial class Student_Class
	{
		public Student_Class()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(Guid SC_GUID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Student_Class");
			strSql.Append(" where SC_GUID=@SC_GUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@SC_GUID", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = SC_GUID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.Student_Class GetModel(Guid stuGuid)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 SC_SN,SC_GUID,SC_StudentGUID,SC_ClassGUID,State from Student_Class ");
            strSql.Append(" where SC_StudentGUID=@SC_StudentGUID and State=1");
            SqlParameter[] parameters = {
					new SqlParameter("@SC_StudentGUID", SqlDbType.UniqueIdentifier,16)
			};
            parameters[0].Value = stuGuid;

            EduModel.Student_Class model = new EduModel.Student_Class();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EduModel.Student_Class model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Student_Class(");
			strSql.Append("SC_GUID,SC_StudentGUID,SC_ClassGUID,State)");
			strSql.Append(" values (");
			strSql.Append("@SC_GUID,@SC_StudentGUID,@SC_ClassGUID,@State)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@SC_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@SC_StudentGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@SC_ClassGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@State", SqlDbType.Int,4)};
			parameters[0].Value = model.SC_GUID;
			parameters[1].Value = model.SC_StudentGUID;
			parameters[2].Value = model.SC_ClassGUID;
			parameters[3].Value = model.State;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.Student_Class model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Student_Class set ");
			strSql.Append("SC_StudentGUID=@SC_StudentGUID,");
			strSql.Append("SC_ClassGUID=@SC_ClassGUID,");
			strSql.Append("State=@State");
			strSql.Append(" where SC_SN=@SC_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SC_StudentGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@SC_ClassGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@SC_SN", SqlDbType.Int,4),
					new SqlParameter("@SC_GUID", SqlDbType.UniqueIdentifier,16)};
			parameters[0].Value = model.SC_StudentGUID;
			parameters[1].Value = model.SC_ClassGUID;
			parameters[2].Value = model.State;
			parameters[3].Value = model.SC_SN;
			parameters[4].Value = model.SC_GUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SC_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Student_Class ");
			strSql.Append(" where SC_SN=@SC_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SC_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = SC_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(Guid SC_GUID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Student_Class ");
			strSql.Append(" where SC_GUID=@SC_GUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@SC_GUID", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = SC_GUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string SC_SNlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Student_Class ");
			strSql.Append(" where SC_SN in ("+SC_SNlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Student_Class GetModel(int SC_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 SC_SN,SC_GUID,SC_StudentGUID,SC_ClassGUID,State from Student_Class ");
			strSql.Append(" where SC_SN=@SC_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SC_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = SC_SN;

			EduModel.Student_Class model=new EduModel.Student_Class();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Student_Class DataRowToModel(DataRow row)
		{
			EduModel.Student_Class model=new EduModel.Student_Class();
			if (row != null)
			{
				if(row["SC_SN"]!=null && row["SC_SN"].ToString()!="")
				{
					model.SC_SN=int.Parse(row["SC_SN"].ToString());
				}
				if(row["SC_GUID"]!=null && row["SC_GUID"].ToString()!="")
				{
					model.SC_GUID= new Guid(row["SC_GUID"].ToString());
				}
				if(row["SC_StudentGUID"]!=null && row["SC_StudentGUID"].ToString()!="")
				{
					model.SC_StudentGUID= new Guid(row["SC_StudentGUID"].ToString());
				}
				if(row["SC_ClassGUID"]!=null && row["SC_ClassGUID"].ToString()!="")
				{
					model.SC_ClassGUID= new Guid(row["SC_ClassGUID"].ToString());
				}
				if(row["State"]!=null && row["State"].ToString()!="")
				{
					model.State=int.Parse(row["State"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select SC_SN,SC_GUID,SC_StudentGUID,SC_ClassGUID,State ");
			strSql.Append(" FROM Student_Class ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" SC_SN,SC_GUID,SC_StudentGUID,SC_ClassGUID,State ");
			strSql.Append(" FROM Student_Class ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Student_Class ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.SC_SN desc");
			}
			strSql.Append(")AS Row, T.*  from Student_Class T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Student_Class";
			parameters[1].Value = "SC_SN";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

