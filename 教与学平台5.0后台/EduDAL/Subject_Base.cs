﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
	/// <summary>
	/// 数据访问类:Subject_Base
	/// </summary>
	public partial class Subject_Base
	{
		public Subject_Base()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(Guid SB_GUID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Subject_Base");
			strSql.Append(" where SB_GUID=@SB_GUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@SB_GUID", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = SB_GUID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EduModel.Subject_Base model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Subject_Base(");
			strSql.Append("SB_GUID,SB_SubjectName,GradeState,State,SB_Index)");
			strSql.Append(" values (");
			strSql.Append("@SB_GUID,@SB_SubjectName,@GradeState,@State,@SB_Index)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@SB_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@SB_SubjectName", SqlDbType.NVarChar,50),
					new SqlParameter("@GradeState", SqlDbType.Int,4),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@SB_Index", SqlDbType.Int,4)};
			parameters[0].Value = Guid.NewGuid();
			parameters[1].Value = model.SB_SubjectName;
			parameters[2].Value = model.GradeState;
			parameters[3].Value = model.State;
			parameters[4].Value = model.SB_Index;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.Subject_Base model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Subject_Base set ");
			strSql.Append("SB_SubjectName=@SB_SubjectName,");
			strSql.Append("GradeState=@GradeState,");
			strSql.Append("State=@State,");
			strSql.Append("SB_Index=@SB_Index");
			strSql.Append(" where SB_SN=@SB_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SB_SubjectName", SqlDbType.NVarChar,50),
					new SqlParameter("@GradeState", SqlDbType.Int,4),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@SB_Index", SqlDbType.Int,4),
					new SqlParameter("@SB_SN", SqlDbType.Int,4),
					new SqlParameter("@SB_GUID", SqlDbType.UniqueIdentifier,16)};
			parameters[0].Value = model.SB_SubjectName;
			parameters[1].Value = model.GradeState;
			parameters[2].Value = model.State;
			parameters[3].Value = model.SB_Index;
			parameters[4].Value = model.SB_SN;
			parameters[5].Value = model.SB_GUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SB_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Subject_Base ");
			strSql.Append(" where SB_SN=@SB_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SB_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = SB_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(Guid SB_GUID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Subject_Base ");
			strSql.Append(" where SB_GUID=@SB_GUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@SB_GUID", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = SB_GUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string SB_SNlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Subject_Base ");
			strSql.Append(" where SB_SN in ("+SB_SNlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Subject_Base GetModel(int SB_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 SB_SN,SB_GUID,SB_SubjectName,GradeState,State,SB_Index from Subject_Base ");
			strSql.Append(" where SB_SN=@SB_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SB_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = SB_SN;

			EduModel.Subject_Base model=new EduModel.Subject_Base();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Subject_Base DataRowToModel(DataRow row)
		{
			EduModel.Subject_Base model=new EduModel.Subject_Base();
			if (row != null)
			{
				if(row["SB_SN"]!=null && row["SB_SN"].ToString()!="")
				{
					model.SB_SN=int.Parse(row["SB_SN"].ToString());
				}
				if(row["SB_GUID"]!=null && row["SB_GUID"].ToString()!="")
				{
					model.SB_GUID= new Guid(row["SB_GUID"].ToString());
				}
				if(row["SB_SubjectName"]!=null)
				{
					model.SB_SubjectName=row["SB_SubjectName"].ToString();
				}
				if(row["GradeState"]!=null && row["GradeState"].ToString()!="")
				{
					model.GradeState=int.Parse(row["GradeState"].ToString());
				}
				if(row["State"]!=null && row["State"].ToString()!="")
				{
					model.State=int.Parse(row["State"].ToString());
				}
				if(row["SB_Index"]!=null && row["SB_Index"].ToString()!="")
				{
					model.SB_Index=int.Parse(row["SB_Index"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select SB_SN,SB_GUID,SB_SubjectName,GradeState,State,SB_Index ");
			strSql.Append(" FROM Subject_Base ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" SB_SN,SB_GUID,SB_SubjectName,GradeState,State,SB_Index ");
			strSql.Append(" FROM Subject_Base ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Subject_Base ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.SB_SN desc");
			}
			strSql.Append(")AS Row, T.*  from Subject_Base T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Subject_Base";
			parameters[1].Value = "SB_SN";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

