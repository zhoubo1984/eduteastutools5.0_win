﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
	/// <summary>
	/// 数据访问类:Subject_Base_Ext
	/// </summary>
	public partial class Subject_Base_Ext
	{
		public Subject_Base_Ext()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(Guid SB_GUID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Subject_Base_Ext");
			strSql.Append(" where SB_GUID=@SB_GUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@SB_GUID", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = SB_GUID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EduModel.Subject_Base_Ext model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Subject_Base_Ext(");
			strSql.Append("SB_GUID,BookType,Normal)");
			strSql.Append(" values (");
			strSql.Append("@SB_GUID,@BookType,@Normal)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@SB_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@BookType", SqlDbType.Int,4),
					new SqlParameter("@Normal", SqlDbType.NVarChar,50)};
			parameters[0].Value =model.SB_GUID;
			parameters[1].Value = model.BookType;
			parameters[2].Value = model.Normal;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.Subject_Base_Ext model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Subject_Base_Ext set ");
			strSql.Append("BookType=@BookType,");
			strSql.Append("Normal=@Normal");
			strSql.Append(" where SN=@SN");
			SqlParameter[] parameters = {
					new SqlParameter("@BookType", SqlDbType.Int,4),
					new SqlParameter("@Normal", SqlDbType.NVarChar,50),
					new SqlParameter("@SN", SqlDbType.Int,4),
					new SqlParameter("@SB_GUID", SqlDbType.UniqueIdentifier,16)};
			parameters[0].Value = model.BookType;
			parameters[1].Value = model.Normal;
			parameters[2].Value = model.SN;
			parameters[3].Value = model.SB_GUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Subject_Base_Ext ");
			strSql.Append(" where SN=@SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
			parameters[0].Value = SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(Guid SB_GUID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Subject_Base_Ext ");
			strSql.Append(" where SB_GUID=@SB_GUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@SB_GUID", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = SB_GUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string SNlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Subject_Base_Ext ");
			strSql.Append(" where SN in ("+SNlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Subject_Base_Ext GetModel(int SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 SN,SB_GUID,BookType,Normal from Subject_Base_Ext ");
			strSql.Append(" where SN=@SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
			parameters[0].Value = SN;

			EduModel.Subject_Base_Ext model=new EduModel.Subject_Base_Ext();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Subject_Base_Ext DataRowToModel(DataRow row)
		{
			EduModel.Subject_Base_Ext model=new EduModel.Subject_Base_Ext();
			if (row != null)
			{
				if(row["SN"]!=null && row["SN"].ToString()!="")
				{
					model.SN=int.Parse(row["SN"].ToString());
				}
				if(row["SB_GUID"]!=null && row["SB_GUID"].ToString()!="")
				{
					model.SB_GUID= new Guid(row["SB_GUID"].ToString());
				}
				if(row["BookType"]!=null && row["BookType"].ToString()!="")
				{
					model.BookType=int.Parse(row["BookType"].ToString());
				}
				if(row["Normal"]!=null)
				{
					model.Normal=row["Normal"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select SN,SB_GUID,BookType,Normal ");
			strSql.Append(" FROM Subject_Base_Ext ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" SN,SB_GUID,BookType,Normal ");
			strSql.Append(" FROM Subject_Base_Ext ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Subject_Base_Ext ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.SN desc");
			}
			strSql.Append(")AS Row, T.*  from Subject_Base_Ext T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Subject_Base_Ext";
			parameters[1].Value = "SN";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

