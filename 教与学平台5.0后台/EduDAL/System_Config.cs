﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
	/// <summary>
	/// 数据访问类:System_Config
	/// </summary>
	public partial class System_Config
	{
		public System_Config()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("SC_SN", "System_Config"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int SC_SN)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from System_Config");
			strSql.Append(" where SC_SN=@SC_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SC_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = SC_SN;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EduModel.System_Config model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into System_Config(");
			strSql.Append("SC_Name,SC_Value,SC_ReMark)");
			strSql.Append(" values (");
			strSql.Append("@SC_Name,@SC_Value,@SC_ReMark)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@SC_Name", SqlDbType.NVarChar,-1),
					new SqlParameter("@SC_Value", SqlDbType.NVarChar,-1),
					new SqlParameter("@SC_ReMark", SqlDbType.NVarChar,-1)};
			parameters[0].Value = model.SC_Name;
			parameters[1].Value = model.SC_Value;
			parameters[2].Value = model.SC_ReMark;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.System_Config model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update System_Config set ");
			strSql.Append("SC_Name=@SC_Name,");
			strSql.Append("SC_Value=@SC_Value,");
			strSql.Append("SC_ReMark=@SC_ReMark");
			strSql.Append(" where SC_SN=@SC_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SC_Name", SqlDbType.NVarChar,-1),
					new SqlParameter("@SC_Value", SqlDbType.NVarChar,-1),
					new SqlParameter("@SC_ReMark", SqlDbType.NVarChar,-1),
					new SqlParameter("@SC_SN", SqlDbType.Int,4)};
			parameters[0].Value = model.SC_Name;
			parameters[1].Value = model.SC_Value;
			parameters[2].Value = model.SC_ReMark;
			parameters[3].Value = model.SC_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SC_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from System_Config ");
			strSql.Append(" where SC_SN=@SC_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SC_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = SC_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string SC_SNlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from System_Config ");
			strSql.Append(" where SC_SN in ("+SC_SNlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.System_Config GetModel(int SC_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 SC_SN,SC_Name,SC_Value,SC_ReMark from System_Config ");
			strSql.Append(" where SC_SN=@SC_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SC_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = SC_SN;

			EduModel.System_Config model=new EduModel.System_Config();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.System_Config DataRowToModel(DataRow row)
		{
			EduModel.System_Config model=new EduModel.System_Config();
			if (row != null)
			{
				if(row["SC_SN"]!=null && row["SC_SN"].ToString()!="")
				{
					model.SC_SN=int.Parse(row["SC_SN"].ToString());
				}
				if(row["SC_Name"]!=null)
				{
					model.SC_Name=row["SC_Name"].ToString();
				}
				if(row["SC_Value"]!=null)
				{
					model.SC_Value=row["SC_Value"].ToString();
				}
				if(row["SC_ReMark"]!=null)
				{
					model.SC_ReMark=row["SC_ReMark"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select SC_SN,SC_Name,SC_Value,SC_ReMark ");
			strSql.Append(" FROM System_Config ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" SC_SN,SC_Name,SC_Value,SC_ReMark ");
			strSql.Append(" FROM System_Config ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM System_Config ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.SC_SN desc");
			}
			strSql.Append(")AS Row, T.*  from System_Config T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "System_Config";
			parameters[1].Value = "SC_SN";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

