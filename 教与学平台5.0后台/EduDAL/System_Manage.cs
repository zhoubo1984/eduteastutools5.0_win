﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
	/// <summary>
	/// 数据访问类:System_Manage
	/// </summary>
	public partial class System_Manage
	{
		public System_Manage()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("SM_SN", "System_Manage"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int SM_SN)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from System_Manage");
			strSql.Append(" where SM_SN=@SM_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SM_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = SM_SN;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string uID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from System_Manage");
            strSql.Append(" where SM_ManageID=@SM_ManageID");
            SqlParameter[] parameters = {
					new SqlParameter("@SM_ManageID", SqlDbType.NVarChar,-1)
			};
            parameters[0].Value = uID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EduModel.System_Manage model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into System_Manage(");
			strSql.Append("SM_GUID,SM_ManageID,SM_ManageName,SM_ManagePsw,SM_UBGuid,SM_State,SM_Role)");
			strSql.Append(" values (");
			strSql.Append("@SM_GUID,@SM_ManageID,@SM_ManageName,@SM_ManagePsw,@SM_UBGuid,@SM_State,@SM_Role)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@SM_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@SM_ManageID", SqlDbType.NVarChar,-1),
					new SqlParameter("@SM_ManageName", SqlDbType.NVarChar,-1),
					new SqlParameter("@SM_ManagePsw", SqlDbType.NVarChar,-1),
					new SqlParameter("@SM_UBGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@SM_State", SqlDbType.Int,4),
					new SqlParameter("@SM_Role", SqlDbType.Int,4)};
			parameters[0].Value = model.SM_GUID;
			parameters[1].Value = model.SM_ManageID;
			parameters[2].Value = model.SM_ManageName;
			parameters[3].Value = model.SM_ManagePsw;
			parameters[4].Value = model.SM_UBGuid;
			parameters[5].Value = model.SM_State;
			parameters[6].Value = model.SM_Role;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.System_Manage model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update System_Manage set ");
			strSql.Append("SM_GUID=@SM_GUID,");
			strSql.Append("SM_ManageID=@SM_ManageID,");
			strSql.Append("SM_ManageName=@SM_ManageName,");
			strSql.Append("SM_ManagePsw=@SM_ManagePsw,");
			strSql.Append("SM_UBGuid=@SM_UBGuid,");
			strSql.Append("SM_State=@SM_State,");
			strSql.Append("SM_Role=@SM_Role");
			strSql.Append(" where SM_SN=@SM_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SM_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@SM_ManageID", SqlDbType.NVarChar,-1),
					new SqlParameter("@SM_ManageName", SqlDbType.NVarChar,-1),
					new SqlParameter("@SM_ManagePsw", SqlDbType.NVarChar,-1),
					new SqlParameter("@SM_UBGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@SM_State", SqlDbType.Int,4),
					new SqlParameter("@SM_Role", SqlDbType.Int,4),
					new SqlParameter("@SM_SN", SqlDbType.Int,4)};
			parameters[0].Value = model.SM_GUID;
			parameters[1].Value = model.SM_ManageID;
			parameters[2].Value = model.SM_ManageName;
			parameters[3].Value = model.SM_ManagePsw;
			parameters[4].Value = model.SM_UBGuid;
			parameters[5].Value = model.SM_State;
			parameters[6].Value = model.SM_Role;
			parameters[7].Value = model.SM_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SM_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from System_Manage ");
			strSql.Append(" where SM_SN=@SM_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SM_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = SM_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string SM_SNlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from System_Manage ");
			strSql.Append(" where SM_SN in ("+SM_SNlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.System_Manage GetModel(int SM_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 SM_SN,SM_GUID,SM_ManageID,SM_ManageName,SM_ManagePsw,SM_UBGuid,SM_State,SM_Role from System_Manage ");
			strSql.Append(" where SM_SN=@SM_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SM_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = SM_SN;

			EduModel.System_Manage model=new EduModel.System_Manage();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.System_Manage DataRowToModel(DataRow row)
		{
			EduModel.System_Manage model=new EduModel.System_Manage();
			if (row != null)
			{
				if(row["SM_SN"]!=null && row["SM_SN"].ToString()!="")
				{
					model.SM_SN=int.Parse(row["SM_SN"].ToString());
				}
				if(row["SM_GUID"]!=null && row["SM_GUID"].ToString()!="")
				{
					model.SM_GUID= new Guid(row["SM_GUID"].ToString());
				}
				if(row["SM_ManageID"]!=null)
				{
					model.SM_ManageID=row["SM_ManageID"].ToString();
				}
				if(row["SM_ManageName"]!=null)
				{
					model.SM_ManageName=row["SM_ManageName"].ToString();
				}
				if(row["SM_ManagePsw"]!=null)
				{
					model.SM_ManagePsw=row["SM_ManagePsw"].ToString();
				}
				if(row["SM_UBGuid"]!=null && row["SM_UBGuid"].ToString()!="")
				{
					model.SM_UBGuid= new Guid(row["SM_UBGuid"].ToString());
				}
				if(row["SM_State"]!=null && row["SM_State"].ToString()!="")
				{
					model.SM_State=int.Parse(row["SM_State"].ToString());
				}
				if(row["SM_Role"]!=null && row["SM_Role"].ToString()!="")
				{
					model.SM_Role=int.Parse(row["SM_Role"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select SM_SN,SM_GUID,SM_ManageID,SM_ManageName,SM_ManagePsw,SM_UBGuid,SM_State,SM_Role ");
			strSql.Append(" FROM System_Manage ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" SM_SN,SM_GUID,SM_ManageID,SM_ManageName,SM_ManagePsw,SM_UBGuid,SM_State,SM_Role ");
			strSql.Append(" FROM System_Manage ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM System_Manage ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.SM_SN desc");
			}
			strSql.Append(")AS Row, T.*  from System_Manage T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "System_Manage";
			parameters[1].Value = "SM_SN";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

