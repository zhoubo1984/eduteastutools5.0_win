﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
	/// <summary>
	/// 数据访问类:System_ServiceAddress
	/// </summary>
	public partial class System_ServiceAddress
	{
		public System_ServiceAddress()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("SS_SN", "System_ServiceAddress"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int SS_SN)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from System_ServiceAddress");
			strSql.Append(" where SS_SN=@SS_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SS_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = SS_SN;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EduModel.System_ServiceAddress model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into System_ServiceAddress(");
			strSql.Append("SS_Name,SS_NetWork,SS_Address)");
			strSql.Append(" values (");
			strSql.Append("@SS_Name,@SS_NetWork,@SS_Address)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@SS_Name", SqlDbType.NVarChar,50),
					new SqlParameter("@SS_NetWork", SqlDbType.Int,4),
					new SqlParameter("@SS_Address", SqlDbType.NVarChar,-1)};
			parameters[0].Value = model.SS_Name;
			parameters[1].Value = model.SS_NetWork;
			parameters[2].Value = model.SS_Address;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.System_ServiceAddress model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update System_ServiceAddress set ");
			strSql.Append("SS_Name=@SS_Name,");
			strSql.Append("SS_NetWork=@SS_NetWork,");
			strSql.Append("SS_Address=@SS_Address");
			strSql.Append(" where SS_SN=@SS_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SS_Name", SqlDbType.NVarChar,50),
					new SqlParameter("@SS_NetWork", SqlDbType.Int,4),
					new SqlParameter("@SS_Address", SqlDbType.NVarChar,-1),
					new SqlParameter("@SS_SN", SqlDbType.Int,4)};
			parameters[0].Value = model.SS_Name;
			parameters[1].Value = model.SS_NetWork;
			parameters[2].Value = model.SS_Address;
			parameters[3].Value = model.SS_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SS_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from System_ServiceAddress ");
			strSql.Append(" where SS_SN=@SS_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SS_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = SS_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string SS_SNlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from System_ServiceAddress ");
			strSql.Append(" where SS_SN in ("+SS_SNlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.System_ServiceAddress GetModel(int SS_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 SS_SN,SS_Name,SS_NetWork,SS_Address from System_ServiceAddress ");
			strSql.Append(" where SS_SN=@SS_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@SS_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = SS_SN;

			EduModel.System_ServiceAddress model=new EduModel.System_ServiceAddress();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.System_ServiceAddress DataRowToModel(DataRow row)
		{
			EduModel.System_ServiceAddress model=new EduModel.System_ServiceAddress();
			if (row != null)
			{
				if(row["SS_SN"]!=null && row["SS_SN"].ToString()!="")
				{
					model.SS_SN=int.Parse(row["SS_SN"].ToString());
				}
				if(row["SS_Name"]!=null)
				{
					model.SS_Name=row["SS_Name"].ToString();
				}
				if(row["SS_NetWork"]!=null && row["SS_NetWork"].ToString()!="")
				{
					model.SS_NetWork=int.Parse(row["SS_NetWork"].ToString());
				}
				if(row["SS_Address"]!=null)
				{
					model.SS_Address=row["SS_Address"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select SS_SN,SS_Name,SS_NetWork,SS_Address ");
			strSql.Append(" FROM System_ServiceAddress ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" SS_SN,SS_Name,SS_NetWork,SS_Address ");
			strSql.Append(" FROM System_ServiceAddress ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM System_ServiceAddress ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.SS_SN desc");
			}
			strSql.Append(")AS Row, T.*  from System_ServiceAddress T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "System_ServiceAddress";
			parameters[1].Value = "SS_SN";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

