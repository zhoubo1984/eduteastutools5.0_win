﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
	/// <summary>
	/// 数据访问类:Teacher_Subject_Book_Class
	/// </summary>
	public partial class Teacher_Subject_Book_Class
	{
		public Teacher_Subject_Book_Class()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(Guid TSBC_GUID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Teacher_Subject_Book_Class");
			strSql.Append(" where TSBC_GUID=@TSBC_GUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@TSBC_GUID", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = TSBC_GUID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EduModel.Teacher_Subject_Book_Class model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Teacher_Subject_Book_Class(");
			strSql.Append("TSBC_GUID,TSBC_TSBGUID,TSBC_ClassGUID,State)");
			strSql.Append(" values (");
			strSql.Append("@TSBC_GUID,@TSBC_TSBGUID,@TSBC_ClassGUID,@State)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@TSBC_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@TSBC_TSBGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@TSBC_ClassGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@State", SqlDbType.Int,4)};
			parameters[0].Value = model.TSBC_GUID;
			parameters[1].Value = model.TSBC_TSBGUID;
			parameters[2].Value = model.TSBC_ClassGUID;
			parameters[3].Value = model.State;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.Teacher_Subject_Book_Class model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Teacher_Subject_Book_Class set ");
			strSql.Append("TSBC_TSBGUID=@TSBC_TSBGUID,");
			strSql.Append("TSBC_ClassGUID=@TSBC_ClassGUID,");
			strSql.Append("State=@State");
			strSql.Append(" where TSBC_SN=@TSBC_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@TSBC_TSBGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@TSBC_ClassGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@TSBC_SN", SqlDbType.Int,4),
					new SqlParameter("@TSBC_GUID", SqlDbType.UniqueIdentifier,16)};
			parameters[0].Value = model.TSBC_TSBGUID;
			parameters[1].Value = model.TSBC_ClassGUID;
			parameters[2].Value = model.State;
			parameters[3].Value = model.TSBC_SN;
			parameters[4].Value = model.TSBC_GUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int TSBC_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Teacher_Subject_Book_Class ");
			strSql.Append(" where TSBC_SN=@TSBC_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@TSBC_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = TSBC_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(Guid TSBC_GUID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Teacher_Subject_Book_Class ");
			strSql.Append(" where TSBC_GUID=@TSBC_GUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@TSBC_GUID", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = TSBC_GUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string TSBC_SNlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Teacher_Subject_Book_Class ");
			strSql.Append(" where TSBC_SN in ("+TSBC_SNlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Teacher_Subject_Book_Class GetModel(int TSBC_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 TSBC_SN,TSBC_GUID,TSBC_TSBGUID,TSBC_ClassGUID,State from Teacher_Subject_Book_Class ");
			strSql.Append(" where TSBC_SN=@TSBC_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@TSBC_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = TSBC_SN;

			EduModel.Teacher_Subject_Book_Class model=new EduModel.Teacher_Subject_Book_Class();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Teacher_Subject_Book_Class DataRowToModel(DataRow row)
		{
			EduModel.Teacher_Subject_Book_Class model=new EduModel.Teacher_Subject_Book_Class();
			if (row != null)
			{
				if(row["TSBC_SN"]!=null && row["TSBC_SN"].ToString()!="")
				{
					model.TSBC_SN=int.Parse(row["TSBC_SN"].ToString());
				}
				if(row["TSBC_GUID"]!=null && row["TSBC_GUID"].ToString()!="")
				{
					model.TSBC_GUID= new Guid(row["TSBC_GUID"].ToString());
				}
				if(row["TSBC_TSBGUID"]!=null && row["TSBC_TSBGUID"].ToString()!="")
				{
					model.TSBC_TSBGUID= new Guid(row["TSBC_TSBGUID"].ToString());
				}
				if(row["TSBC_ClassGUID"]!=null && row["TSBC_ClassGUID"].ToString()!="")
				{
					model.TSBC_ClassGUID= new Guid(row["TSBC_ClassGUID"].ToString());
				}
				if(row["State"]!=null && row["State"].ToString()!="")
				{
					model.State=int.Parse(row["State"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select TSBC_SN,TSBC_GUID,TSBC_TSBGUID,TSBC_ClassGUID,State ");
			strSql.Append(" FROM Teacher_Subject_Book_Class ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" TSBC_SN,TSBC_GUID,TSBC_TSBGUID,TSBC_ClassGUID,State ");
			strSql.Append(" FROM Teacher_Subject_Book_Class ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Teacher_Subject_Book_Class ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.TSBC_SN desc");
			}
			strSql.Append(")AS Row, T.*  from Teacher_Subject_Book_Class T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Teacher_Subject_Book_Class";
			parameters[1].Value = "TSBC_SN";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

