﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
	/// <summary>
	/// 数据访问类:Teacher_Subject_Class_Group
	/// </summary>
	public partial class Teacher_Subject_Class_Group
	{
		public Teacher_Subject_Class_Group()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(Guid TSCG_Guid)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Teacher_Subject_Class_Group");
			strSql.Append(" where TSCG_Guid=@TSCG_Guid ");
			SqlParameter[] parameters = {
					new SqlParameter("@TSCG_Guid", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = TSCG_Guid;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EduModel.Teacher_Subject_Class_Group model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Teacher_Subject_Class_Group(");
			strSql.Append("TSCG_Guid,TSCG_TSGuid,TSCG_ClassGuid,TSCG_GroupName,State)");
			strSql.Append(" values (");
			strSql.Append("@TSCG_Guid,@TSCG_TSGuid,@TSCG_ClassGuid,@TSCG_GroupName,@State)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@TSCG_Guid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@TSCG_TSGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@TSCG_ClassGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@TSCG_GroupName", SqlDbType.NVarChar,-1),
					new SqlParameter("@State", SqlDbType.Int,4)};
			parameters[0].Value = model.TSCG_Guid;
			parameters[1].Value = model.TSCG_TSGuid;
			parameters[2].Value = model.TSCG_ClassGuid;
			parameters[3].Value = model.TSCG_GroupName;
			parameters[4].Value = model.State;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.Teacher_Subject_Class_Group model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Teacher_Subject_Class_Group set ");
			strSql.Append("TSCG_TSGuid=@TSCG_TSGuid,");
			strSql.Append("TSCG_ClassGuid=@TSCG_ClassGuid,");
			strSql.Append("TSCG_GroupName=@TSCG_GroupName,");
			strSql.Append("State=@State");
			strSql.Append(" where TSCG_SN=@TSCG_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@TSCG_TSGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@TSCG_ClassGuid", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@TSCG_GroupName", SqlDbType.NVarChar,-1),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@TSCG_SN", SqlDbType.Int,4),
					new SqlParameter("@TSCG_Guid", SqlDbType.UniqueIdentifier,16)};
			parameters[0].Value = model.TSCG_TSGuid;
			parameters[1].Value = model.TSCG_ClassGuid;
			parameters[2].Value = model.TSCG_GroupName;
			parameters[3].Value = model.State;
			parameters[4].Value = model.TSCG_SN;
			parameters[5].Value = model.TSCG_Guid;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int TSCG_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Teacher_Subject_Class_Group ");
			strSql.Append(" where TSCG_SN=@TSCG_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@TSCG_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = TSCG_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(Guid TSCG_Guid)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Teacher_Subject_Class_Group ");
			strSql.Append(" where TSCG_Guid=@TSCG_Guid ");
			SqlParameter[] parameters = {
					new SqlParameter("@TSCG_Guid", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = TSCG_Guid;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string TSCG_SNlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Teacher_Subject_Class_Group ");
			strSql.Append(" where TSCG_SN in ("+TSCG_SNlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Teacher_Subject_Class_Group GetModel(int TSCG_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 TSCG_SN,TSCG_Guid,TSCG_TSGuid,TSCG_ClassGuid,TSCG_GroupName,State from Teacher_Subject_Class_Group ");
			strSql.Append(" where TSCG_SN=@TSCG_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@TSCG_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = TSCG_SN;

			EduModel.Teacher_Subject_Class_Group model=new EduModel.Teacher_Subject_Class_Group();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Teacher_Subject_Class_Group DataRowToModel(DataRow row)
		{
			EduModel.Teacher_Subject_Class_Group model=new EduModel.Teacher_Subject_Class_Group();
			if (row != null)
			{
				if(row["TSCG_SN"]!=null && row["TSCG_SN"].ToString()!="")
				{
					model.TSCG_SN=int.Parse(row["TSCG_SN"].ToString());
				}
				if(row["TSCG_Guid"]!=null && row["TSCG_Guid"].ToString()!="")
				{
					model.TSCG_Guid= new Guid(row["TSCG_Guid"].ToString());
				}
				if(row["TSCG_TSGuid"]!=null && row["TSCG_TSGuid"].ToString()!="")
				{
					model.TSCG_TSGuid= new Guid(row["TSCG_TSGuid"].ToString());
				}
				if(row["TSCG_ClassGuid"]!=null && row["TSCG_ClassGuid"].ToString()!="")
				{
					model.TSCG_ClassGuid= new Guid(row["TSCG_ClassGuid"].ToString());
				}
				if(row["TSCG_GroupName"]!=null)
				{
					model.TSCG_GroupName=row["TSCG_GroupName"].ToString();
				}
				if(row["State"]!=null && row["State"].ToString()!="")
				{
					model.State=int.Parse(row["State"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select TSCG_SN,TSCG_Guid,TSCG_TSGuid,TSCG_ClassGuid,TSCG_GroupName,State ");
			strSql.Append(" FROM Teacher_Subject_Class_Group ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" TSCG_SN,TSCG_Guid,TSCG_TSGuid,TSCG_ClassGuid,TSCG_GroupName,State ");
			strSql.Append(" FROM Teacher_Subject_Class_Group ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Teacher_Subject_Class_Group ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.TSCG_SN desc");
			}
			strSql.Append(")AS Row, T.*  from Teacher_Subject_Class_Group T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Teacher_Subject_Class_Group";
			parameters[1].Value = "TSCG_SN";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

