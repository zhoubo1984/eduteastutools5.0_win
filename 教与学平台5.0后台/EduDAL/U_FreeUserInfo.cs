﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
	/// <summary>
	/// 数据访问类:U_FreeUserInfo
	/// </summary>
	public partial class U_FreeUserInfo
	{
		public U_FreeUserInfo()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("FI_SN", "U_FreeUserInfo"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int FI_SN)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from U_FreeUserInfo");
			strSql.Append(" where FI_SN=@FI_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@FI_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = FI_SN;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EduModel.U_FreeUserInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into U_FreeUserInfo(");
			strSql.Append("FI_GUID,FI_QQ,FI_Mail,FI_Config)");
			strSql.Append(" values (");
			strSql.Append("@FI_GUID,@FI_QQ,@FI_Mail,@FI_Config)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@FI_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@FI_QQ", SqlDbType.NVarChar,50),
					new SqlParameter("@FI_Mail", SqlDbType.NVarChar,50),
					new SqlParameter("@FI_Config", SqlDbType.NVarChar,-1)};
			parameters[0].Value = model.FI_GUID;
			parameters[1].Value = model.FI_QQ;
			parameters[2].Value = model.FI_Mail;
			parameters[3].Value = model.FI_Config;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.U_FreeUserInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update U_FreeUserInfo set ");
			strSql.Append("FI_GUID=@FI_GUID,");
			strSql.Append("FI_QQ=@FI_QQ,");
			strSql.Append("FI_Mail=@FI_Mail,");
			strSql.Append("FI_Config=@FI_Config");
			strSql.Append(" where FI_SN=@FI_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@FI_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@FI_QQ", SqlDbType.NVarChar,50),
					new SqlParameter("@FI_Mail", SqlDbType.NVarChar,50),
					new SqlParameter("@FI_Config", SqlDbType.NVarChar,-1),
					new SqlParameter("@FI_SN", SqlDbType.Int,4)};
			parameters[0].Value = model.FI_GUID;
			parameters[1].Value = model.FI_QQ;
			parameters[2].Value = model.FI_Mail;
			parameters[3].Value = model.FI_Config;
			parameters[4].Value = model.FI_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int FI_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from U_FreeUserInfo ");
			strSql.Append(" where FI_SN=@FI_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@FI_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = FI_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string FI_SNlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from U_FreeUserInfo ");
			strSql.Append(" where FI_SN in ("+FI_SNlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.U_FreeUserInfo GetModel(int FI_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 FI_SN,FI_GUID,FI_QQ,FI_Mail,FI_Config from U_FreeUserInfo ");
			strSql.Append(" where FI_SN=@FI_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@FI_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = FI_SN;

			EduModel.U_FreeUserInfo model=new EduModel.U_FreeUserInfo();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.U_FreeUserInfo DataRowToModel(DataRow row)
		{
			EduModel.U_FreeUserInfo model=new EduModel.U_FreeUserInfo();
			if (row != null)
			{
				if(row["FI_SN"]!=null && row["FI_SN"].ToString()!="")
				{
					model.FI_SN=int.Parse(row["FI_SN"].ToString());
				}
				if(row["FI_GUID"]!=null && row["FI_GUID"].ToString()!="")
				{
					model.FI_GUID= new Guid(row["FI_GUID"].ToString());
				}
				if(row["FI_QQ"]!=null)
				{
					model.FI_QQ=row["FI_QQ"].ToString();
				}
				if(row["FI_Mail"]!=null)
				{
					model.FI_Mail=row["FI_Mail"].ToString();
				}
					//model.FI_Config=row["FI_Config"].ToString();
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select FI_SN,FI_GUID,FI_QQ,FI_Mail,FI_Config ");
			strSql.Append(" FROM U_FreeUserInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" FI_SN,FI_GUID,FI_QQ,FI_Mail,FI_Config ");
			strSql.Append(" FROM U_FreeUserInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM U_FreeUserInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.FI_SN desc");
			}
			strSql.Append(")AS Row, T.*  from U_FreeUserInfo T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "U_FreeUserInfo";
			parameters[1].Value = "FI_SN";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

