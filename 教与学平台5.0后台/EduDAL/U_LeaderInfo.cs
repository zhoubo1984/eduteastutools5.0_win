﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
	/// <summary>
	/// 数据访问类:U_LeaderInfo
	/// </summary>
	public partial class U_LeaderInfo
	{
		public U_LeaderInfo()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("LI_SN", "U_LeaderInfo"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int LI_SN)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from U_LeaderInfo");
			strSql.Append(" where LI_SN=@LI_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@LI_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = LI_SN;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EduModel.U_LeaderInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into U_LeaderInfo(");
			strSql.Append("LI_GUID,LI_QQ,LI_Email,LI_MobilePhone)");
			strSql.Append(" values (");
			strSql.Append("@LI_GUID,@LI_QQ,@LI_Email,@LI_MobilePhone)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@LI_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@LI_QQ", SqlDbType.NVarChar,50),
					new SqlParameter("@LI_Email", SqlDbType.NVarChar,200),
					new SqlParameter("@LI_MobilePhone", SqlDbType.NVarChar,50)};
			parameters[0].Value = model.LI_GUID;
			parameters[1].Value = model.LI_QQ;
			parameters[2].Value = model.LI_Email;
			parameters[3].Value = model.LI_MobilePhone;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.U_LeaderInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update U_LeaderInfo set ");
			strSql.Append("LI_GUID=@LI_GUID,");
			strSql.Append("LI_QQ=@LI_QQ,");
			strSql.Append("LI_Email=@LI_Email,");
			strSql.Append("LI_MobilePhone=@LI_MobilePhone");
			strSql.Append(" where LI_SN=@LI_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@LI_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@LI_QQ", SqlDbType.NVarChar,50),
					new SqlParameter("@LI_Email", SqlDbType.NVarChar,200),
					new SqlParameter("@LI_MobilePhone", SqlDbType.NVarChar,50),
					new SqlParameter("@LI_SN", SqlDbType.Int,4)};
			parameters[0].Value = model.LI_GUID;
			parameters[1].Value = model.LI_QQ;
			parameters[2].Value = model.LI_Email;
			parameters[3].Value = model.LI_MobilePhone;
			parameters[4].Value = model.LI_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int LI_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from U_LeaderInfo ");
			strSql.Append(" where LI_SN=@LI_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@LI_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = LI_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string LI_SNlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from U_LeaderInfo ");
			strSql.Append(" where LI_SN in ("+LI_SNlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.U_LeaderInfo GetModel(int LI_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 LI_SN,LI_GUID,LI_QQ,LI_Email,LI_MobilePhone from U_LeaderInfo ");
			strSql.Append(" where LI_SN=@LI_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@LI_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = LI_SN;

			EduModel.U_LeaderInfo model=new EduModel.U_LeaderInfo();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.U_LeaderInfo DataRowToModel(DataRow row)
		{
			EduModel.U_LeaderInfo model=new EduModel.U_LeaderInfo();
			if (row != null)
			{
				if(row["LI_SN"]!=null && row["LI_SN"].ToString()!="")
				{
					model.LI_SN=int.Parse(row["LI_SN"].ToString());
				}
				if(row["LI_GUID"]!=null && row["LI_GUID"].ToString()!="")
				{
					model.LI_GUID= new Guid(row["LI_GUID"].ToString());
				}
				if(row["LI_QQ"]!=null)
				{
					model.LI_QQ=row["LI_QQ"].ToString();
				}
				if(row["LI_Email"]!=null)
				{
					model.LI_Email=row["LI_Email"].ToString();
				}
				if(row["LI_MobilePhone"]!=null)
				{
					model.LI_MobilePhone=row["LI_MobilePhone"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select LI_SN,LI_GUID,LI_QQ,LI_Email,LI_MobilePhone ");
			strSql.Append(" FROM U_LeaderInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" LI_SN,LI_GUID,LI_QQ,LI_Email,LI_MobilePhone ");
			strSql.Append(" FROM U_LeaderInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM U_LeaderInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.LI_SN desc");
			}
			strSql.Append(")AS Row, T.*  from U_LeaderInfo T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "U_LeaderInfo";
			parameters[1].Value = "LI_SN";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

