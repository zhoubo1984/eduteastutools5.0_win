﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
    /// <summary>
    /// 数据访问类:U_ParentInfo
    /// </summary>
    public partial class U_ParentInfo
    {
        public U_ParentInfo()
        { }
        #region  BasicMethod

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("PI_SN", "U_ParentInfo");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int PI_SN)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from U_ParentInfo");
            strSql.Append(" where PI_SN=@PI_SN");
            SqlParameter[] parameters = {
					new SqlParameter("@PI_SN", SqlDbType.Int,4)
			};
            parameters[0].Value = PI_SN;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EduModel.U_ParentInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into U_ParentInfo(");
            strSql.Append("PI_GUID,PI_QQ,PI_Mail,PI_Config,PI_MobilePhone)");
            strSql.Append(" values (");
            strSql.Append("@PI_GUID,@PI_QQ,@PI_Mail,@PI_Config,@PI_MobilePhone)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@PI_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@PI_QQ", SqlDbType.NVarChar,50),
					new SqlParameter("@PI_Mail", SqlDbType.NVarChar,50),
					new SqlParameter("@PI_Config", SqlDbType.NVarChar,-1),
                    new SqlParameter("@PI_MobilePhone", SqlDbType.NVarChar,50)};
            parameters[0].Value = model.PI_GUID;
            parameters[1].Value = model.PI_QQ;
            parameters[2].Value = model.PI_Mail;
            parameters[3].Value = model.PI_Config;
            parameters[4].Value = model.PI_MobilePhone;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EduModel.U_ParentInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update U_ParentInfo set ");
            strSql.Append("PI_GUID=@PI_GUID,");
            strSql.Append("PI_QQ=@PI_QQ,");
            strSql.Append("PI_Mail=@PI_Mail,");
            strSql.Append("PI_Config=@PI_Config,");
            strSql.Append("PI_MobilePhone=@PI_MobilePhone");
            strSql.Append(" where PI_SN=@PI_SN");
            SqlParameter[] parameters = {
					new SqlParameter("@PI_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@PI_QQ", SqlDbType.NVarChar,50),
					new SqlParameter("@PI_Mail", SqlDbType.NVarChar,50),
					new SqlParameter("@PI_Config", SqlDbType.NVarChar,-1),
					new SqlParameter("@PI_SN", SqlDbType.Int,4),
                    new SqlParameter("@PI_MobilePhone", SqlDbType.NVarChar,50)};
            parameters[0].Value = model.PI_GUID;
            parameters[1].Value = model.PI_QQ;
            parameters[2].Value = model.PI_Mail;
            parameters[3].Value = model.PI_Config;
            parameters[4].Value = model.PI_SN;
            parameters[5].Value = model.PI_MobilePhone;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int PI_SN)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from U_ParentInfo ");
            strSql.Append(" where PI_SN=@PI_SN");
            SqlParameter[] parameters = {
					new SqlParameter("@PI_SN", SqlDbType.Int,4)
			};
            parameters[0].Value = PI_SN;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string PI_SNlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from U_ParentInfo ");
            strSql.Append(" where PI_SN in (" + PI_SNlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.U_ParentInfo GetModel(int PI_SN)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 PI_SN,PI_GUID,PI_QQ,PI_Mail,PI_Config,PI_MobilePhone from U_ParentInfo ");
            strSql.Append(" where PI_SN=@PI_SN");
            SqlParameter[] parameters = {
					new SqlParameter("@PI_SN", SqlDbType.Int,4)
			};
            parameters[0].Value = PI_SN;

            EduModel.U_ParentInfo model = new EduModel.U_ParentInfo();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.U_ParentInfo DataRowToModel(DataRow row)
        {
            EduModel.U_ParentInfo model = new EduModel.U_ParentInfo();
            if (row != null)
            {
                if (row["PI_SN"] != null && row["PI_SN"].ToString() != "")
                {
                    model.PI_SN = int.Parse(row["PI_SN"].ToString());
                }
                if (row["PI_GUID"] != null && row["PI_GUID"].ToString() != "")
                {
                    model.PI_GUID = new Guid(row["PI_GUID"].ToString());
                }
                if (row["PI_QQ"] != null)
                {
                    model.PI_QQ = row["PI_QQ"].ToString();
                }
                if (row["PI_Mail"] != null)
                {
                    model.PI_Mail = row["PI_Mail"].ToString();
                }
                if (row["PI_MobilePhone"] != null)
                {
                    model.PI_MobilePhone = row["PI_MobilePhone"].ToString();
                }

                //model.PI_Config=row["PI_Config"].ToString();
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select PI_SN,PI_GUID,PI_QQ,PI_Mail,PI_Config,PI_MobilePhone ");
            strSql.Append(" FROM U_ParentInfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" PI_SN,PI_GUID,PI_QQ,PI_Mail,PI_Config,PI_MobilePhone ");
            strSql.Append(" FROM U_ParentInfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM U_ParentInfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.PI_SN desc");
            }
            strSql.Append(")AS Row, T.*  from U_ParentInfo T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "U_ParentInfo";
            parameters[1].Value = "PI_SN";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

