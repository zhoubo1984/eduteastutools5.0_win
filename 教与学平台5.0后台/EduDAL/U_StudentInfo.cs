﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
    /// <summary>
    /// 数据访问类:U_StudentInfo
    /// </summary>
    public partial class U_StudentInfo
    {
        public U_StudentInfo()
        { }
        #region  BasicMethod

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("SI_SN", "U_StudentInfo");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int SI_SN)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from U_StudentInfo");
            strSql.Append(" where SI_SN=@SI_SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SI_SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SI_SN;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EduModel.U_StudentInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into U_StudentInfo(");
            strSql.Append("SI_GUID,SI_ParentGUID,SI_IsAllowOffLine,SI_QQ,SI_Mail,SI_Config)");
            strSql.Append(" values (");
            strSql.Append("@SI_GUID,@SI_ParentGUID,@SI_IsAllowOffLine,@SI_QQ,@SI_Mail,@SI_Config)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@SI_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@SI_ParentGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@SI_IsAllowOffLine", SqlDbType.Bit,1),
					new SqlParameter("@SI_QQ", SqlDbType.NVarChar,50),
					new SqlParameter("@SI_Mail", SqlDbType.NVarChar,50),
					new SqlParameter("@SI_Config", SqlDbType.NVarChar,-1)};
            parameters[0].Value = model.SI_GUID;
            parameters[1].Value = model.SI_ParentGUID;
            parameters[2].Value = model.SI_IsAllowOffLine;
            parameters[3].Value = model.SI_QQ;
            parameters[4].Value = model.SI_Mail;
            parameters[5].Value = model.SI_Config;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EduModel.U_StudentInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update U_StudentInfo set ");
            strSql.Append("SI_GUID=@SI_GUID,");
            strSql.Append("SI_ParentGUID=@SI_ParentGUID,");
            strSql.Append("SI_IsAllowOffLine=@SI_IsAllowOffLine,");
            strSql.Append("SI_QQ=@SI_QQ,");
            strSql.Append("SI_Mail=@SI_Mail,");
            strSql.Append("SI_Config=@SI_Config");
            strSql.Append(" where SI_SN=@SI_SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SI_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@SI_ParentGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@SI_IsAllowOffLine", SqlDbType.Bit,1),
					new SqlParameter("@SI_QQ", SqlDbType.NVarChar,50),
					new SqlParameter("@SI_Mail", SqlDbType.NVarChar,50),
					new SqlParameter("@SI_Config", SqlDbType.NVarChar,-1),
					new SqlParameter("@SI_SN", SqlDbType.Int,4)};
            parameters[0].Value = model.SI_GUID;
            parameters[1].Value = model.SI_ParentGUID;
            parameters[2].Value = model.SI_IsAllowOffLine;
            parameters[3].Value = model.SI_QQ;
            parameters[4].Value = model.SI_Mail;
            parameters[5].Value = model.SI_Config;
            parameters[6].Value = model.SI_SN;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SI_SN)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from U_StudentInfo ");
            strSql.Append(" where SI_SN=@SI_SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SI_SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SI_SN;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string SI_SNlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from U_StudentInfo ");
            strSql.Append(" where SI_SN in (" + SI_SNlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.U_StudentInfo GetModel(int SI_SN)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 SI_SN,SI_GUID,SI_ParentGUID,SI_IsAllowOffLine,SI_QQ,SI_Mail,SI_Config from U_StudentInfo ");
            strSql.Append(" where SI_SN=@SI_SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SI_SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SI_SN;

            EduModel.U_StudentInfo model = new EduModel.U_StudentInfo();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.U_StudentInfo GetModel(Guid guid)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 * from U_StudentInfo ");
            strSql.Append(" where SI_GUID=@SI_GUID");
            SqlParameter[] parameters = {
					new SqlParameter("@SI_GUID", SqlDbType.UniqueIdentifier,16)
			};
            parameters[0].Value = guid;

            EduModel.U_StudentInfo model = new EduModel.U_StudentInfo();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.U_StudentInfo DataRowToModel(DataRow row)
        {
            EduModel.U_StudentInfo model = new EduModel.U_StudentInfo();
            if (row != null)
            {
                if (row["SI_SN"] != null && row["SI_SN"].ToString() != "")
                {
                    model.SI_SN = int.Parse(row["SI_SN"].ToString());
                }
                if (row["SI_GUID"] != null && row["SI_GUID"].ToString() != "")
                {
                    model.SI_GUID = new Guid(row["SI_GUID"].ToString());
                }
                if (row["SI_ParentGUID"] != null && row["SI_ParentGUID"].ToString() != "")
                {
                    model.SI_ParentGUID = new Guid(row["SI_ParentGUID"].ToString());
                }
                if (row["SI_IsAllowOffLine"] != null && row["SI_IsAllowOffLine"].ToString() != "")
                {
                    if ((row["SI_IsAllowOffLine"].ToString() == "1") || (row["SI_IsAllowOffLine"].ToString().ToLower() == "true"))
                    {
                        model.SI_IsAllowOffLine = true;
                    }
                    else
                    {
                        model.SI_IsAllowOffLine = false;
                    }
                }
                if (row["SI_QQ"] != null)
                {
                    model.SI_QQ = row["SI_QQ"].ToString();
                }
                if (row["SI_Mail"] != null)
                {
                    model.SI_Mail = row["SI_Mail"].ToString();
                }
                //model.SI_Config=row["SI_Config"].ToString();
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select SI_SN,SI_GUID,SI_ParentGUID,SI_IsAllowOffLine,SI_QQ,SI_Mail,SI_Config ");
            strSql.Append(" FROM U_StudentInfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" SI_SN,SI_GUID,SI_ParentGUID,SI_IsAllowOffLine,SI_QQ,SI_Mail,SI_Config ");
            strSql.Append(" FROM U_StudentInfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM U_StudentInfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.SI_SN desc");
            }
            strSql.Append(")AS Row, T.*  from U_StudentInfo T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "U_StudentInfo";
            parameters[1].Value = "SI_SN";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

