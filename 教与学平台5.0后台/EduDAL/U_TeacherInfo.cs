﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
	/// <summary>
	/// 数据访问类:U_TeacherInfo
	/// </summary>
	public partial class U_TeacherInfo
	{
		public U_TeacherInfo()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(Guid TI_GUID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from U_TeacherInfo");
			strSql.Append(" where TI_GUID=@TI_GUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@TI_GUID", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = TI_GUID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EduModel.U_TeacherInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into U_TeacherInfo(");
			strSql.Append("TI_GUID,TI_QQ,TI_Mail,TI_MobilePhone,TI_Config)");
			strSql.Append(" values (");
			strSql.Append("@TI_GUID,@TI_QQ,@TI_Mail,@TI_MobilePhone,@TI_Config)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@TI_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@TI_QQ", SqlDbType.NVarChar,50),
					new SqlParameter("@TI_Mail", SqlDbType.NVarChar,50),
					new SqlParameter("@TI_MobilePhone", SqlDbType.NVarChar,50),
					new SqlParameter("@TI_Config", SqlDbType.NVarChar,-1)};
			parameters[0].Value = model.TI_GUID;
			parameters[1].Value = model.TI_QQ;
			parameters[2].Value = model.TI_Mail;
			parameters[3].Value = model.TI_MobilePhone;
			parameters[4].Value = model.TI_Config;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.U_TeacherInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update U_TeacherInfo set ");
			strSql.Append("TI_QQ=@TI_QQ,");
			strSql.Append("TI_Mail=@TI_Mail,");
			strSql.Append("TI_MobilePhone=@TI_MobilePhone,");
			strSql.Append("TI_Config=@TI_Config");
			strSql.Append(" where TI_SN=@TI_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@TI_QQ", SqlDbType.NVarChar,50),
					new SqlParameter("@TI_Mail", SqlDbType.NVarChar,50),
					new SqlParameter("@TI_MobilePhone", SqlDbType.NVarChar,50),
					new SqlParameter("@TI_Config", SqlDbType.NVarChar,-1),
					new SqlParameter("@TI_SN", SqlDbType.Int,4),
					new SqlParameter("@TI_GUID", SqlDbType.UniqueIdentifier,16)};
			parameters[0].Value = model.TI_QQ;
			parameters[1].Value = model.TI_Mail;
			parameters[2].Value = model.TI_MobilePhone;
			parameters[3].Value = model.TI_Config;
			parameters[4].Value = model.TI_SN;
			parameters[5].Value = model.TI_GUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int TI_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from U_TeacherInfo ");
			strSql.Append(" where TI_SN=@TI_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@TI_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = TI_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(Guid TI_GUID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from U_TeacherInfo ");
			strSql.Append(" where TI_GUID=@TI_GUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@TI_GUID", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = TI_GUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string TI_SNlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from U_TeacherInfo ");
			strSql.Append(" where TI_SN in ("+TI_SNlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.U_TeacherInfo GetModel(Guid guid)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 * from U_TeacherInfo ");
            strSql.Append(" where TI_GUID=@TI_GUID");
            SqlParameter[] parameters = {
					new SqlParameter("@TI_GUID", SqlDbType.UniqueIdentifier,16)
			};
            parameters[0].Value = guid;

            EduModel.U_TeacherInfo model = new EduModel.U_TeacherInfo();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.U_TeacherInfo GetModel(int TI_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 TI_SN,TI_GUID,TI_QQ,TI_Mail,TI_MobilePhone,TI_Config from U_TeacherInfo ");
			strSql.Append(" where TI_SN=@TI_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@TI_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = TI_SN;

			EduModel.U_TeacherInfo model=new EduModel.U_TeacherInfo();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.U_TeacherInfo DataRowToModel(DataRow row)
		{
			EduModel.U_TeacherInfo model=new EduModel.U_TeacherInfo();
			if (row != null)
			{
				if(row["TI_SN"]!=null && row["TI_SN"].ToString()!="")
				{
					model.TI_SN=int.Parse(row["TI_SN"].ToString());
				}
				if(row["TI_GUID"]!=null && row["TI_GUID"].ToString()!="")
				{
					model.TI_GUID= new Guid(row["TI_GUID"].ToString());
				}
				if(row["TI_QQ"]!=null)
				{
					model.TI_QQ=row["TI_QQ"].ToString();
				}
				if(row["TI_Mail"]!=null)
				{
					model.TI_Mail=row["TI_Mail"].ToString();
				}
				if(row["TI_MobilePhone"]!=null)
				{
					model.TI_MobilePhone=row["TI_MobilePhone"].ToString();
				}
					//model.TI_Config=row["TI_Config"].ToString();
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select TI_SN,TI_GUID,TI_QQ,TI_Mail,TI_MobilePhone,TI_Config ");
			strSql.Append(" FROM U_TeacherInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" TI_SN,TI_GUID,TI_QQ,TI_Mail,TI_MobilePhone,TI_Config ");
			strSql.Append(" FROM U_TeacherInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM U_TeacherInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.TI_SN desc");
			}
			strSql.Append(")AS Row, T.*  from U_TeacherInfo T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "U_TeacherInfo";
			parameters[1].Value = "TI_SN";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

