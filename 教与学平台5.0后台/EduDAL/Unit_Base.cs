﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
	/// <summary>
	/// 数据访问类:Unit_Base
	/// </summary>
	public partial class Unit_Base
	{
		public Unit_Base()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(Guid UB_GUID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Unit_Base");
			strSql.Append(" where UB_GUID=@UB_GUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@UB_GUID", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = UB_GUID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public int getMaxIndex(Guid UB_GUID)
        {
            int maxIndex = 0;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select Max(UB_Index) from Unit_Base");
            strSql.Append(" where UB_PGUID=@UB_GUID ");
            SqlParameter[] parameters = {
					new SqlParameter("@UB_GUID", SqlDbType.UniqueIdentifier,16)			};
            parameters[0].Value = UB_GUID;
            DataTable dt = DbHelperSQL.Query(strSql.ToString(), parameters).Tables[0];
            if (dt.Rows.Count > 0 && dt.Rows[0][0].ToString() != "")
            {
                maxIndex = int.Parse(dt.Rows[0][0].ToString());
            }

            return maxIndex;
        }

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EduModel.Unit_Base model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Unit_Base(");
			strSql.Append("UB_GUID,UB_PGUID,UB_Name,UB_Path,UB_Index,UB_DBIndex,UB_IsCheck,UB_Type,GradeState,State)");
			strSql.Append(" values (");
			strSql.Append("@UB_GUID,@UB_PGUID,@UB_Name,@UB_Path,@UB_Index,@UB_DBIndex,@UB_IsCheck,@UB_Type,@GradeState,@State)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@UB_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@UB_PGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@UB_Name", SqlDbType.NVarChar,-1),
					new SqlParameter("@UB_Path", SqlDbType.NVarChar,-1),
					new SqlParameter("@UB_Index", SqlDbType.Int,4),
					new SqlParameter("@UB_DBIndex", SqlDbType.NVarChar,-1),
					new SqlParameter("@UB_IsCheck", SqlDbType.Bit,1),
					new SqlParameter("@UB_Type", SqlDbType.Int,4),
					new SqlParameter("@GradeState", SqlDbType.Int,4),
					new SqlParameter("@State", SqlDbType.Int,4)};
			parameters[0].Value = model.UB_GUID;
			parameters[1].Value = model.UB_PGUID;
			parameters[2].Value = model.UB_Name;
			parameters[3].Value = model.UB_Path;
			parameters[4].Value = model.UB_Index;
			parameters[5].Value = model.UB_DBIndex;
			parameters[6].Value = model.UB_IsCheck;
			parameters[7].Value = model.UB_Type;
			parameters[8].Value = model.GradeState;
			parameters[9].Value = model.State;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.Unit_Base model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Unit_Base set ");
			strSql.Append("UB_PGUID=@UB_PGUID,");
			strSql.Append("UB_Name=@UB_Name,");
			strSql.Append("UB_Path=@UB_Path,");
			strSql.Append("UB_Index=@UB_Index,");
			strSql.Append("UB_DBIndex=@UB_DBIndex,");
			strSql.Append("UB_IsCheck=@UB_IsCheck,");
			strSql.Append("UB_Type=@UB_Type,");
			strSql.Append("GradeState=@GradeState,");
			strSql.Append("State=@State");
			strSql.Append(" where UB_SN=@UB_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@UB_PGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@UB_Name", SqlDbType.NVarChar,-1),
					new SqlParameter("@UB_Path", SqlDbType.NVarChar,-1),
					new SqlParameter("@UB_Index", SqlDbType.Int,4),
					new SqlParameter("@UB_DBIndex", SqlDbType.NVarChar,-1),
					new SqlParameter("@UB_IsCheck", SqlDbType.Bit,1),
					new SqlParameter("@UB_Type", SqlDbType.Int,4),
					new SqlParameter("@GradeState", SqlDbType.Int,4),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@UB_SN", SqlDbType.Int,4),
					new SqlParameter("@UB_GUID", SqlDbType.UniqueIdentifier,16)};
			parameters[0].Value = model.UB_PGUID;
			parameters[1].Value = model.UB_Name;
			parameters[2].Value = model.UB_Path;
			parameters[3].Value = model.UB_Index;
			parameters[4].Value = model.UB_DBIndex;
			parameters[5].Value = model.UB_IsCheck;
			parameters[6].Value = model.UB_Type;
			parameters[7].Value = model.GradeState;
			parameters[8].Value = model.State;
			parameters[9].Value = model.UB_SN;
			parameters[10].Value = model.UB_GUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int UB_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Unit_Base ");
			strSql.Append(" where UB_SN=@UB_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@UB_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = UB_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(Guid UB_GUID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Unit_Base ");
			strSql.Append(" where UB_GUID=@UB_GUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@UB_GUID", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = UB_GUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string UB_SNlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Unit_Base ");
			strSql.Append(" where UB_SN in ("+UB_SNlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.Unit_Base GetModel(Guid UB_GUID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 * from Unit_Base ");
            strSql.Append(" where UB_GUID=@UB_GUID");
            SqlParameter[] parameters = {
					new SqlParameter("@UB_GUID", SqlDbType.UniqueIdentifier,16)
			};
            parameters[0].Value = UB_GUID;

            EduModel.Unit_Base model = new EduModel.Unit_Base();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Unit_Base GetModel(int UB_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 UB_SN,UB_GUID,UB_PGUID,UB_Name,UB_Path,UB_Index,UB_DBIndex,UB_IsCheck,UB_Type,GradeState,State from Unit_Base ");
			strSql.Append(" where UB_SN=@UB_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@UB_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = UB_SN;

			EduModel.Unit_Base model=new EduModel.Unit_Base();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Unit_Base DataRowToModel(DataRow row)
		{
			EduModel.Unit_Base model=new EduModel.Unit_Base();
			if (row != null)
			{
				if(row["UB_SN"]!=null && row["UB_SN"].ToString()!="")
				{
					model.UB_SN=int.Parse(row["UB_SN"].ToString());
				}
				if(row["UB_GUID"]!=null && row["UB_GUID"].ToString()!="")
				{
					model.UB_GUID= new Guid(row["UB_GUID"].ToString());
				}
				if(row["UB_PGUID"]!=null && row["UB_PGUID"].ToString()!="")
				{
					model.UB_PGUID= new Guid(row["UB_PGUID"].ToString());
				}
				if(row["UB_Name"]!=null)
				{
					model.UB_Name=row["UB_Name"].ToString();
				}
				if(row["UB_Path"]!=null)
				{
					model.UB_Path=row["UB_Path"].ToString();
				}
				if(row["UB_Index"]!=null && row["UB_Index"].ToString()!="")
				{
					model.UB_Index=int.Parse(row["UB_Index"].ToString());
				}
				if(row["UB_DBIndex"]!=null)
				{
					model.UB_DBIndex=row["UB_DBIndex"].ToString();
				}
				if(row["UB_IsCheck"]!=null && row["UB_IsCheck"].ToString()!="")
				{
					if((row["UB_IsCheck"].ToString()=="1")||(row["UB_IsCheck"].ToString().ToLower()=="true"))
					{
						model.UB_IsCheck=true;
					}
					else
					{
						model.UB_IsCheck=false;
					}
				}
				if(row["UB_Type"]!=null && row["UB_Type"].ToString()!="")
				{
					model.UB_Type=int.Parse(row["UB_Type"].ToString());
				}
				if(row["GradeState"]!=null && row["GradeState"].ToString()!="")
				{
					model.GradeState=int.Parse(row["GradeState"].ToString());
				}
				if(row["State"]!=null && row["State"].ToString()!="")
				{
					model.State=int.Parse(row["State"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select UB_SN,UB_GUID,UB_PGUID,UB_Name,UB_Path,UB_Index,UB_DBIndex,UB_IsCheck,UB_Type,GradeState,State ");
			strSql.Append(" FROM Unit_Base ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" UB_SN,UB_GUID,UB_PGUID,UB_Name,UB_Path,UB_Index,UB_DBIndex,UB_IsCheck,UB_Type,GradeState,State ");
			strSql.Append(" FROM Unit_Base ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Unit_Base ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.UB_SN desc");
			}
			strSql.Append(")AS Row, T.*  from Unit_Base T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Unit_Base";
			parameters[1].Value = "UB_SN";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

