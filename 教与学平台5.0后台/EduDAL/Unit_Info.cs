﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
	/// <summary>
	/// 数据访问类:Unit_Info
	/// </summary>
	public partial class Unit_Info
	{
		public Unit_Info()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(Guid UI_GUID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Unit_Info");
			strSql.Append(" where UI_GUID=@UI_GUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@UI_GUID", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = UI_GUID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EduModel.Unit_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Unit_Info(");
			strSql.Append("UI_GUID)");
			strSql.Append(" values (");
			strSql.Append("@UI_GUID)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@UI_GUID", SqlDbType.UniqueIdentifier,16)};
			parameters[0].Value = model.UI_GUID;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.Unit_Info model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Unit_Info set ");
#warning 系统发现缺少更新的字段，请手工确认如此更新是否正确！ 
			strSql.Append("UI_SN=@UI_SN,");
			strSql.Append("UI_GUID=@UI_GUID");
			strSql.Append(" where UI_SN=@UI_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@UI_SN", SqlDbType.Int,4),
					new SqlParameter("@UI_GUID", SqlDbType.UniqueIdentifier,16)};
			parameters[0].Value = model.UI_SN;
			parameters[1].Value = model.UI_GUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int UI_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Unit_Info ");
			strSql.Append(" where UI_SN=@UI_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@UI_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = UI_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(Guid UI_GUID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Unit_Info ");
			strSql.Append(" where UI_GUID=@UI_GUID ");
			SqlParameter[] parameters = {
					new SqlParameter("@UI_GUID", SqlDbType.UniqueIdentifier,16)			};
			parameters[0].Value = UI_GUID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string UI_SNlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Unit_Info ");
			strSql.Append(" where UI_SN in ("+UI_SNlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Unit_Info GetModel(int UI_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 UI_SN,UI_GUID from Unit_Info ");
			strSql.Append(" where UI_SN=@UI_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@UI_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = UI_SN;

			EduModel.Unit_Info model=new EduModel.Unit_Info();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Unit_Info DataRowToModel(DataRow row)
		{
			EduModel.Unit_Info model=new EduModel.Unit_Info();
			if (row != null)
			{
				if(row["UI_SN"]!=null && row["UI_SN"].ToString()!="")
				{
					model.UI_SN=int.Parse(row["UI_SN"].ToString());
				}
				if(row["UI_GUID"]!=null && row["UI_GUID"].ToString()!="")
				{
					model.UI_GUID= new Guid(row["UI_GUID"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select UI_SN,UI_GUID ");
			strSql.Append(" FROM Unit_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" UI_SN,UI_GUID ");
			strSql.Append(" FROM Unit_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Unit_Info ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.UI_SN desc");
			}
			strSql.Append(")AS Row, T.*  from Unit_Info T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Unit_Info";
			parameters[1].Value = "UI_SN";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

