﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
	/// <summary>
	/// 数据访问类:Unit_ServiceAddress
	/// </summary>
	public partial class Unit_ServiceAddress
	{
		public Unit_ServiceAddress()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("US_SN", "Unit_ServiceAddress"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int US_SN)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Unit_ServiceAddress");
			strSql.Append(" where US_SN=@US_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@US_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = US_SN;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EduModel.Unit_ServiceAddress model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Unit_ServiceAddress(");
			strSql.Append("US_UnitGUID,US_NetworkType,US_ServiceAddress)");
			strSql.Append(" values (");
			strSql.Append("@US_UnitGUID,@US_NetworkType,@US_ServiceAddress)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@US_UnitGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@US_NetworkType", SqlDbType.Int,4),
					new SqlParameter("@US_ServiceAddress", SqlDbType.NVarChar,-1)};
			parameters[0].Value = model.US_UnitGUID;
			parameters[1].Value = model.US_NetworkType;
			parameters[2].Value = model.US_ServiceAddress;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EduModel.Unit_ServiceAddress model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Unit_ServiceAddress set ");
			strSql.Append("US_UnitGUID=@US_UnitGUID,");
			strSql.Append("US_NetworkType=@US_NetworkType,");
			strSql.Append("US_ServiceAddress=@US_ServiceAddress");
			strSql.Append(" where US_SN=@US_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@US_UnitGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@US_NetworkType", SqlDbType.Int,4),
					new SqlParameter("@US_ServiceAddress", SqlDbType.NVarChar,-1),
					new SqlParameter("@US_SN", SqlDbType.Int,4)};
			parameters[0].Value = model.US_UnitGUID;
			parameters[1].Value = model.US_NetworkType;
			parameters[2].Value = model.US_ServiceAddress;
			parameters[3].Value = model.US_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int US_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Unit_ServiceAddress ");
			strSql.Append(" where US_SN=@US_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@US_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = US_SN;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string US_SNlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Unit_ServiceAddress ");
			strSql.Append(" where US_SN in ("+US_SNlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Unit_ServiceAddress GetModel(int US_SN)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 US_SN,US_UnitGUID,US_NetworkType,US_ServiceAddress from Unit_ServiceAddress ");
			strSql.Append(" where US_SN=@US_SN");
			SqlParameter[] parameters = {
					new SqlParameter("@US_SN", SqlDbType.Int,4)
			};
			parameters[0].Value = US_SN;

			EduModel.Unit_ServiceAddress model=new EduModel.Unit_ServiceAddress();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EduModel.Unit_ServiceAddress DataRowToModel(DataRow row)
		{
			EduModel.Unit_ServiceAddress model=new EduModel.Unit_ServiceAddress();
			if (row != null)
			{
				if(row["US_SN"]!=null && row["US_SN"].ToString()!="")
				{
					model.US_SN=int.Parse(row["US_SN"].ToString());
				}
				if(row["US_UnitGUID"]!=null && row["US_UnitGUID"].ToString()!="")
				{
					model.US_UnitGUID= new Guid(row["US_UnitGUID"].ToString());
				}
				if(row["US_NetworkType"]!=null && row["US_NetworkType"].ToString()!="")
				{
					model.US_NetworkType=int.Parse(row["US_NetworkType"].ToString());
				}
				if(row["US_ServiceAddress"]!=null)
				{
					model.US_ServiceAddress=row["US_ServiceAddress"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select US_SN,US_UnitGUID,US_NetworkType,US_ServiceAddress ");
			strSql.Append(" FROM Unit_ServiceAddress ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" US_SN,US_UnitGUID,US_NetworkType,US_ServiceAddress ");
			strSql.Append(" FROM Unit_ServiceAddress ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Unit_ServiceAddress ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.US_SN desc");
			}
			strSql.Append(")AS Row, T.*  from Unit_ServiceAddress T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Unit_ServiceAddress";
			parameters[1].Value = "US_SN";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

