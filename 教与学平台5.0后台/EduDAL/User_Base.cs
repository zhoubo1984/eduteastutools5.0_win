﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using DBUtility;//Please add references
namespace EduDAL
{
    /// <summary>
    /// 数据访问类:User_Base
    /// </summary>
    public partial class User_Base
    {
        public User_Base()
        { }
        #region  BasicMethod

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(Guid UB_GUID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from User_Base");
            strSql.Append(" where UB_GUID=@UB_GUID ");
            SqlParameter[] parameters = {
					new SqlParameter("@UB_GUID", SqlDbType.UniqueIdentifier,16)			};
            parameters[0].Value = UB_GUID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string uID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from User_Base");
            strSql.Append(" where UB_UserID=@UB_UserID and State>-1 ");
            SqlParameter[] parameters = {
					new SqlParameter("@UB_UserID", SqlDbType.NVarChar,-1)};
            parameters[0].Value = uID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EduModel.User_Base model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into User_Base(");
            strSql.Append("UB_GUID,UB_UserID,UB_UserPsw,UB_UserName,UB_Sex,UB_UserType,UB_RegDate,UB_LastLoginDate,UB_LoginTimes,UB_UnitGUID,GradeState,State)");
            strSql.Append(" values (");
            strSql.Append("@UB_GUID,@UB_UserID,@UB_UserPsw,@UB_UserName,@UB_Sex,@UB_UserType,@UB_RegDate,@UB_LastLoginDate,@UB_LoginTimes,@UB_UnitGUID,@GradeState,@State)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@UB_GUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@UB_UserID", SqlDbType.NVarChar,-1),
					new SqlParameter("@UB_UserPsw", SqlDbType.NVarChar,-1),
					new SqlParameter("@UB_UserName", SqlDbType.NVarChar,-1),
					new SqlParameter("@UB_Sex", SqlDbType.Bit,1),
					new SqlParameter("@UB_UserType", SqlDbType.Int,4),
					new SqlParameter("@UB_RegDate", SqlDbType.DateTime),
					new SqlParameter("@UB_LastLoginDate", SqlDbType.DateTime),
					new SqlParameter("@UB_LoginTimes", SqlDbType.Int,4),			
					new SqlParameter("@UB_UnitGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@GradeState", SqlDbType.Int,4),
					new SqlParameter("@State", SqlDbType.Int,4)};
            parameters[0].Value = model.UB_GUID;
            parameters[1].Value = model.UB_UserID;
            parameters[2].Value = model.UB_UserPsw;
            parameters[3].Value = model.UB_UserName;
            parameters[4].Value = model.UB_Sex;
            parameters[5].Value = model.UB_UserType;
            parameters[6].Value = model.UB_RegDate;
            parameters[7].Value = model.UB_LastLoginDate;
            parameters[8].Value = model.UB_LoginTimes;
            parameters[9].Value = model.UB_UnitGUID;
            parameters[10].Value = model.GradeState;
            parameters[11].Value = model.State;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EduModel.User_Base model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update User_Base set ");
            strSql.Append("UB_UserID=@UB_UserID,");
            strSql.Append("UB_UserPsw=@UB_UserPsw,");
            strSql.Append("UB_UserName=@UB_UserName,");
            strSql.Append("UB_Sex=@UB_Sex,");
            strSql.Append("UB_UserType=@UB_UserType,");
            strSql.Append("UB_RegDate=@UB_RegDate,");
            strSql.Append("UB_LastLoginDate=@UB_LastLoginDate,");
            strSql.Append("UB_LoginTimes=@UB_LoginTimes,");
            strSql.Append("UB_UnitGUID=@UB_UnitGUID,");
            strSql.Append("GradeState=@GradeState,");
            strSql.Append("State=@State");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@UB_UserID", SqlDbType.NVarChar,-1),
					new SqlParameter("@UB_UserPsw", SqlDbType.NVarChar,-1),
					new SqlParameter("@UB_UserName", SqlDbType.NVarChar,-1),
					new SqlParameter("@UB_Sex", SqlDbType.Bit,1),
					new SqlParameter("@UB_UserType", SqlDbType.Int,4),
					new SqlParameter("@UB_RegDate", SqlDbType.DateTime),
					new SqlParameter("@UB_LastLoginDate", SqlDbType.DateTime),
					new SqlParameter("@UB_LoginTimes", SqlDbType.Int,4),
					new SqlParameter("@UB_UnitGUID", SqlDbType.UniqueIdentifier,16),
					new SqlParameter("@GradeState", SqlDbType.Int,4),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@SN", SqlDbType.Int,4),
					new SqlParameter("@UB_GUID", SqlDbType.UniqueIdentifier,16)};
            parameters[0].Value = model.UB_UserID;
            parameters[1].Value = model.UB_UserPsw;
            parameters[2].Value = model.UB_UserName;
            parameters[3].Value = model.UB_Sex;
            parameters[4].Value = model.UB_UserType;
            parameters[5].Value = model.UB_RegDate;
            parameters[6].Value = model.UB_LastLoginDate;
            parameters[7].Value = model.UB_LoginTimes;
            parameters[8].Value = model.UB_UnitGUID;
            parameters[9].Value = model.GradeState;
            parameters[10].Value = model.State;
            parameters[11].Value = model.SN;
            parameters[12].Value = model.UB_GUID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SN)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from User_Base ");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SN;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(Guid UB_GUID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from User_Base ");
            strSql.Append(" where UB_GUID=@UB_GUID ");
            SqlParameter[] parameters = {
					new SqlParameter("@UB_GUID", SqlDbType.UniqueIdentifier,16)			};
            parameters[0].Value = UB_GUID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string SNlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from User_Base ");
            strSql.Append(" where SN in (" + SNlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.User_Base GetModel(int SN)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 SN,UB_GUID,UB_UserID,UB_UserPsw,UB_UserName,UB_Sex,UB_UserType,UB_RegDate,UB_LastLoginDate,UB_LoginTimes,UB_UnitGUID,GradeState,State from User_Base ");
            strSql.Append(" where SN=@SN");
            SqlParameter[] parameters = {
					new SqlParameter("@SN", SqlDbType.Int,4)
			};
            parameters[0].Value = SN;

            EduModel.User_Base model = new EduModel.User_Base();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.User_Base GetModel(Guid guid)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 * from User_Base ");
            strSql.Append(" where UB_GUID=@UB_GUID");
            SqlParameter[] parameters = {
					new SqlParameter("@UB_GUID", SqlDbType.UniqueIdentifier,16)
			};
            parameters[0].Value = guid;

            EduModel.User_Base model = new EduModel.User_Base();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EduModel.User_Base DataRowToModel(DataRow row)
        {
            EduModel.User_Base model = new EduModel.User_Base();
            if (row != null)
            {
                if (row["SN"] != null && row["SN"].ToString() != "")
                {
                    model.SN = int.Parse(row["SN"].ToString());
                }
                if (row["UB_GUID"] != null && row["UB_GUID"].ToString() != "")
                {
                    model.UB_GUID = new Guid(row["UB_GUID"].ToString());
                }
                if (row["UB_UserID"] != null)
                {
                    model.UB_UserID = row["UB_UserID"].ToString();
                }
                if (row["UB_UserPsw"] != null)
                {
                    model.UB_UserPsw = row["UB_UserPsw"].ToString();
                }
                if (row["UB_UserName"] != null)
                {
                    model.UB_UserName = row["UB_UserName"].ToString();
                }
                if (row["UB_Sex"] != null && row["UB_Sex"].ToString() != "")
                {
                    if ((row["UB_Sex"].ToString() == "1") || (row["UB_Sex"].ToString().ToLower() == "true"))
                    {
                        model.UB_Sex = true;
                    }
                    else
                    {
                        model.UB_Sex = false;
                    }
                }
                if (row["UB_UserType"] != null && row["UB_UserType"].ToString() != "")
                {
                    model.UB_UserType = int.Parse(row["UB_UserType"].ToString());
                }
                if (row["UB_RegDate"] != null && row["UB_RegDate"].ToString() != "")
                {
                    model.UB_RegDate = DateTime.Parse(row["UB_RegDate"].ToString());
                }
                if (row["UB_LastLoginDate"] != null && row["UB_LastLoginDate"].ToString() != "")
                {
                    model.UB_LastLoginDate = DateTime.Parse(row["UB_LastLoginDate"].ToString());
                }
                if (row["UB_LoginTimes"] != null && row["UB_LoginTimes"].ToString() != "")
                {
                    model.UB_LoginTimes = int.Parse(row["UB_LoginTimes"].ToString());
                }
              
                if (row["UB_UnitGUID"] != null && row["UB_UnitGUID"].ToString() != "")
                {
                    model.UB_UnitGUID = new Guid(row["UB_UnitGUID"].ToString());
                }
                if (row["GradeState"] != null && row["GradeState"].ToString() != "")
                {
                    model.GradeState = int.Parse(row["GradeState"].ToString());
                }
                if (row["State"] != null && row["State"].ToString() != "")
                {
                    model.State = int.Parse(row["State"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select SN,UB_GUID,UB_UserID,UB_UserPsw,UB_UserName,UB_Sex,UB_UserType,UB_RegDate,UB_LastLoginDate,UB_LoginTimes,UB_UnitGUID,GradeState,State ");
            strSql.Append(" FROM User_Base ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" SN,UB_GUID,UB_UserID,UB_UserPsw,UB_UserName,UB_Sex,UB_UserType,UB_RegDate,UB_LastLoginDate,UB_LoginTimes,UB_UnitGUID,GradeState,State ");
            strSql.Append(" FROM User_Base ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM User_Base ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.SN desc");
            }
            strSql.Append(")AS Row, T.*  from User_Base T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "User_Base";
            parameters[1].Value = "SN";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

