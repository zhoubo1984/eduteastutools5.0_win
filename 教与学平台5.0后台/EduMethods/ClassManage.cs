﻿/* ==============================================================================
  * 功能描述：ClassManage  
  * 创 建 者：Fubz
  * 创建日期：2014/7/17 9:59:53
  * ==============================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;


namespace EduMethods
{
    /// <summary>
    /// ClassManage
    /// </summary>
    public class ClassManage
    {
        /// <summary>
        /// 日志文件记录
        /// </summary>
        private static Common.Log log = new Common.Log();

        /// <summary>
        /// 获取单位列表信息
        /// </summary>
        /// <param name="key"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public static string getClassList(int page, int size, string order)
        {
            StringBuilder sb = new StringBuilder();
            Hashtable result = new Hashtable();
            sb.Append(" select UB_GUID,UB_Name,GradeState from dbo.Unit_Base ub ");
            sb.Append(" where 1=1 and State=1  ");
            if (order.Length > 0)
            {
                sb.Append(" order by  " + order);
            }
            else
            {
                sb.Append(" order by UB_Index  ");
            }
            try
            {
                DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
                int count = dt.Rows.Count;
                result["totalCount"] = count;
                count = count % size == 0 ? count / size : (count / size + 1);
                count = count == 0 ? 1 : count;
                if (count == 1)
                {
                    page = 1;
                }
                if (page > count)
                    page = count;
                var query = from aaa in dt.AsEnumerable()
                            select aaa;
                var selectData = query.Skip((page - 1) * size).Take(size);
                dt = selectData.CopyToDataTable<DataRow>();
                ArrayList dataAll = Common.Common.DataTable2ArrayList(dt);
                for (int i = 0; i < dataAll.Count; i++)
                {
                    Hashtable node = (Hashtable)dataAll[i];
                    node["GradeState"] = Common.Common.getMyUnitGradeType(int.Parse(node["GradeState"].ToString()));
                }

                result["Data"] = dataAll;
                result["currPage"] = page;
                result["pageSize"] = size;
            }
            catch (System.Exception ex)
            {
                log.Write("获取单位列表信息失败", ex);
            }

            return Common.JSON.Encode(result);
        }
    }
}