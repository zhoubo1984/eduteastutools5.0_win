﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace EduMethods
{
    public class CommonFunction
    {
        /// <summary>
        /// 记录错误日志信息
        /// </summary>
        private static Common.Log log = new Common.Log();

        /// <summary>
        /// 创建文件路径
        /// </summary>
        /// <param name="resPath"></param>
        public static void CreateFilePath(string resPath)
        {
            resPath = resPath.Replace("\\\\", "/");
            string[] fileList = resPath.Split(new char[] { '/' });
            string filePath = fileList[0];
            for (int i = 1; i < fileList.Length - 1; i++)
            {
                filePath = filePath + "/" + fileList[i];
                CreateFile(filePath);
            }

        }

        /// <summary>
        /// 获取生成文件的guid
        /// </summary>
        /// <param name="resPath"></param>
        /// <returns></returns>
        public static string getResGuid(string resPath)
        {
            string guid = string.Empty;
            int lastLen = resPath.LastIndexOf('.');
            int len = resPath.LastIndexOf('/');
            guid = resPath.Substring(len + 1, (lastLen - len - 1));   //截取字符串
            return guid;
        }

        /// <summary>
        /// 获取文件组成部分
        /// </summary>
        /// <param name="resPath"></param>
        /// <returns></returns>
        public static string[] getResGuidList(string resPath)
        {
            return resPath.Split(new char[] { '/' });
        }

        /// <summary>
        /// 创建文件路径
        /// </summary>
        /// <param name="resPath"></param>
        public static void CreateFileFloder(string resPath)
        {
            string[] fileList = resPath.Split(new char[] { '/' });
            string filePath = fileList[0];
            for (int i = 1; i < fileList.Length; i++)
            {
                filePath = filePath + "//" + fileList[i];
                CreateFile(filePath);
            }

        } 

        /// <summary>
        /// 判断是否存在文件夹，不存在则创建
        /// </summary>
        /// <param name="path"></param>
        public static void CreateFile(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        /// <summary>
        /// 判断是否存在文件夹，存在则删除
        /// </summary>
        /// <param name="path"></param>
        public static void DeleteFile(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }
    }
}