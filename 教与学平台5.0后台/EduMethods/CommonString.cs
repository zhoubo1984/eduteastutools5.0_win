﻿/* ==============================================================================
  * 功能描述：CommonString  
  * 创 建 者：Fubz
  * 创建日期：2014/8/1 14:24:13
  * ==============================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace EduMethods
{
    /// <summary>
    /// CommonString
    /// </summary>
    public class CommonString
    {
        public static string CnCodeDb = ConfigurationSettings.AppSettings["CnCodeDb"];
        public static string NoticeDb = ConfigurationSettings.AppSettings["NoticeDb"];
        public static string EduResDb = ConfigurationSettings.AppSettings["EduResDb"];
        public static string ResPath = ConfigurationSettings.AppSettings["ResPath"];
        public static string SysResService = ConfigurationSettings.AppSettings["SysResService"];

        /// <summary>
        /// 获取资源文件下载路径
        /// </summary>
        /// <returns></returns>
        public static string getResDownPath()
        {
            return DBUtility.DbHelperSQL.Query("select SC_Value from dbo.System_Config where SC_Name='ResourcePath'").Tables[0].Rows[0][0].ToString();
        }
        /// <summary>
        /// 获取单位对应的数据库地址
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        public static string getUnitDBPath(string sid)
        {
            string uDB = string.Empty;
            try
            {
                if (sid != null && sid.Length > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(" select UB_DBIndex from dbo.Unit_Base where UB_GUID=@UB_GUID ");
                    SqlParameter[] parameters = {
					new SqlParameter("@UB_GUID", SqlDbType.UniqueIdentifier,16)			};
                    parameters[0].Value = new Guid(sid);
                    DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString(), parameters).Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        uDB = dt.Rows[0][0].ToString();
                    }
                    else
                        uDB = CommonString.EduResDb;
                }
                else
                    uDB = CommonString.EduResDb;
            }
            catch (System.Exception ex)
            {
            }

            return uDB;
        }
    }
}