﻿/* ==============================================================================
  * 功能描述：DataTotal  
  * 创 建 者：Fubz
  * 创建日期：2014/12/8 14:57:55
  * ==============================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace EduMethods
{
    /// <summary>
    /// DataTotal
    /// </summary>
    public class DataTotal
    {
        /// <summary>
        /// 日志文件记录
        /// </summary>
        private static Common.Log log = new Common.Log();

        /// <summary>
        /// 获取数据统计
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public static string getSchoolTotal(string startTime, string endTime)
        {
            StringBuilder sb = new StringBuilder();
            Hashtable result = new Hashtable();
            DataTable dt = new DataTable();
            dt.Columns.Add("sn", Type.GetType("System.String"));
            dt.Columns.Add("name", Type.GetType("System.String"));
            dt.Columns.Add("regCount", Type.GetType("System.Int32"));
            dt.Columns.Add("UnitResCount", Type.GetType("System.Int32"));
            dt.Columns.Add("UnitAvg", typeof(decimal));
            dt.Columns.Add("EduResCount", Type.GetType("System.Int32"));
            dt.Columns.Add("EduAvg", typeof(decimal));
            DataTable dtList = DBUtility.DbHelperSQL.Query("exec dbo.proc_edu_unit ").Tables[0];
            sb.Append(" select COUNT(T_SN) from dbo.TeacherResource  ");
            sb.Append(" where ResState>=2 ");
            sb.Append(" and T_CreateDate<'" + endTime + "' and T_CreateDate>'" + startTime + "' ");
            DataTable dtTemp = null;
            decimal avgSum = 0;
            decimal avgEdu = 0;
            DataTable dtEdu = DBUtility.DbHelperSQL.Query("select T_UnitGUID,COUNT(T_SN) as psum from dbo.TeacherResource where ResState>=2 group by T_UnitGUID", CommonString.EduResDb).Tables[0];
            DataRow[] drs = null;
            DataRow drEdu = null;
            bool isEduData = true;
            for (int i = 0; i < dtList.Rows.Count; i++)
            {
                dtTemp = DBUtility.DbHelperSQL.Query(sb.ToString(), dtList.Rows[i]["dbIndex"].ToString()).Tables[0];
                drs = dtEdu.Select("T_UnitGUID='" + dtList.Rows[i]["id"].ToString() + "'");
                if (drs != null && drs.Length > 0)
                {
                    drEdu = drs[0];
                    isEduData = true;
                }
                else
                {
                    isEduData = false;
                }
                if (dt != null && dtTemp.Rows.Count > 0)
                {
                    if (dtList.Rows[i]["regSum"].ToString() == "0")
                    {
                        avgSum = 0;
                        avgEdu = 0;
                    }
                    else
                    {
                        avgSum = Math.Round(decimal.Parse(dtTemp.Rows[0][0].ToString()) / decimal.Parse(dtList.Rows[i]["regSum"].ToString()), 2);
                        if (!isEduData)
                        {
                            avgEdu = 0;
                        }
                        else
                            avgEdu = Math.Round(decimal.Parse(drEdu["psum"].ToString()) / decimal.Parse(dtList.Rows[i]["regSum"].ToString()), 2);
                    }
                    if (isEduData)
                    {
                        dt.Rows.Add(new object[] {(i+1),dtList.Rows[i]["name"].ToString(),
                       int.Parse(dtList.Rows[i]["regSum"].ToString()),
                       int.Parse(dtTemp.Rows[0][0].ToString()),
                       avgSum,
                        int.Parse(drEdu["psum"].ToString()),
                       avgEdu
                       });
                    }
                    else
                    {
                        dt.Rows.Add(new object[] {(i+1),dtList.Rows[i]["name"].ToString(),
                       int.Parse(dtList.Rows[i]["regSum"].ToString()),
                       int.Parse(dtTemp.Rows[0][0].ToString()),
                       avgSum,
                        0,
                       0
                       });
                    }

                }
                else
                {
                    if (dtList.Rows[i]["regSum"].ToString() == "0")
                    {
                        avgSum = 0;
                        avgEdu = 0;
                    }
                    else
                    {
                        avgSum = Math.Round(decimal.Parse(dtTemp.Rows[0][0].ToString()) / decimal.Parse(dtList.Rows[i]["regSum"].ToString()), 2);
                        if (!isEduData)
                        {
                            avgEdu = 0;
                        }
                        else
                            avgEdu = Math.Round(decimal.Parse(drEdu["psum"].ToString()) / decimal.Parse(dtList.Rows[i]["regSum"].ToString()), 2);
                    }
                    dt.Rows.Add(new object[] {(i+1),dtList.Rows[i]["name"].ToString(),
                       int.Parse(dtList.Rows[i]["regSum"].ToString()),
                       0,0,0,0
                    });
                }
            }
            DataView dv = dt.DefaultView;
            dv.Sort = "EduAvg  Desc,UnitAvg  Desc";
            dt = dv.ToTable();
            result["data"] = Common.Common.DataTable2ArrayList(dt);

            return Common.JSON.Encode(result);
        }

    }
}