﻿/* ==============================================================================
  * 功能描述：LoginInfo  
  * 创 建 者：Fubz
  * 创建日期：2014/7/10 10:42:28
  * ==============================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EduMethods
{
    /// <summary>
    /// 登录者信息类
    /// </summary>
    [Serializable]
    public class LoginInfo
    {

        private int _loginSN;

        /// <summary>
        /// 登录的用户GUID
        /// </summary>
        public int LoginSN
        {
            get { return _loginSN; }
            set { _loginSN = value; }
        }

        private string _loginGuid;

        /// <summary>
        /// 登录的用户GUID
        /// </summary>
        public string LoginGuid
        {
            get { return _loginGuid; }
            set { _loginGuid = value; }
        }

        private string _loginName;

        /// <summary>
        /// 登录的用户名
        /// </summary>
        public string LoginName
        {
            get { return _loginName; }
            set { _loginName = value; }
        }

        private string _realName;
        /// <summary>
        /// 登录者的真实姓名
        /// </summary>
        public string RealName
        {
            get { return _realName; }
            set { _realName = value; }
        }


        private string _unitname;

        /// <summary>
        /// 单位名称，学校
        /// </summary>
        public string UnitName
        {
            get { return _unitname; }
            set { _unitname = value; }
        }

        private string _unitguid;

        /// <summary>
        /// 学校GUID
        /// </summary>
        public string UnitGuid
        {
            get { return _unitguid; }
            set { _unitguid = value; }
        }

        private int _unitsn;

        /// <summary>
        /// 学校SN
        /// </summary>
        public int UnitSN
        {
            get { return _unitsn; }
            set { _unitsn = value; }
        }

        private int _unitteacheck;

        /// <summary>
        /// 学校用户审核
        /// </summary>
        public int UnitTeaCheck
        {
            get { return _unitteacheck; }
            set { _unitteacheck = value; }
        }

        private int _userrole;

        /// <summary>
        /// 用户角色,1教育局 2学校 3教师
        /// </summary>
        public int UserRole
        {
            get { return _userrole; }
            set { _userrole = value; }
        }

    }

}