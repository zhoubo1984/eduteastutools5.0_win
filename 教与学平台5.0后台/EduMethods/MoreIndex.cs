﻿/* ==============================================================================
  * 功能描述：MoreIndex  
  * 创 建 者：Fubz
  * 创建日期：2014/11/24 11:02:51
  * ==============================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace EduMethods
{
    /// <summary>
    /// MoreIndex
    /// </summary>
    public class MoreIndex
    {
        /// <summary>
        /// 日志文件记录
        /// </summary>
        private static Common.Log log = new Common.Log();

        #region 管理员索引管理
        /// <summary>
        /// 获取索引列表信息
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public static string getIndexList(LoginInfo login)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.Append(" select m.SN,m.IndexGuid,m.State,m.IndexName, ");
                sb.Append(" m.IndexTypeName,m.IndexHref ");
                sb.Append(" from dbo.MoreIndex m  ");
                sb.Append(" inner join dbo.Unit_Base u on u.UB_GUID=m.UnitGuid ");
                sb.Append(" where m.UserGuid='" + login.LoginGuid + "' ");
                sb.Append(" select distinct IndexTypeName from dbo.MoreIndex ");
                sb.Append(" where UserGuid='" + login.LoginGuid + "' ");
                DataSet ds = DBUtility.DbHelperSQL.Query(sb.ToString());
                sb.Remove(0, sb.Length);
                DataTable dtList = ds.Tables[0];
                DataTable dtType = ds.Tables[1];
                sb.Append(" {\"data\": [");
                foreach (DataRow dr in dtType.Rows)
                {
                    DataRow[] drs = dtList.Select("IndexTypeName='" + dr[0] + "'");
                    if (drs.Length > 0)
                    {
                        sb.Append(" {\"IndexTypeName\": \"" + dr[0] + "\",");
                        sb.Append(" \"data\": [");
                        foreach (DataRow d in drs)
                        {
                            sb.Append("{\"IndexName\":\"" + d["IndexName"] + "\",");
                            sb.Append(" \"IndexHref\":\"" + d["IndexHref"] + "\",");
                            sb.Append(" \"SN\":\"" + d["SN"] + "\",");
                            sb.Append(" \"State\":\"" + d["State"] + "\"},");
                        }
                        sb.Remove(sb.Length - 1, 1);
                        sb.Append(" ]},");
                    }
                }
                if (dtType.Rows.Count > 0)
                {
                    sb.Remove(sb.Length - 1, 1);
                }
                sb.Append(" ]}");
            }
            catch (System.Exception ex)
            {
                log.Write("获取更多索引列表信息失败", ex);
            }

            return sb.ToString();
        }


        /// <summary>
        /// 获取索引类型列表
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public static string getIndexTypeList(LoginInfo login)
        {
            DataTable dt = DBUtility.DbHelperSQL.Query("select distinct IndexTypeName from dbo.MoreIndex where UserGuid='" + login.LoginGuid + "' ").Tables[0];
            return Common.JSON.Encode(Common.Common.DataTable2ArrayList(dt));
        }

        #endregion

        #region 教师索引

        /// <summary>
        /// 获取用户的索引列表
        /// </summary>
        /// <param name="ub">用户Model</param>
        /// <returns></returns>
        public static string getTeaIndexList(EduModel.User_Base ub)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.Append(" select * from  ");
                sb.Append(" (select m.SN,m.IndexGuid,m.State,m.IndexName, ");  //教育局
                sb.Append(" m.IndexTypeName,m.IndexHref ");
                sb.Append(" from dbo.MoreIndex m where State=1  ");
                sb.Append(" union ");
                sb.Append(" select m.SN,m.IndexGuid,m.State,m.IndexName, ");  //学校
                sb.Append(" m.IndexTypeName,m.IndexHref ");
                sb.Append(" from dbo.MoreIndex m where State=2  ");
                sb.Append(" and m.UnitGuid='" + ub.UB_UnitGUID + "' ");
                sb.Append(" union ");
                sb.Append(" select m.SN,m.IndexGuid,m.State,m.IndexName, ");  //教师
                sb.Append(" m.IndexTypeName,m.IndexHref ");
                sb.Append(" from dbo.MoreIndex m where State=3 ");
                sb.Append(" and m.UserGuid='" + ub.UB_GUID + "' ");
                sb.Append(" ) as t ");

                sb.Append(" select distinct t.IndexTypeName from   ");
                sb.Append(" (select  ");  //教育局
                sb.Append(" m.IndexTypeName ");
                sb.Append(" from dbo.MoreIndex m where State=1  ");
                sb.Append(" union ");
                sb.Append(" select  ");  //学校
                sb.Append(" m.IndexTypeName ");
                sb.Append(" from dbo.MoreIndex m where State=2  ");
                sb.Append(" and m.UnitGuid='" + ub.UB_UnitGUID + "' ");
                sb.Append(" union ");
                sb.Append(" select  ");  //教师
                sb.Append(" m.IndexTypeName ");
                sb.Append(" from dbo.MoreIndex m where State=3 ");
                sb.Append(" and m.UserGuid='" + ub.UB_GUID + "' ");
                sb.Append(" ) as t ");

                DataSet ds = DBUtility.DbHelperSQL.Query(sb.ToString());
                sb.Remove(0, sb.Length);
                DataTable dtList = ds.Tables[0];
                DataTable dtType = ds.Tables[1];
                sb.Append(" {\"data\": [");
                foreach (DataRow dr in dtType.Rows)
                {
                    DataRow[] drs = dtList.Select("IndexTypeName='" + dr[0] + "'");
                    if (drs.Length > 0)
                    {
                        sb.Append(" {\"IndexTypeName\": \"" + dr[0] + "\",");
                        sb.Append(" \"data\": [");
                        foreach (DataRow d in drs)
                        {
                            sb.Append("{\"IndexName\":\"" + d["IndexName"] + "\",");
                            sb.Append(" \"IndexHref\":\"" + d["IndexHref"] + "\",");
                            sb.Append(" \"SN\":\"" + d["SN"] + "\",");
                            sb.Append(" \"State\":\"" + d["State"] + "\"},");
                        }
                        sb.Remove(sb.Length - 1, 1);
                        sb.Append(" ]},");
                    }
                }
                if (dtType.Rows.Count > 0)
                {
                    sb.Remove(sb.Length - 1, 1);
                }
                sb.Append(" ]}");
            }
            catch (System.Exception ex)
            {
                log.Write("获取更多索引列表信息失败", ex);
            }

            return sb.ToString();
        }

        /// <summary>
        /// 获取索引类型列表
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public static string getTeaIndexTypeList(EduModel.User_Base ub)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select distinct t.IndexTypeName from   ");
            sb.Append(" (select  ");  //教育局
            sb.Append(" m.IndexTypeName ");
            sb.Append(" from dbo.MoreIndex m where State=1  ");
            sb.Append(" union ");
            sb.Append(" select  ");  //学校
            sb.Append(" m.IndexTypeName ");
            sb.Append(" from dbo.MoreIndex m where State=2  ");
            sb.Append(" and m.UnitGuid='" + ub.UB_UnitGUID + "' ");
            sb.Append(" union ");
            sb.Append(" select  ");  //教师
            sb.Append(" m.IndexTypeName ");
            sb.Append(" from dbo.MoreIndex m where State=3 ");
            sb.Append(" and m.UserGuid='" + ub.UB_GUID + "' ");
            sb.Append(" ) as t ");
            DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
            return Common.JSON.Encode(Common.Common.DataTable2ArrayList(dt));
        }

        #endregion

        /// <summary>
        /// 获取索引类型列表
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public static string getIndexBySN(int sn)
        {
            EduBLL.MoreIndex mdBLL = new EduBLL.MoreIndex();
            EduModel.MoreIndex mdModel = mdBLL.GetModel(sn);
            string result = string.Empty;

            result = "{";
            result += "\"typeName\":\"" + mdModel.IndexTypeName + "\",";
            result += "\"name\":\"" + mdModel.IndexName + "\",";
            result += "\"href\":\"" + mdModel.IndexHref + "\",";
            result += "}";
            return result;
        }

    }
}