﻿/* ==============================================================================
  * 功能描述：Notice  
  * 创 建 者：Fubz
  * 创建日期：2014/8/11 8:55:40
  * ==============================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace EduMethods
{

    /// <summary>
    /// Notice
    /// </summary>
    public class Notice
    {
        /// <summary>
        /// 日志文件记录
        /// </summary>
        private static Common.Log log = new Common.Log();


        /// <summary>
        /// 获取公告列表信息
        /// </summary>
        /// <param name="login">当前登录用户</param>
        /// <param name="key">关键字</param>
        /// <param name="page">当前页</param>
        /// <param name="size">页大小</param>
        /// <param name="order">排序</param>
        /// <returns></returns>
        public static string getUnitNoticeList(LoginInfo login, string key, int page, int size, string order)
        {
            StringBuilder sb = new StringBuilder();
            Hashtable result = new Hashtable();
            sb.Append(" SELECT [SN],[N_GUID] ,[N_Title],[N_Content],[N_CreateDate],[N_UserGUID] ");
            sb.Append(" ,[N_UnitGUID],[N_PublishDate],[N_IsPublish]  from dbo.Notice");
            sb.Append(" where N_UnitGUID='" + login.UnitGuid + "' ");

            if (key != null && key.Length > 0)
            {
                sb.Append(" and  N_Title like '%" + key + "%' ");
            }
            if (order.Length > 0)
            {
                sb.Append(" order by  " + order);
            }
            else
            {
                sb.Append(" order by N_CreateDate desc  ");
            }
            try
            {
                DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString(), EduMethods.CommonString.getUnitDBPath(login.UnitGuid)).Tables[0];
                int count = dt.Rows.Count;
                result["totalCount"] = count;
                count = count % size == 0 ? count / size : (count / size + 1);
                count = count == 0 ? 1 : count;
                if (count == 1)
                {
                    page = 1;
                }
                if (page > count)
                    page = count;
                var query = from aaa in dt.AsEnumerable()
                            select aaa;
                var selectData = query.Skip((page - 1) * size).Take(size);
                dt = selectData.CopyToDataTable<DataRow>();
                ArrayList dataAll = Common.Common.DataTable2ArrayList(dt);
                sb.Remove(0, sb.Length);
                sb.Append(" select SM_ManageName from dbo.System_Manage where SM_GUID=@GUID  ");
                SqlParameter[] parameters = {
					new SqlParameter("@GUID", SqlDbType.UniqueIdentifier,16)			};
                DataTable dtTemp = null;
                for (int i = 0; i < dataAll.Count; i++)
                {
                    Hashtable node = (Hashtable)dataAll[i];
                    parameters[0].Value = new Guid(node["N_UserGUID"].ToString());
                    dtTemp = DBUtility.DbHelperSQL.Query(sb.ToString(), parameters).Tables[0];
                    if (dtTemp.Rows.Count > 0)
                    {
                        node["UserName"] = dtTemp.Rows[0][0];
                    }
                    else
                    {
                        node["UserName"] = "";
                    }
                }

                result["Data"] = dataAll;
                result["currPage"] = page;
                result["pageSize"] = size;
            }
            catch (System.Exception ex)
            {
                log.Write("获取公告列表信息失败", ex);
            }

            return Common.JSON.Encode(result);
        }


        /// <summary>
        /// 获取图片列表信息
        /// </summary>
        /// <param name="login">当前登录用户</param>
        /// <param name="key">关键字</param>
        /// <param name="page">当前页</param>
        /// <param name="size">页大小</param>
        /// <param name="order">排序</param>
        /// <returns></returns>
        public static string getUnitPictureList(LoginInfo login, string key, int page, int size, string order)
        {
            StringBuilder sb = new StringBuilder();
            Hashtable result = new Hashtable();
            sb.Append(" SELECT  [SN],[P_Title],[P_Path],P_Href,[P_Upload_Date],[P_Upload_UserGuid],[P_IsShow] ");
            sb.Append(" FROM [dbo].[Picture] where P_UnitGuid='" + login.UnitGuid + "' ");
            if (key != null && key.Length > 0)
            {
                sb.Append(" and  P_Title like '%" + key + "%' ");
            }
            if (order.Length > 0)
            {
                sb.Append(" order by  " + order);
            }
            else
            {
                sb.Append(" order by P_Upload_Date desc  ");
            }
            try
            {
                DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString(), CommonString.NoticeDb).Tables[0];
                int count = dt.Rows.Count;
                result["totalCount"] = count;
                count = count % size == 0 ? count / size : (count / size + 1);
                count = count == 0 ? 1 : count;
                if (count == 1)
                {
                    page = 1;
                }
                if (page > count)
                    page = count;
                var query = from aaa in dt.AsEnumerable()
                            select aaa;
                var selectData = query.Skip((page - 1) * size).Take(size);
                dt = selectData.CopyToDataTable<DataRow>();
                ArrayList dataAll = Common.Common.DataTable2ArrayList(dt);
                sb.Remove(0, sb.Length);
                sb.Append(" select SM_ManageName from dbo.System_Manage where SM_GUID=@GUID  ");
                SqlParameter[] parameters = {
					new SqlParameter("@GUID", SqlDbType.UniqueIdentifier,16)			};
                DataTable dtTemp = null;
                for (int i = 0; i < dataAll.Count; i++)
                {
                    Hashtable node = (Hashtable)dataAll[i];
                    parameters[0].Value = new Guid(node["P_Upload_UserGuid"].ToString());
                    dtTemp = DBUtility.DbHelperSQL.Query(sb.ToString(), parameters).Tables[0];
                    if (dtTemp.Rows.Count > 0)
                    {
                        node["UserName"] = dtTemp.Rows[0][0];
                    }
                    else
                    {
                        node["UserName"] = "";
                    }
                }

                result["Data"] = dataAll;
                result["currPage"] = page;
                result["pageSize"] = size;
            }
            catch (System.Exception ex)
            {
                log.Write("获取前台图片列表信息失败", ex);
            }

            return Common.JSON.Encode(result);
        }

        /// <summary>
        /// 获取软件列表信息
        /// </summary>
        /// <param name="login">当前登录用户</param>
        /// <param name="key">关键字</param>
        /// <param name="page">当前页</param>
        /// <param name="size">页大小</param>
        /// <param name="order">排序</param>
        /// <returns></returns>
        public static string getUnitSoftList(LoginInfo login, string key, int page, int size, string order)
        {
            StringBuilder sb = new StringBuilder();
            Hashtable result = new Hashtable();
            sb.Append(" SELECT [SN],[S_Name],[S_Content],[S_Index],[S_Date] ,[S_UserGUID] ");
            sb.Append(" FROM [dbo].[Soft] where S_UnitGUID='" + login.UnitGuid + "' ");
            if (key != null && key.Length > 0)
            {
                sb.Append(" and  P_Title like '%" + key + "%' ");
            }
            if (order.Length > 0)
            {
                sb.Append(" order by  " + order);
            }
            else
            {
                sb.Append(" order by S_Date desc  ");
            }
            try
            {
                DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString(), CommonString.NoticeDb).Tables[0];
                int count = dt.Rows.Count;
                result["totalCount"] = count;
                count = count % size == 0 ? count / size : (count / size + 1);
                count = count == 0 ? 1 : count;
                if (count == 1)
                {
                    page = 1;
                }
                if (page > count)
                    page = count;
                var query = from aaa in dt.AsEnumerable()
                            select aaa;
                var selectData = query.Skip((page - 1) * size).Take(size);
                dt = selectData.CopyToDataTable<DataRow>();
                ArrayList dataAll = Common.Common.DataTable2ArrayList(dt);
                sb.Remove(0, sb.Length);
                sb.Append(" select SM_ManageName from dbo.System_Manage where SM_GUID=@GUID  ");
                SqlParameter[] parameters = {
					new SqlParameter("@GUID", SqlDbType.UniqueIdentifier,16)			};
                DataTable dtTemp = null;
                for (int i = 0; i < dataAll.Count; i++)
                {
                    Hashtable node = (Hashtable)dataAll[i];
                    parameters[0].Value = new Guid(node["S_UserGUID"].ToString());
                    dtTemp = DBUtility.DbHelperSQL.Query(sb.ToString(), parameters).Tables[0];
                    if (dtTemp.Rows.Count > 0)
                    {
                        node["UserName"] = dtTemp.Rows[0][0];
                    }
                    else
                    {
                        node["UserName"] = "";
                    }
                }

                result["Data"] = dataAll;
                result["currPage"] = page;
                result["pageSize"] = size;
            }
            catch (System.Exception ex)
            {
                log.Write("获取资源工具列表信息失败", ex);
            }

            return Common.JSON.Encode(result);
        }


    }
}