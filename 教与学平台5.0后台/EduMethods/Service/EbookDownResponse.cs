﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EduMethods.Service
{
    /// <summary>
    /// EbookDownResponse
    /// </summary>
    public class EbookDownResponse : ResponseBase
    {
        /// <summary>
        /// 下载地址
        /// </summary>
        public string DownUrl { get; set; }

        /// <summary>
        /// 文件后缀
        /// </summary>
        public string FileExt { get; set; }


        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

    }

}
