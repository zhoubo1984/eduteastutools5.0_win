﻿/* ==============================================================================
  * 功能描述：EbookResponse  
  * 创 建 者：Fubz
  * 创建日期：2016/7/7 9:34:49
  * ==============================================================================
 */
using System.Collections.Generic;

namespace EduMethods
{
    /// <summary>
    /// EbookResponse
    /// </summary>
    public class EbookResponse : ResponseBase
    {

        /// <summary>
        /// 返回教材树列表
        /// </summary>
        public List<Book_Base> list { get; set; }
    }
}