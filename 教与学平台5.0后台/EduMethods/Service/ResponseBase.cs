﻿using EduSys.HttpWeb.Enum.ServiceEnum;
/* ==============================================================================
  * 功能描述：BaseRequest  
  * 创 建 者：Fubz
  * 创建日期：2016/3/14 13:46:14
  * ==============================================================================
 */
using System;
using System.Collections.Generic;

namespace EduMethods
{
    /// <summary>
    /// 返回信息基类
    /// </summary>
    public class ResponseBase
    {
        /// <summary>
        /// 初始化
        /// </summary>
        public ResponseBase()
        {
            IsSuccess = false;
            ResponseCode = ResultCode.ReqError;
            Msg = "请求失败！";
        }

        /// <summary>
        /// 是否成功(方便判断)
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// 请求返回的代码
        /// </summary>
        public ResultCode ResponseCode { get; set; }

        /// <summary>
        /// 返回时间戳
        /// </summary>
        public long TimeSpan { get; set; }


        /// <summary>
        /// 返回信息
        /// </summary>
        public string Msg { get; set; }

        /// <summary>
        /// 错误具体
        /// </summary>
        public Dictionary<string, string> errorList { get; set; }

    }
}
