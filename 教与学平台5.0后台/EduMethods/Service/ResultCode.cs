﻿/* ==============================================================================
  * 功能描述：ResultCode  
  * 创 建 者：Fubz
  * 创建日期：2016/3/14 14:12:27
  * ==============================================================================
 */
using System.ComponentModel;

namespace EduSys.HttpWeb.Enum.ServiceEnum
{
    /// <summary>
    /// 接口返回值代码
    /// </summary>
    [Description("接口返回值代码")]
    public enum ResultCode
    {

        #region 基本请求代码(100~199)
        /// <summary>
        /// 请求成功
        /// </summary>
        [Description("请求成功")]
        Success = 100,

        /// <summary>
        /// 请求失败
        /// </summary>
        [Description("请求失败")]
        ReqError = 101,

        /// <summary>
        /// 服务器内部错误
        /// </summary>
        [Description("服务器内部错误")]
        ServiceInterntError = 102,

        /// <summary>
        /// 输入参数有误
        /// </summary>
        [Description("输入参数有误")]
        ReqParError = 103,

        /// <summary>
        /// 授权有误
        /// </summary>
        [Description("授权有误")]
        VerifyError = 104,

        /// <summary>
        /// 多点登录
        /// </summary>
        [Description("多点登录")]
        LimitLogin = 105,

        /// <summary>
        /// 链接数据库失败
        /// </summary>
        [Description("链接数据库失败")]
        SqlError = 106,

        /// <summary>
        /// 请求时间戳超出时间
        /// </summary>
        [Description("请求时间戳超出时间")]
        ReqTimeSpanOut = 107,


        /// <summary>
        /// 登录信息不存在(根据票证查找不到用户，需要重新登录)
        /// </summary>
        [Description("登录信息不存在(根据票证查找不到用户，需要重新登录)")]
        LoginTimeOut = 108,

        /// <summary>
        /// 票证过期，需要重新登录
        /// </summary>
        [Description("票证过期，需要重新登录")]
        UserTicketTimeOut = 109,



        #endregion

        #region 登录注册(200~299)
        /// <summary>
        /// 登录账号不存在
        /// </summary>
        [Description("登录账号不存在")]
        LoginIDNotExist = 200,

        /// <summary>
        /// 登录密码不正确
        /// </summary>
        [Description("登录密码不正确")]
        LoginPwdError = 201,

        /// <summary>
        /// 账号已停用
        /// </summary>
        [Description("账号已停用")]
        LoginIDNotUse = 202,

        /// <summary>
        /// 注册账号已存在
        /// </summary>
        [Description("注册账号已存在")]
        RegUserIDExist = 203,

        /// <summary>
        /// 注册账号失败
        /// </summary>
        [Description("注册账号失败")]
        RegUserFail = 204,

        #endregion

        #region 查询数据(300~399)

        /// <summary>
        /// 查询列表为空
        /// </summary>
        [Description(" 查询列表为空")]
        QueryDataNotExist = 300,

        #endregion

        #region 操作

        /// <summary>
        /// 操作失败
        /// </summary>
        [Description("操作失败")]
        OperError = 401,

        #endregion
    }
}