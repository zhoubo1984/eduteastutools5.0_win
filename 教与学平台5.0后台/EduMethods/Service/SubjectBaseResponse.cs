﻿/* ==============================================================================
  * 功能描述：SubjectBaseResponse  
  * 创 建 者：Fubz
  * 创建日期：2016/4/18 11:31:33
  * ==============================================================================
 */
using System.Collections.Generic;

namespace EduMethods
{
    /// <summary>
    /// SubjectBaseResponse返回实体
    /// </summary>
    public class SubjectBaseResponse : ResponseBase
    {
        /// <summary>
        /// 资源类型列表
        /// </summary>
        public List<Subject_Base> list { get; set; }

    }
}