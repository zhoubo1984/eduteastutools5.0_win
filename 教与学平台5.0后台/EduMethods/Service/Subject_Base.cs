﻿/* ==============================================================================
  * 功能描述：Subject_Base  
  * 创 建 者：Fubz
  * 创建日期：2016/4/15 10:26:24
  * ==============================================================================
 */
using System;


namespace EduMethods
{
    /// <summary>
    /// 读取数据库对应的数据表 Subject_Base生成实体类
    /// </summary>
    public class Subject_Base
    {

        /// <summary>
        /// 主键SN
        /// </summary>
        public int SB_SN { get; set; }

        /// <summary>
        /// 唯一标示
        /// </summary>
        public Guid SB_GUID { get; set; }

        /// <summary>
        /// 学科名称
        /// </summary>
        public string SB_SubjectName { get; set; }

        /// <summary>
        /// 索引
        /// </summary>
        public int SB_Index { get; set; }

        /// <summary>
        /// 学段
        /// </summary>
        public int GradeState { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// 资源表名
        /// </summary>
        public string SB_SR_TableName { get; set; }

    }
}