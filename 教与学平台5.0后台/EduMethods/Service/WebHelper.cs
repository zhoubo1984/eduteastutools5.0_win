﻿using System;
/* ==============================================================================
  * 功能描述：WebHelper  
  * 创 建 者：Fubz
  * 创建日期：2016/3/14 11:04:23
  * ==============================================================================
 */
using System.IO;
using System.Net;
using System.Text;

namespace EduMethods
{
    /// <summary>
    /// WebHelper
    /// </summary>
    public class WebHelper
    {

        /// <summary>
        /// 获取API数据
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="data">数据参数</param>
        /// <returns></returns>
        public static string GetApiData(string url, string data)
        {
            return GetApiData(url, data, "POST");

        }

        /// <summary>
        /// 获取API数据
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="data">数据参数</param>
        /// <param name="method">方法类型</param>
        /// <returns></returns>
        public static string GetApiData(string url, string data, string method)
        {
            HttpWebRequest request = null;
            string result = "";
            try
            {
                request = WebRequest.Create(url) as HttpWebRequest;
                request.Timeout = 1800000;
                request.ReadWriteTimeout = 600000;
                request.KeepAlive = true;
                //request.Headers.Add("Authorization", "Bearer " + "yDh0g6yODv78ripb9ZgXFqHCLhs8FFXX7HOubOJ60hhoNUgPtxIG6CNjCpDxasYEh2ydhIQ8N-Nsx_aD5yq5B6p4kOly8s3m3V7EzdKiOcEyQsxgroPLVFKo_nJucUsajKc0ENn5edEHMxqwqThZoFguo9PVlMGhJ-SRfPuJH-XOrnQmlz9lfgx-qTO58Hgo_dI9kO4DcfzFTOuDtS8fPwrvrwJp7UVXj94j6sGp7VylIHCi-4HmiS8Yx386nzaCGaIpphYeGftaqezP_l04WWtFxqx5uhT2dRHZZNe5WpCjS_zLYTOv07tP2wXQ2kSf");
                System.Net.ServicePointManager.Expect100Continue = false;
                switch (method.ToUpper())
                {
                    case "GET":
                        request.Method = "GET";
                        break;
                    case "POST":
                        {
                            request.Method = "POST";
                            byte[] bdata = Encoding.UTF8.GetBytes(data);
                            request.ContentType = "application/json;charset=utf-8";
                            request.ContentLength = bdata.Length;

                            Stream streamOut = request.GetRequestStream();
                            streamOut.Write(bdata, 0, bdata.Length);
                            streamOut.Close();
                        }
                        break;
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream streamIn = response.GetResponseStream();
                using (StreamReader reader = new StreamReader(streamIn))
                {
                    result = reader.ReadToEnd();
                }
                streamIn.Close();
                response.Close();

                return result;
            }
            catch (WebException ex)
            {
                HttpWebResponse res = (HttpWebResponse)ex.Response;
                if(res!=null)
                {
                    StreamReader sr = new StreamReader(res.GetResponseStream(), System.Text.Encoding.UTF8);
                    var strHtml = sr.ReadToEnd();
                    return strHtml;
                }

                return string.Empty;
            }

        }


        /// <summary>
        /// 获取API数据
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="data">数据参数</param>
        /// <param name="method">方法类型</param>
        /// <returns></returns>
        public static Stream GeImageApiData(string url, string data, string method)
        {
            HttpWebRequest request = null;
            try
            {
                request = WebRequest.Create(url) as HttpWebRequest;
                request.Timeout = 60000;
                request.KeepAlive = true;
                //request.Headers.Add("Authorization", "Bearer " + "zNYwMkYEpCKijnkU_-JZ9ZfhAxrn-F51Z1xMHx4rdsvsLR-KHwLE5z6_9ooC-2h6xOsMyziF93cp3uq33ihbjE46jXVflaOgIP6AvjkASYqykqFWLd5CnX-Daupi-y4WyUT4cGoQjRQk2LkmGgZbfIyB4c01jfVqFj80G3mooRmfLx3H8hPv2IRWSuw9ZUYuiwUCm_OKfOUBC13yl8_MN7OQPB9tl6pXIpz6jMaVeCdalGVQfO5rrpiW4tx_r1hBDejyaxlhPl_rwCvmN4gzk_cc2kTF1xxoQ4QF4WnlaGng5QyNQECmJ7rCGPr2Evrj");
                System.Net.ServicePointManager.Expect100Continue = false;
                switch (method.ToUpper())
                {
                    case "GET":
                        request.Method = "GET";
                        break;
                    case "POST":
                        {
                            request.Method = "POST";
                            byte[] bdata = Encoding.UTF8.GetBytes(data);
                            request.ContentType = "application/json;charset=utf-8";
                            request.ContentLength = bdata.Length;

                            Stream streamOut = request.GetRequestStream();
                            streamOut.Write(bdata, 0, bdata.Length);
                            streamOut.Close();
                        }
                        break;
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream streamIn = response.GetResponseStream();
                return response.GetResponseStream();
                //using (StreamReader reader = new StreamReader(streamIn))
                //{
                //    result = reader.ReadToEnd();
                //}
                //streamIn.Close();
                //response.Close();

            }
            catch (Exception ex)
            {
                throw;
            }

        }


    }
}