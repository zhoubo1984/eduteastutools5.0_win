﻿/* ==============================================================================
  * 功能描述：SubjectTelation  
  * 创 建 者：Fubz
  * 创建日期：2016/5/17 9:10:37
  * ==============================================================================
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using EduMethods.Service;


namespace EduMethods
{
    /// <summary>
    /// SubjectTelation
    /// </summary>
    public class SubjectRelation
    {
        /// <summary>
        /// 日志文件记录
        /// </summary>
        private static Common.Log log = new Common.Log();

        private static List<TreeModel> listTree = new List<TreeModel>();

        /// <summary>
        /// 获取学科列表信息
        /// </summary>
        /// <param name="login">当前登录用户</param>
        /// <param name="key">关键字</param>
        /// <param name="page">当前页</param>
        /// <param name="size">页大小</param>
        /// <param name="order">排序</param>
        /// <returns></returns>
        public static string getSubjectList(LoginInfo login, string key, int page, int size, string order)
        {
            StringBuilder sb = new StringBuilder();
            Hashtable result = new Hashtable();
            sb.Append(" select s.SN, b.SB_SubjectName,b.SB_GUID,s.SysSubGuid,s.SysSubName ");
            sb.Append(" from dbo.Subject_Base b ");
            sb.Append(" inner join dbo.Subject_SysRes s on s.SubGuid=b.SB_GUID ");
            sb.Append(" order by b.SB_Index ");
            try
            {
                DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
                int count = dt.Rows.Count;
                result["totalCount"] = count;
                if (count == 0)
                {
                    InsertData();
                    dt = DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
                    count = dt.Rows.Count;
                    result["totalCount"] = count;
                }
                count = count % size == 0 ? count / size : (count / size + 1);
                count = count == 0 ? 1 : count;
                if (count == 1)
                {
                    page = 1;
                }
                if (page > count)
                    page = count;
                var query = from aaa in dt.AsEnumerable()
                            select aaa;
                var selectData = query.Skip((page - 1) * size).Take(size);
                dt = selectData.CopyToDataTable<DataRow>();
                result["Data"] = Common.Common.DataTable2ArrayList(dt);
                result["currPage"] = page;
                result["pageSize"] = size;
            }
            catch (System.Exception ex)
            {
                log.Write("获取学科列表信息失败", ex);
            }

            return Common.JSON.Encode(result);
        }

        private static void InsertData()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select b.SB_GUID ");
            sb.Append(" from dbo.Subject_Base b ");
            sb.Append(" order by b.SB_Index ");
            DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];

            sb.Remove(0, sb.Length);
            foreach (DataRow d in dt.Rows)
            {
                sb.Append(" insert into Subject_SysRes  ([SubGuid] ,[SysSubGuid],[SysSubName]) VALUES ('" + d[0] + "',null,'') ");
            }
            DBUtility.DbHelperSQL.ExecuteSql(sb.ToString());
        }

        /// <summary>
        /// 加载学科数据
        /// </summary>
        /// <returns></returns>
        public static string SysResSubject()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("ClientType", 1);
            parameters.Add("ut", 2);
            parameters.Add("TimeSpan", ConvertDateTimeToTimeSpan(DateTime.Now));
            parameters.Add("ApiAction", "SubjectBase/All");
            var data = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
            var responseData = WebHelper.GetApiData(CommonString.SysResService + "SubjectBase/All", data);

            SubjectBaseResponse response = Newtonsoft.Json.JsonConvert.DeserializeObject<SubjectBaseResponse>(responseData);

            if (response.IsSuccess)
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject(response.list);
            }
            else
            {
                return "";
            }


        }
        /// <summary>
        /// 类
        /// </summary>
        public class TreeModel
        {
            public Guid BB_GUID { get; set; }

            public string BB_Name { get; set; }

            public bool isLeaf { get; set; }

            public int GradeState { get; set; }
      
            public int State { get; set; }

            public int SB_Index { get; set; }         
        }

        public static List<TreeModel> SubjectTreeListWithoutId()
        {
            listTree.Clear();
            var parameters = new Dictionary<string, object>();
            parameters.Add("ClientType", 1);
            //parameters.Add("ut", 2);
            parameters.Add("TimeSpan", ConvertDateTimeToTimeSpan(DateTime.Now));
            parameters.Add("ApiAction", "Ebook/SubjectList");
            //parameters.Add("searchGuid", new Guid("1CC1D2A0-160D-4C9C-8159-BE24A5B033B8"));
            var data = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
            var responseData = WebHelper.GetApiData(CommonString.SysResService + "Ebook/SubjectList", data);

            SubjectBaseResponse response = Newtonsoft.Json.JsonConvert.DeserializeObject<SubjectBaseResponse>(responseData);

            if(response!=null&&response.list!=null)
            {
                EduBLL.Subject_Base sbase_bll = new EduBLL.Subject_Base();
                foreach (Subject_Base s in response.list)
                {
                    //如果新建学科中没有这个的话
                    var isexists = sbase_bll.Exists(s.SB_GUID);
                    if (!isexists)
                    {
                        EduModel.Subject_Base sbmodel = new EduModel.Subject_Base() 
                        { 
                            GradeState = s.GradeState,
                            SB_GUID = s.SB_GUID,
                            SB_Index = s.SB_Index,
                            SB_SN = s.SB_SN,
                            SB_SubjectName = s.SB_SubjectName,
                            State = s.State
                        };
                        sbase_bll.Add(sbmodel);
                    }

                    var tree = new TreeModel();
                    tree.BB_GUID = s.SB_GUID;
                    tree.BB_Name = s.SB_SubjectName;
                    tree.isLeaf = false;
                    tree.GradeState = s.GradeState;
                    tree.State = s.State;
                    tree.SB_Index = s.SB_Index;
                    listTree.Add(tree);
                }
            }
            return listTree;
        }

        public static List<Book_Base> SubjectTreeListWithId(string id)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("ClientType", 1);
            // parameters.Add("ut", 2);
            parameters.Add("TimeSpan", ConvertDateTimeToTimeSpan(DateTime.Now));
            parameters.Add("ApiAction", "Ebook/BookList");
            parameters.Add("searchGuid", new Guid(id));

            var data = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
            var responseData = WebHelper.GetApiData(CommonString.SysResService + "Ebook/BookList", data);

            EbookResponse response = Newtonsoft.Json.JsonConvert.DeserializeObject<EbookResponse>(responseData);
            return response.list;
        }

        /// <summary>
        /// 获取教材版本wdm
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string SubjectTreeList(string id)
        {           
            if (string.IsNullOrEmpty(id))
            {
                SubjectTreeListWithoutId();
                return Common.JSON.Encode(listTree);
            }
            else
            {
                var list = SubjectTreeListWithId(id);
                return Common.JSON.Encode(list);
               
            }
        }

        /// <summary>
        /// 获取教材数据wdm
        /// </summary>
        /// <returns></returns>
        public static EbookResponse EbookList(string Guid)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("ClientType", 1);
            parameters.Add("ut", 2);
            parameters.Add("TimeSpan", ConvertDateTimeToTimeSpan(DateTime.Now));
            parameters.Add("ApiAction", "Ebook/BookBase");
            parameters.Add("bookGuid", new Guid(Guid));
              
            var data = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
            var responseData = WebHelper.GetApiData(CommonString.SysResService + "Ebook/BookBase", data);
            EbookResponse response = Newtonsoft.Json.JsonConvert.DeserializeObject<EbookResponse>(responseData);         
            return response;          
        }

        public static string EbookDown(string Guid,ref int filecount)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("ClientType", 1);
            parameters.Add("ut", 2);
            parameters.Add("TimeSpan", ConvertDateTimeToTimeSpan(DateTime.Now));
            parameters.Add("ApiAction", "Ebook/File");
            parameters.Add("bookGuid", new Guid(Guid));

            var data = Newtonsoft.Json.JsonConvert.SerializeObject(parameters);
            var responseData = WebHelper.GetApiData(CommonString.SysResService + "Ebook/File", data);

            EbookDownResponse res = Newtonsoft.Json.JsonConvert.DeserializeObject<EbookDownResponse>(responseData);

            if (res.FileName == null && res.DownUrl == null && res.FileExt == null)
            {
                return res.Msg;
            }
            //要下载到这里
            //string SR_ResIndex = CommonString.ResPath + Guid + "/" + res.FileName;
            string SR_ResIndex = CommonString.ResPath + "/" + res.FileName;
            //下载路径
            string ResIndex = CommonString.SysResService + res.DownUrl;

            CommonFunction.CreateFilePath(SR_ResIndex);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ResIndex);
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            Stream responseStream = response.GetResponseStream();
            Stream stream = new FileStream(SR_ResIndex, FileMode.Create);
            byte[] bArr = new byte[1024];
            int size = responseStream.Read(bArr, 0, bArr.Length);
            while (size > 0)
            {
                stream.Write(bArr, 0, size);
                size = responseStream.Read(bArr, 0, bArr.Length);
            }
            stream.Close();
            responseStream.Close();
            ZipHelper zip = new ZipHelper();
            filecount = zip.UnZip(SR_ResIndex, CommonString.ResPath  + "/", "", true);
            if (res.IsSuccess)
            {
                CommonFunction.DeleteFile(SR_ResIndex);
                return "1";
            }
            else
            {
                return res.Msg;
            }
        }

        /// <summary>
        /// 下载wdm
        /// </summary>
        /// <returns></returns>
        public static string EbookDown(string Guid)
        {
            int filecount = 0;
            return EbookDown(Guid, ref filecount);
        }
        /// <summary>
        /// 电子书升级wdm
        /// </summary>
        /// <returns></returns>
        public static string EbookShengji(string Guid)
        {
            string res = "";
            string[] Guidlist = Guid.Split(new char[] { ',' });
            for (int l = 0; l < Guidlist.Length; l++)
            {
               // Guidlist[l] = "1CC1D2A0-160D-4C9C-8159-BE24A5B033B8";
                if (IsBook(Guidlist[l]))
                {
                    EbookResponse list = EbookList(Guidlist[l]);                   
                    if (list == null || list.Msg=="请求失败！")
                    {
                        res += "升级失败,";
                    }
                    else
                    {
                        if (list.list.Count == 0)
                        {
                            res += "没有对应的电子书，";
                        }
                        else
                        {                            
                            StringBuilder sb = new StringBuilder();
                            sb.Append("select * from Book_Base where  BB_GUID='" + Guidlist[l] + "'");
                            DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString(), CommonString.CnCodeDb).Tables[0];
                            if (dt.Rows[0]["BB_PageIndex"].ToString()!="")
                            {
                                res += "教材已升级，";
                            }
                            else
                            {
                                for (int i = 0; i < list.list.Count; i++)
                                {
                                    sb.Remove(0, sb.Length);
                                    if (list.list[i].BB_PageIndex == null)
                                    {
                                        sb.Append("update Book_Base set BB_PageIndex= null where  BB_GUID='" + list.list[i].BB_GUID + "'");
                                        int num = DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                                        if (num != 1)
                                        {
                                            sb.Remove(0, sb.Length);
                                            sb.Append("insert into Book_Base([BB_GUID],[BB_PGUID],[BB_Name],[BB_Path],[BB_Index],[BB_SubjectGUID],[State],[BB_PageIndex])values('" + list.list[i].BB_GUID + "','" + list.list[i].BB_PGUID + "','" + list.list[i].BB_Name + "','" + list.list[i].BB_Path + "'," + list.list[i].BB_Index + ",'" + list.list[i].BB_SubjectGUID + "'," + list.list[i].BB_Index + "," + list.list[i].BB_PageIndex + ")");
                                            DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                                        }
                                    }
                                    else
                                    {
                                        sb.Append("update Book_Base set BB_PageIndex=" + list.list[i].BB_PageIndex + " where  BB_GUID='" + list.list[i].BB_GUID + "'");
                                        int num = DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                                        if (num != 1)
                                        {
                                            sb.Remove(0, sb.Length);
                                            sb.Append("insert into Book_Base([BB_GUID],[BB_PGUID],[BB_Name],[BB_Path],[BB_Index],[BB_SubjectGUID],[State],[BB_PageIndex])values('" + list.list[i].BB_GUID + "','" + list.list[i].BB_PGUID + "','" + list.list[i].BB_Name + "','" + list.list[i].BB_Path + "'," + list.list[i].BB_Index + ",'" + list.list[i].BB_SubjectGUID + "'," + list.list[i].BB_Index + "," + list.list[i].BB_PageIndex + ")");
                                            DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                                        }
                                    }
                                }
                                //这是下载
                                string r = EbookDown(Guidlist[l]);
                                if (r == "1")
                                {                                   
                                    res += "升级成功,";
                                }
                                else
                                {
                                    res += r + ",";
                                }
                            }
                        }
                    }
                }
                else
                {
                    res += "请选择教材,";
                }
            }
            res = res.Substring(0,res.Length-1);
            return res;
        }

        /// <summary>
        /// 判断guid是不是教材的wdm
        /// </summary>
        /// <returns></returns>
        public static bool IsBook(string Guid)
        {
            for (int i = 0; i < listTree.Count; i++)
            {
                if (listTree[i].BB_GUID.ToString() == Guid)
                {
                    return false;                   
                }
            }
            return true;
        }
        /// <summary>
        /// 更新学科wdm
        /// </summary>
        /// <returns></returns>
        public static string UPSub(string Guid)
        {
            string res = "";
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from Subject_Base where  SB_GUID='" + Guid + "'");
            DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
            if (dt != null)
            {
                if (dt.Rows.Count == 0)
                {
                    for (int i = 0; i < listTree.Count; i++)
                    {
                        if (listTree[i].BB_GUID.ToString() == Guid)
                        {
                            sb.Remove(0, sb.Length);
                            sb.Append("insert into Subject_Base([SB_GUID],[SB_SubjectName],[GradeState],[State],[SB_Index])values('" + Guid + "','" + listTree[i].BB_Name + "'," + listTree[i].GradeState + "," + listTree[i].State + "," + listTree[i].SB_Index + ")");
                            int num = DBUtility.DbHelperSQL.ExecuteSql(sb.ToString());
                            if (num == 1)
                            {
                                return "学科更新成功";
                            }
                            else
                            {
                                return "学科更新失败";
                            }
                        }
                    }

                }
                else
                {
                    return "没有要更新的学科";
                }
            }
            else
            {

            }
            return res;   
        }
        /// <summary>  
        /// 将c# DateTime时间格式转换为Unix时间戳格式  
        /// </summary>  
        /// <param name="time">时间</param>  
        /// <returns>long</returns>  
        public static long ConvertDateTimeToTimeSpan(System.DateTime time)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1, 0, 0, 0, 0));
            long t = (time.Ticks - startTime.Ticks) / 10000;   //除10000调整为13位      
            return t;
        }

    }
}