﻿/* ==============================================================================
  * 功能描述：SubjectTree  
  * 创 建 者：Fubz
  * 创建日期：2014/8/1 14:19:38
  * ==============================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace EduMethods
{
    /// <summary>
    /// SubjectTree
    /// </summary>
    public class SubjectTree
    {
        /// <summary>
        /// 日志文件记录
        /// </summary>
        private static Common.Log log = new Common.Log();

        /// <summary>
        /// 获取教材树
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string getSubjectTreeList(string id)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt = null;
            ArrayList dataAll = null;
            try
            {
                if (id == null || id.Length == 0)
                {
                    sb.Append(" select SB_GUID as ID,SB_SubjectName as name from dbo.Subject_Base where State=1 order by SB_SN ");
                    dt = DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
                }
                else
                {
                    sb.Append(" SELECT BB_GUID as ID,BB_Name as name ");
                    sb.Append(" FROM [dbo].[Book_Base] ");
                    sb.Append(" where BB_PGUID=@PID order by BB_Index");
                    SqlParameter[] parameters = {
					new SqlParameter("@PID", SqlDbType.UniqueIdentifier,16)			};
                    parameters[0].Value = new Guid(id);
                    dt = DBUtility.DbHelperSQL.Query(sb.ToString(), CommonString.CnCodeDb, parameters).Tables[0];
                    if (dt == null || dt.Rows.Count == 0)
                    {
                        sb.Remove(0, sb.Length);
                        sb.Append(" SELECT BB_GUID as ID,BB_Name as name ");
                        sb.Append(" FROM [dbo].[Book_Base] ");
                        sb.Append(" where BB_PGUID is null and BB_SubjectGUID=@PID order by BB_Index");
                        dt = DBUtility.DbHelperSQL.Query(sb.ToString(), CommonString.CnCodeDb, parameters).Tables[0];
                    }
                }
                sb.Remove(0, sb.Length);
                sb.Append(" SELECT BB_GUID from [dbo].[Book_Base] where BB_PGUID=@PID ");
                SqlParameter[] pars = {
					new SqlParameter("@PID", SqlDbType.UniqueIdentifier,16)			};
                dataAll = Common.Common.DataTable2ArrayList(dt);
                DataTable dtTemp = null;
                for (int i = 0; i < dataAll.Count; i++)
                {
                    Hashtable node = (Hashtable)dataAll[i];
                    if (id == null || id.Length == 0)
                    {
                        DataTable dtBook = DBUtility.DbHelperSQL.Query("select BB_GUID as id from [dbo].[Book_Base] where BB_Name='" + node["name"].ToString() + "' ", CommonString.CnCodeDb).Tables[0];
                        if (dtBook != null && dtBook.Rows.Count > 0)
                            pars[0].Value = new Guid(dtBook.Rows[0]["ID"].ToString());
                        else
                            continue;
                    }
                    else
                        pars[0].Value = new Guid(node["ID"].ToString());
                    dtTemp = DBUtility.DbHelperSQL.Query(sb.ToString(), CommonString.CnCodeDb, pars).Tables[0];
                    if (dtTemp != null && dtTemp.Rows.Count > 0)
                        node["hasChildren"] = true;
                    else
                        node["hasChildren"] = false;
                }
            }
            catch (System.Exception ex)
            {
                log.Write("获取教材树失败!", ex);
            }
            return Common.JSON.Encode(dataAll);
        }

        /// <summary>
        /// 获取学校管理员列表信息
        /// </summary>
        /// <param name="login">当前登录信息</param>
        /// <param name="key">关键字</param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public static string getSubjectManage(LoginInfo login, string key, int page, int size, string order)
        {
            StringBuilder sb = new StringBuilder();
            Hashtable result = new Hashtable();
            sb.Append(" select ts.TS_SN,ub.UB_UserID,ub.UB_UserName,sb.SB_SubjectName,ts.IsCheck from dbo.Teacher_Subject ts  ");
            sb.Append(" inner join dbo.Subject_Base sb on sb.SB_GUID=ts.TS_SubjectBaseGUID  ");
            sb.Append(" inner join dbo.User_Base ub on ub.UB_GUID=ts.TS_TeacherGUID ");
            sb.Append(" where ts.State=1 and sb.state=1 and ub.state=1 and ");
            sb.Append(" ts.TS_UnitGUID='" + login.UnitGuid + "' ");
            sb.Append(" and  " + key);
            if (order.Length > 0)
            {
                sb.Append(" order by  " + order);
            }
            else
            {
                sb.Append(" order by  ts.TS_SN desc  ");
            }
            try
            {
                DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
                int count = dt.Rows.Count;
                result["totalCount"] = count;
                if (count == 0)
                {
                    result["Data"] = "";
                    result["currPage"] = page;
                    result["pageSize"] = size;
                    return Common.JSON.Encode(result);
                }
                count = count % size == 0 ? count / size : (count / size + 1);
                count = count == 0 ? 1 : count;
                if (count == 1)
                {
                    page = 1;
                }
                if (page > count)
                    page = count;
                var query = from aaa in dt.AsEnumerable()
                            select aaa;
                var selectData = query.Skip((page - 1) * size).Take(size);
                dt = selectData.CopyToDataTable<DataRow>();
                ArrayList dataAll = Common.Common.DataTable2ArrayList(dt);
                result["Data"] = dataAll;
                result["currPage"] = page;
                result["pageSize"] = size;
            }
            catch (System.Exception ex)
            {
                log.Write("获取单位学科管理员列表信息失败", ex);
            }

            return Common.JSON.Encode(result);
        }

        /// <summary>
        /// 获取科目列表数据
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public static string getSubjectTypeList(LoginInfo login)
        {
            StringBuilder sb = new StringBuilder();
            Hashtable result = new Hashtable();
            sb.Append(" select GradeState from [dbo].[Unit_Base] where UB_GUID='" + login.UnitGuid + "' ");
            DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
            int grade = 0;
            if (dt.Rows.Count > 0 && dt.Rows[0][0].ToString().Length > 0)
            {
                grade = int.Parse(dt.Rows[0][0].ToString());
                sb.Remove(0, sb.Length);
                sb.Append(" select SB_SN as value,SB_SubjectName as text from dbo.Subject_Base ");
                sb.Append(" where State=1 and GradeState in (" + Common.Common.getUnitGradeString(grade) + ") ");
                sb.Append(" order by SB_SN ");
                dt = DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
                DataTable dtTemp = dt.Clone();
                dtTemp.Rows.Add(new object[] { 0, "--全部--" });
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dtTemp.Rows.Add(new object[] { int.Parse(dt.Rows[i]["value"].ToString()), dt.Rows[i]["text"].ToString() });
                }
                ArrayList dataAll = Common.Common.DataTable2ArrayList(dtTemp);
                return Common.JSON.Encode(dataAll);
            }
            else
                return Common.JSON.Encode(result);
        }

    }
}