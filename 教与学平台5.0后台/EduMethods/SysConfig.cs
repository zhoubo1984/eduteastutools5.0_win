﻿/* ==============================================================================
  * 功能描述：SysConfig  
  * 创 建 者：Fubz
  * 创建日期：2014/7/31 16:30:47
  * ==============================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;


namespace EduMethods
{
    /// <summary>
    /// 系统配置 
    /// </summary>
    public class SysConfig
    {
        /// <summary>
        /// 日志文件记录
        /// </summary>
        private static Common.Log log = new Common.Log();
        /// <summary>
        /// 获取单位列表信息
        /// </summary>
        /// <param name="login">当前登录用户</param>
        /// <param name="key">关键字</param>
        /// <param name="page">当前页</param>
        /// <param name="size">页大小</param>
        /// <param name="order">排序</param>
        /// <returns></returns>
        public static string getSubjectList(LoginInfo login, string key, int page, int size, string order)
        {
            StringBuilder sb = new StringBuilder();
            Hashtable result = new Hashtable();
            sb.Append(" SELECT SB_SN,SB_GUID,SB_SubjectName,GradeState,State FROM [dbo].[Subject_Base] sb ");
            sb.Append(" where state>-1 ");
            sb.Append(key);
            if (order.Length > 0)
            {
                sb.Append(" order by  " + order);
            }
            else
            {
                sb.Append(" order by SB_SN desc ");
            }
            try
            {
                DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
                int count = dt.Rows.Count;
                result["totalCount"] = count;
                if (count == 0)
                {
                    result["Data"] = "";
                    result["currPage"] = page;
                    result["pageSize"] = size;
                    return Common.JSON.Encode(result);
                }
                count = count % size == 0 ? count / size : (count / size + 1);
                count = count == 0 ? 1 : count;
                if (count == 1)
                {
                    page = 1;
                }
                if (page > count)
                    page = count;
                var query = from aaa in dt.AsEnumerable()
                            select aaa;
                var selectData = query.Skip((page - 1) * size).Take(size);
                dt = selectData.CopyToDataTable<DataRow>();
                ArrayList dataAll = Common.Common.DataTable2ArrayList(dt);
                for (int i = 0; i < dataAll.Count; i++)
                {
                    Hashtable node = (Hashtable)dataAll[i];
                    Grade gd = new Grade();
                    gd.GradeText = Common.Common.getMyUnitGradeType(int.Parse(node["GradeState"].ToString()));
                    gd.GradeValue = int.Parse(node["GradeState"].ToString());
                    node["Grade"] = gd;
                }

                result["Data"] = dataAll;
                result["currPage"] = page;
                result["pageSize"] = size;
            }
            catch (System.Exception ex)
            {
                log.Write("获取单位列表信息失败", ex);
            }

            return Common.JSON.Encode(result);
        }


    }

}