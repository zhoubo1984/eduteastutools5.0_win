﻿/* ==============================================================================
  * 功能描述：TestData  
  * 创 建 者：Fubz
  * 创建日期：2014/7/10 11:05:22
  * ==============================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace EduMethods
{
    /// <summary>
    /// TestData
    /// </summary>
    public class TestData
    {
        /// <summary>
        /// 日志文件记录
        /// </summary>
        private static Common.Log log = new Common.Log();

        /// <summary>
        /// 获取测评列表数据信息
        /// </summary>
        /// <param name="key"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public static string getEvaluationList(int page, int size, string order)
        {
            StringBuilder sb = new StringBuilder();
            Hashtable result = new Hashtable();
            sb.Append(" select e.SN,e.Eva_Name,e.Eva_CreateTime,cb.CodeValue,e.IsUse, ");
            sb.Append(" t.T_Name,ec.Complexity_Name from dbo.Evaluatin e ");
            sb.Append(" inner join dbo.Code_BasicInfo cb on cb.CodeID=e.Eva_UseSubject ");
            sb.Append(" inner join dbo.Teacher_Base t on t.T_GUID=e.Eva_CreateUser "); //
            sb.Append(" inner join dbo.Evaluation_Complexity ec on ec.Complexity_Guid=e.Eva_ComplexGuid ");
            //判断逻辑，若是管理员显示所有，教师的话只显示自己创建的测评信息
            sb.Append(" where 1=1 ");
            if (order.Length > 0)
            {
                sb.Append(" order by  " + order);
            }
            else
            {
                sb.Append(" order by e.Eva_CreateTime desc ");
            }
            try
            {
                DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
                int count = dt.Rows.Count;
                result["totalCount"] = count;
                count = count % size == 0 ? count / size : (count / size + 1);
                count = count == 0 ? 1 : count;
                if (count == 1)
                {
                    page = 1;
                }
                if (page > count)
                    page = count;
                var query = from aaa in dt.AsEnumerable()
                            select aaa;
                var selectData = query.Skip((page - 1) * size).Take(size);
                dt = selectData.CopyToDataTable<DataRow>();
                ArrayList dataAll = Common.Common.DataTable2ArrayList(dt);
                result["Data"] = dataAll;
                result["currPage"] = page;
                result["pageSize"] = size;
            }
            catch (System.Exception ex)
            {
                log.Write("获取测评列表信息失败", ex);
            }

            return Common.JSON.Encode(result);
        }

        /// <summary>
        /// 获取树节点
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string getTreeDataList(string id)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt = null;
            try
            {
                sb.Append(" select ID,PID,TextBookName,IsLeaf from " + Common.ConfigHelper.GetConfigString("BaseData") + ".dbo.Edu_Book where PID=@pid ");
                SqlParameter[] parameters = {
					new SqlParameter("@pid", SqlDbType.NVarChar,200)};
                parameters[0].Value = id == null ? "" : id;
                dt = DBUtility.DbHelperSQL.Query(sb.ToString(), parameters).Tables[0];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["IsLeaf"].ToString() == "True")
                    {
                        dt.Rows[i]["IsLeaf"] = false;
                    }
                    else
                    {
                        dt.Rows[i]["IsLeaf"] = true;
                    }

                }
            }
            catch (System.Exception ex)
            {
                log.Write("获取教材目录节点信息失败！", ex);
            }

            return Common.JSON.Encode(Common.Common.DataTable2ArrayList(dt));
        }

    }
}