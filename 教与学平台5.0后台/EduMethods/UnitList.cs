﻿/* ==============================================================================
  * 功能描述：UnitList  
  * 创 建 者：Fubz
  * 创建日期：2014/7/14 17:24:04
  * ==============================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace EduMethods
{
    /// <summary>
    /// UnitList
    /// </summary>
    public class UnitList
    {
        /// <summary>
        /// 日志文件记录
        /// </summary>
        private static Common.Log log = new Common.Log();

        /// <summary>
        /// 获取单位列表信息
        /// </summary>
        /// <param name="login">当前登录用户</param>
        /// <param name="key">关键字</param>
        /// <param name="page">当前页</param>
        /// <param name="size">页大小</param>
        /// <param name="order">排序</param>
        /// <returns></returns>
        public static string getUnitList(LoginInfo login, string key, int page, int size, string order)
        {
            StringBuilder sb = new StringBuilder();
            Hashtable result = new Hashtable();
            sb.Append(" select UB_SN,UB_GUID,UB_IsCheck,UB_Name,GradeState,UB_DBIndex from dbo.Unit_Base ub ");
            sb.Append(" where 1=1 and State=1  ");
            sb.Append(" and UB_PGUID=@UB_PGUID ");
            if (order.Length > 0)
            {
                sb.Append(" order by  " + order);
            }
            else
            {
                sb.Append(" order by UB_Index  ");
            }

            try
            {
                SqlParameter[] pars = {
					new SqlParameter("@UB_PGUID", SqlDbType.UniqueIdentifier,16)};
                pars[0].Value = new Guid(login.UnitGuid);

                DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString(), pars).Tables[0];
                int count = dt.Rows.Count;
                result["totalCount"] = count;
                if (count == 0)
                {
                    result["Data"] = "";
                    result["currPage"] = page;
                    result["pageSize"] = size;
                    return Common.JSON.Encode(result);
                }
                count = count % size == 0 ? count / size : (count / size + 1);
                count = count == 0 ? 1 : count;
                if (count == 1)
                {
                    page = 1;
                }
                if (page > count)
                    page = count;
                var query = from aaa in dt.AsEnumerable()
                            select aaa;
                var selectData = query.Skip((page - 1) * size).Take(size);
                dt = selectData.CopyToDataTable<DataRow>();
                ArrayList dataAll = Common.Common.DataTable2ArrayList(dt);
                for (int i = 0; i < dataAll.Count; i++)
                {
                    Hashtable node = (Hashtable)dataAll[i];
                    Grade gd = new Grade();
                    gd.GradeText = Common.Common.getMyUnitGradeType(int.Parse(node["GradeState"].ToString()));
                    gd.GradeValue = int.Parse(node["GradeState"].ToString());
                    node["Grade"] = gd;
                }

                result["Data"] = dataAll;
                result["currPage"] = page;
                result["pageSize"] = size;
            }
            catch (System.Exception ex)
            {
                log.Write("获取单位列表信息失败", ex);
            }

            return Common.JSON.Encode(result);
        }

        /// <summary>
        /// 获取用于选取的下拉单位列表信息
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public static string getUnitDataList(LoginInfo login)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt = null;
            sb.Append(" select UB_GUID,UB_Name from dbo.Unit_Base where State=1 order by UB_SN desc ");
            try
            {
                dt = DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
            }
            catch (System.Exception ex)
            {
                log.Write("获取单位列表信息失败", ex);
            }

            return Common.JSON.Encode(Common.Common.DataTable2ArrayList(dt));
        }


        /// <summary>
        /// 获取单位班级列表信息
        /// </summary>
        /// <param name="login">当前登录用户</param>
        /// <param name="key">关键字</param>
        /// <param name="page">当前页</param>
        /// <param name="size">页大小</param>
        /// <param name="order">排序</param>
        /// <returns></returns>
        public static string getUnitClassList(LoginInfo login, string key, int page, int size, string order)
        {
            StringBuilder sb = new StringBuilder();
            Hashtable result = new Hashtable();
            sb.Append(" select CB_GUID,CB_StartYear,CB_Name,GradeState,State from dbo.Class_Base ");
            sb.Append(" where 1=1 and State>-1 and CB_UnitGUID='" + login.UnitGuid + "'  ");
            sb.Append(" and  " + key);
            if (order.Length > 0)
            {
                sb.Append(" order by  " + order);
            }
            else
            {
                sb.Append(" order by CB_StartYear,CB_Name  ");
            }
            try
            {
                DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
                int count = dt.Rows.Count;
                result["totalCount"] = count;
                if (count == 0)
                {
                    result["Data"] = "";
                    result["currPage"] = page;
                    result["pageSize"] = size;
                    return Common.JSON.Encode(result);
                }
                count = count % size == 0 ? count / size : (count / size + 1);
                count = count == 0 ? 1 : count;
                if (count == 1)
                {
                    page = 1;
                }
                if (page > count)
                    page = count;
                var query = from aaa in dt.AsEnumerable()
                            select aaa;
                var selectData = query.Skip((page - 1) * size).Take(size);
                dt = selectData.CopyToDataTable<DataRow>();
                ArrayList dataAll = Common.Common.DataTable2ArrayList(dt);
                result["Data"] = dataAll;
                result["currPage"] = page;
                result["pageSize"] = size;
            }
            catch (System.Exception ex)
            {
                log.Write("获取单位班级列表信息失败", ex);
            }

            return Common.JSON.Encode(result);
        }

    }

    public class Grade
    {
        public int GradeValue { get; set; }
        public string GradeText { get; set; }
    }
}