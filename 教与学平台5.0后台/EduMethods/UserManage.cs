﻿/* ==============================================================================
  * 功能描述：UserManage  
  * 创 建 者：Fubz
  * 创建日期：2014/7/17 11:27:42
  * ==============================================================================
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace EduMethods
{
    /// <summary>
    /// UserManage
    /// </summary>
    public class UserManage
    {

        /// <summary>
        /// 日志文件记录
        /// </summary>
        private static Common.Log log = new Common.Log();

        /// <summary>
        /// 获取学校管理员列表信息
        /// </summary>
        /// <param name="login">当前登录信息</param>
        /// <param name="key">关键字</param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public static string getUnitUserManage(LoginInfo login, string key, int page, int size, string order)
        {
            StringBuilder sb = new StringBuilder();
            Hashtable result = new Hashtable();
            sb.Append(" select sm.SM_SN,sm.SM_ManageName,sm.SM_ManageID,sm.SM_GUID,sm.SM_State,ub.UB_Name ");
            sb.Append(" from dbo.System_Manage sm   ");
            sb.Append(" inner join dbo.Unit_Base ub on ub.UB_GUID=sm.SM_UBGuid ");
            sb.Append(" where ub.State=1 and sm.SM_State>-1 ");
            if (key != null && key.Length > 0)
            {
                sb.Append(key);
            }
            if (order.Length > 0)
            {
                sb.Append(" order by  " + order);
            }
            else
            {
                sb.Append(" order by sm.SM_SN desc  ");
            }
            try
            {
                DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
                int count = dt.Rows.Count;
                result["totalCount"] = count;
                if (count == 0)
                {
                    result["Data"] = "";
                    result["currPage"] = page;
                    result["pageSize"] = size;
                    return Common.JSON.Encode(result);
                }
                count = count % size == 0 ? count / size : (count / size + 1);
                count = count == 0 ? 1 : count;
                if (count == 1)
                {
                    page = 1;
                }
                if (page > count)
                    page = count;
                var query = from aaa in dt.AsEnumerable()
                            select aaa;
                var selectData = query.Skip((page - 1) * size).Take(size);
                dt = selectData.CopyToDataTable<DataRow>();
                ArrayList dataAll = Common.Common.DataTable2ArrayList(dt);
                result["Data"] = dataAll;
                result["currPage"] = page;
                result["pageSize"] = size;
            }
            catch (System.Exception ex)
            {
                log.Write("获取单位管理员列表信息失败", ex);
            }

            return Common.JSON.Encode(result);
        }


        /// <summary>
        /// 获取学校教师列表信息
        /// </summary>
        /// <param name="login">当前登录信息</param>
        /// <param name="key">关键字</param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public static string getUnitTeacherList(LoginInfo login, string key, int page, int size, string order)
        {
            StringBuilder sb = new StringBuilder();
            Hashtable result = new Hashtable();
            sb.Append(" select ub.SN,ub.UB_GUID,ub.UB_UserID,ub.UB_UserName,ub.UB_Sex,ub.UB_RegDate,ub.GradeState,ub.State, ");
            sb.Append(" ut.TI_Mail,ut.TI_MobilePhone,ut.TI_QQ from dbo.User_Base ub    ");
            sb.Append(" left join dbo.U_TeacherInfo ut on ut.TI_GUID=ub.UB_GUID ");
            sb.Append(" where ub.UB_UserType=1  and ub.State>-1 "); //用户类型  1教师2学生3家长 0自由用户 4领导用户
            sb.Append(" and ub.UB_UnitGUID='" + login.UnitGuid + "' "); //单位
            if (key != null && key.Length > 0)
            {
                sb.Append(" and " + key);
            }
            if (order.Length > 0)
            {
                sb.Append(" order by  " + order);
            }
            else
            {
                sb.Append(" order by ub.SN desc  ");
            }
            try
            {
                DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
                int count = dt.Rows.Count;
                result["totalCount"] = count;
                count = count % size == 0 ? count / size : (count / size + 1);
                count = count == 0 ? 1 : count;
                if (count == 1)
                {
                    page = 1;
                }
                if (page > count)
                    page = count;
                var query = from aaa in dt.AsEnumerable()
                            select aaa;
                var selectData = query.Skip((page - 1) * size).Take(size);
                dt = selectData.CopyToDataTable<DataRow>();
                ArrayList dataAll = Common.Common.DataTable2ArrayList(dt);
                result["Data"] = dataAll;
                result["currPage"] = page;
                result["pageSize"] = size;
            }
            catch (System.Exception ex)
            {
                log.Write("获取单位教师列表信息失败", ex);
            }

            return Common.JSON.Encode(result);
        }

        /// <summary>
        /// 获取单位班级列表信息
        /// </summary>
        /// <param name="login">当前登录用户</param>
        /// <returns></returns>
        public static string getUnitClassList(LoginInfo login, string key)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt = null;
            sb.Append(" select CB_GUID as value,CB_StartYear+CB_Name as text from dbo.Class_Base ");
            sb.Append(" where 1=1 and State>-1 and CB_UnitGUID='" + login.UnitGuid + "'  ");
            sb.Append(" and " + key);
            sb.Append(" order by CB_StartYear,CB_Name  ");
            try
            {
                dt = DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
            }
            catch (System.Exception ex)
            {
                log.Write("获取单位班级列表信息失败", ex);
            }

            return Common.JSON.Encode(Common.Common.DataTable2ArrayList(dt));
        }


        /// <summary>
        /// 获取学校学生列表信息
        /// </summary>
        /// <param name="login">当前登录信息</param>
        /// <param name="key">关键字</param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public static string getUnitStudentList(LoginInfo login, string key, int page, int size, string order)
        {
            StringBuilder sb = new StringBuilder();
            Hashtable result = new Hashtable();
            sb.Append(" select ub.SN,ub.UB_GUID,cb.CB_GUID,ub.UB_UserID,ub.UB_UserName,ub.UB_Sex,ub.UB_RegDate,ub.GradeState,ub.State, ");
            sb.Append(" ut.SI_Mail,ut.SI_QQ,(cb.CB_StartYear+cb.CB_Name) as className from dbo.User_Base ub    ");
            sb.Append(" left join dbo.U_StudentInfo ut on ut.SI_GUID=ub.UB_GUID ");
            sb.Append(" left join dbo.Student_Class sc on (sc.SC_StudentGUID=ub.UB_GUID and sc.state=1 )");
            sb.Append(" left join dbo.Class_Base cb on (cb.CB_GUID=sc.SC_ClassGUID and cb.state=1 )");
            sb.Append(" where ub.UB_UserType=2 and ub.State>-1 "); //用户类型  1教师2学生3家长 0自由用户 4领导用户
            if (!getCheckUnitIsEdu(login.UnitGuid))  //判断是否为教育局
            {
                sb.Append(" and ub.UB_UnitGUID='" + login.UnitGuid + "' ");
            }
            if (key != null && key.Length > 0)
            {
                sb.Append(" and " + key);
            }
            if (order.Length > 0)
            {
                sb.Append(" order by  " + order);
            }
            else
            {
                sb.Append(" order by ub.SN desc  ");
            }
            try
            {
                DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];
                int count = dt.Rows.Count;
                result["totalCount"] = count;
                if (count == 0)
                {
                    result["Data"] = "";
                    result["currPage"] = page;
                    result["pageSize"] = size;
                    return Common.JSON.Encode(result);
                }
                count = count % size == 0 ? count / size : (count / size + 1);
                count = count == 0 ? 1 : count;
                if (count == 1)
                {
                    page = 1;
                }
                if (page > count)
                    page = count;
                var query = from aaa in dt.AsEnumerable()
                            select aaa;
                var selectData = query.Skip((page - 1) * size).Take(size);
                dt = selectData.CopyToDataTable<DataRow>();
                ArrayList dataAll = Common.Common.DataTable2ArrayList(dt);
                result["Data"] = dataAll;
                result["currPage"] = page;
                result["pageSize"] = size;
            }
            catch (System.Exception ex)
            {
                log.Write("获取单位学生列表信息失败", ex);
            }

            return Common.JSON.Encode(result);
        }

        /// <summary>
        /// 判断单位是或否为教育局
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        public static bool getCheckUnitIsEdu(string sid)
        {
            bool flag = false;
            if (sid != null && sid.Length > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(" select count(UB_SN) from dbo.Unit_Base where UB_PGUID=@UB_PGUID ");
                SqlParameter[] parameters = {
					new SqlParameter("@UB_PGUID", SqlDbType.UniqueIdentifier,16)			};
                parameters[0].Value = new Guid(sid);
                DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString(), parameters).Tables[0];
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (int.Parse(dt.Rows[0][0].ToString()) > 0)
                    {
                        flag = true;
                    }
                }
            }
            else
                flag = true;

            return flag;
        }


        /// <summary>
        /// 更新密码
        /// </summary>
        /// <param name="login"></param>
        /// <param name="oldPwd"></param>
        /// <param name="newPwd"></param>
        /// <returns></returns>
        public static string UpdateManagePwd(LoginInfo login, string oldPwd, string newPwd)
        {
            string result = string.Empty;
            try
            {
                EduBLL.System_Manage adBLL = new EduBLL.System_Manage();
                EduModel.System_Manage adModel = adBLL.GetModel(login.LoginSN);
                if (adModel.SM_ManagePsw == oldPwd)
                {
                    adModel.SM_ManagePsw = newPwd;
                    adBLL.Update(adModel);
                    result = "success";
                }
                else
                {
                    result = "pwd_error";
                }
            }
            catch (System.Exception ex)
            {
                log.Write("更新密码失败！", ex);
            }

            return result;
        }

    }
}