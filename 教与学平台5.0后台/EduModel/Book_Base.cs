﻿using System;

namespace EduModel
{
    /// <summary>
    /// 读取数据库对应的数据表 Book_Base生成实体类(电子书)
    /// </summary>
    public class Book_Base
    {

        /// <summary>
        /// 顺序号
        /// </summary>
        public int BB_SN { get; set; }

        /// <summary>
        /// 本表GUID
        /// </summary>
        public Guid BB_GUID { get; set; }

        /// <summary>
        /// 父GUID
        /// </summary>
        public Guid? BB_PGUID { get; set; }

        /// <summary>
        /// 教材名称
        /// </summary>
        public string BB_Name { get; set; }

        /// <summary>
        /// 教材路径
        /// </summary>
        public string BB_Path { get; set; }

        /// <summary>
        /// 教材索引
        /// </summary>
        public int BB_Index { get; set; }

        /// <summary>
        /// 学科GUID
        /// </summary>
        public Guid? BB_SubjectGUID { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public int? State { get; set; }

        /// <summary>
        /// 对应教材页数
        /// </summary>
        public int? BB_PageIndex { get; set; }

    }
}
