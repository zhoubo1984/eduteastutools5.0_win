﻿using System;
namespace EduModel
{
	/// <summary>
	/// 班级信息
	/// </summary>
	[Serializable]
	public partial class Class_Base
	{
		public Class_Base()
		{}
		#region Model
		private int _cb_sn;
		private Guid _cb_guid;
		private Guid _cb_unitguid;
		private string _cb_startyear;
		private string _cb_name;
		private int _gradestate;
		private int _state=1;
		private int _cb_index=0;
		/// <summary>
		/// 顺序号
		/// </summary>
		public int CB_SN
		{
			set{ _cb_sn=value;}
			get{return _cb_sn;}
		}
		/// <summary>
		/// 本表GUID
		/// </summary>
		public Guid CB_GUID
		{
			set{ _cb_guid=value;}
			get{return _cb_guid;}
		}
		/// <summary>
		/// 对应单位GUID
		/// </summary>
		public Guid CB_UnitGUID
		{
			set{ _cb_unitguid=value;}
			get{return _cb_unitguid;}
		}
		/// <summary>
		/// 入学年份即哪一届
		/// </summary>
		public string CB_StartYear
		{
			set{ _cb_startyear=value;}
			get{return _cb_startyear;}
		}
		/// <summary>
		/// 班级名称
		/// </summary>
		public string CB_Name
		{
			set{ _cb_name=value;}
			get{return _cb_name;}
		}
		/// <summary>
		/// 学段信息 1 小学 2初中 4高中
		/// </summary>
		public int GradeState
		{
			set{ _gradestate=value;}
			get{return _gradestate;}
		}
		/// <summary>
		/// 状态 1 启用 0关闭
		/// </summary>
		public int State
		{
			set{ _state=value;}
			get{return _state;}
		}
		/// <summary>
		/// 排序字段
		/// </summary>
		public int CB_Index
		{
			set{ _cb_index=value;}
			get{return _cb_index;}
		}
		#endregion Model

	}
}

