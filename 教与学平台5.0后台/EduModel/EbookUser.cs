﻿using System;
namespace EduModel
{
	/// <summary>
	/// 班级信息
	/// </summary>
	[Serializable]
	public partial class EbookUser
	{
		public EbookUser()
		{}
		#region Model
		private int _sn;
		private string _eb_id;
		private string _eb_name;
		private bool _isuse;
		private string _eb_psw;
		private string _eb_accountid;
		/// <summary>
		/// 
		/// </summary>
		public int SN
		{
			set{ _sn=value;}
			get{return _sn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EB_ID
		{
			set{ _eb_id=value;}
			get{return _eb_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EB_Name
		{
			set{ _eb_name=value;}
			get{return _eb_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool IsUse
		{
			set{ _isuse=value;}
			get{return _isuse;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EB_Psw
		{
			set{ _eb_psw=value;}
			get{return _eb_psw;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EB_AccountID
		{
			set{ _eb_accountid=value;}
			get{return _eb_accountid;}
		}
		#endregion Model

	}
}

