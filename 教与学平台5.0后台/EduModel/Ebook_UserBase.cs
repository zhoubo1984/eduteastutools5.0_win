﻿using System;
namespace EduModel
{
	/// <summary>
	/// 班级信息
	/// </summary>
	[Serializable]
	public partial class Ebook_UserBase
	{
		public Ebook_UserBase()
		{}
		#region Model
		private int _sn;
		private Guid _user_guid;
		private string _eb_userid;
		/// <summary>
		/// 
		/// </summary>
		public int SN
		{
			set{ _sn=value;}
			get{return _sn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid User_Guid
		{
			set{ _user_guid=value;}
			get{return _user_guid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Eb_UserID
		{
			set{ _eb_userid=value;}
			get{return _eb_userid;}
		}
		#endregion Model

	}
}

