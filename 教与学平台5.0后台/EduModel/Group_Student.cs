﻿using System;
namespace EduModel
{
	/// <summary>
	/// 班级信息
	/// </summary>
	[Serializable]
	public partial class Group_Student
	{
		public Group_Student()
		{}
		#region Model
		private int _gs_sn;
		private Guid _gs_guid;
		private Guid _gs_tscgguid;
		private Guid _gs_studentguid;
		private bool _gs_isleader;
		/// <summary>
		/// 
		/// </summary>
		public int GS_SN
		{
			set{ _gs_sn=value;}
			get{return _gs_sn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid GS_Guid
		{
			set{ _gs_guid=value;}
			get{return _gs_guid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid GS_TSCGGuid
		{
			set{ _gs_tscgguid=value;}
			get{return _gs_tscgguid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid GS_StudentGuid
		{
			set{ _gs_studentguid=value;}
			get{return _gs_studentguid;}
		}
		/// <summary>
		/// 是否组长
		/// </summary>
		public bool GS_IsLeader
		{
			set{ _gs_isleader=value;}
			get{return _gs_isleader;}
		}
		#endregion Model

	}
}

