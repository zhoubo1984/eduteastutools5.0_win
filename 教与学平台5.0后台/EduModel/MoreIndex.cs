﻿using System;
namespace EduModel
{
	/// <summary>
	/// 班级信息
	/// </summary>
	[Serializable]
	public partial class MoreIndex
	{
		public MoreIndex()
		{}
		#region Model
		private int _sn;
		private Guid _indexguid;
		private Guid _userguid;
		private Guid _unitguid;
		private string _indextypename;
		private string _indexname;
		private string _indexhref;
		private int? _state;
		private byte[] _indeximage;
		/// <summary>
		/// 编号
		/// </summary>
		public int SN
		{
			set{ _sn=value;}
			get{return _sn;}
		}
		/// <summary>
		/// 索引GUID标示
		/// </summary>
		public Guid IndexGuid
		{
			set{ _indexguid=value;}
			get{return _indexguid;}
		}
		/// <summary>
		/// 用户GUID
		/// </summary>
		public Guid UserGuid
		{
			set{ _userguid=value;}
			get{return _userguid;}
		}
		/// <summary>
		/// 单位GUID
		/// </summary>
		public Guid UnitGuid
		{
			set{ _unitguid=value;}
			get{return _unitguid;}
		}
		/// <summary>
		/// 索引类型名称
		/// </summary>
		public string IndexTypeName
		{
			set{ _indextypename=value;}
			get{return _indextypename;}
		}
		/// <summary>
		/// 索引名称
		/// </summary>
		public string IndexName
		{
			set{ _indexname=value;}
			get{return _indexname;}
		}
		/// <summary>
		/// 索引链接
		/// </summary>
		public string IndexHref
		{
			set{ _indexhref=value;}
			get{return _indexhref;}
		}
		/// <summary>
		/// 标示，1教育局，2单位，3教师
		/// </summary>
		public int? State
		{
			set{ _state=value;}
			get{return _state;}
		}
		/// <summary>
		/// 
		/// </summary>
		public byte[] IndexImage
		{
			set{ _indeximage=value;}
			get{return _indeximage;}
		}
		#endregion Model

	}
}

