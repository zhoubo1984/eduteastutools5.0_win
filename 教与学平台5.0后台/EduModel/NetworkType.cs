﻿using System;
namespace EduModel
{
	/// <summary>
	/// 班级信息
	/// </summary>
	[Serializable]
	public partial class NetworkType
	{
		public NetworkType()
		{}
		#region Model
		private int _nt_id;
		private string _nt_name;
		/// <summary>
		/// 网络类型ID  默认0，不进行选择，则默认0
		/// </summary>
		public int NT_ID
		{
			set{ _nt_id=value;}
			get{return _nt_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string NT_Name
		{
			set{ _nt_name=value;}
			get{return _nt_name;}
		}
		#endregion Model

	}
}

