﻿using System;
namespace EduModel
{
    /// <summary>
    /// Notice:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Notice
    {
        public Notice()
        { }
        #region Model
        private int _sn;
        private Guid _n_guid;
        private string _n_title;
        private string _n_content;
        private DateTime? _n_createdate = DateTime.Now;
        private Guid _n_userguid;
        private Guid _n_unitguid;
        private DateTime? _n_publishdate = DateTime.Now;
        private bool _n_ispublish;
        /// <summary>
        /// 
        /// </summary>
        public int SN
        {
            set { _sn = value; }
            get { return _sn; }
        }
        /// <summary>
        /// 标示位
        /// </summary>
        public Guid N_GUID
        {
            set { _n_guid = value; }
            get { return _n_guid; }
        }
        /// <summary>
        /// 新闻标题
        /// </summary>
        public string N_Title
        {
            set { _n_title = value; }
            get { return _n_title; }
        }
        /// <summary>
        /// 内容
        /// </summary>
        public string N_Content
        {
            set { _n_content = value; }
            get { return _n_content; }
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? N_CreateDate
        {
            set { _n_createdate = value; }
            get { return _n_createdate; }
        }
        /// <summary>
        /// 添加用户
        /// </summary>
        public Guid N_UserGUID
        {
            set { _n_userguid = value; }
            get { return _n_userguid; }
        }
        /// <summary>
        /// 添加单位
        /// </summary>
        public Guid N_UnitGUID
        {
            set { _n_unitguid = value; }
            get { return _n_unitguid; }
        }
        /// <summary>
        /// 发布时间
        /// </summary>
        public DateTime? N_PublishDate
        {
            set { _n_publishdate = value; }
            get { return _n_publishdate; }
        }
        /// <summary>
        /// 是否发布
        /// </summary>
        public bool N_IsPublish
        {
            set { _n_ispublish = value; }
            get { return _n_ispublish; }
        }
        #endregion Model

    }
}

