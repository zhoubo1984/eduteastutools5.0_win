﻿using System;
namespace EduModel
{
    /// <summary>
    /// Picture:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Picture
    {
        public Picture()
        { }
        #region Model
        private int _sn;
        private string _p_title;
        private string _p_path;
        private DateTime? _p_upload_date = DateTime.Now;
        private Guid _p_upload_userguid;
        private Guid _p_unitguid;
        private bool _p_isshow = true;
        private string _p_href;
        /// <summary>
        /// 
        /// </summary>
        public int SN
        {
            set { _sn = value; }
            get { return _sn; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string P_Title
        {
            set { _p_title = value; }
            get { return _p_title; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string P_Href
        {
            set { _p_href = value; }
            get { return _p_href; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string P_Path
        {
            set { _p_path = value; }
            get { return _p_path; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? P_Upload_Date
        {
            set { _p_upload_date = value; }
            get { return _p_upload_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Guid P_Upload_UserGuid
        {
            set { _p_upload_userguid = value; }
            get { return _p_upload_userguid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Guid P_UnitGuid
        {
            set { _p_unitguid = value; }
            get { return _p_unitguid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool P_IsShow
        {
            set { _p_isshow = value; }
            get { return _p_isshow; }
        }
        #endregion Model

    }
}

