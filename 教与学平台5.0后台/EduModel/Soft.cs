﻿using System;
namespace EduModel
{
	/// <summary>
	/// Soft:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Soft
	{
		public Soft()
		{}
		#region Model
		private int _sn;
		private Guid _s_guid;
		private string _s_name;
		private string _s_content;
		private string _s_index;
		private DateTime? _s_date;
		private Guid _s_userguid;
		private Guid _s_unitguid;
		/// <summary>
		/// 
		/// </summary>
		public int SN
		{
			set{ _sn=value;}
			get{return _sn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid S_GUID
		{
			set{ _s_guid=value;}
			get{return _s_guid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string S_Name
		{
			set{ _s_name=value;}
			get{return _s_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string S_Content
		{
			set{ _s_content=value;}
			get{return _s_content;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string S_Index
		{
			set{ _s_index=value;}
			get{return _s_index;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? S_Date
		{
			set{ _s_date=value;}
			get{return _s_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid S_UserGUID
		{
			set{ _s_userguid=value;}
			get{return _s_userguid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid S_UnitGUID
		{
			set{ _s_unitguid=value;}
			get{return _s_unitguid;}
		}
		#endregion Model

	}
}

