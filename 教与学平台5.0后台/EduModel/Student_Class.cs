﻿using System;
namespace EduModel
{
	/// <summary>
	/// 学生班级信息
	/// </summary>
	[Serializable]
	public partial class Student_Class
	{
		public Student_Class()
		{}
		#region Model
		private int _sc_sn;
		private Guid _sc_guid;
		private Guid _sc_studentguid;
		private Guid _sc_classguid;
		private int? _state;
		/// <summary>
		/// 顺序号
		/// </summary>
		public int SC_SN
		{
			set{ _sc_sn=value;}
			get{return _sc_sn;}
		}
		/// <summary>
		/// 本表GUID
		/// </summary>
		public Guid SC_GUID
		{
			set{ _sc_guid=value;}
			get{return _sc_guid;}
		}
		/// <summary>
		/// 对应学生GUID
		/// </summary>
		public Guid SC_StudentGUID
		{
			set{ _sc_studentguid=value;}
			get{return _sc_studentguid;}
		}
		/// <summary>
		/// 对应班级GUID
		/// </summary>
		public Guid SC_ClassGUID
		{
			set{ _sc_classguid=value;}
			get{return _sc_classguid;}
		}
		/// <summary>
		/// 0 不启用 1启用 -1删除
		/// </summary>
		public int? State
		{
			set{ _state=value;}
			get{return _state;}
		}
		#endregion Model

	}
}

