﻿using System;
namespace EduModel
{
	/// <summary>
	/// 学科信息
	/// </summary>
	[Serializable]
	public partial class Subject_Base
	{
		public Subject_Base()
		{}
		#region Model
		private int _sb_sn;
		private Guid _sb_guid;
		private string _sb_subjectname;
		private int _gradestate;
		private int _state=1;
		private int _sb_index;
		/// <summary>
		/// 顺序号
		/// </summary>
		public int SB_SN
		{
			set{ _sb_sn=value;}
			get{return _sb_sn;}
		}
		/// <summary>
		/// 本表GUID
		/// </summary>
		public Guid SB_GUID
		{
			set{ _sb_guid=value;}
			get{return _sb_guid;}
		}
		/// <summary>
		/// 学科名称
		/// </summary>
		public string SB_SubjectName
		{
			set{ _sb_subjectname=value;}
			get{return _sb_subjectname;}
		}
		/// <summary>
		/// 学段 1,2,4分别代表：小学,初中,高中
		/// </summary>
		public int GradeState
		{
			set{ _gradestate=value;}
			get{return _gradestate;}
		}
		/// <summary>
		/// 0 不启用 1启用
		/// </summary>
		public int State
		{
			set{ _state=value;}
			get{return _state;}
		}
		/// <summary>
		/// 排序顺序
		/// </summary>
		public int SB_Index
		{
			set{ _sb_index=value;}
			get{return _sb_index;}
		}
		#endregion Model

	}
}

