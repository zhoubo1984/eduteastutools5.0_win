﻿using System;
namespace EduModel
{
	/// <summary>
	/// 学科信息
	/// </summary>
	[Serializable]
	public partial class Subject_Base_Ext
	{
		public Subject_Base_Ext()
		{}
		#region Model
		private int _sn;
		private Guid _sb_guid;
		private int _booktype=0;
		private string _normal;
		/// <summary>
		/// 序号
		/// </summary>
		public int SN
		{
			set{ _sn=value;}
			get{return _sn;}
		}
		/// <summary>
		/// 学科GUID
		/// </summary>
		public Guid SB_GUID
		{
			set{ _sb_guid=value;}
			get{return _sb_guid;}
		}
		/// <summary>
		/// 对应的网络教材类型
		/// </summary>
		public int BookType
		{
			set{ _booktype=value;}
			get{return _booktype;}
		}
		/// <summary>
		/// 只有：语文  数学  英语  物理  化学  生物  地理
		/// </summary>
		public string Normal
		{
			set{ _normal=value;}
			get{return _normal;}
		}
		#endregion Model

	}
}

