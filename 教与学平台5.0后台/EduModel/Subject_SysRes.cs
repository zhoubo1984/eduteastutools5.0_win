﻿using System;
namespace EduModel
{
	/// <summary>
	/// Subject_SysRes:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Subject_SysRes
	{
		public Subject_SysRes()
		{}
		#region Model
		private int _sn;
		private Guid _subguid;
		private Guid _syssubguid;
		private string _syssubname;
		/// <summary>
		/// 
		/// </summary>
		public int SN
		{
			set{ _sn=value;}
			get{return _sn;}
		}
		/// <summary>
		/// 学科GUID
		/// </summary>
		public Guid SubGuid
		{
			set{ _subguid=value;}
			get{return _subguid;}
		}
		/// <summary>
		/// 系统资源学科
		/// </summary>
		public Guid SysSubGuid
		{
			set{ _syssubguid=value;}
			get{return _syssubguid;}
		}
		/// <summary>
		/// 系统资源学科名称
		/// </summary>
		public string SysSubName
		{
			set{ _syssubname=value;}
			get{return _syssubname;}
		}
		#endregion Model

	}
}

