﻿using System;
namespace EduModel
{
	/// <summary>
	/// 1
	/// </summary>
	[Serializable]
	public partial class System_Config
	{
		public System_Config()
		{}
		#region Model
		private int _sc_sn;
		private string _sc_name;
		private string _sc_value;
		private string _sc_remark;
		/// <summary>
		/// 
		/// </summary>
		public int SC_SN
		{
			set{ _sc_sn=value;}
			get{return _sc_sn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SC_Name
		{
			set{ _sc_name=value;}
			get{return _sc_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SC_Value
		{
			set{ _sc_value=value;}
			get{return _sc_value;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SC_ReMark
		{
			set{ _sc_remark=value;}
			get{return _sc_remark;}
		}
		#endregion Model

	}
}

