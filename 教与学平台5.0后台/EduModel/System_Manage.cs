﻿using System;
namespace EduModel
{
	/// <summary>
	/// 1
	/// </summary>
	[Serializable]
	public partial class System_Manage
	{
		public System_Manage()
		{}
		#region Model
		private int _sm_sn;
		private Guid _sm_guid;
		private string _sm_manageid;
		private string _sm_managename;
		private string _sm_managepsw;
		private Guid _sm_ubguid;
		private int? _sm_state;
		private int? _sm_role=1;
		/// <summary>
		/// 
		/// </summary>
		public int SM_SN
		{
			set{ _sm_sn=value;}
			get{return _sm_sn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid SM_GUID
		{
			set{ _sm_guid=value;}
			get{return _sm_guid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SM_ManageID
		{
			set{ _sm_manageid=value;}
			get{return _sm_manageid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SM_ManageName
		{
			set{ _sm_managename=value;}
			get{return _sm_managename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SM_ManagePsw
		{
			set{ _sm_managepsw=value;}
			get{return _sm_managepsw;}
		}
		/// <summary>
		/// 单位GUID
		/// </summary>
		public Guid SM_UBGuid
		{
			set{ _sm_ubguid=value;}
			get{return _sm_ubguid;}
		}
		/// <summary>
		/// 管理员状态
		/// </summary>
		public int? SM_State
		{
			set{ _sm_state=value;}
			get{return _sm_state;}
		}
		/// <summary>
		/// 1:管理员 2：局长
		/// </summary>
		public int? SM_Role
		{
			set{ _sm_role=value;}
			get{return _sm_role;}
		}
		#endregion Model

	}
}

