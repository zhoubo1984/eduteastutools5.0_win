﻿using System;
namespace EduModel
{
	/// <summary>
	/// 1
	/// </summary>
	[Serializable]
	public partial class System_ServiceAddress
	{
		public System_ServiceAddress()
		{}
		#region Model
		private int _ss_sn;
		private string _ss_name;
		private int _ss_network;
		private string _ss_address;
		/// <summary>
		/// 序号
		/// </summary>
		public int SS_SN
		{
			set{ _ss_sn=value;}
			get{return _ss_sn;}
		}
		/// <summary>
		/// 服务名称
		/// </summary>
		public string SS_Name
		{
			set{ _ss_name=value;}
			get{return _ss_name;}
		}
		/// <summary>
		/// 网络类型 默认取的是0
		/// </summary>
		public int SS_NetWork
		{
			set{ _ss_network=value;}
			get{return _ss_network;}
		}
		/// <summary>
		/// 服务地址
		/// </summary>
		public string SS_Address
		{
			set{ _ss_address=value;}
			get{return _ss_address;}
		}
		#endregion Model

	}
}

