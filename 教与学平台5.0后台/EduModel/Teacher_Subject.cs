﻿using System;
namespace EduModel
{
	/// <summary>
	/// 教师学科信息
	/// </summary>
	[Serializable]
	public partial class Teacher_Subject
	{
		public Teacher_Subject()
		{}
		#region Model
		private int _ts_sn;
		private Guid _ts_guid;
		private Guid _ts_teacherguid;
		private Guid _ts_unitguid;
		private Guid _ts_subjectbaseguid;
		private bool _ischeck= false;
		private int _state=1;
		/// <summary>
		/// 顺序号
		/// </summary>
		public int TS_SN
		{
			set{ _ts_sn=value;}
			get{return _ts_sn;}
		}
		/// <summary>
		/// 本表GUID
		/// </summary>
		public Guid TS_GUID
		{
			set{ _ts_guid=value;}
			get{return _ts_guid;}
		}
		/// <summary>
		/// 对应教师GUID
		/// </summary>
		public Guid TS_TeacherGUID
		{
			set{ _ts_teacherguid=value;}
			get{return _ts_teacherguid;}
		}
		/// <summary>
		/// 对应单位GUID
		/// </summary>
		public Guid TS_UnitGUID
		{
			set{ _ts_unitguid=value;}
			get{return _ts_unitguid;}
		}
		/// <summary>
		/// 对应学科GUID
		/// </summary>
		public Guid TS_SubjectBaseGUID
		{
			set{ _ts_subjectbaseguid=value;}
			get{return _ts_subjectbaseguid;}
		}
		/// <summary>
		/// 审核权限 0 不审核 1 审核
		/// </summary>
		public bool IsCheck
		{
			set{ _ischeck=value;}
			get{return _ischeck;}
		}
		/// <summary>
		/// 0 不启用 1启用 -1删除
		/// </summary>
		public int State
		{
			set{ _state=value;}
			get{return _state;}
		}
		#endregion Model

	}
}

