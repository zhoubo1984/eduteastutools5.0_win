﻿using System;
namespace EduModel
{
	/// <summary>
	/// 教师学科教材信息
	/// </summary>
	[Serializable]
	public partial class Teacher_Subject_Book
	{
		public Teacher_Subject_Book()
		{}
		#region Model
		private int _tsb_sn;
		private Guid _tsb_guid;
		private Guid _ts_guid;
		private Guid _bb_guid;
		private int _state;
		/// <summary>
		/// 序列号
		/// </summary>
		public int TSB_SN
		{
			set{ _tsb_sn=value;}
			get{return _tsb_sn;}
		}
		/// <summary>
		/// 唯一标示
		/// </summary>
		public Guid TSB_GUID
		{
			set{ _tsb_guid=value;}
			get{return _tsb_guid;}
		}
		/// <summary>
		/// Teacher_Subject标识   教师所教学科
		/// </summary>
		public Guid TS_GUID
		{
			set{ _ts_guid=value;}
			get{return _ts_guid;}
		}
		/// <summary>
		/// BookBase 唯一标示  所使用教材
		/// </summary>
		public Guid BB_GUID
		{
			set{ _bb_guid=value;}
			get{return _bb_guid;}
		}
		/// <summary>
		/// 状态  0 关闭  1 启用  -1 删除
		/// </summary>
		public int State
		{
			set{ _state=value;}
			get{return _state;}
		}
		#endregion Model

	}
}

