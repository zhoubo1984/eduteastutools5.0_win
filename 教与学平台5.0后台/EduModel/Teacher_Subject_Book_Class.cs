﻿using System;
namespace EduModel
{
	/// <summary>
	/// 教师学科教材信息
	/// </summary>
	[Serializable]
	public partial class Teacher_Subject_Book_Class
	{
		public Teacher_Subject_Book_Class()
		{}
		#region Model
		private int _tsbc_sn;
		private Guid _tsbc_guid;
		private Guid _tsbc_tsbguid;
		private Guid _tsbc_classguid;
		private int _state=1;
		/// <summary>
		/// 序号
		/// </summary>
		public int TSBC_SN
		{
			set{ _tsbc_sn=value;}
			get{return _tsbc_sn;}
		}
		/// <summary>
		/// 唯一标示
		/// </summary>
		public Guid TSBC_GUID
		{
			set{ _tsbc_guid=value;}
			get{return _tsbc_guid;}
		}
		/// <summary>
		/// Teacher_Subject_Book标示
		/// </summary>
		public Guid TSBC_TSBGUID
		{
			set{ _tsbc_tsbguid=value;}
			get{return _tsbc_tsbguid;}
		}
		/// <summary>
		/// 班级Guid
		/// </summary>
		public Guid TSBC_ClassGUID
		{
			set{ _tsbc_classguid=value;}
			get{return _tsbc_classguid;}
		}
		/// <summary>
		/// 状态  0 关闭  1 启用  -1 删除
		/// </summary>
		public int State
		{
			set{ _state=value;}
			get{return _state;}
		}
		#endregion Model

	}
}

