﻿using System;
namespace EduModel
{
	/// <summary>
	/// 教师学科教材信息
	/// </summary>
	[Serializable]
	public partial class Teacher_Subject_Class_Group
	{
		public Teacher_Subject_Class_Group()
		{}
		#region Model
		private int _tscg_sn;
		private Guid _tscg_guid;
		private Guid _tscg_tsguid;
		private Guid _tscg_classguid;
		private string _tscg_groupname;
		private int _state=1;
		/// <summary>
		/// 
		/// </summary>
		public int TSCG_SN
		{
			set{ _tscg_sn=value;}
			get{return _tscg_sn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid TSCG_Guid
		{
			set{ _tscg_guid=value;}
			get{return _tscg_guid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid TSCG_TSGuid
		{
			set{ _tscg_tsguid=value;}
			get{return _tscg_tsguid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid TSCG_ClassGuid
		{
			set{ _tscg_classguid=value;}
			get{return _tscg_classguid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string TSCG_GroupName
		{
			set{ _tscg_groupname=value;}
			get{return _tscg_groupname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int State
		{
			set{ _state=value;}
			get{return _state;}
		}
		#endregion Model

	}
}

