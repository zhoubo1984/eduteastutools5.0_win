﻿using System;
namespace EduModel
{
	/// <summary>
	/// 自有用户扩展信息
	/// </summary>
	[Serializable]
	public partial class U_FreeUserInfo
	{
		public U_FreeUserInfo()
		{}
		#region Model
		private int _fi_sn;
		private Guid _fi_guid;
		private string _fi_qq;
		private string _fi_mail;
		private  string _fi_config;
		/// <summary>
		/// 序列号
		/// </summary>
		public int FI_SN
		{
			set{ _fi_sn=value;}
			get{return _fi_sn;}
		}
		/// <summary>
		/// 对应散户GUID
		/// </summary>
		public Guid FI_GUID
		{
			set{ _fi_guid=value;}
			get{return _fi_guid;}
		}
		/// <summary>
		/// QQ
		/// </summary>
		public string FI_QQ
		{
			set{ _fi_qq=value;}
			get{return _fi_qq;}
		}
		/// <summary>
		/// 电子邮件
		/// </summary>
		public string FI_Mail
		{
			set{ _fi_mail=value;}
			get{return _fi_mail;}
		}
		/// <summary>
		/// 其他配置
		/// </summary>
		public  string FI_Config
		{
			set{ _fi_config=value;}
			get{return _fi_config;}
		}
		#endregion Model

	}
}

