﻿using System;
namespace EduModel
{
	/// <summary>
	/// 领导扩展信息
	/// </summary>
	[Serializable]
	public partial class U_LeaderInfo
	{
		public U_LeaderInfo()
		{}
		#region Model
		private int _li_sn;
		private Guid _li_guid;
		private string _li_qq;
		private string _li_email;
		private string _li_mobilephone;
		/// <summary>
		/// 顺序号
		/// </summary>
		public int LI_SN
		{
			set{ _li_sn=value;}
			get{return _li_sn;}
		}
		/// <summary>
		/// 对应领导GUID
		/// </summary>
		public Guid LI_GUID
		{
			set{ _li_guid=value;}
			get{return _li_guid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LI_QQ
		{
			set{ _li_qq=value;}
			get{return _li_qq;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LI_Email
		{
			set{ _li_email=value;}
			get{return _li_email;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LI_MobilePhone
		{
			set{ _li_mobilephone=value;}
			get{return _li_mobilephone;}
		}
		#endregion Model

	}
}

