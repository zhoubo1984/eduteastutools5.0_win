﻿using System;
namespace EduModel
{
    /// <summary>
    /// 学生家长扩展信息
    /// </summary>
    [Serializable]
    public partial class U_ParentInfo
    {
        public U_ParentInfo()
        { }
        #region Model
        private int _pi_sn;
        private Guid _pi_guid;
        private string _pi_qq;
        private string _pi_mail;
        private string _pi_config;
        private string _pi_mobilephone;
        /// <summary>
        /// 顺序号
        /// </summary>
        public int PI_SN
        {
            set { _pi_sn = value; }
            get { return _pi_sn; }
        }
        /// <summary>
        /// 对应家长GUID
        /// </summary>
        public Guid PI_GUID
        {
            set { _pi_guid = value; }
            get { return _pi_guid; }
        }
        /// <summary>
        /// QQ
        /// </summary>
        public string PI_QQ
        {
            set { _pi_qq = value; }
            get { return _pi_qq; }
        }
        /// <summary>
        /// 电子邮件
        /// </summary>
        public string PI_Mail
        {
            set { _pi_mail = value; }
            get { return _pi_mail; }
        }

        /// <summary>
        /// 联系方式
        /// </summary>
        public string PI_MobilePhone
        {
            set { _pi_mobilephone = value; }
            get { return _pi_mobilephone; }
        }

        /// <summary>
        /// 其他配置
        /// </summary>
        public string PI_Config
        {
            set { _pi_config = value; }
            get { return _pi_config; }
        }
        #endregion Model

    }
}

