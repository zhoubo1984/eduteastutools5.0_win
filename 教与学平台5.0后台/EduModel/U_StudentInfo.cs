﻿using System;
namespace EduModel
{
	/// <summary>
	/// 学生扩展信息
	/// </summary>
	[Serializable]
	public partial class U_StudentInfo
	{
		public U_StudentInfo()
		{}
		#region Model
		private int _si_sn;
		private Guid _si_guid;
		private Guid _si_parentguid;
		private bool _si_isallowoffline= false;
		private string _si_qq;
		private string _si_mail;
		private  string _si_config;
		/// <summary>
		/// 顺序号
		/// </summary>
		public int SI_SN
		{
			set{ _si_sn=value;}
			get{return _si_sn;}
		}
		/// <summary>
		/// 对应学生GUID
		/// </summary>
		public Guid SI_GUID
		{
			set{ _si_guid=value;}
			get{return _si_guid;}
		}
		/// <summary>
		/// 对应家长GUID
		/// </summary>
		public Guid SI_ParentGUID
		{
			set{ _si_parentguid=value;}
			get{return _si_parentguid;}
		}
		/// <summary>
		/// 是否允许离线登录
		/// </summary>
		public bool SI_IsAllowOffLine
		{
			set{ _si_isallowoffline=value;}
			get{return _si_isallowoffline;}
		}
		/// <summary>
		/// QQ
		/// </summary>
		public string SI_QQ
		{
			set{ _si_qq=value;}
			get{return _si_qq;}
		}
		/// <summary>
		/// 电子邮件
		/// </summary>
		public string SI_Mail
		{
			set{ _si_mail=value;}
			get{return _si_mail;}
		}
		/// <summary>
		/// 其他配置
		/// </summary>
		public  string SI_Config
		{
			set{ _si_config=value;}
			get{return _si_config;}
		}
		#endregion Model

	}
}

