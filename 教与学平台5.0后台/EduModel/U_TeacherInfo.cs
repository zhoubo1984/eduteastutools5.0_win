﻿using System;
namespace EduModel
{
	/// <summary>
	/// 教师扩展信息
	/// </summary>
	[Serializable]
	public partial class U_TeacherInfo
	{
		public U_TeacherInfo()
		{}
		#region Model
		private int _ti_sn;
		private Guid _ti_guid;
		private string _ti_qq;
		private string _ti_mail;
		private string _ti_mobilephone;
		private  string _ti_config;
		/// <summary>
		/// 顺序号
		/// </summary>
		public int TI_SN
		{
			set{ _ti_sn=value;}
			get{return _ti_sn;}
		}
		/// <summary>
		/// 对应教师GUID
		/// </summary>
		public Guid TI_GUID
		{
			set{ _ti_guid=value;}
			get{return _ti_guid;}
		}
		/// <summary>
		/// QQ
		/// </summary>
		public string TI_QQ
		{
			set{ _ti_qq=value;}
			get{return _ti_qq;}
		}
		/// <summary>
		/// 电子邮件
		/// </summary>
		public string TI_Mail
		{
			set{ _ti_mail=value;}
			get{return _ti_mail;}
		}
		/// <summary>
		/// 手机号
		/// </summary>
		public string TI_MobilePhone
		{
			set{ _ti_mobilephone=value;}
			get{return _ti_mobilephone;}
		}
		/// <summary>
		/// 其他配置
		/// </summary>
		public  string TI_Config
		{
			set{ _ti_config=value;}
			get{return _ti_config;}
		}
		#endregion Model

	}
}

