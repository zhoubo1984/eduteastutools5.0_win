﻿using System;
namespace EduModel
{
	/// <summary>
	/// 单位表
	/// </summary>
	[Serializable]
	public partial class Unit_Base
	{
		public Unit_Base()
		{}
		#region Model
		private int _ub_sn;
		private Guid _ub_guid;
		private Guid _ub_pguid;
		private string _ub_name;
		private string _ub_path;
		private int _ub_index;
		private string _ub_dbindex;
		private bool _ub_ischeck= true;
		private int _ub_type=0;
		private int _gradestate;
		private int _state=1;
		/// <summary>
		/// 顺序号
		/// </summary>
		public int UB_SN
		{
			set{ _ub_sn=value;}
			get{return _ub_sn;}
		}
		/// <summary>
		/// 本表GUID
		/// </summary>
		public Guid UB_GUID
		{
			set{ _ub_guid=value;}
			get{return _ub_guid;}
		}
		/// <summary>
		/// 父GUID
		/// </summary>
		public Guid UB_PGUID
		{
			set{ _ub_pguid=value;}
			get{return _ub_pguid;}
		}
		/// <summary>
		/// 单位名称
		/// </summary>
		public string UB_Name
		{
			set{ _ub_name=value;}
			get{return _ub_name;}
		}
		/// <summary>
		/// 单位路径
		/// </summary>
		public string UB_Path
		{
			set{ _ub_path=value;}
			get{return _ub_path;}
		}
		/// <summary>
		/// 单位索引
		/// </summary>
		public int UB_Index
		{
			set{ _ub_index=value;}
			get{return _ub_index;}
		}
		/// <summary>
		/// 对应数据库索引或名称
		/// </summary>
		public string UB_DBIndex
		{
			set{ _ub_dbindex=value;}
			get{return _ub_dbindex;}
		}
		/// <summary>
		/// 是否进行审核后共享 否则直接共享
		/// </summary>
		public bool UB_IsCheck
		{
			set{ _ub_ischeck=value;}
			get{return _ub_ischeck;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int UB_Type
		{
			set{ _ub_type=value;}
			get{return _ub_type;}
		}
		/// <summary>
		/// 1,2,4分别代表：小学,初中,高中
		/// </summary>
		public int GradeState
		{
			set{ _gradestate=value;}
			get{return _gradestate;}
		}
		/// <summary>
		/// 用户状态  1启用0禁用 -1删除
		/// </summary>
		public int State
		{
			set{ _state=value;}
			get{return _state;}
		}
		#endregion Model

	}
}

