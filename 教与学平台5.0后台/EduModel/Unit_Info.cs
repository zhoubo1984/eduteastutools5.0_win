﻿using System;
namespace EduModel
{
	/// <summary>
	/// 单位信息扩展表
	/// </summary>
	[Serializable]
	public partial class Unit_Info
	{
		public Unit_Info()
		{}
		#region Model
		private int _ui_sn;
		private Guid _ui_guid;
		/// <summary>
		/// 序列号
		/// </summary>
		public int UI_SN
		{
			set{ _ui_sn=value;}
			get{return _ui_sn;}
		}
		/// <summary>
		/// 对应单位GUID
		/// </summary>
		public Guid UI_GUID
		{
			set{ _ui_guid=value;}
			get{return _ui_guid;}
		}
		#endregion Model

	}
}

