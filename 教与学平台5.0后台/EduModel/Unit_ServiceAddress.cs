﻿using System;
namespace EduModel
{
	/// <summary>
	/// 单位信息扩展表
	/// </summary>
	[Serializable]
	public partial class Unit_ServiceAddress
	{
		public Unit_ServiceAddress()
		{}
		#region Model
		private int _us_sn;
		private Guid _us_unitguid;
		private int _us_networktype;
		private string _us_serviceaddress;
		/// <summary>
		/// 
		/// </summary>
		public int US_SN
		{
			set{ _us_sn=value;}
			get{return _us_sn;}
		}
		/// <summary>
		/// 
		/// </summary>
		public Guid US_UnitGUID
		{
			set{ _us_unitguid=value;}
			get{return _us_unitguid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int US_NetworkType
		{
			set{ _us_networktype=value;}
			get{return _us_networktype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string US_ServiceAddress
		{
			set{ _us_serviceaddress=value;}
			get{return _us_serviceaddress;}
		}
		#endregion Model

	}
}

