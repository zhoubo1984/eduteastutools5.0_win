﻿using System;
namespace EduModel
{
	/// <summary>
	/// 用户表
	/// </summary>
	[Serializable]
	public partial class User_Base
	{
		public User_Base()
		{}
		#region Model
		private int _sn;
		private Guid _ub_guid;
		private string _ub_userid;
		private string _ub_userpsw;
		private string _ub_username;
		private bool _ub_sex;
		private int _ub_usertype;
		private DateTime _ub_regdate= DateTime.Now;
		private DateTime _ub_lastlogindate= DateTime.Now;
		private int _ub_logintimes=0;
		private Guid _ub_unitguid;
		private int _gradestate;
		private int _state=1;
		/// <summary>
		/// 顺序号
		/// </summary>
		public int SN
		{
			set{ _sn=value;}
			get{return _sn;}
		}
		/// <summary>
		/// 统一标示符
		/// </summary>
		public Guid UB_GUID
		{
			set{ _ub_guid=value;}
			get{return _ub_guid;}
		}
		/// <summary>
		/// 用户账号
		/// </summary>
		public string UB_UserID
		{
			set{ _ub_userid=value;}
			get{return _ub_userid;}
		}
		/// <summary>
		/// 用户密码
		/// </summary>
		public string UB_UserPsw
		{
			set{ _ub_userpsw=value;}
			get{return _ub_userpsw;}
		}
		/// <summary>
		/// 真实姓名
		/// </summary>
		public string UB_UserName
		{
			set{ _ub_username=value;}
			get{return _ub_username;}
		}
		/// <summary>
		/// 性别
		/// </summary>
		public bool UB_Sex
		{
			set{ _ub_sex=value;}
			get{return _ub_sex;}
		}
		/// <summary>
		/// 用户类型  1教师2学生3家长 0自由用户 4领导用户
		/// </summary>
		public int UB_UserType
		{
			set{ _ub_usertype=value;}
			get{return _ub_usertype;}
		}
		/// <summary>
		/// 注册时间
		/// </summary>
		public DateTime UB_RegDate
		{
			set{ _ub_regdate=value;}
			get{return _ub_regdate;}
		}
		/// <summary>
		/// 最后登陆时间
		/// </summary>
		public DateTime UB_LastLoginDate
		{
			set{ _ub_lastlogindate=value;}
			get{return _ub_lastlogindate;}
		}
		/// <summary>
		/// 登陆次数
		/// </summary>
		public int UB_LoginTimes
		{
			set{ _ub_logintimes=value;}
			get{return _ub_logintimes;}
		}
	
		/// <summary>
		/// 单位
		/// </summary>
		public Guid UB_UnitGUID
		{
			set{ _ub_unitguid=value;}
			get{return _ub_unitguid;}
		}
		/// <summary>
		/// 1,2,4分别代表：小学,初中,高中(会出现多学段)
		/// </summary>
		public int GradeState
		{
			set{ _gradestate=value;}
			get{return _gradestate;}
		}
		/// <summary>
		/// 用户状态  1启用0禁用 -1删除
		/// </summary>
		public int State
		{
			set{ _state=value;}
			get{return _state;}
		}
		#endregion Model

	}
}

