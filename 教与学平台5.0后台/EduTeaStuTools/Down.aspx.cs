﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EduTeaStuTools
{
    public partial class Down : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //判断登录，只有登录用户才能下载资源
                if (Session["WebLoginInfo"] != null)
                {
                    //权限判断，对应文件的权限.目前是所有老师都可以,无具体
                    //....

                    if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                    {
                        DownLoad(Request.QueryString["id"]);
                    }
                }
                else
                {
                    Common.ClientUI.AlertClose(this, "对不起，您尚未登录不能下载！");
                }
            }
        }

        public void DownLoad(string id)
        {
            System.IO.Stream iStream = null;
            byte[] buffer = new Byte[10000];
            long dataToRead;
            string filename = string.Empty;//客户端保存的文件名
            if (id == "2")
            {
                filename = "学生模板.xls";
            }
            else if (id == "1")
            {
                filename = "教师模板.xls";
            }
            else if (id == "3")
            {
                filename = "班级模板.xls";
            }
            else
            {
                Common.ClientUI.AlertClose(this, "对不起,您请求下载的文件不存在！");
                return;
            }
            string filepath = Server.MapPath("Resource/Template/") + filename;
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(filepath);
            if (!fileInfo.Exists)
            {
                Common.ClientUI.AlertClose(this, "对不起,您要下载的文件已不存在！");
                return;
            }
            try
            {
                iStream = new System.IO.FileStream(filepath, System.IO.FileMode.Open,
                System.IO.FileAccess.Read, System.IO.FileShare.Read);
                dataToRead = iStream.Length;
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(filename));
                //Response.AddHeader("Content-Disposition", "attachment; filename=11.rar");
                Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                Response.TransmitFile(filepath);
            }
            catch (Exception ex)
            {
                Common.ClientUI.AlertClose(this, "Error : " + ex.Message);

            }
            finally
            {
                if (iStream != null)
                {
                    iStream.Close();
                }
            }
        }

    }
}