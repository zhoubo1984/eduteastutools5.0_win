﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EduTeaStuTools
{
    public partial class dOWNsOFT : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //判断登录，只有登录用户才能下载资源
                if (Session["WebLoginInfo"] != null)
                {
                    //权限判断，对应文件的权限.目前是所有老师都可以,无具体
                    //....

                    if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                    {
                        DownLoad(Request.QueryString["id"]);
                    }
                }
                else
                {
                    Common.ClientUI.AlertClose(this, "对不起，您尚未登录不能下载！");
                }
            }
        }

        public void DownLoad(string id)
        {
            System.IO.Stream iStream = null;
            byte[] buffer = new Byte[10000];
            long dataToRead;
            EduBLL.Soft sBll = new EduBLL.Soft();
            EduModel.Soft sModel = sBll.GetModel(int.Parse(id));
            string filename = sModel.S_Name;//客户端保存的文件名

            string filepath = Server.MapPath("UploadFile/Soft/") + sModel.S_Index;
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(filepath);
            if (!fileInfo.Exists)
            {
                Common.ClientUI.AlertClose(this, "对不起,您要下载的文件已不存在！");
                return;
            }
            try
            {
                iStream = new System.IO.FileStream(filepath, System.IO.FileMode.Open,
                System.IO.FileAccess.Read, System.IO.FileShare.Read);
                dataToRead = iStream.Length;
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(sModel.S_Name));
                Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                Response.TransmitFile(filepath);
            }
            catch (Exception ex)
            {
                Common.ClientUI.AlertClose(this, "Error : " + ex.Message);

            }
            finally
            {
                if (iStream != null)
                {
                    iStream.Close();
                }
            }
        }

    }
}