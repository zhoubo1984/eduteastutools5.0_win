﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Data;
using System.Web.SessionState;
using Aspose.Cells;

namespace EduTeaStuTools.Handler
{
    /// <summary>
    /// kendo_Upload_File 的摘要说明
    /// </summary>
    public class kendo_Upload_File : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Request.ContentEncoding = Encoding.GetEncoding("UTF-8");
            context.Response.Charset = "utf-8";
            string fileType = context.Request.Params["fileType"];

            HttpPostedFile file = context.Request.Files["files"];
            string uploadpath = context.Server.MapPath("~/UploadFile/ImportData/");
            Common.Log log = new Common.Log();
            try
            {
                if (file != null)
                {
                    EduMethods.LoginInfo login = new EduMethods.LoginInfo();
                    if (context.Session["WebLoginInfo"] != null)
                    {
                        login = (EduMethods.LoginInfo)context.Session["WebLoginInfo"];
                    }
                    //int endIndex = file.FileName.LastIndexOf(@"\");
                    //string str = file.FileName.Substring(endIndex + 1, file.FileName.Length - endIndex - 1);
                    uploadpath = getFilePath(uploadpath) + file.FileName;
                    file.SaveAs(uploadpath);
                    string result = string.Empty;
                    if (fileType == "teacher")
                    {
                        result = ImportTeacher(uploadpath, login);
                    }
                    else if (fileType == "student")
                    {
                        result = ImportStudent(uploadpath, login);
                    }
                    else if (fileType == "class")
                    {
                        result = ImportClass(uploadpath, login);
                    }
                    if (result != "success")    //有数据没有导入成功问题
                    {
                        log.Write(result, Common.MsgType.ImportError);
                    }
                    //下面这句代码缺少的话，上传成功后上传队列的显示不会自动消失
                    context.Response.Write("");
                }
            }
            catch (System.Exception ex)
            {
                log.Write("上传文件失败！", ex);
            }

        }
        /// <summary>
        /// 获取用户对应xml的文件地址
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public string getFilePath(string mainPath)
        {
            string path = mainPath;
            CreateFile(path);         //跟目录
            path += Guid.NewGuid() + "_";
            CreateFile(path);         //上传文件
            return path;
        }

        /// <summary>
        /// 判断是否存在文件夹，不存在则创建
        /// </summary>
        /// <param name="path"></param>
        public void CreateFile(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        /// <summary>
        /// 导入教师数据
        /// </summary>
        /// <param name="path">上传文件路径</param>
        /// <param name="login">当前登录用户</param>
        /// <returns></returns>
        private string ImportTeacher(string path, EduMethods.LoginInfo login)
        {
            string result = string.Empty;
            Workbook wb = new Workbook(path);
            Worksheet sheet = wb.Worksheets[0]; //工作表
            Cells cells = sheet.Cells;//单元格
            EduBLL.User_Base ubBLL = new EduBLL.User_Base();
            EduModel.User_Base ubModel = new EduModel.User_Base();
            EduBLL.U_TeacherInfo utBll = new EduBLL.U_TeacherInfo();
            EduModel.U_TeacherInfo utModel = new EduModel.U_TeacherInfo();
            int errCount = 0; //添加失败的数量
            int existCount = 0;  //重复数据
            int successCount = 0;
            for (int i = 1; i < cells.Rows.Count; i++)
            {
                ubModel.UB_GUID = Guid.NewGuid();
                ubModel.UB_UserID = cells[i, 0].StringValue;
                if (ubModel.UB_UserID == null || ubModel.UB_UserID.Length == 0)
                {
                    continue;
                }
                ubModel.UB_UserName = cells[i, 1].StringValue;
                ubModel.UB_UserPsw = cells[i, 2].StringValue;
                ubModel.State = 1;
                ubModel.GradeState = Common.Common.getGradeValue(cells[i, 3].StringValue);
                ubModel.UB_UserType = 1;           //教师
                ubModel.UB_Sex = cells[i, 4].StringValue == "男" ? true : false;
                ubModel.UB_UnitGUID = new Guid(login.UnitGuid);
                if (!ubBLL.Exists(ubModel.UB_UserID))
                {
                    if (ubBLL.Add(ubModel) > 0)
                    {
                        utModel = new EduModel.U_TeacherInfo();
                        utModel.TI_GUID = ubModel.UB_GUID;
                        if (cells[i, 5] != null && cells[i, 5].StringValue != "")
                        {
                            utModel.TI_QQ = cells[i, 5].StringValue;
                        }
                        if (cells[i, 6] != null && cells[i, 6].StringValue != "")
                        {
                            utModel.TI_Mail = cells[i, 6].StringValue;
                        }
                        if (cells[i, 7] != null && cells[i, 7].StringValue != "")
                        {
                            utModel.TI_MobilePhone = cells[i, 7].StringValue;
                        }
                        if (utBll.Add(utModel) > 0)
                        {
                            successCount++;
                        }
                        else
                        {
                            errCount++;
                        }
                    }
                    else
                    {
                        errCount++;
                    }
                }
                else
                    existCount++;
            }
            if (successCount < cells.Rows.Count)
            {
                result = "数据导入结果：总数据" + cells.Rows.Count + "条,失败" + errCount + "条，重复" + existCount + "条,成功" + successCount + "条";
            }
            else
            {
                result = "success";
            }

            return result;
        }

        /// <summary>
        /// 导入学生数据
        /// </summary>
        /// <param name="path">上传文件路径</param>
        /// <param name="login">当前登录用户</param>
        /// <returns></returns>
        private string ImportStudent(string path, EduMethods.LoginInfo login)
        {
            string result = string.Empty;
            Workbook wb = new Workbook(path);
            Worksheet sheet = wb.Worksheets[0]; //工作表
            Cells cells = sheet.Cells;//单元格
            EduBLL.User_Base ubBLL = new EduBLL.User_Base();
            EduModel.User_Base ubModel = new EduModel.User_Base();
            EduBLL.Class_Base cbBLL = new EduBLL.Class_Base();
            EduModel.Class_Base cbModel = new EduModel.Class_Base();
            EduBLL.Student_Class scBLL = new EduBLL.Student_Class();
            EduModel.Student_Class scModel = new EduModel.Student_Class();
            EduBLL.U_StudentInfo utBll = new EduBLL.U_StudentInfo();
            EduModel.U_StudentInfo utModel = new EduModel.U_StudentInfo();
            EduModel.User_Base pModel = new EduModel.User_Base();
            EduBLL.U_ParentInfo upBll = new EduBLL.U_ParentInfo();
            EduModel.U_ParentInfo upModel = new EduModel.U_ParentInfo();
            int errCount = 0; //添加失败的数量
            int existCount = 0;  //重复数据
            int successCount = 0;
            for (int i = 1; i < cells.Rows.Count; i++)
            {
                ubModel.UB_GUID = Guid.NewGuid();
                ubModel.UB_UserID = cells[i, 0].StringValue;
                if (ubModel.UB_UserID == null || ubModel.UB_UserID.Length == 0)
                {
                    continue;
                }
                ubModel.UB_UserName = cells[i, 1].StringValue;
                ubModel.UB_UserPsw = cells[i, 2].StringValue;
                ubModel.State = 1;
                ubModel.GradeState = Common.Common.getGradeValue(cells[i, 3].StringValue);
                ubModel.UB_UserType = 2;           //学生
                ubModel.UB_Sex = cells[i, 4].StringValue == "男" ? true : false;
                ubModel.UB_UnitGUID = new Guid(login.UnitGuid);
                if (!ubBLL.Exists(ubModel.UB_UserID))
                {
                    if (ubBLL.Add(ubModel) > 0)      //添加user表
                    {
                        //添加家长start
                        if (cells[i, 9].StringValue != "")
                        {
                            if (!ubBLL.Exists(cells[i, 9].StringValue)) //不存在继续添加
                            {
                                //添加基础信息
                                pModel.UB_GUID = Guid.NewGuid();
                                pModel.UB_UserID = cells[i, 9].StringValue;
                                pModel.UB_UserName = cells[i, 10].StringValue;
                                pModel.UB_UserPsw = cells[i, 11].StringValue;
                                pModel.State = 1;
                                pModel.GradeState = Common.Common.getGradeValue(cells[i, 3].StringValue);
                                pModel.UB_UserType = 3;           //学生
                                pModel.UB_Sex = cells[i, 12].StringValue == "男" ? true : false;
                                pModel.UB_UnitGUID = new Guid(login.UnitGuid);
                                if (ubBLL.Add(pModel) > 0)       //添加扩展信息
                                {
                                    upModel = new EduModel.U_ParentInfo();
                                    upModel.PI_GUID = pModel.UB_GUID;
                                    if (cells[i, 13] != null && cells[i, 13].StringValue != "")
                                    {
                                        upModel.PI_MobilePhone = cells[i, 13].StringValue;
                                    }
                                    if (cells[i, 14] != null && cells[i, 14].StringValue != "")
                                    {
                                        upModel.PI_QQ = cells[i, 14].StringValue;
                                    }
                                    if (cells[i, 15] != null && cells[i, 15].StringValue != "")
                                    {
                                        upModel.PI_Mail = cells[i, 15].StringValue;
                                    }
                                    if (upBll.Add(upModel) == 0)
                                    {
                                        errCount++;
                                    }
                                }
                            }
                        }
                        //添加家长end

                        utModel = new EduModel.U_StudentInfo();
                        utModel.SI_GUID = ubModel.UB_GUID;
                        if (cells[i, 7] != null && cells[i, 7].StringValue != "")
                        {
                            utModel.SI_QQ = cells[i, 7].StringValue;
                        }
                        if (cells[i, 8] != null && cells[i, 8].StringValue != "")
                        {
                            utModel.SI_Mail = cells[i, 8].StringValue;
                        }
                        if (utBll.Add(utModel) > 0)     //添加学生表
                        {
                            cbModel = new EduModel.Class_Base();
                            cbModel.CB_UnitGUID = new Guid(login.UnitGuid);
                            cbModel.GradeState = ubModel.GradeState;
                            if (cells[i, 5] != null && cells[i, 5].StringValue != "")
                            {
                                cbModel.CB_StartYear = cells[i, 5].StringValue;
                            }
                            if (cells[i, 6] != null && cells[i, 6].StringValue != "")
                            {
                                cbModel.CB_Name = cells[i, 6].StringValue;
                            }
                            cbModel = cbBLL.GetModel(cbModel);
                            if (cbModel != null)
                            {
                                scModel = new EduModel.Student_Class();
                                scModel.SC_ClassGUID = cbModel.CB_GUID;
                                scModel.SC_GUID = Guid.NewGuid();
                                scModel.State = 1;
                                scModel.SC_StudentGUID = ubModel.UB_GUID;
                                if (scBLL.Add(scModel) > 0)
                                    successCount++;
                                else
                                    errCount++;
                            }
                            else
                            {
                                errCount++;
                            }

                        }
                        else
                        {
                            errCount++;
                        }
                    }
                    else
                    {
                        errCount++;
                    }
                }
                else
                    existCount++;
            }
            if (successCount < cells.Rows.Count)
            {
                result = "数据导入结果：总数据" + (cells.Rows.Count - 1) + "条,失败" + errCount + "条，重复" + existCount + "条,成功" + successCount + "条";
            }
            else
            {
                result = "success";
            }

            return result;
        }

        /// <summary>
        /// 导入班级数据
        /// </summary>
        /// <param name="path">上传文件路径</param>
        /// <param name="login">当前登录用户</param>
        /// <returns></returns>
        private string ImportClass(string path, EduMethods.LoginInfo login)
        {
            string result = string.Empty;
            Workbook wb = new Workbook(path);
            Worksheet sheet = wb.Worksheets[0]; //工作表
            Cells cells = sheet.Cells;//单元格
            EduBLL.Class_Base cbBll = new EduBLL.Class_Base();
            EduModel.Class_Base cbModel = new EduModel.Class_Base();
            int errCount = 0; //添加失败的数量
            int existCount = 0;  //重复数据
            int successCount = 0;
            for (int i = 1; i < cells.Rows.Count; i++)
            {
                if (!string.IsNullOrEmpty(cells[i, 0].StringValue) && !string.IsNullOrEmpty(cells[i, 1].StringValue) && !string.IsNullOrEmpty(cells[i, 2].StringValue))
                {
                    cbModel.CB_GUID = Guid.NewGuid();
                    cbModel.CB_UnitGUID = new Guid(login.UnitGuid);
                    cbModel.CB_Name = cells[i, 2].StringValue;
                    cbModel.CB_StartYear = cells[i, 1].StringValue;
                    cbModel.GradeState = Common.Common.getGradeValue(cells[i, 0].StringValue);
                    cbModel.State = 1;
                    if (!cbBll.Exists(cbModel))
                    {
                        if (cbBll.Add(cbModel) > 0)
                        {
                            successCount++;
                        }
                        else
                        {
                            errCount++;
                        }
                    }
                    else
                    {
                        existCount++;
                    }
                }
                else
                    errCount++;
            }
            if (successCount < cells.Rows.Count)
            {
                result = "数据导入结果：总数据" + cells.Rows.Count + "条,失败" + errCount + "条，重复" + existCount + "条,成功" + successCount + "条";
            }
            else
            {
                result = "success";
            }

            return result;
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }


    }
}