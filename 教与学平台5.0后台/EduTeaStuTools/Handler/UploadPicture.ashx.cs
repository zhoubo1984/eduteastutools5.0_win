﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Data;

namespace EduTeaStuTools.Handler
{
    /// <summary>
    /// UploadPicture 的摘要说明
    /// </summary>
    public class UploadPicture : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Request.ContentEncoding = Encoding.GetEncoding("UTF-8");
            context.Response.Charset = "utf-8";
            string fileType = context.Request.Params["fileType"];
            string guid = context.Request.Params["guid"];
            HttpPostedFile file = context.Request.Files["Filedata"];
            string uploadpath = context.Server.MapPath("../UploadFile/UnitPic/");
            Common.Log log = new Common.Log();
            try
            {
                if (file != null)
                {
                    uploadpath = uploadpath + guid + "_" + file.FileName;
                    file.SaveAs(uploadpath);
                    //下面这句代码缺少的话，上传成功后上传队列的显示不会自动消失
                    //context.Response.Write("1");
                }
            }
            catch (System.Exception ex)
            {
                log.Write("上传图片失败！", ex);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}