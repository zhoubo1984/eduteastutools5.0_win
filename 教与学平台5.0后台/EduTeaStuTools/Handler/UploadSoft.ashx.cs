﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Data;

namespace EduTeaStuTools.Handler
{
    /// <summary>
    /// UploadSoft 的摘要说明
    /// </summary>
    public class UploadSoft : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Request.ContentEncoding = Encoding.GetEncoding("UTF-8");
            context.Response.Charset = "utf-8";
            string guid = context.Request["guid"];
            HttpPostedFile file = context.Request.Files["files"];
            string uploadpath = context.Server.MapPath("../UploadFile/Soft/");
            Common.Log log = new Common.Log();
            try
            {
                if (file != null)
                {
                    int length = file.FileName.LastIndexOf('\\');
                    string name = file.FileName.Substring(length + 1, file.FileName.Length - length - 1);
                    uploadpath = uploadpath + guid + "_" + name;
                    file.SaveAs(uploadpath);
                    //下面这句代码缺少的话，上传成功后上传队列的显示不会自动消失
                    context.Response.Write("");
                }
            }
            catch (System.Exception ex)
            {
                log.Write("上传软件失败！", ex);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}