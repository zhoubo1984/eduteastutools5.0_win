﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace EduTeaStuTools.Handler
{
    /// <summary>
    /// getSubTeaManage 的摘要说明
    /// </summary>
    public class getSubTeaManage : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            Common.Log log = new Common.Log();
            StringBuilder sb = new StringBuilder();  //返回数据结果信息
            EduMethods.LoginInfo login = new EduMethods.LoginInfo();
            if (context.Session["WebLoginInfo"] != null)
            {
                login = (EduMethods.LoginInfo)context.Session["WebLoginInfo"];
            }
            try
            {
                string type = context.Request.Form["type"];
                if (type == null)
                {
                    type = context.Request["type"];
                }

                string sort = string.Empty;
                string key = " 1=1 ";  //关键字
                EduBLL.Teacher_Subject tsBll = new EduBLL.Teacher_Subject();
                EduModel.Teacher_Subject tsModel = new EduModel.Teacher_Subject();
                switch (type)
                {
                    case "getSubjectList":  //列表
                        int pIndex = Convert.ToInt32(context.Request.Form["page"]);
                        int pSize = Convert.ToInt32(context.Request.Form["pageSize"]);
                        if (!string.IsNullOrEmpty(context.Request["sort[0][field]"]) && !string.IsNullOrEmpty(context.Request["sort[0][dir]"]))
                        {
                            sort = context.Request["sort[0][field]"] + "  " + context.Request["sort[0][dir]"];
                        }
                        if (!string.IsNullOrEmpty(context.Request["filter[filters][0][filter][filters][0][value]"]))
                        {
                            if (context.Request["filter[filters][0][filter][filters][0][value]"] != "0")
                                key += " and sb.SB_SN=" + context.Request["filter[filters][0][filter][filters][0][value]"] + "";
                        }
                        if (!string.IsNullOrEmpty(context.Request["filter[filters][0][filter][filters][1][value]"]))
                        {
                            if (context.Request["filter[filters][0][filter][filters][1][value]"] != "all")
                                key += " and ub.UB_UserID  like '%" + context.Request["filter[filters][0][filter][filters][1][value]"] + "%'";
                        }
                        if (!string.IsNullOrEmpty(context.Request["filter[filters][0][filter][filters][2][value]"]))
                        {
                            if (context.Request["filter[filters][0][filter][filters][2][value]"] != "all")
                                key += " and ub.UB_UserName like '%" + context.Request["filter[filters][0][filter][filters][2][value]"] + "%'";
                        }
                        sb.Append(EduMethods.SubjectTree.getSubjectManage(login, key, pIndex, pSize, sort));
                        break;
                    case "UpdateSubjectState": //更新状态
                        if (!string.IsNullOrEmpty(context.Request["TS_SN"]))
                        {
                            tsModel = tsBll.GetModel(int.Parse(context.Request["TS_SN"]));
                            tsModel.IsCheck = tsModel.IsCheck == true ? false : true;
                            if (tsBll.Update(tsModel))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                    case "getSubjectTypeList":
                        sb.Append(EduMethods.SubjectTree.getSubjectTypeList(login));
                        break;
                }
            }
            catch (Exception e)
            {
                log.Write(e, Common.MsgType.Error);
            }
            context.Response.Write(sb.Length > 0 ? sb.ToString() : "error");
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}