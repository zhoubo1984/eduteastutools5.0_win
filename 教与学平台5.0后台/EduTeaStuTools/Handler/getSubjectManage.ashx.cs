﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace EduTeaStuTools.Handler
{
    /// <summary>
    /// getSubjectManage 的摘要说明
    /// </summary>
    public class getSubjectManage : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            Common.Log log = new Common.Log();
            StringBuilder sb = new StringBuilder();  //返回数据结果信息
            EduMethods.LoginInfo login = new EduMethods.LoginInfo();
            if (context.Session["WebLoginInfo"] != null)
            {
                login = (EduMethods.LoginInfo)context.Session["WebLoginInfo"];
            }
            try
            {
                string type = context.Request.Form["type"];
                if (type == null)
                {
                    type = context.Request["type"];
                }

                string sort = string.Empty;
                string key = " and 1=1 ";  //关键字
                EduBLL.Subject_Base sbBll = new EduBLL.Subject_Base();
                EduModel.Subject_Base sbModel = new EduModel.Subject_Base();
                switch (type)
                {
                    case "getSubjectList":  //列表
                        int pIndex = Convert.ToInt32(context.Request.Form["page"]);
                        int pSize = Convert.ToInt32(context.Request.Form["pageSize"]);
                        if (!string.IsNullOrEmpty(context.Request["sort[0][field]"]) && !string.IsNullOrEmpty(context.Request["sort[0][dir]"]))
                        {
                            sort = context.Request["sort[0][field]"] + "  " + context.Request["sort[0][dir]"];
                        }
                        if (!string.IsNullOrEmpty(context.Request["filter[filters][0][filter][filters][0][value]"]))
                        {
                            key += " and SB_SubjectName like '%" + context.Request["filter[filters][0][filter][filters][0][value]"] + "%'";
                        }
                        if (!string.IsNullOrEmpty(context.Request["filter[filters][0][filter][filters][1][value]"]))
                        {
                            if (context.Request["filter[filters][0][filter][filters][1][value]"] != "all")
                                key += " and GradeState=" + context.Request["filter[filters][0][filter][filters][1][value]"];
                        }

                        sb.Append(EduMethods.SysConfig.getSubjectList(login, key, pIndex, pSize, sort));
                        break;
                    case "editSubject":  //更新
                        if (!string.IsNullOrEmpty(context.Request["SB_SN"]))
                        {
                            sbModel = sbBll.GetModel(int.Parse(context.Request["SB_SN"]));

                            if (!string.IsNullOrEmpty(context.Request["SB_SubjectName"]))
                            {
                                sbModel.SB_SubjectName = context.Request["SB_SubjectName"];
                            }
                            if (!string.IsNullOrEmpty(context.Request["Grade"]))
                            {
                                sbModel.GradeState = int.Parse(context.Request["Grade"]);
                            }
                            if (!string.IsNullOrEmpty(context.Request["State"]))
                            {
                                sbModel.State = int.Parse(context.Request["State"]);
                            }

                            if (sbBll.Update(sbModel))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                    case "addSubject":  //更新
                        sbModel.SB_GUID = Guid.NewGuid();
                        if (!string.IsNullOrEmpty(context.Request["SB_SubjectName"]))
                        {
                            sbModel.SB_SubjectName = context.Request["SB_SubjectName"];
                        }
                        if (!string.IsNullOrEmpty(context.Request["Grade"]))
                        {
                            sbModel.GradeState = int.Parse(context.Request["Grade"]);
                        }
                        if (!string.IsNullOrEmpty(context.Request["State"]))
                        {
                            sbModel.State = int.Parse(context.Request["State"]);
                        }

                        if (sbBll.Add(sbModel) > 0)
                        {
                            sb.Append("success");
                        }
                        break;

                    case "deleteSubject": //删除
                        if (!string.IsNullOrEmpty(context.Request["SB_SN"]))
                        {
                            sbModel = sbBll.GetModel(int.Parse(context.Request["SB_SN"]));
                            sbModel.State = -1;
                            if (sbBll.Update(sbModel))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                    case "UpdateSubjectState": //更新状态
                        if (!string.IsNullOrEmpty(context.Request["SB_SN"]))
                        {
                            sbModel = sbBll.GetModel(int.Parse(context.Request["SB_SN"]));
                            sbModel.State = sbModel.State == 1 ? 0 : 1;
                            if (sbBll.Update(sbModel))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                log.Write(e, Common.MsgType.Error);
            }
            context.Response.Write(sb.Length > 0 ? sb.ToString() : "error");
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}