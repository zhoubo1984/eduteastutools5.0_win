﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace EduTeaStuTools.Handler
{
    /// <summary>
    /// getSubjectRelation 的摘要说明
    /// </summary>
    public class getSubjectRelation : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            Common.Log log = new Common.Log();
            StringBuilder sb = new StringBuilder();  //返回数据结果信息
            EduMethods.LoginInfo login = new EduMethods.LoginInfo();
            if (context.Session["WebLoginInfo"] != null)
            {
                login = (EduMethods.LoginInfo)context.Session["WebLoginInfo"];
            }
            try
            {
                string type = context.Request.Form["type"];
                if (type == null)
                {
                    type = context.Request["type"];
                }

                string sort = string.Empty;
                string key = " and 1=1 ";  //关键字
                EduBLL.Subject_SysRes sbBll = new EduBLL.Subject_SysRes();
                EduModel.Subject_SysRes sbModel = new EduModel.Subject_SysRes();
                switch (type)
                {
                    case "getSubjectList":  //列表
                        int pIndex = Convert.ToInt32(context.Request.Form["page"]);
                        int pSize = Convert.ToInt32(context.Request.Form["pageSize"]);
                        if (!string.IsNullOrEmpty(context.Request["sort[0][field]"]) && !string.IsNullOrEmpty(context.Request["sort[0][dir]"]))
                        {
                            sort = context.Request["sort[0][field]"] + "  " + context.Request["sort[0][dir]"];
                        }
                        if (!string.IsNullOrEmpty(context.Request["filter[filters][0][filter][filters][0][value]"]))
                        {
                            key += " and SB_SubjectName like '%" + context.Request["filter[filters][0][filter][filters][0][value]"] + "%'";
                        }

                        sb.Append(EduMethods.SubjectRelation.getSubjectList(login, key, pIndex, pSize, sort));
                        break;
                    case "SysResList":  //系统资源的学科
                        sb.Append(EduMethods.SubjectRelation.SysResSubject());
                        break;

                        case "Ebook":  //电子书升级wdm
                        string guid = context.Request.Form["guid"];
                        if (!string.IsNullOrEmpty(guid))
                         {
                             sb.Append(EduMethods.SubjectRelation.EbookShengji(guid));
                         }
                        break;
                 
                    case "editSubject":  //更新
                        if (!string.IsNullOrEmpty(context.Request["SN"]))
                        {
                            sbModel = sbBll.GetModel(int.Parse(context.Request["SN"]));

                            if (!string.IsNullOrEmpty(context.Request["SysGuid"]))
                            {
                                sbModel.SysSubGuid = new Guid(context.Request["SysGuid"]);
                            }
                            if (!string.IsNullOrEmpty(context.Request["SysName"]))
                            {
                                sbModel.SysSubName = context.Request["SysName"];
                            }
                            if (sbBll.Update(sbModel))
                            {
                                sb.Append("success");
                            }
                        }
                        break;

                }
            }
            catch (Exception e)
            {
                log.Write(e, Common.MsgType.Error);
            }
            context.Response.Write(sb.Length > 0 ? sb.ToString() : "error");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}