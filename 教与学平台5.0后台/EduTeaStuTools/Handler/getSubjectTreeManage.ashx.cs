﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.SessionState;


namespace EduTeaStuTools.Handler
{
    /// <summary>
    /// getSubjectTreeManage 的摘要说明
    /// </summary>
    public class getSubjectTreeManage : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            Common.Log log = new Common.Log();
            StringBuilder sb = new StringBuilder();  //返回数据结果信息
            EduMethods.LoginInfo login = new EduMethods.LoginInfo();
            if (context.Session["WebLoginInfo"] != null)
            {
                login = (EduMethods.LoginInfo)context.Session["WebLoginInfo"];
            }
            try
            {
                string type = context.Request.Form["type"];
                if (type == null)
                {
                    type = context.Request["type"];
                }

                string sort = string.Empty;
                string key = string.Empty;  //关键字
                if (!string.IsNullOrEmpty(context.Request["key"]))
                    key = context.Request["key"];
                switch (type)
                {
                    case "getSubjectTreeList":  //列表
                        sb.Append(EduMethods.SubjectTree.getSubjectTreeList(context.Request["ID"]));
                        break;
                    case "SubjectTreeList":  //列表 
                        sb.Append(EduMethods.SubjectRelation.SubjectTreeList(context.Request["BB_GUID"]));
                        break;
                }
            }
            catch (Exception e)
            {
                log.Write(e, Common.MsgType.Error);
            }
            context.Response.Write(sb.Length > 0 ? sb.ToString() : "error");
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}