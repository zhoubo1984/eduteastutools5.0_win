﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace EduTeaStuTools.Handler
{
    /// <summary>
    /// getTeaIndexMoreManage 的摘要说明
    /// </summary>
    public class getTeaIndexMoreManage : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            Common.Log log = new Common.Log();
            StringBuilder sb = new StringBuilder();  //返回数据结果信息
            EduBLL.User_Base ubBll = new EduBLL.User_Base();
            EduModel.User_Base ubModel = ubBll.GetModel(new Guid(context.Request["userGuid"]));
            try
            {
                string type = context.Request.Form["type"];
                if (type == null)
                {
                    type = context.Request["type"];
                }

                string sort = string.Empty;
                string key = string.Empty;  //关键字
                if (!string.IsNullOrEmpty(context.Request["key"]))
                    key = context.Request["key"];
                EduBLL.MoreIndex mdBLL = new EduBLL.MoreIndex();
                EduModel.MoreIndex mdModel = new EduModel.MoreIndex();
                switch (type)
                {
                    case "getIndexMoreList":  //列表
                        sb.Append(EduMethods.MoreIndex.getTeaIndexList(ubModel));
                        break;
                    case "editIndex":  //更新
                        if (!string.IsNullOrEmpty(context.Request["SN"]))
                        {
                            mdModel = mdBLL.GetModel(int.Parse(context.Request["SN"]));
                            if (!string.IsNullOrEmpty(context.Request["IndexName"]))
                            {
                                mdModel.IndexName = context.Request["IndexName"];
                            }
                            if (!string.IsNullOrEmpty(context.Request["IndexHref"]))
                            {
                                mdModel.IndexHref = context.Request["IndexHref"];
                            }
                            if (!string.IsNullOrEmpty(context.Request["IndexTypeName"]))
                            {
                                mdModel.IndexTypeName = context.Request["IndexTypeName"];
                            }
                            if (mdBLL.Update(mdModel))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                    case "addIndex":  //添加
                        mdModel.State = 3;  //教师
                        mdModel.UserGuid = ubModel.UB_GUID;
                        mdModel.UnitGuid = ubModel.UB_UnitGUID;
                        mdModel.IndexGuid = Guid.NewGuid();
                        if (!string.IsNullOrEmpty(context.Request["IndexName"]))
                        {
                            mdModel.IndexName = context.Request["IndexName"];
                        }
                        if (!string.IsNullOrEmpty(context.Request["IndexHref"]))
                        {
                            mdModel.IndexHref = context.Request["IndexHref"];
                        }
                        if (!string.IsNullOrEmpty(context.Request["IndexTypeName"]))
                        {
                            mdModel.IndexTypeName = context.Request["IndexTypeName"];
                        }
                        if (mdBLL.Add(mdModel) > 0)
                        {
                            sb.Append("success");
                        }
                        break;
                    case "deleteIndex":      //删除索引
                        if (!string.IsNullOrEmpty(context.Request["SN"]))
                        {
                            if (mdBLL.Delete(int.Parse(context.Request["SN"])))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                    case "getIndexTypeList":  //获取索引列表信息
                        sb.Append(EduMethods.MoreIndex.getTeaIndexTypeList(ubModel));
                        break;
                    case "getIndexBySn":     //获取单个索引信息
                        if (!string.IsNullOrEmpty(context.Request["SN"]))
                            sb.Append(EduMethods.MoreIndex.getIndexBySN(int.Parse(context.Request["SN"])));
                        break;

                }
            }
            catch (Exception e)
            {
                log.Write(e, Common.MsgType.Error);
            }
            context.Response.Write(sb.Length > 0 ? sb.ToString() : "error");
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}