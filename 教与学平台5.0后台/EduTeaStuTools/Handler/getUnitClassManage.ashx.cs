﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace EduTeaStuTools.Handler
{
    /// <summary>
    /// getUnitClassManage 的摘要说明
    /// </summary>
    public class getUnitClassManage : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            Common.Log log = new Common.Log();
            StringBuilder sb = new StringBuilder();  //返回数据结果信息
            EduMethods.LoginInfo login = new EduMethods.LoginInfo();
            if (context.Session["WebLoginInfo"] != null)
            {
                login = (EduMethods.LoginInfo)context.Session["WebLoginInfo"];
            }
            try
            {
                string type = context.Request.Form["type"];
                if (type == null)
                {
                    type = context.Request["type"];
                }

                string sort = string.Empty;
                string key = " 1=1 ";  //关键字
                EduBLL.Class_Base cbBll = new EduBLL.Class_Base();
                EduModel.Class_Base cbModel = new EduModel.Class_Base();
                switch (type)
                {
                    case "getUnitClassList":  //列表
                        int pIndex = Convert.ToInt32(context.Request.Form["page"]);
                        int pSize = Convert.ToInt32(context.Request.Form["pageSize"]);
                        if (!string.IsNullOrEmpty(context.Request["sort[0][field]"]) && !string.IsNullOrEmpty(context.Request["sort[0][dir]"]))
                        {
                            sort = context.Request["sort[0][field]"] + "  " + context.Request["sort[0][dir]"];
                        }
                        if (!string.IsNullOrEmpty(context.Request["filter[filters][0][filter][filters][0][value]"]))
                        {
                            if (context.Request["filter[filters][0][filter][filters][0][value]"] != "all")
                                key += " and CB_Name  like '%" + context.Request["filter[filters][0][filter][filters][0][value]"] + "%'";
                        }
                        sb.Append(EduMethods.UnitList.getUnitClassList(login, key, pIndex, pSize, sort));
                        break;
                    case "editUnitClass":  //更新
                        if (!string.IsNullOrEmpty(context.Request["CB_GUID"]))
                        {
                            cbModel = cbBll.GetModel(new Guid(context.Request["CB_GUID"]));

                            if (!string.IsNullOrEmpty(context.Request["CB_StartYear"]))
                            {
                                cbModel.CB_StartYear = context.Request["CB_StartYear"];
                            }
                            if (!string.IsNullOrEmpty(context.Request["GradeState"]))
                            {
                                cbModel.GradeState = int.Parse(context.Request["GradeState"]);
                            }
                            if (!string.IsNullOrEmpty(context.Request["CB_Name"]))
                            {
                                cbModel.CB_Name = context.Request["CB_Name"];
                            }
                            if (!string.IsNullOrEmpty(context.Request["State"]))
                            {
                                cbModel.State = int.Parse(context.Request["State"]);
                            }

                            if (cbBll.Update(cbModel))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                    case "addUnitClass":  //更新
                        cbModel.CB_GUID = Guid.NewGuid();
                        cbModel.CB_UnitGUID = new Guid(login.UnitGuid);
                        if (!string.IsNullOrEmpty(context.Request["CB_StartYear"]))
                        {
                            cbModel.CB_StartYear = context.Request["CB_StartYear"];
                        }
                        if (!string.IsNullOrEmpty(context.Request["GradeState"]))
                        {
                            cbModel.GradeState = int.Parse(context.Request["GradeState"]);
                        }
                        if (!string.IsNullOrEmpty(context.Request["CB_Name"]))
                        {
                            cbModel.CB_Name = context.Request["CB_Name"];
                        }
                        if (!string.IsNullOrEmpty(context.Request["State"]))
                        {
                            cbModel.State = int.Parse(context.Request["State"]);
                        }

                        if (cbBll.Add(cbModel) > 0)
                        {
                            sb.Append("success");
                        }
                        break;

                    case "deleteUnitClass": //删除
                        if (!string.IsNullOrEmpty(context.Request["UB_SN"]))
                        {
                            cbModel = cbBll.GetModel(new Guid(context.Request["UB_SN"]));
                            cbModel.State = -1;
                            if (cbBll.Update(cbModel))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                    case "UpdateClassState": //更新状态
                        if (!string.IsNullOrEmpty(context.Request["UB_SN"]))
                        {
                            cbModel = cbBll.GetModel(new Guid(context.Request["UB_SN"]));
                            cbModel.State = cbModel.State == 1 ? 0 : 1;
                            if (cbBll.Update(cbModel))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                log.Write(e, Common.MsgType.Error);
            }
            context.Response.Write(sb.Length > 0 ? sb.ToString() : "error");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}