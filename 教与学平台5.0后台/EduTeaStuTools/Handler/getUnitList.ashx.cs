﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace EduTeaStuTools.Handler
{
    /// <summary>
    /// getUnitListData 的摘要说明
    /// </summary>
    public class getUnitListData : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            Common.Log log = new Common.Log();
            StringBuilder sb = new StringBuilder();  //返回数据结果信息
            EduMethods.LoginInfo login = new EduMethods.LoginInfo();
            if (context.Session["WebLoginInfo"] != null)
            {
                login = (EduMethods.LoginInfo)context.Session["WebLoginInfo"];
            }
            try
            {
                string type = context.Request.Form["type"];
                if (type == null)
                {
                    type = context.Request["type"];
                }

                string sort = string.Empty;
                string key = string.Empty;  //关键字
                if (!string.IsNullOrEmpty(context.Request["key"]))
                    key = context.Request["key"];
                EduBLL.Unit_Base ubBll = new EduBLL.Unit_Base();
                EduModel.Unit_Base ubModel = new EduModel.Unit_Base();
                switch (type)
                {
                    case "getUnitList":  //列表
                        int pIndex = Convert.ToInt32(context.Request.Form["page"]);
                        int pSize = Convert.ToInt32(context.Request.Form["pageSize"]);
                        if (!string.IsNullOrEmpty(context.Request["sort[0][field]"]) && !string.IsNullOrEmpty(context.Request["sort[0][dir]"]))
                        {
                            sort = context.Request["sort[0][field]"] + "  " + context.Request["sort[0][dir]"];
                        }
                        sb.Append(EduMethods.UnitList.getUnitList(login, key, pIndex, pSize, sort));
                        break;
                    case "editUnit":  //更新
                        if (!string.IsNullOrEmpty(context.Request["UB_SN"]))
                        {
                            ubModel = ubBll.GetModel(int.Parse(context.Request["UB_SN"]));
                            if (!string.IsNullOrEmpty(context.Request["UB_Name"]))
                            {
                                ubModel.UB_Name = context.Request["UB_Name"];
                            }
                            if (!string.IsNullOrEmpty(context.Request["Grade"]))
                            {
                                ubModel.GradeState = int.Parse(context.Request["Grade"]);
                            }
                            if (!string.IsNullOrEmpty(context.Request["UB_DBIndex"]))
                            {
                                ubModel.UB_DBIndex = context.Request["UB_DBIndex"];
                            }
                            if (ubBll.Update(ubModel))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                    case "addUnit":  //添加
                        ubModel.State = 1;
                        ubModel.UB_GUID = Guid.NewGuid();
                        if (!string.IsNullOrEmpty(context.Request["UB_Name"]))
                        {
                            ubModel.UB_Name = context.Request["UB_Name"];
                        }
                        if (!string.IsNullOrEmpty(context.Request["Grade"]))
                        {
                            ubModel.GradeState = int.Parse(context.Request["Grade"]);
                        }
                        if (!string.IsNullOrEmpty(context.Request["UB_DBIndex"]))
                        {
                            ubModel.UB_DBIndex = context.Request["UB_DBIndex"];
                        }
                        if (!string.IsNullOrEmpty(context.Request["IsCheck"]))
                        {
                            ubModel.UB_IsCheck = int.Parse(context.Request["IsCheck"]) == 1 ? true : false;
                        }
                        EduModel.Unit_Base ubParent = ubBll.GetModel(new Guid(login.UnitGuid));
                        ubModel.UB_Index = ubBll.getMaxIndex(ubParent.UB_GUID) + 1;
                        ubModel.UB_Path = ubParent.UB_Path + "_000" + ubModel.UB_Index;
                        ubModel.UB_PGUID = ubParent.UB_GUID;
                        if (ubBll.Add(ubModel) > 0)
                        {
                            sb.Append("success");
                        }
                        break;
                    case "deleteUnit": //删除
                        if (!string.IsNullOrEmpty(context.Request["UB_SN"]))
                        {
                            ubModel = ubBll.GetModel(int.Parse(context.Request["UB_SN"]));
                            ubModel.State = 0;
                            if (ubBll.Update(ubModel))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                    case "getUnitManageList":
                        sb.Append(EduMethods.UnitList.getUnitDataList(login));
                        break;
                }
            }
            catch (Exception e)
            {
                log.Write(e, Common.MsgType.Error);
            }
            context.Response.Write(sb.Length > 0 ? sb.ToString() : "error");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}