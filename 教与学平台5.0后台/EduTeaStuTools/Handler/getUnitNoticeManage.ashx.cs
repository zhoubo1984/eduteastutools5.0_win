﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace EduTeaStuTools.Handler
{
    /// <summary>
    /// getUnitNoticeManage 的摘要说明
    /// </summary>
    public class getUnitNoticeManage : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            Common.Log log = new Common.Log();
            StringBuilder sb = new StringBuilder();  //返回数据结果信息
            EduMethods.LoginInfo login = new EduMethods.LoginInfo();
            if (context.Session["WebLoginInfo"] != null)
            {
                login = (EduMethods.LoginInfo)context.Session["WebLoginInfo"];
            }
            try
            {
                string type = context.Request.Form["type"];
                if (type == null)
                {
                    type = context.Request["type"];
                }

                string sort = string.Empty;
                string key = string.Empty;  //关键字
                if (!string.IsNullOrEmpty(context.Request["key"]))
                    key = context.Request["key"];
                EduBLL.Notice ntBll = new EduBLL.Notice();
                EduModel.Notice ntModel = new EduModel.Notice();
                switch (type)
                {
                    case "getUnitNoticeList":  //列表
                        int pIndex = Convert.ToInt32(context.Request.Form["page"]);
                        int pSize = Convert.ToInt32(context.Request.Form["pageSize"]);
                        if (!string.IsNullOrEmpty(context.Request["sort[0][field]"]) && !string.IsNullOrEmpty(context.Request["sort[0][dir]"]))
                        {
                            sort = context.Request["sort[0][field]"] + "  " + context.Request["sort[0][dir]"];
                        }
                        sb.Append(EduMethods.Notice.getUnitNoticeList(login, key, pIndex, pSize, sort));
                        break;
                    case "addNotice":
                        if (!string.IsNullOrEmpty(context.Request["N_Title"]))
                        {
                            ntModel.N_Title = Microsoft.JScript.GlobalObject.decodeURIComponent(context.Request["N_Title"]).TrimEnd();
                        }
                        if (!string.IsNullOrEmpty(context.Request["N_Content"]))
                        {
                            ntModel.N_Content = Microsoft.JScript.GlobalObject.decodeURIComponent(context.Request["N_Content"]).TrimEnd();
                        }
                        ntModel.N_UnitGUID = new Guid(login.UnitGuid);
                        ntModel.N_UserGUID = new Guid(login.LoginGuid);
                        ntModel.N_CreateDate = DateTime.Now;
                        ntModel.N_PublishDate = DateTime.Now;
                        ntModel.N_IsPublish = true;
                        ntModel.N_GUID = Guid.NewGuid();
                        if (ntBll.Add(ntModel, EduMethods.CommonString.getUnitDBPath(login.UnitGuid)) > 0)
                        {
                            sb.Append("success");
                        }
                        break;
                    case "editNotice":
                        if (!string.IsNullOrEmpty(context.Request["SN"]))
                        {
                            ntModel = ntBll.GetModel(int.Parse(context.Request["SN"]), EduMethods.CommonString.getUnitDBPath(login.UnitGuid));
                            if (!string.IsNullOrEmpty(context.Request["N_Title"]))
                            {
                                ntModel.N_Title = Microsoft.JScript.GlobalObject.decodeURIComponent(context.Request["N_Title"]).TrimEnd();
                            }
                            if (!string.IsNullOrEmpty(context.Request["N_Content"]))
                            {
                                ntModel.N_Content = Microsoft.JScript.GlobalObject.decodeURIComponent(context.Request["N_Content"]).TrimEnd();
                            }
                            if (ntBll.Update(ntModel, EduMethods.CommonString.getUnitDBPath(login.UnitGuid)))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                    case "deleteNotice":
                        if (!string.IsNullOrEmpty(context.Request["SN"]))
                        {
                            if (ntBll.Delete(int.Parse(context.Request["SN"]), EduMethods.CommonString.getUnitDBPath(login.UnitGuid)))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                log.Write(e, Common.MsgType.Error);
            }
            context.Response.Write(sb.Length > 0 ? sb.ToString() : "error");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}