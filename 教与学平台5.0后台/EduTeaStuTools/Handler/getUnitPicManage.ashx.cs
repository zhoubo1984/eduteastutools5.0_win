﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace EduTeaStuTools.Handler
{
    /// <summary>
    /// getUnitPicManage 的摘要说明
    /// </summary>
    public class getUnitPicManage : IHttpHandler, IRequiresSessionState
    {


        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            Common.Log log = new Common.Log();
            StringBuilder sb = new StringBuilder();  //返回数据结果信息
            EduMethods.LoginInfo login = new EduMethods.LoginInfo();
            if (context.Session["WebLoginInfo"] != null)
            {
                login = (EduMethods.LoginInfo)context.Session["WebLoginInfo"];
            }
            try
            {
                string type = context.Request.Form["type"];
                if (type == null)
                {
                    type = context.Request["type"];
                }

                string sort = string.Empty;
                string key = string.Empty;  //关键字
                if (!string.IsNullOrEmpty(context.Request["key"]))
                    key = context.Request["key"];
                EduBLL.Picture pBll = new EduBLL.Picture();
                EduModel.Picture pModel = new EduModel.Picture();
                switch (type)
                {
                    case "getUnitPictureList":  //列表
                        int pIndex = Convert.ToInt32(context.Request.Form["page"]);
                        int pSize = Convert.ToInt32(context.Request.Form["pageSize"]);
                        if (!string.IsNullOrEmpty(context.Request["sort[0][field]"]) && !string.IsNullOrEmpty(context.Request["sort[0][dir]"]))
                        {
                            sort = context.Request["sort[0][field]"] + "  " + context.Request["sort[0][dir]"];
                        }
                        sb.Append(EduMethods.Notice.getUnitPictureList(login, key, pIndex, pSize, sort));
                        break;
                    case "addPicture":
                        if (!string.IsNullOrEmpty(context.Request["P_Title"]))
                        {
                            pModel.P_Title = Microsoft.JScript.GlobalObject.decodeURIComponent(context.Request["P_Title"]).TrimEnd();
                        }
                        if (!string.IsNullOrEmpty(context.Request["P_Path"]))
                        {
                            pModel.P_Path = Microsoft.JScript.GlobalObject.decodeURIComponent(context.Request["P_Path"]).TrimEnd();
                        }
                        pModel.P_UnitGuid = new Guid(login.UnitGuid);
                        pModel.P_Upload_UserGuid = new Guid(login.LoginGuid);
                        pModel.P_Upload_Date = DateTime.Now;
                        if (pBll.Add(pModel) > 0)
                        {
                            sb.Append("success");
                        }
                        break;
                    case "updatePicture":
                        if (!string.IsNullOrEmpty(context.Request["SN"]))
                        {
                            pModel = pBll.GetModel(int.Parse(context.Request["SN"]));
                            pModel.P_IsShow = pModel.P_IsShow == true ? false : true;
                            if (pBll.Update(pModel))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                    case "deletePicture":
                        if (!string.IsNullOrEmpty(context.Request["SN"]))
                        {
                            if (pBll.Delete(int.Parse(context.Request["SN"])))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                log.Write(e, Common.MsgType.Error);
            }
            context.Response.Write(sb.Length > 0 ? sb.ToString() : "error");
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}