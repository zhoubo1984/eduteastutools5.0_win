﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace EduTeaStuTools.Handler
{
    /// <summary>
    /// getUnitSoftManage 的摘要说明
    /// </summary>
    public class getUnitSoftManage : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            Common.Log log = new Common.Log();
            StringBuilder sb = new StringBuilder();  //返回数据结果信息
            EduMethods.LoginInfo login = new EduMethods.LoginInfo();
            if (context.Session["WebLoginInfo"] != null)
            {
                login = (EduMethods.LoginInfo)context.Session["WebLoginInfo"];
            }
            try
            {
                string type = context.Request.Form["type"];
                if (type == null)
                {
                    type = context.Request["type"];
                }
                string sort = string.Empty;
                string key = string.Empty;  //关键字
                if (!string.IsNullOrEmpty(context.Request["key"]))
                    key = context.Request["key"];
                EduBLL.Soft sBll = new EduBLL.Soft();
                EduModel.Soft sModel = new EduModel.Soft();
                switch (type)
                {
                    case "getUnitSoftList":  //列表
                        int pIndex = Convert.ToInt32(context.Request.Form["page"]);
                        int pSize = Convert.ToInt32(context.Request.Form["pageSize"]);
                        if (!string.IsNullOrEmpty(context.Request["sort[0][field]"]) && !string.IsNullOrEmpty(context.Request["sort[0][dir]"]))
                        {
                            sort = context.Request["sort[0][field]"] + "  " + context.Request["sort[0][dir]"];
                        }
                        sb.Append(EduMethods.Notice.getUnitSoftList(login, key, pIndex, pSize, sort));
                        break;
                    case "addSoft":
                        if (!string.IsNullOrEmpty(context.Request["S_Name"]))
                        {
                            sModel.S_Name = Microsoft.JScript.GlobalObject.decodeURIComponent(context.Request["S_Name"]).TrimEnd();
                        }
                        if (!string.IsNullOrEmpty(context.Request["S_Content"]))
                        {
                            sModel.S_Content = Microsoft.JScript.GlobalObject.decodeURIComponent(context.Request["S_Content"]).TrimEnd();
                        }
                        if (!string.IsNullOrEmpty(context.Request["res_path"]))
                        {
                            sModel.S_Index = Microsoft.JScript.GlobalObject.decodeURIComponent(context.Request["res_path"]).TrimEnd();
                        }
                        sModel.S_UnitGUID = new Guid(login.UnitGuid);
                        sModel.S_UserGUID = new Guid(login.LoginGuid);
                        sModel.S_Date = DateTime.Now;
                        sModel.S_GUID = Guid.NewGuid();
                        if (sBll.Add(sModel) > 0)
                        {
                            sb.Append("success");
                        }
                        break;
                    case "deleteSoft":
                        if (!string.IsNullOrEmpty(context.Request["SN"]))
                        {
                            if (sBll.Delete(int.Parse(context.Request["SN"])))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                log.Write(e, Common.MsgType.Error);
            }
            context.Response.Write(sb.Length > 0 ? sb.ToString() : "error");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}