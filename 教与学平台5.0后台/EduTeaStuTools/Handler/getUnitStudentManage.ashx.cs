﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.SessionState;


namespace EduTeaStuTools.Handler
{
    /// <summary>
    /// getUnitStudentManage 的摘要说明
    /// </summary>
    public class getUnitStudentManage : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            Common.Log log = new Common.Log();
            StringBuilder sb = new StringBuilder();  //返回数据结果信息
            EduMethods.LoginInfo login = new EduMethods.LoginInfo();
            if (context.Session["WebLoginInfo"] != null)
            {
                login = (EduMethods.LoginInfo)context.Session["WebLoginInfo"];
            }
            try
            {
                string type = context.Request.Form["type"];
                if (type == null)
                {
                    type = context.Request["type"];
                }

                string sort = string.Empty;
                string key = " 1=1 ";  //关键字
                EduBLL.U_StudentInfo utBll = new EduBLL.U_StudentInfo();
                EduModel.U_StudentInfo utModel = new EduModel.U_StudentInfo();
                EduBLL.User_Base ubBLL = new EduBLL.User_Base();
                EduModel.User_Base ubModel = new EduModel.User_Base();
                switch (type)
                {
                    case "getUnitStudentList":  //列表
                        int pIndex = Convert.ToInt32(context.Request.Form["page"]);
                        int pSize = Convert.ToInt32(context.Request.Form["pageSize"]);
                        if (!string.IsNullOrEmpty(context.Request["sort[0][field]"]) && !string.IsNullOrEmpty(context.Request["sort[0][dir]"]))
                        {
                            sort = context.Request["sort[0][field]"] + "  " + context.Request["sort[0][dir]"];
                        }
                        if (!string.IsNullOrEmpty(context.Request["filter[filters][0][filter][filters][0][value]"]))
                        {
                            if (context.Request["filter[filters][0][filter][filters][0][value]"] != "all")
                                key += " and ub.UB_UserID  like '%" + context.Request["filter[filters][0][filter][filters][0][value]"] + "%'";
                        }
                        if (!string.IsNullOrEmpty(context.Request["filter[filters][0][filter][filters][1][value]"]))
                        {
                            if (context.Request["filter[filters][0][filter][filters][1][value]"] != "all")
                                key += " and ub.UB_UserName like '%" + context.Request["filter[filters][0][filter][filters][1][value]"] + "%'";
                        }
                        sb.Append(EduMethods.UserManage.getUnitStudentList(login, key, pIndex, pSize, sort));
                        break;
                    case "editUnitStudent":  //更新
                        if (!string.IsNullOrEmpty(context.Request["UB_GUID"]))
                        {
                            utModel = utBll.GetModel(new Guid(context.Request["UB_GUID"]));
                            if (!string.IsNullOrEmpty(context.Request["SI_Mail"]))
                            {
                                utModel.SI_Mail = context.Request["SI_Mail"];
                            }
                            if (!string.IsNullOrEmpty(context.Request["SI_QQ"]))
                            {
                                utModel.SI_QQ = context.Request["SI_QQ"];
                            }
                            ubModel = ubBLL.GetModel(new Guid(context.Request["UB_GUID"]));

                            if (!string.IsNullOrEmpty(context.Request["GradeState"]))
                            {
                                ubModel.GradeState = int.Parse(context.Request["GradeState"]);
                            }

                            if (!string.IsNullOrEmpty(context.Request["classGuid"]))
                            {
                                EduBLL.Student_Class scBLL = new EduBLL.Student_Class();
                                EduModel.Student_Class sc = new EduModel.Student_Class();

                                sc = scBLL.GetModel(ubModel.UB_GUID);
                                if (sc != null && sc.SC_ClassGUID != null && sc.SC_ClassGUID != Guid.Empty)
                                {

                                    if (sc.SC_ClassGUID != new Guid(context.Request["classGuid"]))
                                    {
                                        sc.State = -1;
                                        scBLL.Update(sc);
                                        sc = new EduModel.Student_Class();
                                        sc.SC_StudentGUID = ubModel.UB_GUID;
                                        sc.SC_ClassGUID = new Guid(context.Request["classGuid"]);
                                        sc.SC_GUID = Guid.NewGuid();
                                        sc.State = 1;
                                        scBLL.Add(sc);
                                    }
                                }
                                else
                                {
                                    sc = new EduModel.Student_Class();
                                    sc.SC_StudentGUID = ubModel.UB_GUID;
                                    sc.SC_ClassGUID = new Guid(context.Request["classGuid"]);
                                    sc.SC_GUID = Guid.NewGuid();
                                    sc.State = 1;
                                    scBLL.Add(sc);
                                }
                            }

                            if (!string.IsNullOrEmpty(context.Request["UB_UserName"]))
                            {
                                  ubModel.UB_UserName = context.Request["UB_UserName"];
                            }
                            if (!string.IsNullOrEmpty(context.Request["UB_UserID"]))
                            {
                                ubModel.UB_UserID = context.Request["UB_UserID"];
                            }
                            if (!string.IsNullOrEmpty(context.Request["State"]))
                            {
                                ubModel.State = int.Parse(context.Request["State"]);
                            }
                            if (!string.IsNullOrEmpty(context.Request["UB_Sex"]))
                            {
                                ubModel.UB_Sex = int.Parse(context.Request["UB_Sex"]) == 1 ? true : false;
                            }

                            if (utBll.Update(utModel) && ubBLL.Update(ubModel))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                    case "addUnitStudent":  //添加
                        ubModel.UB_GUID = Guid.NewGuid();
                        ubModel.UB_RegDate = DateTime.Now;
                        ubModel.UB_UserPsw = "123456";
                        ubModel.UB_UserType = 2;
                        ubModel.UB_UnitGUID = new Guid(login.UnitGuid);
                        ubModel.State = 1;
                        if (!string.IsNullOrEmpty(context.Request["GradeState"]))
                        {
                            ubModel.GradeState = int.Parse(context.Request["GradeState"]);
                        }
                        if (!string.IsNullOrEmpty(context.Request["UB_UserName"]))
                        {
                            ubModel.UB_UserName = context.Request["UB_UserName"];
                        }
                        if (!string.IsNullOrEmpty(context.Request["UB_UserID"]))
                        {
                            ubModel.UB_UserID = context.Request["UB_UserID"];
                        }
                        if (!string.IsNullOrEmpty(context.Request["UB_Sex"]))
                        {
                            ubModel.UB_Sex = int.Parse(context.Request["UB_Sex"]) == 1 ? true : false;
                        }
                        if (!ubBLL.Exists(ubModel.UB_UserID))
                        {
                            ubBLL.Add(ubModel);
                            utModel.SI_GUID = ubModel.UB_GUID;
                            if (!string.IsNullOrEmpty(context.Request["SI_Mail"]))
                            {
                                utModel.SI_Mail = context.Request["SI_Mail"];
                            }
                            if (!string.IsNullOrEmpty(context.Request["SI_QQ"]))
                            {
                                utModel.SI_QQ = context.Request["SI_QQ"];
                            }
                            EduBLL.Student_Class scBLL = new EduBLL.Student_Class();
                            EduModel.Student_Class scModel = new EduModel.Student_Class();
                            scModel.SC_GUID = Guid.NewGuid();
                            scModel.SC_StudentGUID = ubModel.UB_GUID;
                            scModel.State = 1;
                            if (!string.IsNullOrEmpty(context.Request["classGuid"]))
                            {
                                scModel.SC_ClassGUID = new Guid(context.Request["classGuid"]);
                            }
                            if (utBll.Add(utModel) > 0 && scBLL.Add(scModel) > 0)
                            {
                                sb.Append("success");
                            }
                        }
                        else
                        {
                            sb.Append("该账号已存在！");
                        }
                        break;

                    case "deleteUnitStudent": //删除
                        if (!string.IsNullOrEmpty(context.Request["UB_SN"]))
                        {
                            ubModel = ubBLL.GetModel(new Guid(context.Request["UB_SN"]));
                            ubModel.State = -1;
                            if (ubBLL.Update(ubModel))
                            {
                                sb.Append("success");
                            }
                        }
                        break;

                    case "updateStudentPwd":
                        if (!string.IsNullOrEmpty(context.Request["UB_SN"]))
                        {
                            ubModel = ubBLL.GetModel(new Guid(context.Request["UB_SN"]));
                            ubModel.UB_UserPsw = "123456";
                            if (ubBLL.Update(ubModel))
                            {
                                sb.Append("success");
                            }
                        }
                        break;

                    case "getClassList":  //获取班级列表信息
                        if (!string.IsNullOrEmpty(context.Request["filter[filters][0][filter][filters][0][value]"]))
                        {
                            if (context.Request["filter[filters][0][filter][filters][0][value]"] != "all")
                                key += " and GradeState=" + context.Request["filter[filters][0][filter][filters][0][value]"] + "";
                        }
                        sb.Append(EduMethods.UserManage.getUnitClassList(login, key));
                        break;
                    case "IsExist":
                        if (!string.IsNullOrEmpty(context.Request["UB_UserID"]))
                        {
                            if (ubBLL.Exists(context.Request["UB_UserID"]))
                            {
                                sb.Append("1");
                            }
                            else
                            {
                                sb.Append("0");
                            }
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                log.Write(e, Common.MsgType.Error);
            }
            context.Response.Write(sb.Length > 0 ? sb.ToString() : "error");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}