﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace EduTeaStuTools.Handler
{
    /// <summary>
    /// getUserManage 的摘要说明
    /// </summary>
    public class getUserManage : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            Common.Log log = new Common.Log();
            StringBuilder sb = new StringBuilder();  //返回数据结果信息
            EduMethods.LoginInfo login = new EduMethods.LoginInfo();
            if (context.Session["WebLoginInfo"] != null)
            {
                login = (EduMethods.LoginInfo)context.Session["WebLoginInfo"];
            }
            try
            {
                string type = context.Request.Form["type"];
                if (type == null)
                {
                    type = context.Request["type"];
                }

                string sort = string.Empty;
                string key = " and 1=1 ";

                EduBLL.System_Manage smBll = new EduBLL.System_Manage();
                EduModel.System_Manage smModel = new EduModel.System_Manage();
                switch (type)
                {
                    case "getUnitUserList":  //列表
                        int pIndex = Convert.ToInt32(context.Request.Form["page"]);
                        int pSize = Convert.ToInt32(context.Request.Form["pageSize"]);
                        if (!string.IsNullOrEmpty(context.Request["sort[0][field]"]) && !string.IsNullOrEmpty(context.Request["sort[0][dir]"]))
                        {
                            sort = context.Request["sort[0][field]"] + "  " + context.Request["sort[0][dir]"];
                        }
                        if (!string.IsNullOrEmpty(context.Request["filter[filters][0][filter][filters][0][value]"]))
                        {
                            key += " and sm.SM_ManageID  like '%" + context.Request["filter[filters][0][filter][filters][0][value]"] + "%'";
                        }
                        if (!string.IsNullOrEmpty(context.Request["filter[filters][0][filter][filters][1][value]"]))
                        {
                            key += " and sm.SM_ManageName like '%" + context.Request["filter[filters][0][filter][filters][1][value]"] + "%'";
                        }
                        sb.Append(EduMethods.UserManage.getUnitUserManage(login, key, pIndex, pSize, sort));
                        break;
                    case "editUnitUser":  //更新
                        if (!string.IsNullOrEmpty(context.Request["SM_SN"]))
                        {
                            smModel = smBll.GetModel(int.Parse(context.Request["SM_SN"]));
                            if (!string.IsNullOrEmpty(context.Request["SM_ManageName"]))
                            {
                                smModel.SM_ManageName = context.Request["SM_ManageName"];
                            }
                            if (!string.IsNullOrEmpty(context.Request["SM_State"]))
                            {
                                smModel.SM_State = int.Parse(context.Request["SM_State"]);
                            }
                            if (smBll.Update(smModel))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                    case "addUnitUser":  //添加
                        smModel.SM_State = 1;
                        smModel.SM_GUID = Guid.NewGuid();
                        smModel.SM_Role = 1;  //普通管理员
                        if (!string.IsNullOrEmpty(context.Request["SM_ManageID"]))
                        {
                            smModel.SM_ManageID = context.Request["SM_ManageID"];
                        }
                        if (!string.IsNullOrEmpty(context.Request["SM_ManageName"]))
                        {
                            smModel.SM_ManageName = context.Request["SM_ManageName"];
                        }
                        if (!string.IsNullOrEmpty(context.Request["UB_Name"]))
                        {
                            smModel.SM_UBGuid = new Guid(context.Request["UB_Name"]);
                        }
                        if (!string.IsNullOrEmpty(context.Request["SM_State"]))
                        {
                            smModel.SM_State = int.Parse(context.Request["SM_State"]);
                        }
                        smModel.SM_ManagePsw = "123456";
                        if (smBll.Add(smModel) > 0)
                        {
                            sb.Append("success");
                        }
                        break;
                    case "deleteUnitUser": //删除
                        if (!string.IsNullOrEmpty(context.Request["SM_SN"]))
                        {
                            smModel = smBll.GetModel(int.Parse(context.Request["SM_SN"]));
                            smModel.SM_State = -1;
                            if (smBll.Update(smModel))
                            {
                                sb.Append("success");
                            }
                        }
                        break;
                    case "IsExist":
                        if (!string.IsNullOrEmpty(context.Request["UB_UserID"]))
                        {
                            if (smBll.Exists(context.Request["UB_UserID"]))
                            {
                                sb.Append("1");
                            }
                            else
                            {
                                sb.Append("0");
                            }
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                log.Write(e, Common.MsgType.Error);
            }
            context.Response.Write(sb.Length > 0 ? sb.ToString() : "error");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}