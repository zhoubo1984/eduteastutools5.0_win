﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Collections;
using System.Web.SessionState;
using System.IO;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;

namespace EduTeaStuTools.Handler
{
    /// <summary>
    /// getUserLogin 的摘要说明
    /// </summary>
    public class getUserLogin : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            Common.Log log = new Common.Log();
            string clientIP = string.Empty;        //客户端IP
            if (context.Request.ServerVariables["HTTP_VIA"] != null) // using proxy
            {
                clientIP = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();  // Return real client IP.
            }
            else// not using proxy or can't get the Client IP
            {
                clientIP = context.Request.ServerVariables["REMOTE_ADDR"].ToString(); //While it can't get the Client IP, it will return proxy IP.
            }
            StringBuilder sb = new StringBuilder();  //返回数据结果信息
            try
            {
                string type = context.Request.Form["type"];
                if (type == null)
                {
                    type = context.Request["type"];
                }

                string sort = string.Empty;
                string key = string.Empty;  //关键字
                if (!string.IsNullOrEmpty(context.Request["key"]))
                    key = context.Request["key"];
                EduMethods.LoginInfo login = new EduMethods.LoginInfo();
                if (context.Session["WebLoginInfo"] != null)
                {
                    login = (EduMethods.LoginInfo)context.Session["WebLoginInfo"];
                }
                switch (type)
                {
                    case "userLogin"://
                        //if (!string.IsNullOrEmpty(context.Request["name"]) && !string.IsNullOrEmpty(context.Request["pwd"]) && !string.IsNullOrEmpty(context.Request["code"]))
                        if (!string.IsNullOrEmpty(context.Request["name"]) && !string.IsNullOrEmpty(context.Request["pwd"]))
                        {
                            sb.Append(CheckUserLogin(context, context.Request["name"], context.Request["pwd"], context.Request["code"]));
                        }
                        break;
                    case "updatePwd":
                        if (!string.IsNullOrEmpty(context.Request["oldPwd"]) && !string.IsNullOrEmpty(context.Request["newPwd"]))
                        {
                            sb.Append(EduMethods.UserManage.UpdateManagePwd(login, context.Request["oldPwd"], context.Request["newPwd"]));
                        }
                        break;
                    case "userInfo":
                        if (login != null)
                        {
                            sb.Append(login.RealName + "|" + login.UserRole);
                        }
                        break;
                    case "userExit":
                        if (context.Session["WebLoginInfo"] != null)  //退出系统
                        {
                            context.Session.Remove("WebLoginInfo");
                            context.Session.Abandon();
                            context.Session.Clear();
                            sb.Append("success");
                        }
                        else
                        {
                            sb.Append("error");
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                log.Write(e, Common.MsgType.Error);
            }
            context.Response.Write(sb.Length > 0 ? sb.ToString() : "error");
        }



        /// <summary>
        /// 验证用户登录
        /// </summary>
        /// <param name="c"></param>
        /// <param name="userName">用户名</param>
        /// <param name="userPwd">密码</param>
        /// <param name="code">验证码</param>
        /// <returns></returns>
        public string CheckUserLogin(HttpContext c, string userName, string userPwd, string code)
        {
            //if (c.Session["CheckCode"] == null)
            //{
            //    return "3";
            //}
            //else if (c.Session["CheckCode"].ToString().ToLower() != code.ToLower())
            //{
            //    return "4";
            //}
            EduMethods.LoginInfo login = new EduMethods.LoginInfo();
            string result = string.Empty;
            StringBuilder sb = new StringBuilder();
            DataTable dt = null;
            sb.Append(" select sm.SM_GUID,sm.SM_SN,sm.SM_ManageName,ub.UB_GUID,ub.UB_PGUID,ub.UB_SN,ub.UB_Name  ");
            sb.Append(" from dbo.System_Manage sm  ");
            sb.Append(" inner join dbo.Unit_Base ub on sm.SM_UBGuid=ub.UB_GUID ");
            sb.Append(" where sm.SM_ManageID=@SM_ManageID and sm.SM_ManagePsw=@SM_ManagePsw and sm.SM_State=1 ");
            SqlParameter[] parameters = {
					                  new SqlParameter("@SM_ManageID", SqlDbType.NVarChar,-1),
				                      new SqlParameter("@SM_ManagePsw", SqlDbType.NVarChar,-1)};
            parameters[0].Value = userName;
            parameters[1].Value = userPwd;
            dt = DBUtility.DbHelperSQL.Query(sb.ToString(), parameters).Tables[0];
            if (dt.Rows.Count > 0)
            {
                login.LoginSN = int.Parse(dt.Rows[0]["SM_SN"].ToString());
                login.LoginGuid = dt.Rows[0]["SM_GUID"].ToString();
                login.LoginName = userName;
                login.RealName = dt.Rows[0]["SM_ManageName"].ToString();
                login.UnitGuid = dt.Rows[0]["UB_GUID"].ToString();
                login.UnitName = dt.Rows[0]["UB_Name"].ToString();
                login.UnitSN = int.Parse(dt.Rows[0]["UB_SN"].ToString());
                if (!string.IsNullOrEmpty(dt.Rows[0]["UB_PGUID"].ToString()))
                {
                    login.UserRole = 2;
                }
                else
                {
                    login.UserRole = 1;
                }

                c.Session["WebLoginInfo"] = login;
                result = "1";
            }
            else
            {
                result = "2";
            }
            return result;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}