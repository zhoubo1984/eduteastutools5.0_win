﻿function verifyIsNull(str) {
    if (str.length == 0) {
        return false;
    }
    else {
        return true;
    }
}
//验证邮箱 
function verfifyEmail(strEmail) {
    var reg = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
    if (reg.test(strEmail)) {
        return true;
    } else {
        return false;
    }
}
//验证qq号码
function verifyQQ(strQQ) {
    var reg = /^[1-9]\d{4,12}$/;
    if (reg.test(strQQ)) {
        return true;
    } else {
        return false;
    }
}
//验证电话号码
function verifyPhone(strPhone) {
    var reg = /((\d{11})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)/;
    if (reg.test(strPhone)) {
        return true;
    } else {
        return false;
    }
}
