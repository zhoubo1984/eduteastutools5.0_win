﻿$(function () {
    setInterval("GetTime()", 1000);
});
function BindGrade() {
    $.ajax({
        type: "post",
        url: "Handler/getUnitInfo.ashx",
        async: false,
        data: { type: 'getUnitGradeList' },
        success: function (result) {
            if (result == "error") {
                alert('数据加载失败... ...');
                return;
            }
            var json = eval('(' + result + ')');
            var div_body = '';
            div_body = "<ul>";
            div_body += "<li><a href=\"javascript:void(0)\" id='index' onclick='goBackIndex()'>首页</a></li>";
            $.each(json, function (i, item) {
                div_body += "<li><a href=\"javascript:void(0)\" id=\"daohang_a_" + item.GB_Type + "\" onclick='setLiCss(" + item.GB_Type + ")'>" + item.GB_Name + "</a></li>";
            });
            div_body += "</ul>";
            $("#div_daohang").html(div_body);
        }
    });
}
function GetTime() {
    var mon, day, now, hour, min, ampm, time, str, tz, end, beg, sec;
    /*
    mon = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec");
    */
    mon = new Array("1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月",
            "9月", "10月", "11月", "12月");
    /*
    day = new Array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
    */
    day = new Array("星期天", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六");
    now = new Date();
    hour = now.getHours();
    min = now.getMinutes();
    sec = now.getSeconds();
    if (hour < 10) {
        hour = "0" + hour;
    }
    if (min < 10) {
        min = "0" + min;
    }
    if (sec < 10) {
        sec = "0" + sec;
    }
    $("#Timer").html(
            "<nobr>" + now.getFullYear() + "年" + mon[now.getMonth()] + now.getDate() + "日" + hour
                    + ":" + min + ":" + sec + " " + day[now.getDay()] + "</nobr>");

}
function AddFavorite(sURL, sTitle) {
    try {
        window.external.addFavorite(sURL, sTitle);
    }
    catch (e) {
        try {
            window.sidebar.addPanel(sTitle, sURL, "");
        }
        catch (e) {
            alert("加入收藏失败，请使用Ctrl+D进行添加");
        }
    }
}
//设为首页 <a onclick="SetHome(this,window.location)">设为首页</a>
function SetHome(obj, vrl) {
    try {
        obj.style.behavior = 'url(#default#homepage)'; obj.setHomePage(vrl);
    }
    catch (e) {
        if (window.netscape) {
            try {
                netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
            }
            catch (e) {
                alert("此操作被浏览器拒绝！\n请在浏览器地址栏输入“about:config”并回车\n然后将 [signed.applets.codebase_principal_support]的值设置为'true',双击即可。");
            }
            var prefs = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
            prefs.setCharPref('browser.startup.homepage', vrl);
        }
    }
}

//设置点击样式
function setLiCss(num) {
    var id = 'daohang_a_' + num;
    $("#div_daohang ul li a").each(function (i, e) {
        if ($(e).attr("id") != 'index') {
            if ($(e).attr("id") == id) {
                $(e).addClass('dianji');
                $("#div_xiala").html('');
                $.ajax({
                    type: "post",
                    url: "Handler/getUnitInfo.ashx",
                    async: false,
                    data: { type: 'getUnitGradeSubject', gradeType: num },
                    success: function (result) {
                        if (result == "error") {
                            alert('数据加载失败... ...');
                            return;
                        }
                        var json = eval('(' + result + ')');
                        var div_body = '';
                        div_body = "<ul>";
                        $.each(json.data, function (i, item) {
                            div_body += "<li><a href=\"#\"  onclick='gotoPage(" + item.sn + ")'>" + item.name + "</a></li>";
                            //div_body += "<li><a href=\"ClassList.html?sub=" + item.sn + "\">" + item.name + "</a></li>";
                        });
                        div_body += "</ul>";
                        $("#div_xiala").html(div_body);

                    }
                });
                $("#div_xiala").show();
            }
            else
                $(e).removeClass('dianji');
        }
    });

}

function goBackIndex() {
    $("#div_xiala").hide();
    $("#div_daohang ul li a").each(function (i, e) {
        if ($(e).attr("id") != 'index') {
            $(e).removeClass('dianji');
        }
    });
    window.location.href = "Index.html";
}

function addSelect() {
    $.ajax({
        type: "post",
        url: "Handler/getUnitInfo.ashx",
        async: false,
        data: { type: 'getUnitSubjectList' },
        success: function (result) {
            if (result == "error") {
                alert('数据加载失败... ...');
                return;
            }
            var json = eval('(' + result + ')');
            $("#selectSubject").html();
            var div_body = '';
            $.each(json, function (i, item) {
                div_body += '<option value="' + item.SB_SN + '">' + item.SB_SubjectName + '</option>';
            });
            $("#selectSubject").html(div_body);
        }
    });
}

function gotoPage(num) {
    window.location.href = "ClassList.html?sub=" + num + "&type=" + $("#selectType").val();
}
function searchSubject() {
    if ($("#keyword").val().length == 0)
        alert('请输入查询关键字！')
    else
        window.location.href = "ClassList.html?sub=" + $("#selectSubject").val() + "&type=" + $("#selectType").val() + "&key=" + $("#keyword").val();
}