﻿
var SlUploadUrl = getRootPath() + "/FileStore/SlUpload/Upload.aspx";
var DownloadUrl = getRootPath() + "/FileStore/Download.aspx";

function getFile(url, fileId) {
    window.open(url + "?FileId=" + fileId);
}

function getRootPath() {
    var pathName = window.location.pathname.substring(1);
    var webName = pathName == '' ? '' : pathName.substring(0, pathName.indexOf('/'));
    return window.location.protocol + '//' + window.location.host;
}


//将文件Id中的文件名处理掉
function fixFileIds(fileIds) {
    var result = fileIds.replace(/[^_,0-9]/g, function (n) {
        return "";
    }).replace(/_+/g, "_");

    return result;
}

function getMiniFileName(fileName) {
    var start = fileName.indexOf('_');
    var end = fileName.lastIndexOf('_');
    if (end == start)
        end = fileName.length;
    return fileName.substr(start + 1, end - start - 1);
}

function DownloadFile(fileId) {
    var url = DownloadUrl;
    window.open(url + "?FileId=" + fileId);
}

