﻿document.write("<link href='/CommonWebResource/CoreLib/MiniUI/File/MiniSingleFile.css' rel='stylesheet' type='text/css' />");
document.write('<script src="/CommonWebResource/CoreLib/MiniUI/File/fileStore.js" type="text/javascript" ></sc' + 'ript>');


//--------------------------------mini-SingleFile对象定义开始 从mini-Control继承----------------------------------------
mini.ux.SingleFile = function () {
    mini.ux.SingleFile.superclass.constructor.call(this);
    this.file = "";
    this.initControls();
    this.initEvents();
}

mini.extend(mini.ux.SingleFile, mini.Control, {
    formField: true,
    title: "上传",
    width: 200,
    required: false, //是否必填
    readonly: false, //是否只读
    disabled: false, //是否禁用
    maximumupload: "", //上传的最大文件大小
    maxnumbertoupload: "1", //上传的最大文件数量
    filter: "", //上传的文件类型
    allowthumbnail: false, //是否缩略图
    src: "system", //所属系统模块
    uiCls: "mini-singlefile",
    initControls: function () {
        mini.parse(this.el);
        var attrs = mini.ux.SingleFile.superclass.getAttrs.call(this, this.el);
        var $border = $("<span>").addClass("mini-singlefile-border");
        $border.append($("<input>").attr("type", "text").addClass("mini-singlefile-input").attr("readonly", "readonly"));
        var $icon = $("<span>").addClass("mini-singlefile-buttons");
        $icon.append($("<span>").addClass("mini-singlefile-remove").attr("title", "删除"));
        $icon.append(($("<span>").addClass("mini-singlefile-button")).append($("<span>").addClass("mini-singlefile-upload").attr("title", "上传")));
        $border.append($icon);
        $(this.el).append($border);
        this._fileBorder = $border;
        this._txtDownLoad = $(this.el).find("input.mini-singlefile-input");
        this._btnRemove = $(this.el).find("span.mini-singlefile-remove");
        this._btnUpLoad = $(this.el).find("span.mini-singlefile-upload");
    },
    initEvents: function () {
        var ctrl = this;
        this._txtDownLoad.click(function (event) {
            ctrl.downloadFile();
        });
        this._btnRemove.click(function (event) {
            ctrl.removeValue();
        });
        this._btnUpLoad.click(function (event) {
            ctrl.uploadFile();
        });
    },
    render: function () {
        var filename = getMiniFileName(this.file);
        this._txtDownLoad.attr("value", filename);
    },
    setValue: function (file) {
        this.file = $.trim(file);
        this.render();
    },
    getValue: function () {
        return $.trim(this.file);
    },
    getText: function () {
        var filename = getMiniFileName(this.getValue());
        return filename;
    },
    setReadOnly: function (isReadonly) {
        if (isReadonly) {
            this.readonly = true;
            this.addCls("asLabel");
            this._btnRemove.hide();
            this._btnUpLoad.hide();
        }
        else {
            this.readonly = false;
            this.removeCls("asLabel");
            this._btnRemove.show();
            this._btnUpLoad.show(); ;
        }
    },
    setDisabled: function (isDisabled) {
        if (isDisabled) {
            this.disabled = true;
            this._fileBorder.css("backgroundColor", "rgb(240, 240, 240)");
            this._txtDownLoad.attr("disabled", true);
            this._btnRemove.attr("disabled", true);
            this._btnUpLoad.attr("disabled", true);
        }
        else {
            this.disabled = false;
            this._fileBorder.css("backgroundColor", "");
            this._txtDownLoad.removeAttr("disabled");
            this._btnRemove.removeAttr("disabled");
            this._btnUpLoad.removeAttr("disabled");
        }
    },
    setRequired: function (isRequired) {
        if (isRequired) {
            this.required = true;
            this._fileBorder.css("backgroundColor", "rgb(255, 255, 230)");
        }
        else {
            this.required = false;
            this._fileBorder.css("backgroundColor", "");
        }
    },
    validate: function () {
        var isVal = this.isValid();
        var errorId = this.id + "-errorIcon";
        if (!isVal) {
            if ($("#" + errorId).length == 0) {
                //添加error图标
                var imgError = $("<img id='" + errorId + "' src='/CommonWebResource/Theme/Default/MiniUI/images/panel/error.gif' width='14' height='14' title='不能为空' />");
                $("#" + this.id).after(imgError);
            }
            return false;
        }
        else {
            $("#" + errorId).remove();
            return true;
        }
    },
    isValid: function () {
        if (this.required) {
            if (this.getValue() != "") {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return true;
        }
    },
    addValue: function (value) {
        var oldValue = this.file;
        if ($.trim(oldValue) != "") {
            this.file = $.trim(value);
            callbackSetValue(this.id);
        }
        else {
            this.setValue(value);
        }
    },
    removeValue: function () {
        if ($.trim(this.file) != "") {
            var ctrl = this;
            var file = ctrl.file;
            mini.confirm("确定删除文件?", "确定",
                function (action) {
                    if (action == "ok") {
                        ctrl.file = "";
                        callbackSetValue(ctrl.id);
                    }
                }
            );
        }
    },
    uploadFile: function () {
        var value = "FileIds";
        var fileCtrl = this;
        mini.open({
            url: SlUploadUrl + "?value=" + value + "&" + "FileMode=multi&IsLog=undefined&Filter=" + escape(fileCtrl.filter) + "&MaximumUpload=" + fileCtrl.maximumupload + "&MaxNumberToUpload=" + fileCtrl.maxnumbertoupload + "&AllowThumbnail=" + fileCtrl.allowthumbnail + "&RelateId=&Src=" + fileCtrl.src,
            width: 500,
            height: 300,
            ondestroy: function (rtnValue) {
                if (rtnValue.substring(0, 3) == "err") {
                    alert(rtnValue);
                }
                else {
                    fileCtrl.addValue(rtnValue);
                }
            }
        });
    },
    downloadFile: function () {
        if ($.trim(this.file) != "") {
            DownloadFile(escape(this.file));
        }
    },
    getAttrs: function (el) {
        var attrs = mini.ux.SingleFile.superclass.getAttrs.call(this, el);
        mini._ParseString(el, attrs,
            [
                "required", "readonly", "disabled", "maximumupload", "filter", "allowthumbnail", "src"]
        );
        return attrs;
    }
});

function callbackSetValue(ctrlId) {
    if (ctrlId == "")
        return;
    var minictrl = mini.get(ctrlId);
    var val = minictrl.file;
    minictrl.setValue(val);
}

mini.regClass(mini.ux.SingleFile, "singlefile");
