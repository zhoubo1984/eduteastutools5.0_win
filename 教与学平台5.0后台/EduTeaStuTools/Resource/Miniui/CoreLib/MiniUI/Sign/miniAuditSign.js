﻿//--------------------------------mini-AuditSign会签控件------------------------------------------

mini.ux.AuditSign = function () {
    mini.ux.AuditSign.superclass.constructor.call(this);
    this.initScript();
}

mini.extend(mini.ux.AuditSign, mini.Control, {
    formField: true,
    uiCls: "mini-auditsign",
    defaultTmpl: "/CommonWebResource/CoreLib/MiniUI/Sign/auditSignTmpl.js", //审批签名模板
    tmplurl: "", //自定义模板
    tmplItem: "AuditItem",
    tmplName: "auditSignTmpl",
    initScript: function () {
        ajaxPage(this.tmplName, this.defaultTmpl);
    },
    render: function () {
        var renderHTML = "";
        var itemName = this.tmplItem;
        var tmplName = this.tmplName;
        $.each(this.dataField, function (i, auditSign) {
            var tmpItem = eval(tmplName + "_AuditSignTempleteItem");
            for (var prop in auditSign) {
                if (typeof (auditSign[prop]) == "string") {
                    tmpItem = tmpItem.replace("$" + prop, auditSign[prop]);
                }
            }
            renderHTML += tmpItem;
        });
        this.el.innerHTML = eval(tmplName + "_AuditSignTemplete").replace("$" + itemName, renderHTML);
    },
    setTmplurl: function (url) {
        this.tmplName = getFileNameNoExt(url);
        ajaxPage(this.tmplName, url);
    },
    setValue: function (val) {
        this.dataField = val;
        this.render();
    },
    getValue: function () {
        return this.dataField;
    },
    getFormValue: function () {
        return mini.encode(this.dataField);
    },
    getAttrs: function (el) {
        var attrs = mini.ux.AuditSign.superclass.getAttrs.call(this, el);
        mini._ParseString(el, attrs,
            [
                "tmplurl"]
        );
        return attrs;
    }
});

mini.regClass(mini.ux.AuditSign, "auditsign");

//--------------------------------辅助方法------------------------------------------
function getFileNameNoExt(url) {
    var fileName = getFileName(url);
    return fileName.replace(getFileExt(fileName));
}

function getFileName(url) {
    var pos = url.lastIndexOf("/");
    if (pos == -1) {
        pos = url.lastIndexOf("\\")
    }
    var filename = url.substr(pos + 1)
    var ext = getFileExt(filename);
    return filename.replace(ext, "");
}

//取文件后缀名
function getFileExt(filepath) {
    if (filepath != "") {
        var pos = "." + filepath.replace(/.+\./, "");
        return pos;
    }
} 

function ajaxPage(sId, url) {
    var oXmlHttp = getHttpRequest();
    oXmlHttp.onreadystatechange = function () {
        //4代表数据发送完毕
        if (oXmlHttp.readyState == 4) {
            //0为访问的本地，200代表访问服务器成功，304代表没做修改访问的是缓存
            if (oXmlHttp.status == 200 || oXmlHttp.status == 0 || oXmlHttp.status == 304) {
                includeJS(sId, oXmlHttp.responseText);
            }
            else {
            }
        }
    }
    oXmlHttp.open("GET", url, false);
    oXmlHttp.send(null);
}

function getHttpRequest() {
    if (window.ActiveXObject)//IE
    {
        return new ActiveXObject("MsXml2.XmlHttp");
    }
    else if (window.XMLHttpRequest)//其他
    {
        return new XMLHttpRequest();
    }
}

function includeJS(sId, source) {
    if ((source != null) && (!document.getElementById(sId))) {
        var myHead = document.getElementsByTagName("HEAD").item(0);
        var myScript = document.createElement("script");
        myScript.language = "javascript";
        myScript.type = "text/javascript";
        myScript.id = sId;
        try {
            myScript.appendChild(document.createTextNode(source));
        }
        catch (ex) {
            myScript.text = source;
        }
        myHead.appendChild(myScript);
    }
}