﻿mini.ux.SignPic = function () {
    mini.ux.SignPic.superclass.constructor.call(this);
    this.initControls();
}

mini.extend(mini.ux.SignPic, mini.Control, {
    uiCls: "mini-signpic",
    width: 80,
    height: 30,
    UserGuid: "",
    src: "/MvcConfig/Image/GetSignPic?UserGuid=",
    noneImageSrc: "/CommonWebResource/RelateResource/image/transparent.gif",
    initControls: function () {
        var $img = $("<img>");
        $(this.el).append($img);
        this._Image = $img;
        this._Image.attr("src", this.noneImageSrc).height(this.height).width(this.width);
    },
    setUserGuid: function (UserGuid) {
        this.UserGuid = UserGuid;
        if ($.trim(UserGuid) != "")
            this._Image.attr("src", this.src + UserGuid);
        else
            this._Image.attr("src", this.noneImageSrc);
    },
    setValue: function (value) {
        this.setUserGuid(value);
    },
    getValue: function () {
        return this.UserGuid;
    },
    getAttrs: function (el) {
        var attrs = mini.ux.SignPic.superclass.getAttrs.call(this, el);
        mini._ParseString(el, attrs,
            [
                "UserGuid", "width", "height"]
        );
        return attrs;
    }
});

mini.regClass(mini.ux.SignPic, "signpic");
