﻿
function mySearch() {
    var key = $("#keyWords").val();
    if (key.length == 0)
        return;
    getList(1);
}

function getSearchList(page) {
    $.ajax({
        type: "post",
        url: "Handler/getResourceList.ashx",
        data: { type: 'search', key: key, page: page },
        success: function (result) {
            if (result == "error") {
                $("#div_list").empty();
                mini.alert("没有数据");
                return;
            }
            var json = eval('(' + result + ')');
            bindSearchDataList(1, json);
        }
    });
}

function goToSearchPage(page) {
    getSearchList(page);
}

function markSearchResource(i, mark, id, tName) {
    var cookMark = getCookie("mark_res_" + i);
    if (cookMark != null)
        mark = cookMark;
    if (mark != '') {
        mini.confirm("资源已加入收藏夹，您是否要取消收藏？", "消息提示框",
        function (action) {
            if (action == "ok") {
                $.ajax({
                    type: "post",
                    url: "Handler/getResourceList.ashx",
                    data: { type: 'removeFav', id: mark },
                    success: function (result) {
                        if (result != "error") {
                            mini.alert("您已成功取消对该资源的收藏！");
                            $("#mark_res_" + i).attr("src", "Resource/Images/fav01.gif");
                            delCookie("mark_res_" + i);
                        }
                        else
                            mini.alert("移除收藏夹失败！");
                    }
                });
            }
        }
    );
    }
    else {
        mini.confirm("是否添加到收藏夹藏？", "消息提示框",
        function (action) {
            if (action == "ok") {
                $.ajax({
                    type: "post",
                    url: "Handler/getResourceList.ashx",
                    data: { type: 'markFavourite', id: id, tName: tName },
                    success: function (result) {
                        if (result != "error") {
                            mini.alert("已经成功添加到收藏夹！");
                            $("#mark_res_" + i).attr("src", "Resource/Images/fav02.gif");
                            setCookie("mark_res_" + i, result);
                        }
                        else
                            mini.alert("添加到收藏夹失败，请您重新添加！");
                    }
                });
            }
        }
       );
    }
}


function bindSearchDataList(page, json) {
    removeCookie();
    $("#div_list").empty();
    var body = "";
    body += "  <br />";
    body += "                                            <div>";
    body += "                                                <div class=\"weizhi\">";
    body += "                                                    <div class=\"weizhi_b\">";
    body += "                                                        当前位置：数据搜索";
    body += "                                                    </div>";
    body += "  <div style=\"float: right; padding-top: 5px; padding-right: 5px;\">";
    body += "                                                        媒体类型：<select id=\"select_type\" style=\"width: 100px;\" onchange=\"selectType()\">";
    body += "                                                            <option value=\"all\">请选择</option>";
    body += "                                                            <option value=\"txt\">文本</option>";
    body += "                                                            <option value=\"jpg\">图片</option>";
    body += "                                                            <option value=\"mp3\">音频</option>";
    body += "                                                            <option value=\"rmvb\">视频</option>";
    body += "                                                            <option value=\"swf\">动漫</option>";
    body += "                                                            <option value=\"exe\">应用</option>";
    body += "                                                        </select>";
    body += "                                                    </div>";
    body += "                                                </div>";
    body += "                                                <div align=\"left\" class=\"list_right_right\">";
    body += "                                                    <div style=\"float: left;\">找到相关资源约" + json.total + "个，用时" + json.time + "秒</div>";
    body += "                                                    <div style=\"width: 340px; height: 30px; float: right;\">";
    body += "                                                        <div class=\"botton_b\" style=\"margin-top: 4px;\">";
    body += "                                                            <div class=\"zc_zi2 FOAT_F\">";
    body += "                                                                <a href=\"javascript:research(\'\');\">在结果中搜索</a>";
    body += "                                                            </div>";
    body += "                                                        </div>";

    body += "                                                            <div class=\"text2\" style=\"margin-top: 7px;\">";
    body += "                                                                <input name=\"text\" id=\"keyWords\" class=\"search_right\" onkeydown=\"if(window.event.keyCode==13) {research();return false;}\" type=\"text\" size=\"30\" value=\"\" />";
    body += "                                                            </div>";
    body += "                                                        </div>";

    body += "                                                    </div>";
    var startIndex = 1;
    var endIndex = 1;
    if (json.count <= 9) {
        startIndex = 1;
        endIndex = json.count;
    }
    else {
        if (page <= 5) {
            startIndex = 1;
            endIndex = 9;
        }
        else {
            if (json.count - page >= 4) {
                startIndex = page - 4;
                endIndex = page + 4;
            }
            else {
                startIndex = json.count - 9;
                endIndex = json.count;
            }
        }
    }
    body += "                                                    <div class=\"yema\" style=\"width: 100%; height: 24px; text-align: center;\">";
    body += "                                                        <div class=\"H_C\" style=\"height: 24px; margin-bottom: 10px;\">";
    body += "                                                            <ul style=\"height: 24px; text-align: center; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px;\">";
    if (page > 1)
        body += "<li style=\"width: 50px; display: inline; cursor: pointer;\" onclick=\"goToSearchPage(" + (page - 1) + ")\">上一页</li>";

    for (var i = startIndex; i <= endIndex; i++) {
        if (i != page)
            body += "<li style=\"width: 20px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; margin-left: 5px; display: inline; cursor: pointer;\" onclick=\"goToPage(" + i + ")\">" + i + "</li>";
        else {
            body += "<li style=\"width: 20px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; margin-left: 5px; display: inline;\">";
            body += "<span class=\"currNo\" style=\"font-size: 14px; font-weight: bold;\">" + i + "</span>";
            body += "</li>";
        }
    }
    if (page < json.count)
        body += "<li style=\"width: 50px; display: inline; cursor: pointer;\" onclick=\"goToSearchPage(" + (page + 1) + ")\">下一页</li>";

    body += "                                                            </ul>";
    body += "                                                        </div>";
    body += "                                                        <br />";
    body += "                                                    </div>";
    $.each(json.data, function (i, item) {
        body += "                                                    <div class=\"zy\">";
        body += "                                                        <div class=\"zy_a\">";
        body += "                                                            <ul style=\"width: 100%; height: 32px;\">";
        body += "                                                                <li style=\"width: 32px; height: 32px; float: left;\">";
        body += "                                                                    <img width=\"32\" height=\"32\" src=\"Resource/Images/" + item.kind + "\" />";
        body += "                                                                </li>";
        body += "                                                                <li style=\"height: 32px; line-height: 32px; margin-left: 5px; float: left;\">";
        body += "                                                                    <a style=\"text-decoration: none;\" href=\"classDetails.aspx?id=" + item.id + "&tName=" + item.tName + "\" target=\"_blank\">";
        body += " <span class=\"keming\" style=\"color: #4a25c3; font-weight: bold; text-decoration: underline;\">" + item.name + "</span>-- 大小：" + item.filePath;
        body += "  ";
        body += "                                                                    </a>";
        body += "                                                                </li>";
        body += "                                                            </ul>";
        body += "                                                        </div>";
        body += "                                                        <p class=\"zy_a\">";
        body += "                                                            " + item.readme.length == 0 ? "无简介" : item.readme;
        body += "                                                        </p>";
        body += "                                                        <div class=\"zy_a zy_xinxi\">";
        body += "    <table border=\"0\" cellspacing=\"0\">";
        body += "                                                            <tr>";
        body += "                                                                <td width=\"75%\">文本 - 适应对象：学生,教师 </td>";
        body += "                                                                <td align=\"right\" width=\"25%\">";
        body += "                                                                    <a style=\"cursor: pointer;\">";
        if (item.mark.length > 0) {
            body += "             <img src=\"Resource/Images/fav02.gif\" id=\"mark_res_" + i + "\" onclick=\"markSearchResource(" + i + "," + item.mark + "," + item.id + ",'" + item.tName + "')\" />我要收藏 </a><a style=\"cursor: pointer;\" target='_blank' href=\"Down.aspx?id=" + item.id + "&tName=" + item.tName + "\">";
        }
        else {
            body += "             <img src=\"Resource/Images/fav01.gif\" id=\"mark_res_" + i + "\" onclick=\"markSearchResource(" + i + ",''," + item.id + ",'" + item.tName + "')\" />我要收藏 </a><a style=\"cursor: pointer;\" target='_blank' href=\"Down.aspx?id=" + item.id + "&tName=" + item.tName + "\">";
        }
        body += "  <img src=\"Resource/Images/down.gif\" />我要下载</a>";
        body += "                                                                </td>";
        body += "                                                            </tr>";
        body += "                                                        </table>";
        body += "                                                        </div>";
        body += "                                                    </div>";
    });
    body += "                                                    <div class=\"yema\" style=\"width: 100%; height: 24px; text-align: center;\">";
    body += "                                                        <div class=\"H_C\" style=\"height: 24px; margin-bottom: 10px;\">";
    body += "                                                            <ul style=\"height: 24px; text-align: center; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px;\">";
    if (page > 1)
        body += "<li style=\"width: 50px; display: inline; cursor: pointer;\" onclick=\"goToSearchPage(" + (page - 1) + ")\">上一页</li>";

    for (var i = startIndex; i <= endIndex; i++) {
        if (i != page)
            body += "<li style=\"width: 20px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; margin-left: 5px; display: inline; cursor: pointer;\" onclick=\"goToPage(" + i + ")\">" + i + "</li>";
        else {
            body += "<li style=\"width: 20px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; margin-left: 5px; display: inline;\">";
            body += "<span class=\"currNo\" style=\"font-size: 14px; font-weight: bold;\">" + i + "</span>";
            body += "</li>";
        }
    }
    if (page < json.count)
        body += "<li style=\"width: 50px; display: inline; cursor: pointer;\" onclick=\"goToSearchPage(" + (page + 1) + ")\">下一页</li>";

    body += "                                                            </ul>";
    body += "                                                        </div>";
    body += "                                                </div>";
    body += "                                            </div>";
    $("#div_list").html(body);
}