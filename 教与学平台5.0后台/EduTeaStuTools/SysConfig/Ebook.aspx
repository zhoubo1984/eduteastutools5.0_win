﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ebook.aspx.cs" Inherits="EduTeaStuTools.SysConfig.Ebook" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>电子书升级</title>
    <script src="../Resource/JavaScript/jquery-1.6.2.min.js"></script>
    <script src="../Resource/Miniui/boot.js"></script>
    <link href="../Resource/Style/style2.css" rel="stylesheet" />
    <link href="../Resource/Style/fenye.css" rel="stylesheet" />
    <style type="text/css">  
    .processcontainer{  
       width:450px;  
       border:1px solid #6C9C2C;  
       height:25px;      
     } 
    #processbar{  
       background:#95CA0D;  
       float:left; 
       height:100%;  
       text-align:center;  
       line-height:150%; 
     }  
    </style>  
    <script>
        var stuList = new Array();//当前树节点                      
        $(function () {
           // $(".processcontainer").hide();
            var tree = mini.get("treeClass");
            tree.url = "../Handler/getSubjectTreeManage.ashx?type=SubjectTreeList";
            tree.load(tree.url);
            stuList.splice(0);   //清空数组  
            
            $("#Shengji").click(function (e) {
                if (stuList.length == 0) {
                    alert('请选择教材节点！');
                    e.preventDefault();
                    return;
                }
                var guidlist = stuList.join(',');
                $("#txtGuid").val(guidlist);
            })
            $("#ShengjiSJK").click(function (e) {
                if (stuList.length == 0) {
                    alert('请选择教材节点！');
                    e.preventDefault();
                    return;
                }
                var guidlist = stuList.join(',');
                $("#txtGuid").val(guidlist);
            })
        });
        function show() {
            $(".processcontainer").show();
        }
        function hide() {        
            $(".processcontainer").hide();
        }
        function Shuchu(key1) {          
            alert(key1);
        }
        function setProcess(key1) {         
            var processbar = document.getElementById("processbar");
            processbar.style.width = parseInt(key1) + "%";
            processbar.innerHTML = processbar.style.width;
            if (processbar.style.width == "100%") {
                window.clearInterval(bartimer);
            }
        }
        //页面画节点绑定事件
        function onDrawNode(e) {
            var node = e.node;
            var tree = e.sender;
            var isLeaf = tree.isLeaf(node);
            if (isLeaf == false) {
                e.nodeStyle = 'font-weight:bold;';
            } else {
                e.nodeStyle = "font-style:italic;"; //nodeStyle
                e.nodeCls = "blueColor";            //nodeCls
            }
        }
        //节点选择框
        function onNodeCheck(e) {
            var node = e.node;
            //节点选中
            if (node.checked) {
                //判断列表中有没有这个节点
                if (!IsContain(node.BB_GUID)) {
                    //没选中 就加入
                    stuList.push(node.BB_GUID);
                }
            }//节点选中后取消
            else {
                //如果列表中没有  就移除这一项
                if (IsContain(node.BB_GUID)) {
                    SpliceItem(node.BB_GUID);
                }
            }
        }

        //判断是否存在
        function IsContain(value) {
            for (var i = 0; i < stuList.length; i++) {
                if (stuList[i] == value)
                    return true;
            }
            return false;
        }

        //移除某一项 
        function SpliceItem(name) {
            for (var i = 0; i < stuList.length; i++) {
                if (stuList[i] == name) {
                    stuList.splice(i, 1);
                }
            }
        }
        function GetQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]);
            return null;
        }        
    </script>
</head>
<body style="height: 580px;">
    <form id="form1" runat="server">
        <asp:Label ID="Label3" runat="server" Text="" Visible="False"></asp:Label>
        <br /> 
        <input runat="server" id="txtGuid" type="hidden" />
        <asp:Button ID="ShengjiSJK" runat="server" Text="升级教材目录" OnClick="ShengjiSJK_Click" />  
         <asp:Button ID="Shengji" runat="server" Text="升级教材目录及资源"  OnClick="Shengji_Click" />        
        <br />       
        <%-- <div class="processcontainer">  
            <div id="processbar" style="width:0%;"></div>  
        </div> --%>
        <asp:Label ID="Label1" runat="server" Text="" Visible="False"></asp:Label>
        <asp:Label ID="Label2" runat="server" Text="" Visible="False"></asp:Label>
        <br /> 
        <div class="middle_left_middle0">
            <div class="fl-nei-buttom">
                <ul id="treeClass" class="mini-tree" style="width: 460px; height: 500px; padding: 5px; margin: 0px;" allowselect="false"
                    textfield="BB_Name" idfield="BB_GUID" resultastree="false" showcheckbox="true" checkrecursive="false" autocheckparent="false" parentfield="BB_PGUID" showarrow="true"
                    enablehottrack="false" ondrawnode="onDrawNode" onnodecheck="onNodeCheck">
                </ul>
            </div>
        </div>
    </form>
</body>
</html>
 