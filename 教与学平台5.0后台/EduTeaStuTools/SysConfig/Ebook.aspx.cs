﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using EduMethods;
using System.IO;

namespace EduTeaStuTools.SysConfig
{
    public partial class Ebook : System.Web.UI.Page
    {
        //private static int Pnum = 0;

        //private static int Countnum = 1;

        //private static object LockObject = new Object();

        //// 定义数据检查Timer
        //private static System.Timers.Timer CheckUpdatetimer = new System.Timers.Timer();

        //// 检查更新锁
        //private static int CheckUpDateLock = 0;


        public void Page_Load(object sender, EventArgs e)
        {
            //  ClientScript.RegisterStartupScript(this.GetType(), "myscript", "<script> hide()</script>");
            if (!IsPostBack)
            {

            }
            else
            {
                //UpdateProgress1.Visible = true;
            }
        }

        /////
        ///// 设定数据检查Timer参数
        /////
        //internal  void GetTimerStart()
        //{
            
           
        //    // 循环间隔时间(10分钟)
        //    CheckUpdatetimer.Interval = 1000;
        //    // 允许Timer执行
        //    CheckUpdatetimer.Enabled = true;
        //    // 定义回调
        //    CheckUpdatetimer.Elapsed += new ElapsedEventHandler(CheckUpdatetimer_Elapsed);
        //    // 定义多次循环
        //    CheckUpdatetimer.AutoReset = true;
        //}

        /////
        ///// timer事件
        /////
        /////
        /////
        //private  void CheckUpdatetimer_Elapsed(object sender, ElapsedEventArgs e)
        //{
        //    // 加锁检查更新锁
        //    lock (LockObject)
        //    {
        //        if (CheckUpDateLock == 0) CheckUpDateLock = 1;
        //        else return;
        //    }

        //    //More code goes here.
        //    //具体实现功能的方法
        //    Check();
        //    // 解锁更新检查锁
        //    lock (LockObject)
        //    {
        //        CheckUpDateLock = 0;
        //    }
        //}
        //public void Check()
        //{
        //    int process = Pnum * 100 / Countnum;
        //    StringBuilder  sb=new StringBuilder();
        //    sb.Append("<script> setProcess(" + process + ")</script>");
        //    ClientScript.RegisterStartupScript(this.GetType(), "myscript", sb.ToString());
        //    sb.Clear();
        //}
        protected void Shengji_Click(object sender, EventArgs e)
        {
            Label3.Visible = false;
            Label1.Visible = false;
            Label2.Visible = false;
            //ClientScript.RegisterStartupScript(this.GetType(), "myscript", "<script> show()</script>");
            
            string res = "";
            string guid = txtGuid.Value;
            if (!string.IsNullOrEmpty(guid))
            {
                string[] Guidlist = guid.Split(new char[] { ',' });
                for (int l = 0; l < Guidlist.Length; l++)
                {
                   // Guidlist[l] = "1CC1D2A0-160D-4C9C-8159-BE24A5B033B8";
                    if (SubjectRelation.IsBook(Guidlist[l]))
                    {
                        res +=Pand(Guidlist[l]);
                    }
                    else
                    {
                        res += SubjectRelation.UPSub(Guidlist[l]);
                        res += Pand(Guidlist[l]);
                    }
                }
                res = res.Substring(0, res.Length - 1);
            }
            Label3.Visible = true;
            //ClientScript.RegisterStartupScript(this.GetType(), "myscript", "<script> Shuchu(" + res + ")</script>");
            Label3.Text = res;
        }
        public string Chuli(EbookResponse list, string Guidlist)
        {
            string res = "";
            int Pnum = 0;
            int Countnum = 0;
            StringBuilder sb = new StringBuilder();
            Countnum += list.list.Count;
            Label1.Visible = true;
            Label1.Text = "共有" + Countnum + "条数据。";
            // GetTimerStart();
            for (int i = 0; i < list.list.Count; i++)
            {
                sb.Remove(0, sb.Length);
                string name = list.list[i].BB_Name.Replace("'","''");
                if (string.IsNullOrEmpty(list.list[i].BB_PageIndex.ToString()))
                {
                    sb.Append("update Book_Base set BB_PageIndex= null where  BB_GUID='" + list.list[i].BB_GUID + "'");
                    int num = DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                    if (num != 1)
                    {
                        if (string.IsNullOrEmpty(list.list[i].BB_PGUID.ToString()))
                        {
                            sb.Remove(0, sb.Length);
                            sb.Append("insert into Book_Base([BB_GUID],[BB_PGUID],[BB_Name],[BB_Path],[BB_Index],[BB_SubjectGUID],[State],[BB_PageIndex])values('" + list.list[i].BB_GUID + "',null,'" + name + "','" + list.list[i].BB_Path + "'," + list.list[i].BB_Index + ",'" + list.list[i].BB_SubjectGUID + "'," + list.list[i].State + ",null )");
                            DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                        }
                        else
                        {
                            sb.Remove(0, sb.Length);
                            sb.Append("insert into Book_Base([BB_GUID],[BB_PGUID],[BB_Name],[BB_Path],[BB_Index],[BB_SubjectGUID],[State],[BB_PageIndex])values('" + list.list[i].BB_GUID + "','" + list.list[i].BB_PGUID + "','" + name + "','" + list.list[i].BB_Path + "'," + list.list[i].BB_Index + ",'" + list.list[i].BB_SubjectGUID + "'," + list.list[i].State + ",null )");
                            DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                        }
                        
                    }
                }
                else
                {
                    sb.Append("update Book_Base set BB_PageIndex=" + list.list[i].BB_PageIndex + " where  BB_GUID='" + list.list[i].BB_GUID + "'");
                    int num = DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                    if (num != 1)
                    {
                        if (string.IsNullOrEmpty(list.list[i].BB_PGUID.ToString()))
                        {
                            sb.Remove(0, sb.Length);
                            sb.Append("insert into Book_Base([BB_GUID],[BB_PGUID],[BB_Name],[BB_Path],[BB_Index],[BB_SubjectGUID],[State],[BB_PageIndex])values('" + list.list[i].BB_GUID + "',null,'" + name + "','" + list.list[i].BB_Path + "'," + list.list[i].BB_Index + ",'" + list.list[i].BB_SubjectGUID + "'," + list.list[i].State + "," + list.list[i].BB_PageIndex + ")");
                            DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                        }
                        else
                        {
                            sb.Remove(0, sb.Length);
                            sb.Append("insert into Book_Base([BB_GUID],[BB_PGUID],[BB_Name],[BB_Path],[BB_Index],[BB_SubjectGUID],[State],[BB_PageIndex])values('" + list.list[i].BB_GUID + "','" + list.list[i].BB_PGUID + "','" + name + "','" + list.list[i].BB_Path + "'," + list.list[i].BB_Index + ",'" + list.list[i].BB_SubjectGUID + "'," + list.list[i].State + "," + list.list[i].BB_PageIndex + ")");
                            DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                        }
                    }
                }
                Pnum++;
                Label2.Visible = true;
                Label2.Text = "已升级" + Pnum + "条数据。";
            }
            //这是下载
            string r = SubjectRelation.EbookDown(Guidlist);
            if (r == "1")
            {

                res += "升级成功,";
            }
            else
            {
                res += r + ",";
            }
            Label2.Visible = true;
            Label2.Text = "已升级" + Pnum + "条数据。";
            return res;
        }

        public string ChuliSJK(EbookResponse list, string Guidlist)
        {
            string res = "";
            int Pnum = 0;
            int Countnum = 0;
            StringBuilder sb = new StringBuilder();
            Countnum += list.list.Count;
            Label1.Visible = true;
            Label1.Text = "共有" + Countnum + "条数据。";
            // GetTimerStart();
            for (int i = 0; i < list.list.Count; i++)
            {
                sb.Remove(0, sb.Length);
                string name = list.list[i].BB_Name.Replace("'", "''");
                if (string.IsNullOrEmpty(list.list[i].BB_PageIndex.ToString()))
                {
                    sb.Append("update Book_Base set BB_PageIndex= null where  BB_GUID='" + list.list[i].BB_GUID + "'");
                    int num = DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                    if (num != 1)
                    {
                        if (string.IsNullOrEmpty(list.list[i].BB_PGUID.ToString()))
                        {
                            sb.Remove(0, sb.Length);
                            sb.Append("insert into Book_Base([BB_GUID],[BB_PGUID],[BB_Name],[BB_Path],[BB_Index],[BB_SubjectGUID],[State],[BB_PageIndex])values('" + list.list[i].BB_GUID + "',null,'" + name + "','" + list.list[i].BB_Path + "'," + list.list[i].BB_Index + ",'" + list.list[i].BB_SubjectGUID + "'," + list.list[i].State + ",null )");
                            DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                        }
                        else
                        {
                            sb.Remove(0, sb.Length);
                            sb.Append("insert into Book_Base([BB_GUID],[BB_PGUID],[BB_Name],[BB_Path],[BB_Index],[BB_SubjectGUID],[State],[BB_PageIndex])values('" + list.list[i].BB_GUID + "','" + list.list[i].BB_PGUID + "','" + name + "','" + list.list[i].BB_Path + "'," + list.list[i].BB_Index + ",'" + list.list[i].BB_SubjectGUID + "'," + list.list[i].State + ",null )");
                            DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                        }

                    }
                }
                else
                {
                    sb.Append("update Book_Base set BB_PageIndex=" + list.list[i].BB_PageIndex + " where  BB_GUID='" + list.list[i].BB_GUID + "'");
                    int num = DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                    if (num != 1)
                    {
                        if (string.IsNullOrEmpty(list.list[i].BB_PGUID.ToString()))
                        {
                            sb.Remove(0, sb.Length);
                            sb.Append("insert into Book_Base([BB_GUID],[BB_PGUID],[BB_Name],[BB_Path],[BB_Index],[BB_SubjectGUID],[State],[BB_PageIndex])values('" + list.list[i].BB_GUID + "',null,'" + name + "','" + list.list[i].BB_Path + "'," + list.list[i].BB_Index + ",'" + list.list[i].BB_SubjectGUID + "'," + list.list[i].State + "," + list.list[i].BB_PageIndex + ")");
                            DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                        }
                        else
                        {
                            sb.Remove(0, sb.Length);
                            sb.Append("insert into Book_Base([BB_GUID],[BB_PGUID],[BB_Name],[BB_Path],[BB_Index],[BB_SubjectGUID],[State],[BB_PageIndex])values('" + list.list[i].BB_GUID + "','" + list.list[i].BB_PGUID + "','" + name + "','" + list.list[i].BB_Path + "'," + list.list[i].BB_Index + ",'" + list.list[i].BB_SubjectGUID + "'," + list.list[i].State + "," + list.list[i].BB_PageIndex + ")");
                            DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                        }
                    }
                }
                Pnum++;
                Label2.Visible = true;
                Label2.Text = "已升级" + Pnum + "条数据。";
            }
            res = "升级成功，";
  
            //Label2.Visible = true;
            //Label2.Text = "已升级" + Pnum + "条数据。";
            return res;
        }

        public string PandSJK(string Guidlist)
        {
            string res = "";
            EbookResponse list = SubjectRelation.EbookList(Guidlist);
            if (list == null)
            {
                res += "升级失败,";
            }
            else
            {
                if (list.list == null || list.list.Count == 0)
                {
                    res += "没有对应的电子书，";
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("select * from Book_Base where  BB_PGUID='" + Guidlist + "'");
                    DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString(), CommonString.CnCodeDb).Tables[0];

                    if (dt != null)
                    {
                        //有数据
                        if (dt.Rows.Count != 0)
                        {//升级过 有文件夹                                      
                            if (!string.IsNullOrEmpty(dt.Rows[0]["BB_PageIndex"].ToString()) && Directory.Exists(CommonString.ResPath + Guidlist))
                            {
                                res += "教材已升级，";
                            }//没升级  没文件夹
                            else
                            {
                                res += ChuliSJK(list, Guidlist);
                            }
                        }//没数据 肯定没升级
                        else
                        {
                            res += ChuliSJK(list, Guidlist);
                        }
                    }
                    else
                    {
                        res += "无教材表,";
                    }

                }
            }
            return res;
        }

        public string Pand(string Guidlist)
        {
            string res = "";
            EbookResponse list = SubjectRelation.EbookList(Guidlist);
            if (list == null)
            {
                res += "升级失败,";
            }
            else
            {
                if (list.list == null || list.list.Count == 0)
                {
                    res += "没有对应的电子书，";
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("select * from Book_Base where  BB_PGUID='" + Guidlist + "'");
                    DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString(), CommonString.CnCodeDb).Tables[0];

                    if (dt != null)
                    {
                        //有数据
                        if (dt.Rows.Count != 0)
                        {//升级过 有文件夹                                      
                            if (!string.IsNullOrEmpty(dt.Rows[0]["BB_PageIndex"].ToString()) && Directory.Exists(CommonString.ResPath + Guidlist))
                            {
                                res += "教材已升级，";
                            }//没升级  没文件夹
                            else
                            {
                                res += Chuli(list, Guidlist);
                            }
                        }//没数据 肯定没升级
                        else
                        {
                            res += Chuli(list, Guidlist);
                        }
                    }
                    else
                    {
                        res += "无教材表,";
                    }

                }
            }
            return res;
        }

        protected void ShengjiSJK_Click(object sender, EventArgs e)
        {
            Label3.Visible = false;
            Label1.Visible = false;
            Label2.Visible = false;
            //ClientScript.RegisterStartupScript(this.GetType(), "myscript", "<script> show()</script>");

            string res = "";
            string guid = txtGuid.Value;
            if (!string.IsNullOrEmpty(guid))
            {
                string[] Guidlist = guid.Split(new char[] { ',' });
                for (int l = 0; l < Guidlist.Length; l++)
                {
                    // Guidlist[l] = "1CC1D2A0-160D-4C9C-8159-BE24A5B033B8";
                    if (SubjectRelation.IsBook(Guidlist[l]))
                    {
                        res += PandSJK(Guidlist[l]);
                    }
                    else
                    {
                        res += SubjectRelation.UPSub(Guidlist[l]);
                        res += PandSJK(Guidlist[l]);
                    }
                }
                res = res.Substring(0, res.Length - 1);
            }
            Label3.Visible = true;
            //ClientScript.RegisterStartupScript(this.GetType(), "myscript", "<script> Shuchu(" + res + ")</script>");
            Label3.Text = res;
        }
    }
}