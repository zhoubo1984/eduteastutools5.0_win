﻿namespace EduTeaStuTools_Win
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.Shengji = new System.Windows.Forms.Button();
            this.treeView2 = new System.Windows.Forms.TreeView();
            this.lblTotalProgress = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lvResult = new System.Windows.Forms.ListView();
            this.itemName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.itemSubject = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.itemGuid = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.itemResult = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.itemDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.itemFileCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnClearLog = new System.Windows.Forms.Button();
            this.btnRefreashTree = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.CheckBoxes = true;
            this.treeView1.Location = new System.Drawing.Point(12, 12);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(364, 599);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterCheck);
            this.treeView1.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterExpand);
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // Shengji
            // 
            this.Shengji.Location = new System.Drawing.Point(708, 12);
            this.Shengji.Name = "Shengji";
            this.Shengji.Size = new System.Drawing.Size(168, 23);
            this.Shengji.TabIndex = 2;
            this.Shengji.Text = "升级教材目录及资源";
            this.Shengji.UseVisualStyleBackColor = true;
            this.Shengji.Click += new System.EventHandler(this.Shengji_Click);
            // 
            // treeView2
            // 
            this.treeView2.Location = new System.Drawing.Point(404, 12);
            this.treeView2.Name = "treeView2";
            this.treeView2.Size = new System.Drawing.Size(274, 599);
            this.treeView2.TabIndex = 7;
            // 
            // lblTotalProgress
            // 
            this.lblTotalProgress.AutoSize = true;
            this.lblTotalProgress.Location = new System.Drawing.Point(899, 17);
            this.lblTotalProgress.Name = "lblTotalProgress";
            this.lblTotalProgress.Size = new System.Drawing.Size(0, 12);
            this.lblTotalProgress.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(708, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 9;
            this.label2.Text = "本次操作日志";
            // 
            // lvResult
            // 
            this.lvResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvResult.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.itemName,
            this.itemSubject,
            this.itemGuid,
            this.itemResult,
            this.itemDate,
            this.itemFileCount});
            this.lvResult.Location = new System.Drawing.Point(708, 64);
            this.lvResult.Name = "lvResult";
            this.lvResult.Size = new System.Drawing.Size(868, 479);
            this.lvResult.TabIndex = 11;
            this.lvResult.UseCompatibleStateImageBehavior = false;
            this.lvResult.View = System.Windows.Forms.View.Details;
            // 
            // itemName
            // 
            this.itemName.Text = "名称";
            this.itemName.Width = 195;
            // 
            // itemSubject
            // 
            this.itemSubject.DisplayIndex = 4;
            this.itemSubject.Text = "学科";
            this.itemSubject.Width = 161;
            // 
            // itemGuid
            // 
            this.itemGuid.DisplayIndex = 1;
            this.itemGuid.Text = "编号";
            this.itemGuid.Width = 87;
            // 
            // itemResult
            // 
            this.itemResult.DisplayIndex = 2;
            this.itemResult.Text = "操作结果";
            this.itemResult.Width = 175;
            // 
            // itemDate
            // 
            this.itemDate.DisplayIndex = 3;
            this.itemDate.Text = "生成时间";
            this.itemDate.Width = 119;
            // 
            // itemFileCount
            // 
            this.itemFileCount.DisplayIndex = 5;
            this.itemFileCount.Text = "文件数";
            // 
            // btnClearLog
            // 
            this.btnClearLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearLog.Location = new System.Drawing.Point(1501, 17);
            this.btnClearLog.Name = "btnClearLog";
            this.btnClearLog.Size = new System.Drawing.Size(75, 23);
            this.btnClearLog.TabIndex = 12;
            this.btnClearLog.Text = "清空日志";
            this.btnClearLog.UseVisualStyleBackColor = true;
            this.btnClearLog.Click += new System.EventHandler(this.btnClearLog_Click);
            // 
            // btnRefreashTree
            // 
            this.btnRefreashTree.Location = new System.Drawing.Point(901, 12);
            this.btnRefreashTree.Name = "btnRefreashTree";
            this.btnRefreashTree.Size = new System.Drawing.Size(75, 23);
            this.btnRefreashTree.TabIndex = 13;
            this.btnRefreashTree.Text = "刷新左侧树";
            this.btnRefreashTree.UseVisualStyleBackColor = true;
            this.btnRefreashTree.Click += new System.EventHandler(this.btnRefreashTree_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1588, 640);
            this.Controls.Add(this.btnRefreashTree);
            this.Controls.Add(this.btnClearLog);
            this.Controls.Add(this.lvResult);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblTotalProgress);
            this.Controls.Add(this.treeView2);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.Shengji);
            this.Name = "Form1";
            this.Text = "教与学平台电子教材工具";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button Shengji;
        private System.Windows.Forms.TreeView treeView2;
        private System.Windows.Forms.Label lblTotalProgress;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView lvResult;
        private System.Windows.Forms.ColumnHeader itemName;
        private System.Windows.Forms.ColumnHeader itemGuid;
        private System.Windows.Forms.ColumnHeader itemResult;
        private System.Windows.Forms.ColumnHeader itemDate;
        private System.Windows.Forms.Button btnClearLog;
        private System.Windows.Forms.ColumnHeader itemSubject;
        private System.Windows.Forms.ColumnHeader itemFileCount;
        private System.Windows.Forms.Button btnRefreashTree;
    }
}

