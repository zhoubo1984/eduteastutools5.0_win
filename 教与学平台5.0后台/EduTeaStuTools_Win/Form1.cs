﻿
using EduMethods;
using EduTeaStuTools_Win.Tools;
using EduTeaStuTools_Win.ViewTools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace EduTeaStuTools_Win
{
    public partial class Form1 : Form
    {
        LoadingControl ploading;
        private List<string> stuList = new List<string>();
        private List<string> existsbooknames = new List<string>();
        delegate void addListViewDelegate(ListViewItem lvitem);
        delegate void setlabelText(string content);
        delegate void addTreeNode(TreeNode parentnode,TreeNode subnewNode);
       

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadExistsBookNames();
            InitTrees();
        }

        private void LoadExistsBookNames()
        {
            try
            {
                DirectoryInfo dresdirpath = new DirectoryInfo(CommonString.ResPath);
                existsbooknames = (from u in Directory.GetDirectories(CommonString.ResPath)
                                   select Path.GetFileName(u).ToLower()).ToList();
            }
            catch(Exception ex)
            {
                LogHelper.LogError(string.Format("报错方法：{0}，内容：{1}", "LoadExistsBookNames", ex.Message));
            }
            
        }

        private void InitTrees()
        {
            try
            {
                treeView1.Nodes.Clear();
                treeView2.Nodes.Clear();

                var modellist = SubjectRelation.SubjectTreeListWithoutId();
                var parentnodeOne = new TreeNode("小学");
                var parentnodeTwo = new TreeNode("初中");
                var parentnodeThree = new TreeNode("高中");
                var parentnodeFour = new TreeNode("其它");
                
                treeView1.Nodes.Add(parentnodeOne);
                treeView1.Nodes.Add(parentnodeTwo);
                treeView1.Nodes.Add(parentnodeThree);
                treeView1.Nodes.Add(parentnodeFour);

                foreach (var treemodel in modellist)
                {
                    var treenode = new TreeNode(treemodel.BB_Name);
                    treenode.Tag = treemodel;
                    if(treemodel.BB_Name.StartsWith("小学"))
                    {
                        parentnodeOne.Nodes.Add(treenode);
                    }
                    else if(treemodel.BB_Name.StartsWith("初中"))
                    {
                        parentnodeTwo.Nodes.Add(treenode);
                    }
                    else if(treemodel.BB_Name.StartsWith("高中"))
                    {
                        parentnodeThree.Nodes.Add(treenode);
                    }
                    else
                    {
                        parentnodeFour.Nodes.Add(treenode);
                    }
                    
                    treenode.Nodes.Add(string.Empty);
                }
                //parentnode.Expand();
            }
            catch(Exception ex)
            {
                LogHelper.LogError(string.Format("报错方法：{0}，内容：{1}", "InitTrees", ex.Message));
            }
        }

        /// <summary>
        /// 教材树全选操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeView1_AfterCheck(object sender, TreeViewEventArgs e)
        {
            try
            {
                if (e.Action != TreeViewAction.Unknown)
                {
                    TreeNodeOperate.CheckAllChildNodes(e.Node, e.Node.Checked);

                    //当所有子节点选中，使父节点选中
                    bool bolChecked = true;
                    if (e.Node.Parent != null)
                    {
                        for (int i = 0; i < e.Node.Parent.Nodes.Count; i++)
                        {
                            if (e.Node.Parent.Nodes[i].Checked == false)
                                bolChecked = false;
                        }
                        e.Node.Parent.Checked = bolChecked;
                    }
                }
            }
            catch(Exception ex)
            {
                LogHelper.LogError(string.Format("报错方法：{0}，内容：{1}", "treeView1_AfterCheck", ex.Message));
            }
        }

        /// <summary>
        /// 升级电子书
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Shengji_Click(object sender, EventArgs e)
        {
            try
            {
                ploading = LoadingControl.getLoading();
                ploading.SetExecuteMethod(ShengjiWork);
                ploading.ShowDialog();
            }
            catch(Exception ex)
            {
                LogHelper.LogError(string.Format("报错方法：{0}，内容：{1}", "Shengji_Click", ex.Message));
            }
        }

        /// <summary>
        /// 升级异步动作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ShengjiWork()
        {
            try
            {
                LoadingControl loading = LoadingControl.getLoading();
                var selectednodes = new List<TreeNode>();

                if (treeView1.Nodes.Count > 0)
                {
                    for (var i = 0; i < 4; i++)
                    {
                        TreeNodeOperate.GetSelectedTreeNode(treeView1.Nodes[i], selectednodes);
                    }
                }

                if (selectednodes.Count == 0)
                {
                    MessageBox.Show("请先选择一个教材节点");
                    return;
                }
                string res = string.Empty;
                for (var i = 0; i < selectednodes.Count; i++)
                {
                    var book = (selectednodes[i].Tag as Book_Base);
                    if (book == null) continue;
                    var nodeguid = book.BB_GUID;
                    if (!SubjectRelation.IsBook(nodeguid.ToString()))
                    {
                        res += SubjectRelation.UPSub(nodeguid.ToString());
                    }
                    int managecount = 0;//世纪处理的章节文件数量
                    res = Pand(nodeguid.ToString(), ref managecount);
                    var newlvitem = new ListViewItem();
                    newlvitem.Text = book.BB_Name;
                    newlvitem.SubItems.Add(selectednodes[i].Parent.Text);
                    newlvitem.SubItems.Add(book.BB_GUID.ToString());
                    newlvitem.SubItems.Add(res.TrimEnd(','));
                    newlvitem.SubItems.Add(DateTime.Now.ToString());
                    newlvitem.SubItems.Add(managecount.ToString());
                    LogHelper.Log(book.BB_Name, selectednodes[i].Parent.Text, book.BB_GUID.ToString(), res, managecount);
                    AddListViewItem(newlvitem);
                    if (res.Contains("升级成功"))
                    {
                        selectednodes[i].ForeColor = Color.Green;
                    }
                    loading.SetCaptionAndDescription("", "正在处理" + book.BB_Name, "执行进度" + (i + 1) + "/" + selectednodes.Count);
                }
                LoadingControl.getLoading().CloseLoadingForm();
            }
            catch (Exception ex)
            {
                LogHelper.LogError(string.Format("报错方法：{0}，内容：{1}", "ShengjiWork", ex.Message));
            }

            //MessageBox.Show("执行完毕！");
        }

        /// <summary>
        /// 升级动作
        /// </summary>
        /// <param name="Guidlist"></param>
        /// <returns></returns>
        public string Pand(string Guidlist,ref int managecount)
        {
            
            string res = "";
            try
            {
                EbookResponse list = SubjectRelation.EbookList(Guidlist);
                if (list == null)
                {
                    res += "升级失败,";
                }
                else
                {
                    if (list.list == null || list.list.Count == 0)
                    {
                        res += "没有对应的电子书，";
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append("select * from Book_Base where  BB_PGUID='" + Guidlist + "'");
                        DataTable dt = DBUtility.DbHelperSQL.Query(sb.ToString(), CommonString.CnCodeDb).Tables[0];

                        if (dt != null)
                        {
                            res += Chuli(list, Guidlist, ref managecount);
                            //有数据
                            //if (dt.Rows.Count != 0)
                            //{   
                            //    //这个地方调整为无论是否修改过都进行升级
                            //    //升级过 有文件夹                                      
                            //    //if (!string.IsNullOrEmpty(dt.Rows[0]["BB_PageIndex"].ToString()) && Directory.Exists(CommonString.ResPath + Guidlist))
                            //    //{
                            //    //    res += "教材已升级，";
                            //    //}//没升级  没文件夹
                            //    //else
                            //    //{
                            //    //    res += Chuli(list, Guidlist);
                            //    //}
                            //}//没数据 肯定没升级
                            //else
                            //{
                            //    res += Chuli(list, Guidlist);
                            //}
                        }
                        else
                        {
                            res += "无教材表,";
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                LogHelper.LogError(string.Format("报错方法：{0}，内容：{1}", "Pand", ex.Message));
            }
            
            return res;
        }

        /// <summary>
        /// 更新本地数据
        /// </summary>
        /// <param name="list"></param>
        /// <param name="Guidlist"></param>
        /// <returns></returns>
        public string Chuli(EbookResponse list, string Guidlist,ref int downfilecount)
        {
            string res = "";
            try
            {
                int Pnum = 0;
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < list.list.Count; i++)
                {
                    if (list.list[i].State != 0 && list.list[i].State != 1)
                    {
                        string invalidstatemsg = string.Format("服务器端获取到异常的State数据,State为{2},Guid为{0},GuidList为{1}", list.list[i].BB_GUID, Guidlist, list.list[i].State);
                        LogHelper.LogCommon(invalidstatemsg);
                        MessageBox.Show(invalidstatemsg, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }


                    sb.Remove(0, sb.Length);
                    string name = list.list[i].BB_Name.Replace("'", "''");
                    if (string.IsNullOrEmpty(list.list[i].BB_PageIndex.ToString()))
                    {
                        sb.Append("update Book_Base set BB_PageIndex= null,BB_Name='" + name + "' where  BB_GUID='" + list.list[i].BB_GUID + "'");
                        int num = DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                        if (num != 1)
                        {
                            if (string.IsNullOrEmpty(list.list[i].BB_PGUID.ToString()))
                            {
                                sb.Remove(0, sb.Length);
                                sb.Append("insert into Book_Base([BB_GUID],[BB_PGUID],[BB_Name],[BB_Path],[BB_Index],[BB_SubjectGUID],[State],[BB_PageIndex])values('" + list.list[i].BB_GUID + "',null,'" + name + "','" + list.list[i].BB_Path + "'," + list.list[i].BB_Index + ",'" + list.list[i].BB_SubjectGUID + "'," + list.list[i].State + ",null )");
                                DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                            }
                            else
                            {
                                sb.Remove(0, sb.Length);
                                sb.Append("insert into Book_Base([BB_GUID],[BB_PGUID],[BB_Name],[BB_Path],[BB_Index],[BB_SubjectGUID],[State],[BB_PageIndex])values('" + list.list[i].BB_GUID + "','" + list.list[i].BB_PGUID + "','" + name + "','" + list.list[i].BB_Path + "'," + list.list[i].BB_Index + ",'" + list.list[i].BB_SubjectGUID + "'," + list.list[i].State + ",null )");
                                DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                            }
                        }
                    }
                    else
                    {
                        sb.Append("update Book_Base set BB_PageIndex=" + list.list[i].BB_PageIndex + " ,BB_Name='" + name + "' where  BB_GUID='" + list.list[i].BB_GUID + "'");
                        int num = DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                        if (num != 1)
                        {
                            if (string.IsNullOrEmpty(list.list[i].BB_PGUID.ToString()))
                            {
                                sb.Remove(0, sb.Length);
                                sb.Append("insert into Book_Base([BB_GUID],[BB_PGUID],[BB_Name],[BB_Path],[BB_Index],[BB_SubjectGUID],[State],[BB_PageIndex])values('" + list.list[i].BB_GUID + "',null,'" + name + "','" + list.list[i].BB_Path + "'," + list.list[i].BB_Index + ",'" + list.list[i].BB_SubjectGUID + "'," + list.list[i].State + "," + list.list[i].BB_PageIndex + ")");
                                DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                            }
                            else
                            {
                                sb.Remove(0, sb.Length);
                                sb.Append("insert into Book_Base([BB_GUID],[BB_PGUID],[BB_Name],[BB_Path],[BB_Index],[BB_SubjectGUID],[State],[BB_PageIndex])values('" + list.list[i].BB_GUID + "','" + list.list[i].BB_PGUID + "','" + name + "','" + list.list[i].BB_Path + "'," + list.list[i].BB_Index + ",'" + list.list[i].BB_SubjectGUID + "'," + list.list[i].State + "," + list.list[i].BB_PageIndex + ")");
                                DBUtility.DbHelperSQL.ExecuteSql(sb.ToString(), CommonString.CnCodeDb);
                            }
                        }
                    }
                    Pnum++;
                }
                //这是下载
                string r = SubjectRelation.EbookDown(Guidlist, ref downfilecount);
                if (r == "1")
                {
                    res += "升级成功,";
                }
                else
                {
                    res += r + ",";
                }
            }
            catch(Exception ex)
            {
                LogHelper.LogError(string.Format("报错方法：{0}，内容：{1}", "Chuli", ex.Message));
            }
            
            return res;
        }

        /// <summary>
        /// 展开学科的时候加载教材
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeView1_AfterExpand(object sender, TreeViewEventArgs e)
        {
            try
            {
                if (e.Node.Tag != null)
                {
                    if (e.Node.Nodes.Count > 0 && e.Node.Nodes[0].Tag == null)
                    {
                        //1CC1D2A0-160D-4C9C-8159-BE24A5B033B8
                        e.Node.Nodes.Clear();
                        var treemodel = e.Node.Tag as EduMethods.SubjectRelation.TreeModel;
                        var modellist = EduMethods.SubjectRelation.SubjectTreeListWithId(treemodel.BB_GUID.ToString()).OrderBy(item => item.BB_Name.Split('.')[0]).ThenBy(item => item.BB_Index);
                        if (modellist != null && modellist.Count() > 0)
                        {
                            foreach (var model in modellist)
                            {
                                var nodetext = this.existsbooknames.Contains(model.BB_GUID.ToString()) ? model.BB_Name + string.Format("({0})", "存在本地文件") : model.BB_Name;
                                TreeNode tn = new TreeNode(model.BB_Name + string.Format("({0})", model.BB_GUID));
                                if (this.existsbooknames.Contains(model.BB_GUID.ToString().ToLower()))
                                {
                                    tn.Text += string.Format("({0})", "存在本地文件");
                                    tn.ForeColor = Color.Green;
                                }
                                tn.Tag = model;
                                e.Node.Nodes.Add(tn);
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                LogHelper.LogError(string.Format("报错方法：{0}，内容：{1}", "treeView1_AfterExpand", ex.Message));
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                if (e.Action != TreeViewAction.Unknown)
                {
                    if (e.Node.Tag is Book_Base)
                    {
                        var bookbase = e.Node.Tag as Book_Base;
                        treeView2.Nodes.Clear();
                        ploading = LoadingControl.getLoading();
                        ploading.SetExecuteMethod(() =>
                        {
                            var booklist = EduMethods.SubjectRelation.EbookList(bookbase.BB_GUID.ToString());
                            if (booklist.list != null)
                            {
                                GenerateTreeView2(booklist.list);
                            }
                            else
                            {
                                MessageBox.Show("该节点下没有数据");
                            }
                            LoadingControl.getLoading().CloseLoadingForm();
                        });
                        ploading.ShowDialog();
                        treeView2.ExpandAll();
                    }
                }
            }
            catch(Exception ex)
            {
                LogHelper.LogError(string.Format("报错方法：{0}，内容：{1}", "treeView1_AfterSelect", ex.Message));
            }            
        }

        private void GenerateTreeView2(List<Book_Base> booklist)
        {
            try
            {
                foreach (var book in booklist.Where(item => item.BB_PGUID == null))
                {
                    TreeNode tn = new TreeNode(book.BB_Name + string.Format(" {0}", book.BB_PageIndex == null ? "无页码" : book.BB_PageIndex.Value.ToString()));
                    tn.Tag = book;
                    AddSectionTreeNode(null, tn);
                    RecireNode(tn, booklist);
                }
            }
            catch(Exception ex)
            {
                LogHelper.LogError(string.Format("报错方法：{0}，内容：{1}", "GenerateTreeView2", ex.Message));
            } 
        }

        private void RecireNode(TreeNode node,List<Book_Base> booklist)
        {
            try
            {
                foreach (var book in booklist.Where(item => item.BB_PGUID == (node.Tag as Book_Base).BB_GUID))
                {
                    TreeNode tn = new TreeNode(book.BB_Name + string.Format(" {0}", book.BB_PageIndex == null ? "无页码" : book.BB_PageIndex.Value.ToString()));
                    tn.Tag = book;
                    //node.Nodes.Add(tn);
                    AddSectionTreeNode(node, tn);

                    //if (book.BB_PageIndex != null)
                        RecireNode(tn, booklist);

                }
            }
            catch(Exception ex)
            {
                LogHelper.LogError(string.Format("报错方法：{0}，内容：{1}", "RecireNode", ex.Message));
            }
        }

        /// <summary>
        /// 线程中添加ListView项
        /// </summary>
        /// <param name="lvitem"></param>

        private void AddListViewItem(ListViewItem lvitem)
        {
            if (this.lvResult.InvokeRequired)
            {
                Invoke(new addListViewDelegate(AddListViewItem), new object[] { lvitem });
            }
            else
            {
                this.lvResult.Items.Insert(0, lvitem);
            }
        }

        private void AddSectionTreeNode(TreeNode parentnode,TreeNode subnode)
        {
            if (this.treeView2.InvokeRequired)
            {
                Invoke(new addTreeNode(AddSectionTreeNode), new object[] { parentnode,subnode });
            }
            else
            {
                if (parentnode == null)
                    treeView2.Nodes.Add(subnode);
                else
                    parentnode.Nodes.Add(subnode);
            }
        }

        private void btnClearLog_Click(object sender, EventArgs e)
        {
            this.lvResult.Items.Clear();
        }

        private void btnRefreashTree_Click(object sender, EventArgs e)
        {
            LoadExistsBookNames();
            InitTrees();
        }
    }
}
