﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace EduTeaStuTools_Win.Tools
{
    public class LogHelper
    {
        public static void Log(string bookname,string subjectnane,string guid,string result,int downfilecount)
        {
            var logdir = new DirectoryInfo("logs");
            if(!logdir.Exists)
            {
                logdir.Create();
            }

            var todaydir = logdir.GetDirectories().ToList().Where(dir => dir.Name == DateTime.Now.ToLongDateString()).FirstOrDefault();
            if(todaydir==null)
            {
                todaydir = new DirectoryInfo(Path.Combine(logdir.FullName, DateTime.Now.ToLongDateString()));
                todaydir.Create();
            }

            //创建相应的文件
            StreamWriter sw = new StreamWriter(Path.Combine(todaydir.FullName, "log.txt"),true,Encoding.UTF8);
            string logcontent = "处理时间:" + DateTime.Now.ToString();
            sw.WriteLine(logcontent);
            logcontent = string.Format("学科：{0}，教材：{1}，教材编号：{2}，处理结果：{3},下载文件数：{4}", subjectnane, bookname, guid, result,downfilecount);
            sw.WriteLine(logcontent);
            sw.Close();

        }

        public static void LogCommon(string content)
        {
            var todaydir = getLogTodayDir();

            StreamWriter sw = new StreamWriter(Path.Combine(todaydir, "log.txt"), true, Encoding.UTF8);
            string logcontent = "处理时间:" + DateTime.Now.ToString();
            sw.WriteLine(logcontent);
            logcontent = string.Format(content);
            sw.WriteLine(logcontent);
            sw.Close();
        }

        public static void LogError(string content)
        {
            var todaydir = getErrorTodayDir();

            StreamWriter sw = new StreamWriter(Path.Combine(todaydir, "log.txt"), true, Encoding.UTF8);
            string logcontent = "处理时间:" + DateTime.Now.ToString();
            sw.WriteLine(logcontent);
            logcontent = string.Format(content);
            sw.WriteLine(logcontent);
            sw.Close();
        }

        private static string getErrorTodayDir()
        {
            return getTodayDir("errors");
        }

        private static string getLogTodayDir()
        {
            return getTodayDir("logs");
        }

        private static string getTodayDir(string dirname)
        {
            var logdir = new DirectoryInfo("logs");
            if (!logdir.Exists)
            {
                logdir.Create();
            }

            var todaydir = logdir.GetDirectories().ToList().Where(dir => dir.Name == DateTime.Now.ToLongDateString()).FirstOrDefault();
            if (todaydir == null)
            {
                todaydir = new DirectoryInfo(Path.Combine(logdir.FullName, DateTime.Now.ToLongDateString()));
                todaydir.Create();
            }

            return todaydir.FullName;
        }
    }
}
