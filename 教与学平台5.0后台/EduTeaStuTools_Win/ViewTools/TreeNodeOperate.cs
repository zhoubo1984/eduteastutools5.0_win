﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EduTeaStuTools_Win.ViewTools
{
    public class TreeNodeOperate
    {
        /// <summary>
        /// 当树形控件节点checkbox选中或者取消选中的时候操作其子节点也为选中或者未选中
        /// </summary>
        public static void CheckAllChildNodes(TreeNode treeNode, bool nodeChecked)
        {

            foreach (TreeNode node in treeNode.Nodes)
            {
                node.Checked = nodeChecked;
                if (node.Nodes.Count > 0)
                {
                    CheckAllChildNodes(node, nodeChecked);
                }
            }
        }

        /// <summary>
        /// 获取某节点下所有的选中节点
        /// </summary>
        /// <param name="node"></param>
        /// <param name="selectednodes"></param>
        public static void  GetSelectedTreeNode(TreeNode node,List<TreeNode> selectednodes)
        {
            foreach (TreeNode childnode in node.Nodes)
            {
                if (childnode.Checked)
                {
                    selectednodes.Add(childnode);
                }
                GetSelectedTreeNode(childnode, selectednodes);
            }
        }
    }
}
